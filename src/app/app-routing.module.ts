import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AuthGuardService } from "./auth-guard.service";

const routes: Routes = [
  {
    path: "",
    loadChildren: () =>
      import("./layout/layout.module").then((m) => m.LayoutModule),
  },
  // {
  //     path: "about",
  //     component: AboutComponent
  // },
  // {
  //     path: 'courses/:id',
  //     component: CourseComponent,
  //     resolve: {
  //         course: CourseResolver
  //     }
  // },
  {
    path: "**",
    redirectTo: "/",
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      relativeLinkResolution: "legacy",
      useHash: true,
    }),
  ],
  exports: [RouterModule],
  providers: [AuthGuardService],
})
export class AppRoutingModule {}
