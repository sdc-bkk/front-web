import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import { BusinessTypeApi } from "../api/business-type-api";

@Injectable()
export class BusinessTypeService {
  constructor(private businessTypeApi: BusinessTypeApi) {}

  getDrpBusinessTypeList(): Observable<any> {
    return this.businessTypeApi.getDrpBusinessTypeList();
  }
}
