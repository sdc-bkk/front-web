import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import { RoadApi } from "../api/road-api";

@Injectable()
export class RoadService {
  constructor(private roadApi: RoadApi) {}

  getRoadDrpList(): Observable<any> {
    return this.roadApi.getRoadDrpList();
  }
}
