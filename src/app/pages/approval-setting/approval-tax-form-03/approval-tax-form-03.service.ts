import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import { Frontaxform03Api } from "../../../api/fronttaxform03-api";

@Injectable({
  providedIn: 'root'
})
export class ApprovalTaxForm03Service {
  constructor(private frontaxform03Api: Frontaxform03Api) {}

  getList(data): Observable<any> {
    return this.frontaxform03Api.getList(data);
  }

  getListByMonthly(data): Observable<any> {
    return this.frontaxform03Api.getListByMonthly(data);
  }

  getData(data): Observable<any> {
    return this.frontaxform03Api.getData(data);
  }

  saveData(data): Observable<any> {
    return this.frontaxform03Api.saveData(data);
  }

  deleteData(data): Observable<any> {
    return this.frontaxform03Api.deleteData(data);
  }

  sendApprove(data): Observable<any> {
    return this.frontaxform03Api.sendApprove(data);
  }

  getStatusAndFinesByMonth(data): Observable<any> {
    return this.frontaxform03Api.getStatusAndFinesByMonth(data);
  }

  getListStatusLog(data): Observable<any> {
    return this.frontaxform03Api.getListStatusLog(data);
  }
}
