import { ViewChild, TemplateRef } from "@angular/core";
import { Component, ViewEncapsulation, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Subject } from "rxjs";
import { finalize, takeUntil } from "rxjs/operators";
import { ConfigService } from "../../../../shared/services/config/config.service";
import { DbMenuService } from "../../../../shared/localstorage/db-menu-service";
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";
import Swal from "sweetalert2";
import { DbUserLocalstorage } from "../../../../shared/services/localstorage/db-user-localstorage";
import { FileUploader } from "../../../../shared/components/file-upload/file-uploader.class";
import { REST_URL } from "../../../../shared/services/config/rest-url";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { ApprovalTaxForm03Service } from "../approval-tax-form-03.service";
import Utils from "../../../../shared/util/utils";
import { ApprovalTaxForm01Service } from "../../approval-tax-form-01/approval-tax-form-01.service";

@Component({
  selector: ".approval-tax-form-03-list-data-inner-wrapper",
  templateUrl: "./approval-tax-form-03-list-data.component.html",
  styleUrls: ["./approval-tax-form-03-list-data.component.scss"],
  encapsulation: ViewEncapsulation.Emulated,
})
export class ApprovalTaxForm03ListDataComponent implements OnInit {
  yearly: any;
  monthly: any;
  monthName: any;

  /////////////////// Config ///////////////////
  public allExpandState = false;
  panelOpenState: boolean = false;
  allRowsSelected: boolean;
  selectedCheck = 0;
  isSearchActive: boolean = false;
  ///////////////// End Config /////////////////

  ///////////////// Variable /////////////////
  public model: any = {};
  public loading = false;
  private unsubscribeAll = new Subject();
  /////////////// End Variable ////////////////

  //////////////// Value /////////////////////
  dataList: any[] = [];
  dataListTemp: any[] = [];
  maxitemPerPage: number = this.config.paging.ITEMPERPAGE;
  maxSize: number = this.config.paging.MAXSIZE;
  totalItemdataList: number;
  searchValue = {
    taxNo: "",
    startDate: "",
    endDate: "",
    docNo: "",
    stationName: "",
    status: "",
    statusAcc: "",
  };
  checkAll: any;
  public currentMenuDataBase: any;

  @ViewChild("modalcheckAllSystem")
  public modalcheckAllSystem: TemplateRef<any>;
  modalReferencecheckAllSystem: NgbModalRef;

  modalcheckAllSystem_model: any = {
    name: "",
    county: "",
  };

  @ViewChild("modalReplyAllSystem")
  public modalReplyAllSystemAllSystem: TemplateRef<any>;
  modalReferenceReplyAllSystemAllSystem: NgbModalRef;

  modalReplyAllSystem_model: any = {
    name: "",
    county: "",
    detail: "",
  };

  uploader: FileUploader = new FileUploader({
    autoUpload: true,
    url: REST_URL.ADDRESS_PERMISSION.importData,
  });

  ba_currentmounthPn03: any;

  resultPage: any = {
    pageNo: 1,
    totalItem: 0,
    itemPerPage: 0,
    collectionSize: 0,
  };

  drpListStatus: any[] = [];

  // ส่งแบบ
  @ViewChild("sendDesign_modal")
  public sendDesign_modal: TemplateRef<any>;
  sendDesign_modalReference: NgbModalRef;

  sendDesignData_modal: any = [];
  // ส่งแบบ

  // ไม่ผ่าน

  @ViewChild("statusNotApprove_modal")
  public statusNotApprove_modal: TemplateRef<any>;
  statusNotApprove_modalReference: NgbModalRef;

  check_agree: any = false;
  currentUser: any;

  getTaxForm03PDF: any = "taxexportwebservice/api/formExport/getTaxForm03PDF/";

  getInvoicePDF: any = "taxexportwebservice/api/formExport/getInvoicePDF/";

  @ViewChild("statusDesign_modal")
  public statusDesign_modal: TemplateRef<any>;
  statusDesign_modalReference: NgbModalRef;

  _stationName: any = "";
  statusDesignModal: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public config: ConfigService,
    private dbUserLocalstorage: DbUserLocalstorage,
    private dbMenuService: DbMenuService,
    private modalService: NgbModal,
    public ngxService: NgxUiLoaderService,
    private approvalTaxForm01Service: ApprovalTaxForm01Service,
    public approvalTaxForm03Service: ApprovalTaxForm03Service
  ) {}

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem("ba_currentUser"));
    this.yearly = this.route.snapshot.params["yearly"] || "0";
    this.monthly = this.route.snapshot.params["monthly"] || "0";

    this.monthName = Utils.ConvertMonthTH(this.monthly);

    this.getTaxForm03PDF = this.config.urllinkReport + this.getTaxForm03PDF;

    this.getInvoicePDF = this.config.urllinkReport + this.getInvoicePDF;

    this.getDrpListStatus();
    this.getListByMonthly(1);
  }

  ngOnDestroy() {}

  public getDrpListStatus() {
    this.approvalTaxForm01Service.getDrpListStatus().subscribe((res) => {
      //console.log("res getDrpListTambon");
      //console.log(res);

      if (res.result) {
        this.drpListStatus = res.result;

        //console.log("this.drpListStatus");
        //console.log(this.drpListStatus);
      } else {
        this.drpListStatus = [];
      }
    });
  }

  public selectAll(event) {
    this.allRowsSelected = event;
    if (event) {
      this.dataList.map((val) => {
        if (val.mainStatus == "0" || val.mainStatus == "2") {
          val.selected = true;

          let hasData = this.dataListTemp.find((dataList) => {
            return dataList.taxForm03Id == val.taxForm03Id;
          });

          if (!hasData) {
            return this.dataListTemp.push(val);
          }
        }
      });
    } else {
      this.dataListTemp = this.dataListTemp.reduce((result, val) => {
        let hasData = this.dataList.find((data) => {
          return data.taxForm03Id == val.taxForm03Id;
        });

        if (!hasData) {
          result = [...result, val];
        }

        return result;
      }, []);

      this.dataList.map((val) => {
        val.selected = false;
        return val;
      });
    }
    this.selectedCheck = this.dataListTemp.length;
  }

  public countselectedCheck(event, item) {
    this.dataListTemp = this.dataListTemp.filter(
      (temp) => temp.taxForm03Id != item.taxForm03Id
    );
    if (event) {
      this.selectedCheck++;
      this.dataListTemp.push(item);
    } else {
      this.selectedCheck--;
    }

    let CountCheckTrue = 0;
    this.dataList.map((item) => {
      let dataList = this.dataListTemp.find(
        (temp) => item.taxForm03Id == temp.taxForm03Id
      );

      if (dataList) {
        item.selected = true;
        CountCheckTrue++;
      } else {
        item.selected = false;
      }
      return item;
    });

    if (CountCheckTrue == this.dataList.length) {
      this.allRowsSelected = true;
    } else {
      this.allRowsSelected = false;
    }
  }

  public searchActive() {
    if (this.isSearchActive == false) {
      this.isSearchActive = true;
    } else {
      this.isSearchActive = false;
    }
    this.config.setIsSearchActive(this.isSearchActive);
  }
  public triggerClose(event) {
    this.isSearchActive = !this.isSearchActive;
  }

  public refreshDataSearch() {
    this.searchValue = {
      taxNo: "",
      startDate: "",
      endDate: "",
      docNo: "",
      stationName: "",
      status: "",
      statusAcc: "",
    };

    this.config.searchValue = this.searchValue;
    this.getListByMonthly(1);
  }

  public getListByMonthly(page) {
    let startDate;
    if (this.searchValue.startDate != null) {
      startDate = Utils.dateTostring(this.searchValue.startDate);
    } else {
      startDate = "";
    }

    let endDate;
    if (this.searchValue.endDate != null) {
      endDate = Utils.dateTostring(this.searchValue.endDate);
    } else {
      endDate = "";
    }

    let data = {
      memberName: this.currentUser.memberName,
      yearly: this.yearly,
      monthly: this.monthly,

      taxNo: this.searchValue.taxNo,

      startDate: startDate,
      endDate: endDate,

      docNo: this.searchValue.docNo,
      stationName: this.searchValue.stationName,
      status: this.searchValue.status,
      statusAcc: this.searchValue.statusAcc,

      page: 1,
      itemPerPage: 10,
      orderBy: "updatedDate",
      sort: "desc",
    };
    // console.log("data");
    // console.log(data);

    this.config.resultPage.pageNo = page;

    this.ngxService.start();

    this.approvalTaxForm03Service.getListByMonthly(data).subscribe((res) => {
      console.log("res approvalTaxForm03Service  getList");
      console.log(res);

      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

      if (res.result) {
        let selected = 0;
        res.result.map((item) => {
          let dataList = this.dataListTemp.find(
            (temp) => item.taxForm03Id == temp.taxForm03Id
          );
          if (dataList) {
            selected++;
            item.selected = true;
          } else {
            item.selected = false;
          }
          return item;
        });

        this.dataList = res.result;

        this.totalItemdataList = res.resultPage.totalItem;
        this.config.resultPage.totalItem = res.resultPage.totalItem;

        if (selected == res.result.length && selected > 0) {
          this.allRowsSelected = true;
        } else {
          this.allRowsSelected = false;
        }

        if (res.result.lengt == 0) {
          this.allRowsSelected = false;
        }

        this.resultPage.totalItem = res.resultPage.totalItem;
        if (this.totalItemdataList < selected) {
          this.selectedCheck = res.resultPage.totalItem;
        } else {
          this.selectedCheck = this.dataListTemp.length;
        }
      } else {
        this.dataList = [];
        this.totalItemdataList = 0;
        this.config.resultPage.totalItem = 0;
        this.selectedCheck = 0;
      }
    });
  }

  public add() {
    //this.yearly = this.route.snapshot.params["yearly"] || "0";
    //this.monthly = this.route.snapshot.params["monthly"] || "0";

    this.dbMenuService.set_backTaxForm(
      "/fontOffice/approval-setting/approval-tax-form-03/list-data/" +
        this.yearly +
        "/" +
        this.monthly
    );

    this.router.navigateByUrl(
      "/fontOffice/approval-setting/approval-tax-form-03/update/" +
        this.yearly +
        "/" +
        this.monthly +
        "/" +
        0
    );
  }

  public update(item) {
    this.dbMenuService.set_backTaxForm(
      "/fontOffice/approval-setting/approval-tax-form-03/list-data/" +
        this.yearly +
        "/" +
        this.monthly
    );

    this.router.navigateByUrl(
      "/fontOffice/approval-setting/approval-tax-form-03/update/" +
        this.yearly +
        "/" +
        this.monthly +
        "/" +
        item.taxForm03Id
    );

    // this.router.navigateByUrl(
    //   "/fontOffice/approval-setting/approval-tax-form-03/update/" +
    //     this.id +
    //     "/" +
    //     id
    // );
  }

  public view(item) {
    this.dbMenuService.set_backTaxForm(
      "/fontOffice/approval-setting/approval-tax-form-03/list-data/" +
        this.yearly +
        "/" +
        this.monthly
    );

    this.router.navigateByUrl(
      "/fontOffice/approval-setting/approval-tax-form-03/view/" +
        this.yearly +
        "/" +
        this.monthly +
        "/" +
        item.taxForm03Id
    );

    // this.router.navigateByUrl(
    //   "/fontOffice/approval-setting/approval-tax-form-03/update/" +
    //     this.id +
    //     "/" +
    //     id
    // );
  }

  public delete(item, mes) {
    if (mes != "row") {
      if (this.dataListTemp.length != 0) {
        Swal.fire({
          title: "คุณต้องการลบใช่หรือไม่",
          text:
            "จำนวนรายการที่ต้องการลบ " + this.dataListTemp.length + " รายการ",
          icon: "warning",
          showCancelButton: true,
          confirmButtonText: "ตกลง",
          cancelButtonText: "ยกเลิก",
        }).then((result) => {
          if (result.value) {
            let d = {
              idList: [],
              updatedBy: "1",
            };

            this.dataListTemp.forEach((v) => {
              d.idList.push(v.taxForm03Id);
            });

            this.ngxService.start();

            this.approvalTaxForm03Service.deleteData(d).subscribe((res) => {
              this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
              // console.log("res delete");
              // console.log(res);
              if (res.result) {
                Swal.fire("สำเร็จ", "ลบข้อมูลสำเร็จ", "success");
                this.dataListTemp = [];
                // ดักตอนลบ
                this.getListByMonthly(1);
              } else {
                Swal.fire("ไม่สำเร็จ", res.resultMsg, "error");
                this.dataListTemp = [];
                // ดักตอนลบ
                this.getListByMonthly(1);
              }
            });

            // Swal.fire("ไม่สำเร็จ", "ลบข้อมูลไม่สำเร็จ", "error");
            // For more information about handling dismissals please visit
            // https://sweetalert2.github.io/#handling-dismissals
          } else if (result.dismiss === Swal.DismissReason.cancel) {
            // Swal.fire("Cancelled", "Your imaginary file is safe :)", "error");
          }
        });
      } else {
        Swal.fire("กรุณาเลือกรายการ", "", "error");
      }
    } else {
      Swal.fire({
        title: "คุณต้องการลบใช่หรือไม่",
        text: item.stationName,
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "ตกลง",
        cancelButtonText: "ยกเลิก",
      }).then((result) => {
        if (result.value) {
          let d = {
            idList: [],
            updatedBy: "1",
          };
          d.idList.push(item.taxForm03Id);

          console.log("item");
          console.log(item);

          this.ngxService.start();

          this.approvalTaxForm03Service.deleteData(d).subscribe((res) => {
            this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
            // console.log("res delete");
            // console.log(res);
            if (res.result) {
              Swal.fire("สำเร็จ", "ลบข้อมูลสำเร็จ", "success");
              this.dataListTemp = [];
              // ดักตอนลบ
              this.getListByMonthly(1);
            } else {
              Swal.fire("ไม่สำเร็จ", res.resultMsg, "error");
              this.dataListTemp = [];
              // ดักตอนลบ
              this.getListByMonthly(1);
            }
          });

          // Swal.fire("ไม่สำเร็จ", "ลบข้อมูลไม่สำเร็จ", "error");
          // For more information about handling dismissals please visit
          // https://sweetalert2.github.io/#handling-dismissals
        } else if (result.dismiss === Swal.DismissReason.cancel) {
          // Swal.fire("Cancelled", "Your imaginary file is safe :)", "error");
        }
      });
    }
  }

  cancel() {
    this.router.navigate(["/fontOffice/approval-setting/approval-tax-form-03"]);
  }

  public clickuploaderUpFileImport() {
    const element: HTMLElement = document.getElementById(
      "file-input-import"
    ) as HTMLElement;
    element.click();
  }

  public fileOverBase(event) {
    if (event) {
      this.ngxService.start();
      this.uploader.getUploadListener().subscribe((res) => {
        this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
        this.getListByMonthly(1);
      });
    }
  }

  // ส่งแบบ
  public open_sendDesign(modalItems) {
    this.check_agree = false;
    let sendDesignData_modal = [];
    // tslint:disable-next-line:triple-equals
    if (this.dataListTemp != undefined && this.dataListTemp.length != 0) {
      sendDesignData_modal = this.dataListTemp;

      // for (let i = 0; i <= 10; i++) {
      //   sendDesignData_modal.push(this.dataListTemp[i]);
      // }

      this.sendDesignData_modal = sendDesignData_modal;

      console.log("this.sendDesignData_modal");
      console.log(this.sendDesignData_modal);

      this.sendDesign_modalReference = this.modalService.open(modalItems, {
        size: "lg",
        windowClass: "modal-xl",
      });
    } else {
      Swal.fire({
        icon: "warning",
        title: "แจ้งเตือน",
        text: "กรุณาเลือกรายการที่จะส่งแบบ",
        showConfirmButton: false,
        timer: 3000,
      });
    }
  }

  public submit_sendDesign() {
    let d = this.sendDesignData_modal.find((val) => {
      return val.mainStatus != "0" || val.mainStatus != "2";
    });

    console.log("d");
    console.log(d);

    if (!d) {
      //console.log("!d");

      Swal.fire({
        icon: "warning",
        title: "แจ้งเตือน",
        text: "ไม่สามารถส่งแบบได้เนื้องจากมีการส่งแบบไปแล้ว",
        showConfirmButton: false,
        timer: 3000,
      });
    } else {
      //console.log("d");

      let idList = [];
      this.sendDesignData_modal.forEach((v) => {
        idList.push(v.taxForm03Id);
      });

      let d = {
        taxForm03Id: "",
        actionBy: this.currentUser.memberName,
        remark: "",
        idList: idList,
      };

      this.approvalTaxForm03Service.sendApprove(d).subscribe((res) => {
        console.log("res");
        console.log(res);
        if (res.result) {
          Swal.fire({
            icon: "success",
            title: "สำเร็จ",
            text:
              "บันทึกข้อมูลสำเร็จ จำนวนรายการที่ยืนยันการยื่นแบบ ภน.03 " +
              idList.length +
              " รายการ",
            showConfirmButton: false,
            timer: 1500,
          });

          this.dataListTemp = [];

          setTimeout(() => {
            //this.dataListTemp = [];
            this.sendDesign_modalReference.close();
            this.getListByMonthly(1);
          }, 1500);
        } else {
          Swal.fire("ไม่สำเร็จ", res.message, "error");
        }
      });
    }
  }

  public close_sendDesign() {
    this.sendDesign_modalReference.close();
  }
  // ส่งแบบ

  public open_statusDesign(item) {
    this.getListStatusLog(item);

    // this.statusDesignModal = item;
  }

  public getListStatusLog(item) {
    this.statusDesignModal = [];
    const payload = {
      taxForm03Id: item.taxForm03Id,
    };

    console.log("payload");
    console.log(payload);

    this.ngxService.start();

    this._stationName = item.stationName;

    this.approvalTaxForm03Service.getListStatusLog(payload).subscribe((res) => {
      console.log("res getListStatusLog");
      console.log(res);

      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

      if (res.result) {
        res.result.forEach((element) => {
          element.select = true;
        });

        this.statusDesignModal = res.result;

        this.statusDesignModal.stationName = item.stationName;

        console.log("this.statusDesignModal");
        console.log(this.statusDesignModal);
      } else {
        this.statusDesignModal = res.result;
      }

      this.statusDesign_modalReference = this.modalService.open(
        this.statusDesign_modal,
        {
          size: "lg",
          windowClass: "modal-xl",
        }
      );
    });
  }

  public close_statusDesign() {
    this.statusDesign_modalReference.close();
  }
}
