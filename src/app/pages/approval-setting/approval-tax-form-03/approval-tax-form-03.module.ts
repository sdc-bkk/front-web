import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { NgxsModule } from "@ngxs/store";
import { SharedModule } from "../../../shared/shared.module";
import { ApprovalTaxForm03ListDataComponent } from "./approval-tax-form-03-list-data/approval-tax-form-03-list-data.component";
import { ApprovalTaxForm03ListComponent } from "./approval-tax-form-03-list/approval-tax-form-03-list.component";
import { ApprovalTaxForm03UpdateComponent } from "./approval-tax-form-03-update/approval-tax-form-03-update.component";
import { ApprovalTaxForm03Component } from "./approval-tax-form-03.component";
import { ApprovalTaxForm03Routes } from "./approval-tax-form-03.routes";
import { ApprovalTaxForm03Service } from "./approval-tax-form-03.service";
import { DbUserLocalstorage } from "../../../shared/services/localstorage/db-user-localstorage";
import { AddressApi } from "../../../api/address-api";
import { BusinessStatusApi } from "../../../api/business-status-api";
import { Frontaxform01Api } from "../../../api/fronttaxform01-api";
import { Frontaxform02Api } from "../../../api/fronttaxform02-api";
import { RoadApi } from "../../../api/road-api";
import { SoiApi } from "../../../api/soi-api";
import { AddressService } from "../../address.service";
import { BusinessStatusService } from "../../business-status.service";
import { RoadService } from "../../road.service";
import { SoiService } from "../../soi.service";
import { ApprovalTaxForm01Service } from "../approval-tax-form-01/approval-tax-form-01.service";
import { ApprovalTaxForm02Service } from "../approval-tax-form-02/approval-tax-form-02.service";
import { OilTypeApi } from "../../../api/oil-type-api";
import { OilTypeService } from "../../oil-type.service";
import { Unity } from "../../Unity";
import { UploadApi } from "../../../api/upload-api";
import { UploadService } from "../../upload.service";
import { FrontretailApi } from "../../../api/frontretail-api";
import { FrontretailService } from "../../frontretail.service";
import { Frontaxform03Api } from "../../../api/fronttaxform03-api";
import { ApprovalTaxForm03ViewComponent } from "./approval-tax-form-03-view/approval-tax-form-03-view.component";

@NgModule({
  declarations: [
    ApprovalTaxForm03Component,
    ApprovalTaxForm03ListComponent,
    ApprovalTaxForm03ListDataComponent,
    ApprovalTaxForm03UpdateComponent,
    ApprovalTaxForm03ViewComponent,
  ],
  providers: [
    Frontaxform01Api,
    ApprovalTaxForm01Service,
    BusinessStatusService,
    BusinessStatusApi,
    AddressService,
    AddressApi,
    RoadService,
    RoadApi,
    SoiService,
    SoiApi,
    OilTypeService,
    OilTypeApi,
    Frontaxform02Api,
    ApprovalTaxForm02Service,

    Frontaxform03Api,
    ApprovalTaxForm03Service,
    DbUserLocalstorage,
    Unity,
    UploadApi,
    UploadService,
    FrontretailApi,
    FrontretailService,
  ],
  imports: [CommonModule, SharedModule, ApprovalTaxForm03Routes],
})
export class ApprovalTaxForm03Module {}
