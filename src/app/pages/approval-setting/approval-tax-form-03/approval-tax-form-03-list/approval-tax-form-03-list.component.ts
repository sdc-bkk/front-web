import { ViewChild, TemplateRef } from "@angular/core";
import { Component, ViewEncapsulation, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Subject } from "rxjs";
import { finalize, takeUntil } from "rxjs/operators";
import { ConfigService } from "../../../../shared/services/config/config.service";
import { DbMenuService } from "../../../../shared/localstorage/db-menu-service";
import Swal from "sweetalert2";
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";
import { FileUploader } from "../../../../shared/components/file-upload/file-uploader.class";
import { REST_URL } from "../../../../shared/services/config/rest-url";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { DbUserLocalstorage } from "../../../../shared/services/localstorage/db-user-localstorage";
import { ApprovalTaxForm03Service } from "../approval-tax-form-03.service";
import Utils from "../../../../shared/util/utils";

@Component({
  selector: ".approval-tax-form-03-list-inner-wrapper",
  templateUrl: "./approval-tax-form-03-list.component.html",
  styleUrls: ["./approval-tax-form-03-list.component.scss"],
  encapsulation: ViewEncapsulation.Emulated,
})
export class ApprovalTaxForm03ListComponent implements OnInit {
  /////////////////// Config ///////////////////
  public allExpandState = false;
  panelOpenState: boolean = false;
  allRowsSelected: boolean;
  selectedCheck = 0;
  isSearchActive: boolean = false;
  ///////////////// End Config /////////////////

  ///////////////// Variable /////////////////
  public model: any = {};
  public loading = false;
  private unsubscribeAll = new Subject();
  /////////////// End Variable ////////////////

  //////////////// Value /////////////////////
  dataList: any[] = [];
  dataListTemp: any[] = [];
  maxitemPerPage: number = this.config.paging.ITEMPERPAGE;
  maxSize: number = this.config.paging.MAXSIZE;
  totalItemdataList: number;
  searchValue = {
    yearly: "",
    monthly: "",
    memberName: "",
  };
  checkAll: any;
  check_agree: any = false;
  public currentMenuDataBase: any;

  resultPage: any = {
    pageNo: 1,
    totalItem: 0,
    itemPerPage: 0,
    collectionSize: 0,
  };

  loader: any;

  drpContentTypeList: any = [];

  // ส่งแบบ
  @ViewChild("sendDesign_modal")
  public sendDesign_modal: TemplateRef<any>;
  sendDesign_modalReference: NgbModalRef;

  sendDesignData_modal: any = [];
  // ส่งแบบ

  // ไม่ผ่าน

  @ViewChild("statusNotApprove_modal")
  public statusNotApprove_modal: TemplateRef<any>;
  statusNotApprove_modalReference: NgbModalRef;

  statusNotApproveData_modal: any = {
    reportid: "",
    regisdate: "",
    texno: "",
    companyname: "",
    pn01: "",
    r1: "",
    statusName: "",
  };

  // ไม่ผ่าน

  report_pn01 = "/api/report/";

  @ViewChild("statusDesign_modal")
  public statusDesign_modal: TemplateRef<any>;
  statusDesign_modalReference: NgbModalRef;

  statusDesignModal: any;

  drpListStatus: any[] = [];

  Month: any = new Date().getMonth() + 1;
  year: any = new Date().getFullYear() + 543;

  drpyear: any = [];

  uploader: FileUploader = new FileUploader({
    autoUpload: true,
    url: REST_URL.ADDRESS_PERMISSION.importData,
  });

  currentUser: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public config: ConfigService,
    private dbUserLocalstorage: DbUserLocalstorage,
    private dbMenuService: DbMenuService,
    private modalService: NgbModal,
    public ngxService: NgxUiLoaderService,
    public approvalTaxForm03Service: ApprovalTaxForm03Service
  ) {}

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem("ba_currentUser"));

    this.drpyear.push(this.year - 1);
    this.drpyear.push(this.year);

    this.getList(1);
  }

  ngOnDestroy() {}

  public searchActive() {
    if (this.isSearchActive == false) {
      this.isSearchActive = true;
    } else {
      this.isSearchActive = false;
    }
    this.config.setIsSearchActive(this.isSearchActive);
  }
  public triggerClose(event) {
    this.isSearchActive = !this.isSearchActive;
  }

  public refreshDataSearch() {
    this.searchValue = {
      yearly: "",
      monthly: "",
      memberName: "",
    };

    this.config.searchValue = this.searchValue;
    this.getList(1);
  }

  public selectAll(event) {
    this.allRowsSelected = event;
    if (event) {
      this.dataList.map((val) => {
        val.selected = true;

        let hasData = this.dataListTemp.find((dataList) => {
          return dataList.yearly == val.yearly;
        });

        if (!hasData) {
          return this.dataListTemp.push(val);
        }
      });
    } else {
      this.dataListTemp = this.dataListTemp.reduce((result, val) => {
        let hasData = this.dataList.find((data) => {
          return data.yearly == val.yearly;
        });

        if (!hasData) {
          result = [...result, val];
        }

        return result;
      }, []);

      this.dataList.map((val) => {
        val.selected = false;
        return val;
      });
    }
    this.selectedCheck = this.dataListTemp.length;
  }

  public countselectedCheck(event, item) {
    this.dataListTemp = this.dataListTemp.filter(
      (temp) => temp.yearly != item.yearly
    );
    if (event) {
      this.selectedCheck++;
      this.dataListTemp.push(item);
    } else {
      this.selectedCheck--;
    }

    let CountCheckTrue = 0;
    this.dataList.map((item) => {
      let dataList = this.dataListTemp.find(
        (temp) => item.yearly == temp.yearly
      );

      if (dataList) {
        item.selected = true;
        CountCheckTrue++;
      } else {
        item.selected = false;
      }
      return item;
    });

    if (CountCheckTrue == this.dataList.length) {
      this.allRowsSelected = true;
    } else {
      this.allRowsSelected = false;
    }
  }

  public getList(page) {
    let data = {
      page: page,
      itemPerPage: this.config.paging.ITEMPERPAGE,
      orderBy: "createdDate",
      sort: "desc",

      yearly: this.searchValue.yearly,
      monthly:Utils.ConvertMonthNumber(this.searchValue.monthly) ,
      memberName: this.currentUser.memberName,
    };
    console.log("data");
    console.log(data);

    this.config.resultPage.pageNo = page;

    this.ngxService.start();

    this.approvalTaxForm03Service.getList(data).subscribe((res) => {
      console.log("res approvalTaxForm03Service  getList");
      console.log(res);

      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

      if (res.result) {
        let selected = 0;
        res.result.map((item) => {
          let dataList = this.dataListTemp.find(
            (temp) => item.yearly == temp.yearly
          );
          if (dataList) {
            selected++;
            item.selected = true;
          } else {
            item.selected = false;
          }
          return item;
        });

        this.dataList = res.result;
        this.totalItemdataList = res.resultPage.totalItem;
        this.config.resultPage.totalItem = res.resultPage.totalItem;

        if (selected == res.result.length && selected > 0) {
          this.allRowsSelected = true;
        } else {
          this.allRowsSelected = false;
        }

        if (res.result.lengt == 0) {
          this.allRowsSelected = false;
        }

        this.resultPage.totalItem = res.resultPage.totalItem;
        if (this.totalItemdataList < selected) {
          this.selectedCheck = res.resultPage.totalItem;
        } else {
          this.selectedCheck = this.dataListTemp.length;
        }
      } else {
        this.dataList = [];
        this.totalItemdataList = 0;
        this.config.resultPage.totalItem = 0;
        this.selectedCheck = 0;
      }
    });
  }

  public add() {
    // Month: any = new Date().getMonth() + 1;
    // year: any = new Date().getFullYear() + 543;

    this.dbMenuService.set_backTaxForm(
      "/fontOffice/approval-setting/approval-tax-form-03"
    );

    this.router.navigateByUrl(
      "/fontOffice/approval-setting/approval-tax-form-03/update/" +
        this.year +
        "/" +
        this.Month +
        "/" +
        0
    );
  }
  public view(item) {
    // this.dbUserLocalstorage.add_currentMounthPN03(
    //   item.monthName + " " + item.yearly
    // );

    console.log("item");
    console.log(item);

    this.router.navigate([
      "/fontOffice/approval-setting/approval-tax-form-03/list-data/" +
        item.yearly.toString() +
        "/" +
        item.monthly.toString(),
    ]);
  }

  public clickuploaderUpFileImport() {
    const element: HTMLElement = document.getElementById(
      "file-input-import"
    ) as HTMLElement;
    element.click();
  }

  public fileOverBase(event) {
    if (event) {
      this.ngxService.start();
      this.uploader.getUploadListener().subscribe((res) => {
        this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
        this.getList(1);
      });
    }
  }
}
