import {
  Component,
  ViewEncapsulation,
  OnInit,
  TemplateRef,
  ViewChild,
} from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Subject } from "rxjs/Rx";
import { finalize, takeUntil } from "rxjs/operators";
import { ConfigService } from "../../../../shared/services/config/config.service";
// import createNumberMask from 'text-mask-addons/dist/createNumberMask';
import Utils from "../../../../shared/util/utils";
import {
  NgbModal,
  NgbModalConfig,
  NgbModalRef,
} from "@ng-bootstrap/ng-bootstrap";
import Swal from "sweetalert2";
import { ApprovalTaxForm01Service } from "../../approval-tax-form-01/approval-tax-form-01.service";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { OilTypeService } from "../../../oil-type.service";
import { Unity } from "../../../Unity";
import { BusinessStatusService } from "../../../business-status.service";
import { UploadService } from "../../../upload.service";
import { FrontretailService } from "../../../frontretail.service";
import { isTemplateExpression } from "typescript";
import createNumberMask from "text-mask-addons/dist/createNumberMask";
import { DbUserLocalstorage } from "../../../../shared/services/localstorage/db-user-localstorage";
import { DbMenuService } from "../../../../shared/localstorage/db-menu-service";
import { ApprovalTaxForm03Service } from "../approval-tax-form-03.service";
import { REST_URL } from "../../../../shared/services/config/rest-url";
import { FileUploader } from "../../../../shared/components/file-upload/file-uploader.class";
import { ApprovalTaxForm02Service } from "../../approval-tax-form-02/approval-tax-form-02.service";

@Component({
  selector: ".approval-tax-form-03-update-inner-wrapper",
  templateUrl: "./approval-tax-form-03-update.component.html",
  styleUrls: ["./approval-tax-form-03-update.component.scss"],
  encapsulation: ViewEncapsulation.Emulated,
})
export class ApprovalTaxForm03UpdateComponent implements OnInit {
  public currencyMask = createNumberMask({
    prefix: "",
    suffix: "",
    includeThousandsSeparator: true,
    thousandsSeparatorSymbol: ",",
    allowDecimal: true,
    decimalSymbol: ".",
    decimalLimit: 4,
    integerLimit: null,
    requireDecimal: false,
    allowNegative: false,
    allowLeadingZeroes: false,
  });

  /////////////// End Variable ////////////////

  id: any = "0";
  yearly: any = "0";
  monthly: any = "0";

  dataList: any[] = [];
  private unsubscribeAll = new Subject();
  //////////////// Value /////////////////////
  data = {
    taxForm03Id: "0",
    refRetailId: "",
    refStationId: "",
    docNo: "",
    docDate: "",

    yearly: "",
    monthly: "",
    monthName: "",

    oilTypeId: "", // รายการน้ำมัน/ก๊าซปิโครเลียม
    oilTypeIdName: "", // รายการน้ำมัน/ก๊าซปิโครเลียม
    balance: "", // ยอดยกมา
    inQty: "", // ปริมาณนำเข้า
    outQty: "", // ปริมาณจำหน่าย
    remain: "", // คงเหลือยกไป

    statusAcc: "",
    statusAccDesc: "",
    status: "",
    statusName: "",

    addTimes: "",
    sequence: "",
    extraMoney: "",
    extraRate: "",
    extraStartDate: "",
    extraEndDate: "",
    taxTotal: "",

    accountList: [
      // {
      //     "taxForm03Id": "QbH2TVK3ahnjQK2StvFI5A",
      //     "fileName": "houseFileList.jpg",
      //     "pathFile": "14158953-f5fe-4a66-a680-14aa099da5a5.pdf",
      //     "fileType": "I"
      // }
    ],
    summaryList: [
      // {
      //     "taxForm03Id": "QbH2TVK3ahnjQK2StvFI5A",
      //     "sequence": 1,
      //     "oilTypeId": "RQ-eWw45LnFawRr7Dtfphg",
      //     "oilTypeName": "แก๊สโซฮอล์ 91",
      //     "qty": 1000.0,
      //     "rate": 1.2,
      //     "amount": 1200.0,
      //     "amountBaht": 1200,
      //     "amountSatang": 0
      // },
    ],
    detailList: [
      // {
      //     "taxForm03Id": "QbH2TVK3ahnjQK2StvFI5A",
      //     "sequence": 1,
      //     "oilTypeId": "RQ-eWw45LnFawRr7Dtfphg",
      //     "oilTypeName": "แก๊สโซฮอล์ 91",
      //     "balance": 1000.0,
      //     "inQty": 1000.0,
      //     "outQty": 1200.0,
      //     "remain": 800.0
      // },
    ],
    refRetail: {
      retailId: "",
      mainStatus: "",
      mainStatusName: "",
      ownerName: "",
      taxNo: "",
      customerType: "",
      idCard: "",
      idCardAt: "",
      corpNo: "",
      corpDate: "",
      houseNo: "",
      address: "",
      soi: "",
      road: "",
      tambonId: "",
      amphurId: "",
      provinceId: "",
      postcode: "",
      fullAddress: "",
      mobile: "",
      email: "",

      houseFileNum: "",
      cerFileNum: "",
      agentFileNum: "",

      houseFileList: [],
      cerFileList: [],
      agentFileList: [],
    },
    refStation: {
      retailStationId: "",
      retailId: "",
      stationCode: "",
      stationName: "",
      businessTypeList: [
        // {
        //     "id": "wFlYj87tnX307XVrn9r6kw",
        //     "name": "ปิโตรเลียม"
        // }
      ],
      houseNo: "",
      address: "",
      soi: "",
      road: "",
      tambonId: "",
      tambonName: "",
      amphurId: "",
      amphurName: "",
      provinceId: "",
      provinceName: "",
      postcode: "",
      fullAddress: "",
      mobile: "",
      email: "",
      status: "",
      statusName: "",
      houseFileNum: "",
      mapFileNum: "",
      houseFileList: [
        // {
        //     "retailStationId": "Uxinj2kJGt9P29YYu_bHpQ",
        //     "fileName": "letterAudit.pdf",
        //     "pathFile": "3b4c48c9-9c81-450d-8828-bbc0f8b899ff.pdf",
        //     "fileType": "P"
        // }
      ],
      mapFileList: [
        // {
        //     "retailStationId": "Uxinj2kJGt9P29YYu_bHpQ",
        //     "fileName": "solvent01.pdf",
        //     "pathFile": "8f376a0f-e1c4-46fb-870c-793815a5b748.pdf",
        //     "fileType": "P"
        // }
      ],
    },

    finesMonth: "",
    finesPerMonth: "",
    finesStartDate: "",
    finesEndDate: "",
    finesTotal: "",
    refRetailStationId: "",
  };

  // + -
  panelOpenState = true;

  drpListStation: any = []; // ชื่อสถานการค้าปลีก

  ba_currentmounthPn03: any; // เดือน

  drpOilTypeList: any = [];

  currentUser: any;

  taxNoValidation: any = false;

  drpListStationByTaxNo: any = [];

  Month: any = new Date().getMonth() + 1;
  year: any = new Date().getFullYear() + 543;

  drpyear: any = []; // ปี
  drpmonth: any[] = [];

  displayMount: any;

  _cancel: any;

  sum_amountBaht: any = 0; // รวม(บาท)
  sum_amountBaht_show: any = "";
  sum_amountSatang: any = 0; // รวม(สตางค์)
  sum_amountSatang_show: any = "";

  plus_extra_money_Baht: any = 0; // บวกเงินเพิ่ม (ร้อยละ xx ต่อเดือนของค่าภาษีืที่ต้่องชำระหรือชำระขาดตั้งแต่ xxxx) (บาท)
  plus_extra_money_Satang: any = 0; // บวกเงินเพิ่ม (ร้อยละ xx ต่อเดือนของค่าภาษีืที่ต้่องชำระหรือชำระขาดตั้งแต่ xxxx) (สตางค์)
  plus_extra_money_Satang_show: any = "";

  total_tax_payable_Baht: any = 0; // ยอดเงินภาษีที่ต้องชำระทั้งสิ้น (บาท)
  total_tax_payable_Baht_show: any = "";
  total_tax_payable_Satang: any = 0; // ยอดเงินภาษีที่ต้องชำระทั้งสิ้น (บาท)
  total_tax_payable_Satang_show: any = "";

  // this.sumtotoalAumot = 0; // รวม(บาท)
  // this.sumdottotoalAumot = 0;  // รวม(สตางค์)

  // เอกสารงบเดือนแสดงรายรับ-จ่ายน้ำมัน/ก๊าซปิโตรเลียม
  delect_accountList: any[] = [];

  uploader_accountList: FileUploader = new FileUploader({
    autoUpload: true,
    url: REST_URL.MAIN_UPLOADFILE.uploadFile,
  });
  // เอกสารงบเดือนแสดงรายรับ-จ่ายน้ำมัน/ก๊าซปิโตรเลียม

  _refRetailId: any = "";

  getDataByMemberNameStatus: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private configModal: NgbModalConfig,
    public config: ConfigService,
    private modalService: NgbModal,
    private oilTypeService: OilTypeService,
    public ngxService: NgxUiLoaderService,
    private unity: Unity,
    private approvalTaxForm01Service: ApprovalTaxForm01Service,
    private businessStatusService: BusinessStatusService,
    private approvalTaxForm02Service: ApprovalTaxForm02Service,
    private uploadService: UploadService,
    private frontretailService: FrontretailService,
    private dbMenuService: DbMenuService,
    private approvalTaxForm03Service: ApprovalTaxForm03Service
  ) {
    configModal.backdrop = "static";
  }

  ngOnInit() {
    this.yearly = this.route.snapshot.params["yearly"] || "0";
    this.monthly = this.route.snapshot.params["monthly"] || "0";
    this.id = this.route.snapshot.params["id"] || "0";

    this.currentUser = JSON.parse(localStorage.getItem("ba_currentUser"));
    this._cancel = this.dbMenuService.get_backTaxForm();

    this.panelOpenState = true;

    this.drpyear.push(this.year - 1);
    this.drpyear.push(this.year);

    if (this.yearly != "0") {
      this.data.yearly = this.yearly;
    }

    if (this.monthly != "0") {
      this.data.monthly = this.monthly;
    }

    // console.log("this.Month");
    // console.log(this.Month);

    // this.Month = 12;

    if (this.Month == 1) {
      let d = {
        id: "1",
        name: "มกราคม",
      };
      this.drpmonth.push(d);

      this.displayMount = "มกราคม";
    } else if (this.Month == 2) {
      let d = {
        id: "1",
        name: "มกราคม",
      };
      this.drpmonth.push(d);
      let d1 = {
        id: "2",
        name: "กุมภาพันธ์",
      };
      this.drpmonth.push(d1);

      this.displayMount = "กุมภาพันธ์";
    } else if (this.Month == 3) {
      let d = {
        id: "1",
        name: "มกราคม",
      };
      this.drpmonth.push(d);
      let d1 = {
        id: "2",
        name: "กุมภาพันธ์",
      };
      this.drpmonth.push(d1);
      let d2 = {
        id: "3",
        name: "มีนาคม",
      };
      this.drpmonth.push(d2);

      this.displayMount = "มีนาคม";
    } else if (this.Month == 4) {
      let d = {
        id: "1",
        name: "มกราคม",
      };
      this.drpmonth.push(d);
      let d1 = {
        id: "2",
        name: "กุมภาพันธ์",
      };
      this.drpmonth.push(d1);
      let d2 = {
        id: "3",
        name: "มีนาคม",
      };
      this.drpmonth.push(d2);
      let d3 = {
        id: "4",
        name: "เมษายน",
      };
      this.drpmonth.push(d3);

      this.displayMount = "เมษายน";
    } else if (this.Month == 5) {
      let d = {
        id: "1",
        name: "มกราคม",
      };
      this.drpmonth.push(d);
      let d1 = {
        id: "2",
        name: "กุมภาพันธ์",
      };
      this.drpmonth.push(d1);
      let d2 = {
        id: "3",
        name: "มีนาคม",
      };
      this.drpmonth.push(d2);
      let d3 = {
        id: "4",
        name: "เมษายน",
      };
      this.drpmonth.push(d3);
      let d4 = {
        id: "5",
        name: "พฤษภาคม",
      };
      this.drpmonth.push(d4);

      this.displayMount = "พฤษภาคม";
    } else if (this.Month == 6) {
      let d = {
        id: "1",
        name: "มกราคม",
      };
      this.drpmonth.push(d);
      let d1 = {
        id: "2",
        name: "กุมภาพันธ์",
      };
      this.drpmonth.push(d1);
      let d2 = {
        id: "3",
        name: "มีนาคม",
      };
      this.drpmonth.push(d2);
      let d3 = {
        id: "4",
        name: "เมษายน",
      };
      this.drpmonth.push(d3);
      let d4 = {
        id: "5",
        name: "พฤษภาคม",
      };
      this.drpmonth.push(d4);
      let d5 = {
        id: "6",
        name: "มิถุนายน",
      };
      this.drpmonth.push(d5);

      this.displayMount = "มิถุนายน";
    } else if (this.Month == 7) {
      let d = {
        id: "1",
        name: "มกราคม",
      };
      this.drpmonth.push(d);
      let d1 = {
        id: "2",
        name: "กุมภาพันธ์",
      };
      this.drpmonth.push(d1);
      let d2 = {
        id: "3",
        name: "มีนาคม",
      };
      this.drpmonth.push(d2);
      let d3 = {
        id: "4",
        name: "เมษายน",
      };
      this.drpmonth.push(d3);
      let d4 = {
        id: "5",
        name: "พฤษภาคม",
      };
      this.drpmonth.push(d4);
      let d5 = {
        id: "6",
        name: "มิถุนายน",
      };
      this.drpmonth.push(d5);
      let d6 = {
        id: "7",
        name: "กรกฎาคม",
      };
      this.drpmonth.push(d6);

      this.displayMount = "กรกฎาคม";
    } else if (this.Month == 8) {
      let d = {
        id: "1",
        name: "มกราคม",
      };
      this.drpmonth.push(d);
      let d1 = {
        id: "2",
        name: "กุมภาพันธ์",
      };
      this.drpmonth.push(d1);
      let d2 = {
        id: "3",
        name: "มีนาคม",
      };
      this.drpmonth.push(d2);
      let d3 = {
        id: "4",
        name: "เมษายน",
      };
      this.drpmonth.push(d3);
      let d4 = {
        id: "5",
        name: "พฤษภาคม",
      };
      this.drpmonth.push(d4);
      let d5 = {
        id: "6",
        name: "มิถุนายน",
      };
      this.drpmonth.push(d5);
      let d6 = {
        id: "7",
        name: "กรกฎาคม",
      };
      this.drpmonth.push(d6);
      let d7 = {
        id: "8",
        name: "สิงหาคม",
      };
      this.drpmonth.push(d7);

      this.displayMount = "สิงหาคม";
    } else if (this.Month == 9) {
      let d = {
        id: "1",
        name: "มกราคม",
      };
      this.drpmonth.push(d);
      let d1 = {
        id: "2",
        name: "กุมภาพันธ์",
      };
      this.drpmonth.push(d1);
      let d2 = {
        id: "3",
        name: "มีนาคม",
      };
      this.drpmonth.push(d2);
      let d3 = {
        id: "4",
        name: "เมษายน",
      };
      this.drpmonth.push(d3);
      let d4 = {
        id: "5",
        name: "พฤษภาคม",
      };
      this.drpmonth.push(d4);
      let d5 = {
        id: "6",
        name: "มิถุนายน",
      };
      this.drpmonth.push(d5);
      let d6 = {
        id: "7",
        name: "กรกฎาคม",
      };
      this.drpmonth.push(d6);
      let d7 = {
        id: "8",
        name: "สิงหาคม",
      };
      this.drpmonth.push(d7);
      let d8 = {
        id: "9",
        name: "กันยายน",
      };
      this.drpmonth.push(d8);

      this.displayMount = "กันยายน";
    } else if (this.Month == 10) {
      let d = {
        id: "1",
        name: "มกราคม",
      };
      this.drpmonth.push(d);
      let d1 = {
        id: "2",
        name: "กุมภาพันธ์",
      };
      this.drpmonth.push(d1);
      let d2 = {
        id: "3",
        name: "มีนาคม",
      };
      this.drpmonth.push(d2);
      let d3 = {
        id: "4",
        name: "เมษายน",
      };
      this.drpmonth.push(d3);
      let d4 = {
        id: "5",
        name: "พฤษภาคม",
      };
      this.drpmonth.push(d4);
      let d5 = {
        id: "6",
        name: "มิถุนายน",
      };
      this.drpmonth.push(d5);
      let d6 = {
        id: "7",
        name: "กรกฎาคม",
      };
      this.drpmonth.push(d6);
      let d7 = {
        id: "8",
        name: "สิงหาคม",
      };
      this.drpmonth.push(d7);
      let d8 = {
        id: "9",
        name: "กันยายน",
      };
      this.drpmonth.push(d8);
      let d9 = {
        id: "10",
        name: "ตุลาคม",
      };
      this.drpmonth.push(d9);

      this.displayMount = "ตุลาคม";
    } else if (this.Month == 11) {
      let d = {
        id: "1",
        name: "มกราคม",
      };
      this.drpmonth.push(d);
      let d1 = {
        id: "2",
        name: "กุมภาพันธ์",
      };
      this.drpmonth.push(d1);
      let d2 = {
        id: "3",
        name: "มีนาคม",
      };
      this.drpmonth.push(d2);
      let d3 = {
        id: "4",
        name: "เมษายน",
      };
      this.drpmonth.push(d3);
      let d4 = {
        id: "5",
        name: "พฤษภาคม",
      };
      this.drpmonth.push(d4);
      let d5 = {
        id: "6",
        name: "มิถุนายน",
      };
      this.drpmonth.push(d5);
      let d6 = {
        id: "7",
        name: "กรกฎาคม",
      };
      this.drpmonth.push(d6);
      let d7 = {
        id: "8",
        name: "สิงหาคม",
      };
      this.drpmonth.push(d7);
      let d8 = {
        id: "9",
        name: "กันยายน",
      };
      this.drpmonth.push(d8);
      let d9 = {
        id: "10",
        name: "ตุลาคม",
      };
      this.drpmonth.push(d9);
      let d10 = {
        id: "11",
        name: "พฤศจิกายน ",
      };
      this.drpmonth.push(d10);

      this.displayMount = "พฤศจิกายน";
    } else if (this.Month == 12) {
      let d = {
        id: "1",
        name: "มกราคม",
      };
      this.drpmonth.push(d);
      let d1 = {
        id: "2",
        name: "กุมภาพันธ์",
      };
      this.drpmonth.push(d1);
      let d2 = {
        id: "3",
        name: "มีนาคม",
      };
      this.drpmonth.push(d2);
      let d3 = {
        id: "4",
        name: "เมษายน",
      };
      this.drpmonth.push(d3);
      let d4 = {
        id: "5",
        name: "พฤษภาคม",
      };
      this.drpmonth.push(d4);
      let d5 = {
        id: "6",
        name: "มิถุนายน",
      };
      this.drpmonth.push(d5);
      let d6 = {
        id: "7",
        name: "กรกฎาคม",
      };
      this.drpmonth.push(d6);
      let d7 = {
        id: "8",
        name: "สิงหาคม",
      };
      this.drpmonth.push(d7);
      let d8 = {
        id: "9",
        name: "กันยายน",
      };
      this.drpmonth.push(d8);
      let d9 = {
        id: "10",
        name: "ตุลาคม",
      };
      this.drpmonth.push(d9);
      let d10 = {
        id: "11",
        name: "พฤศจิกายน ",
      };
      this.drpmonth.push(d10);
      let d11 = {
        id: "12",
        name: "ธันวาคม ",
      };
      this.drpmonth.push(d11);

      this.displayMount = "ธันวาคม";
    }

    // ปี
    this.data.yearly = this.year;

    // เดือน
    this.data.monthly = this.Month.toString();

    this.displayMount = this.displayMount + " " + this.year.toString();

    this.getDrpOilTypeList();
    this.getDataByMemberName();
    if (this.id != "0") {
      this.getData(this.id);
    }
  }

  public setChangeyear(item) {
    this.drpmonth = [];

    if (item == this.year) {
      if (this.Month == 1) {
        let d = {
          id: "1",
          name: "มกราคม",
        };
        this.drpmonth.push(d);

        this.displayMount = "มกราคม";
      } else if (this.Month == 2) {
        let d = {
          id: "1",
          name: "มกราคม",
        };
        this.drpmonth.push(d);
        let d1 = {
          id: "2",
          name: "กุมภาพันธ์",
        };
        this.drpmonth.push(d1);
      } else if (this.Month == 3) {
        let d = {
          id: "1",
          name: "มกราคม",
        };
        this.drpmonth.push(d);
        let d1 = {
          id: "2",
          name: "กุมภาพันธ์",
        };
        this.drpmonth.push(d1);
        let d2 = {
          id: "3",
          name: "มีนาคม",
        };
        this.drpmonth.push(d2);
      } else if (this.Month == 4) {
        let d = {
          id: "1",
          name: "มกราคม",
        };
        this.drpmonth.push(d);
        let d1 = {
          id: "2",
          name: "กุมภาพันธ์",
        };
        this.drpmonth.push(d1);
        let d2 = {
          id: "3",
          name: "มีนาคม",
        };
        this.drpmonth.push(d2);
        let d3 = {
          id: "4",
          name: "เมษายน",
        };
        this.drpmonth.push(d3);
      } else if (this.Month == 5) {
        let d = {
          id: "1",
          name: "มกราคม",
        };
        this.drpmonth.push(d);
        let d1 = {
          id: "2",
          name: "กุมภาพันธ์",
        };
        this.drpmonth.push(d1);
        let d2 = {
          id: "3",
          name: "มีนาคม",
        };
        this.drpmonth.push(d2);
        let d3 = {
          id: "4",
          name: "เมษายน",
        };
        this.drpmonth.push(d3);
        let d4 = {
          id: "5",
          name: "พฤษภาคม",
        };
        this.drpmonth.push(d4);
      } else if (this.Month == 6) {
        let d = {
          id: "1",
          name: "มกราคม",
        };
        this.drpmonth.push(d);
        let d1 = {
          id: "2",
          name: "กุมภาพันธ์",
        };
        this.drpmonth.push(d1);
        let d2 = {
          id: "3",
          name: "มีนาคม",
        };
        this.drpmonth.push(d2);
        let d3 = {
          id: "4",
          name: "เมษายน",
        };
        this.drpmonth.push(d3);
        let d4 = {
          id: "5",
          name: "พฤษภาคม",
        };
        this.drpmonth.push(d4);
        let d5 = {
          id: "6",
          name: "มิถุนายน",
        };
        this.drpmonth.push(d5);
      } else if (this.Month == 7) {
        let d = {
          id: "1",
          name: "มกราคม",
        };
        this.drpmonth.push(d);
        let d1 = {
          id: "2",
          name: "กุมภาพันธ์",
        };
        this.drpmonth.push(d1);
        let d2 = {
          id: "3",
          name: "มีนาคม",
        };
        this.drpmonth.push(d2);
        let d3 = {
          id: "4",
          name: "เมษายน",
        };
        this.drpmonth.push(d3);
        let d4 = {
          id: "5",
          name: "พฤษภาคม",
        };
        this.drpmonth.push(d4);
        let d5 = {
          id: "6",
          name: "มิถุนายน",
        };
        this.drpmonth.push(d5);
        let d6 = {
          id: "7",
          name: "กรกฎาคม",
        };
        this.drpmonth.push(d6);
      } else if (this.Month == 8) {
        let d = {
          id: "1",
          name: "มกราคม",
        };
        this.drpmonth.push(d);
        let d1 = {
          id: "2",
          name: "กุมภาพันธ์",
        };
        this.drpmonth.push(d1);
        let d2 = {
          id: "3",
          name: "มีนาคม",
        };
        this.drpmonth.push(d2);
        let d3 = {
          id: "4",
          name: "เมษายน",
        };
        this.drpmonth.push(d3);
        let d4 = {
          id: "5",
          name: "พฤษภาคม",
        };
        this.drpmonth.push(d4);
        let d5 = {
          id: "6",
          name: "มิถุนายน",
        };
        this.drpmonth.push(d5);
        let d6 = {
          id: "7",
          name: "กรกฎาคม",
        };
        this.drpmonth.push(d6);
        let d7 = {
          id: "8",
          name: "สิงหาคม",
        };
        this.drpmonth.push(d7);
      } else if (this.Month == 9) {
        let d = {
          id: "1",
          name: "มกราคม",
        };
        this.drpmonth.push(d);
        let d1 = {
          id: "2",
          name: "กุมภาพันธ์",
        };
        this.drpmonth.push(d1);
        let d2 = {
          id: "3",
          name: "มีนาคม",
        };
        this.drpmonth.push(d2);
        let d3 = {
          id: "4",
          name: "เมษายน",
        };
        this.drpmonth.push(d3);
        let d4 = {
          id: "5",
          name: "พฤษภาคม",
        };
        this.drpmonth.push(d4);
        let d5 = {
          id: "6",
          name: "มิถุนายน",
        };
        this.drpmonth.push(d5);
        let d6 = {
          id: "7",
          name: "กรกฎาคม",
        };
        this.drpmonth.push(d6);
        let d7 = {
          id: "8",
          name: "สิงหาคม",
        };
        this.drpmonth.push(d7);
        let d8 = {
          id: "9",
          name: "กันยายน",
        };
        this.drpmonth.push(d8);
      } else if (this.Month == 10) {
        let d = {
          id: "1",
          name: "มกราคม",
        };
        this.drpmonth.push(d);
        let d1 = {
          id: "2",
          name: "กุมภาพันธ์",
        };
        this.drpmonth.push(d1);
        let d2 = {
          id: "3",
          name: "มีนาคม",
        };
        this.drpmonth.push(d2);
        let d3 = {
          id: "4",
          name: "เมษายน",
        };
        this.drpmonth.push(d3);
        let d4 = {
          id: "5",
          name: "พฤษภาคม",
        };
        this.drpmonth.push(d4);
        let d5 = {
          id: "6",
          name: "มิถุนายน",
        };
        this.drpmonth.push(d5);
        let d6 = {
          id: "7",
          name: "กรกฎาคม",
        };
        this.drpmonth.push(d6);
        let d7 = {
          id: "8",
          name: "สิงหาคม",
        };
        this.drpmonth.push(d7);
        let d8 = {
          id: "9",
          name: "กันยายน",
        };
        this.drpmonth.push(d8);
        let d9 = {
          id: "10",
          name: "ตุลาคม",
        };
        this.drpmonth.push(d9);
      } else if (this.Month == 11) {
        let d = {
          id: "1",
          name: "มกราคม",
        };
        this.drpmonth.push(d);
        let d1 = {
          id: "2",
          name: "กุมภาพันธ์",
        };
        this.drpmonth.push(d1);
        let d2 = {
          id: "3",
          name: "มีนาคม",
        };
        this.drpmonth.push(d2);
        let d3 = {
          id: "4",
          name: "เมษายน",
        };
        this.drpmonth.push(d3);
        let d4 = {
          id: "5",
          name: "พฤษภาคม",
        };
        this.drpmonth.push(d4);
        let d5 = {
          id: "6",
          name: "มิถุนายน",
        };
        this.drpmonth.push(d5);
        let d6 = {
          id: "7",
          name: "กรกฎาคม",
        };
        this.drpmonth.push(d6);
        let d7 = {
          id: "8",
          name: "สิงหาคม",
        };
        this.drpmonth.push(d7);
        let d8 = {
          id: "9",
          name: "กันยายน",
        };
        this.drpmonth.push(d8);
        let d9 = {
          id: "10",
          name: "ตุลาคม",
        };
        this.drpmonth.push(d9);
        let d10 = {
          id: "11",
          name: "พฤศจิกายน ",
        };
        this.drpmonth.push(d10);
      } else if (this.Month == 12) {
        let d = {
          id: "1",
          name: "มกราคม",
        };
        this.drpmonth.push(d);
        let d1 = {
          id: "2",
          name: "กุมภาพันธ์",
        };
        this.drpmonth.push(d1);
        let d2 = {
          id: "3",
          name: "มีนาคม",
        };
        this.drpmonth.push(d2);
        let d3 = {
          id: "4",
          name: "เมษายน",
        };
        this.drpmonth.push(d3);
        let d4 = {
          id: "5",
          name: "พฤษภาคม",
        };
        this.drpmonth.push(d4);
        let d5 = {
          id: "6",
          name: "มิถุนายน",
        };
        this.drpmonth.push(d5);
        let d6 = {
          id: "7",
          name: "กรกฎาคม",
        };
        this.drpmonth.push(d6);
        let d7 = {
          id: "8",
          name: "สิงหาคม",
        };
        this.drpmonth.push(d7);
        let d8 = {
          id: "9",
          name: "กันยายน",
        };
        this.drpmonth.push(d8);
        let d9 = {
          id: "10",
          name: "ตุลาคม",
        };
        this.drpmonth.push(d9);
        let d10 = {
          id: "11",
          name: "พฤศจิกายน ",
        };
        this.drpmonth.push(d10);
        let d11 = {
          id: "12",
          name: "ธันวาคม ",
        };
        this.drpmonth.push(d11);
      }
    } else {
      this.drpmonth = [];

      let d = {
        id: "1",
        name: "มกราคม",
      };
      this.drpmonth.push(d);
      let d1 = {
        id: "2",
        name: "กุมภาพันธ์",
      };
      this.drpmonth.push(d1);
      let d2 = {
        id: "3",
        name: "มีนาคม",
      };
      this.drpmonth.push(d2);
      let d3 = {
        id: "4",
        name: "เมษายน",
      };
      this.drpmonth.push(d3);
      let d4 = {
        id: "5",
        name: "พฤษภาคม",
      };
      this.drpmonth.push(d4);
      let d5 = {
        id: "6",
        name: "มิถุนายน",
      };
      this.drpmonth.push(d5);
      let d6 = {
        id: "7",
        name: "กรกฎาคม",
      };
      this.drpmonth.push(d6);
      let d7 = {
        id: "8",
        name: "สิงหาคม",
      };
      this.drpmonth.push(d7);
      let d8 = {
        id: "9",
        name: "กันยายน",
      };
      this.drpmonth.push(d8);
      let d9 = {
        id: "10",
        name: "ตุลาคม",
      };
      this.drpmonth.push(d9);
      let d10 = {
        id: "11",
        name: "พฤศจิกายน ",
      };
      this.drpmonth.push(d10);
      let d11 = {
        id: "12",
        name: "ธันวาคม ",
      };
      this.drpmonth.push(d11);
    }

    //
    if (this.data.monthly != "") {
      let m = this.displayMount.split("/");

      this.displayMount = m[0] + "/" + item.toString();
    } else {
      this.displayMount = item.toString();
    }
  }

  public setChangemonth(item) {
    let m_name = Utils.ConvertMonthTH(item);
    console.log("setChangemonth item : m_name : "+ m_name+" : "+this.data.yearly);
    if (this.data.yearly != "") {
      console.log("this.displayMount : ",this.displayMount)
      let m = this.displayMount.split("/");

      this.displayMount = m_name + "/" + this.data.yearly;
    } else {
      this.displayMount = m_name;
    }
  }

  // public getDataByMemberName() {
  //   const payload = {
  //     memberName: this.currentUser.memberName,
  //   };

  //   this.ngxService.start();

  //   this.frontretailService.getDataByMemberName(payload).subscribe((res) => {
  //     // console.log("res getDataByMemberName");
  //     // console.log(res);
  //     this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
  //     if (res.result) {
  //       this.data.refRetail = res.result;

  //       this.getListStationByTaxNo(this.data.refRetail.taxNo);

  //       //_refRetailId: any = "";

  //       this._refRetailId = res.result.retailId;

  //       //console.log("res getDataByMemberName");
  //       //console.log(this.data);
  //     }
  //   });
  // }

  public getDataByMemberName() {
    const payload = {
      memberName: this.currentUser.memberName,
    };

    this.ngxService.start();

    this.frontretailService.getDataByMemberName(payload).subscribe((res) => {
      console.log("res getDataByMemberName");
      console.log(res);
      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

      //this.getDataByMemberNameStatus = res.status;

      if (res.status == "false") {
        //this.data.firstRecord = false;
        this.getDataByMemberNameStatus = false;
      } else {
        res.result.firstRecord = true;
        this.getDataByMemberNameStatus = true;

        //this.setForm(res.result);

        this._refRetailId = res.result.retailId;

        this.data.refRetailId = this._refRetailId;

        this.data.refRetail = res.result;

        this.getListStationByTaxNo(this.data.refRetail.taxNo);

        // console.log("this.data_getDataByMemberName");
        // console.log(this.data_getDataByMemberName);
      }
    });
  }

  ngOnDestroy() {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }

  public getListStationByTaxNo(item) {
    const payload = {
      taxNo: item,
    };

    this.ngxService.start();

    this.frontretailService.getListStationByTaxNo2(payload).subscribe((res) => {
      //console.log("res getListStationByTaxNo");
      //console.log(res);
      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
      if (res.result) {
        this.drpListStationByTaxNo = res.result;
      } else {
        this.drpListStationByTaxNo = [];
      }

      //this.getDataByMemberNameStatus = res.status;
    });
  }

  public getDrpOilTypeList() {
    this.oilTypeService.getDrpOilTypeList().subscribe((res) => {
      //console.log("res getDrpOilTypeList");
      //console.log(res);

      if (res.result) {
        res.result.forEach((element) => {
          element.select = true;
        });

        this.drpOilTypeList = res.result;
      } else {
        this.drpOilTypeList = [];
      }
    });
  }

  public getStatusAndFinesByMonth() {
    const payload = {
      refRetailStationId: this.data.refStationId,
      yearly: this.data.yearly,
      monthly: this.data.monthly,
      memberName: this.currentUser.memberName,
    };

    this.ngxService.start();

    this.approvalTaxForm03Service
      .getStatusAndFinesByMonth(payload)
      .subscribe((res) => {
        console.log("res getStatusAndFinesByMonth");
        console.log(res);
        this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
        if (res.result) {
          // save
          this.data.addTimes = res.result.addTimes;
          this.data.statusAcc = res.result.statusAcc;
          this.data.statusAccDesc = res.result.statusAccDesc;
          // save
          this.data.finesMonth = res.result.finesMonth;
          this.data.finesPerMonth = res.result.finesPerMonth;
          //
          this.data.finesStartDate = res.result.finesStartDate;
          this.data.finesEndDate = res.result.finesEndDate;
          //
          this.data.finesTotal = res.result.finesTotal;

          console.log("res getStatusAndFinesByMonth this.data");
          console.log(this.data);

          // let BahtSplit = res.result.finesTotal.toLocaleString().split(".");

          // //console.log("BahtSplit");
          // //console.log(BahtSplit);

          // this.plus_extra_money_Baht =
          //   BahtSplit[0] == undefined ? 0 : BahtSplit[0];
          // this.plus_extra_money_Satang =
          //   BahtSplit[1] == undefined ? 0 : BahtSplit[1];
          // this.plus_extra_money_Satang_show =
          //   BahtSplit[1] == undefined ? "00" : BahtSplit[1];
          //this.data.monthly = res.result.monthly;
          this.data.refRetailStationId = res.result.refRetailStationId;

          //           addTimes: 0
          // finesEndDate: "มกราคม 2564"
          // finesPerMonth: 1.5
          // finesStartDate: "ตุลาคม 2563"
          // finesTotal: 15
          // monthly: 2
          // refRetailStationId: "KHnf_uO5if_gfzHJp5prsA"
          // statusAcc: "1"
          // statusAccDesc: "ภายในกำหนดเวลา"
          // yearly: 2564
          if (this.data.summaryList.length != 0) {
            this.calSum();
          }
        } else {
        }
      });
  }

  public getData(item) {
    const payload = {
      taxForm03Id: item,
    };

    this.ngxService.start();

    this.approvalTaxForm03Service.getData(payload).subscribe((res) => {
      //console.log("res approvalTaxForm03Service getData");
      //console.log(res);

      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

      if (res.result) {
        this.setForm(res.result);
      }
    });
  }

  public getDataStation(item) {
    const payload = {
      id: item,
    };

    this.ngxService.start();

    this.frontretailService.getDataStation(payload).subscribe((res) => {
      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
    });
  }

  setForm(res: any) {
    console.log("res setForm");
    console.log(res);
    // refRetail
    res.refRetail.houseFileList.forEach((element) => {
      element.id = element.contentId;
      element.pathFile = element.pathFile;
      element.ShowFile =
        REST_URL.MAIN_UPLOADFILE.getFile +
        "/" +
        element.pathFile +
        "/" +
        "taxform03";

      element.pathFileName = element.fileName;
      element.type = element.fileType;
    });

    res.refRetail.cerFileList.forEach((element) => {
      element.id = element.contentId;
      element.pathFile = element.pathFile;
      element.ShowFile =
        REST_URL.MAIN_UPLOADFILE.getFile +
        "/" +
        element.pathFile +
        "/" +
        "taxform03";

      element.pathFileName = element.fileName;
      element.type = element.fileType;
    });

    res.refRetail.agentFileList.forEach((element) => {
      element.id = element.contentId;
      element.pathFile = element.pathFile;
      element.ShowFile =
        REST_URL.MAIN_UPLOADFILE.getFile +
        "/" +
        element.pathFile +
        "/" +
        "taxform03";

      element.pathFileName = element.fileName;
      element.type = element.fileType;
    });
    // refRetail

    // refStation
    res.refStation.houseFileList.forEach((element) => {
      element.id = element.contentId;
      element.pathFile = element.pathFile;
      element.ShowFile =
        REST_URL.MAIN_UPLOADFILE.getFile +
        "/" +
        element.pathFile +
        "/" +
        "taxform03";

      element.pathFileName = element.fileName;
      element.type = element.fileType;
    });

    res.refStation.mapFileList.forEach((element) => {
      element.id = element.contentId;
      element.pathFile = element.pathFile;
      element.ShowFile =
        REST_URL.MAIN_UPLOADFILE.getFile +
        "/" +
        element.pathFile +
        "/" +
        "taxform03";

      element.pathFileName = element.fileName;
      element.type = element.fileType;
    });
    // refStation

    let accountList = [];
    // เอกสารงบเดือนแสดงรายรับ-จ่ายน้ำมัน/ก๊าซปิโตรเลียม

    if (res.accountList != undefined) {
      res.accountList.forEach((element) => {
        let m = {
          id: element.id,
          pathFile: element.pathFile,
          ShowFile:
            REST_URL.MAIN_UPLOADFILE.getFile +
            "/" +
            element.pathFile +
            "/" +
            "taxform03",
          pathFileName: element.fileName,
          Status: 200,
          type: element.fileType,
        };

        accountList.push(m);
      });
    }

    // เอกสารงบเดือนแสดงรายรับ-จ่ายน้ำมัน/ก๊าซปิโตรเลียม

    // งบเดือนแสดงการรับ-จ่ายน้ำมัน / ก๊าซปิโครเลียม

    res.detailList.forEach((element) => {
      element.sequence = element.sequence;
      element.oilTypeId = element.oilTypeId;
      element.oilTypeName = element.oilTypeName;
      element.balance = element.balance;
      element.inQty = element.inQty;
      element.receiver = element.balance + element.inQty;
      element.outQty = element.outQty;
      element.pay = element.outQty;
      element.remain = element.remain;

      // balance: balance, // คงเหลือยกมา
      // inQty: inQty, // ปริมาณนำเข้า
      // receiver: balance + inQty, // รวมรับ

      // outQty: outQty, // ปริมาณการจำหน่าย
      // pay: outQty, // รวมจ่าย
      // remain: remain, // 	คงเหลือยกไป
    });

    let sum_amount = 0;
    res.summaryList.forEach((element) => {
      element.sequence = element.sequence;
      element.oilTypeId = element.oilTypeId;
      element.oilTypeName = element.oilTypeName;

      element.qty = element.qty;
      element.rate = element.rate;
      element.amount = element.amount;

      let _amountSplit = element.amount.toLocaleString().split(".");

      element.amountBaht = _amountSplit[0] == undefined ? 0 : _amountSplit[0];
      element.amountBaht_show =
        _amountSplit[0] == undefined ? "00" : _amountSplit[0].toLocaleString();
      //
      element.amountSatang = _amountSplit[1] == undefined ? 0 : _amountSplit[1];
      element.amountSatang_show =
        _amountSplit[1] == undefined ? "00" : _amountSplit[1].toString();

      sum_amount = sum_amount + element.amount;

      // sequence: this.data.summaryList.length + 1,
      // oilTypeId: oilType.oilTypeId, // ชนิดน้ำมันก๊าซ
      // oilTypeName: oilType.oilTypeName, // ชนิดน้ำมันก๊าซ

      // qty: 1000,
      // rate: 1.2,
      // amount: 1200,
      // amountBaht: 1200,
      // amountSatang: 0,
    });

    //console.log("res.summaryList");
    //console.log(res.summaryList);

    sum_amount = parseFloat(sum_amount.toFixed(2)); // รวมบาท

    //console.log("sum_amount");
    //console.log(sum_amount);

    // รวม เงิน ในตาราง

    let _sum_amountSplit = sum_amount.toLocaleString().split(".");

    // --------------------------------------------------------------
    this.sum_amountBaht =
      _sum_amountSplit[0] == undefined ? 0 : _sum_amountSplit[0]; // รวม(บาท)
    this.sum_amountBaht_show =
      _sum_amountSplit[0] == undefined ? "00" : _sum_amountSplit[0].toString(); // รวม(บาท)
    this.sum_amountSatang =
      _sum_amountSplit[1] == undefined ? 0 : _sum_amountSplit[1]; // รวม(สตางค์)
    this.sum_amountSatang_show =
      _sum_amountSplit[1] == undefined ? "00" : _sum_amountSplit[1];
    // -------------------------------------------------------------

    //this.getStatusAndFinesByMonth();

    // รวมบวกเพิ่ม

    let _extraMoneySplit = res.extraMoney.toLocaleString().split(".");

    this.plus_extra_money_Baht =
      _extraMoneySplit[0] == undefined ? 0 : _extraMoneySplit[0]; // บวกเงินเพิ่ม (ร้อยละ xx ต่อเดือนของค่าภาษีืที่ต้่องชำระหรือชำระขาดตั้งแต่ xxxx)
    this.plus_extra_money_Satang =
      _extraMoneySplit[1] == undefined ? 0 : _extraMoneySplit[1]; // บวกเงินเพิ่ม (ร้อยละ xx ต่อเดือนของค่าภาษีืที่ต้่องชำระหรือชำระขาดตั้งแต่ xxxx)
    this.plus_extra_money_Satang_show =
      _extraMoneySplit[1] == undefined
        ? "00"
        : _extraMoneySplit[1].length == 1
          ? _extraMoneySplit[1] + "0"
          : _extraMoneySplit[1];

    // รวมทั้งสิ้น
    let _total = sum_amount + parseFloat(res.extraMoney);

    _total = parseFloat(_total.toFixed(2));

    let _totalSplit = _total.toLocaleString().split(".");

    console.log("_total");
    console.log(_total);

    // --------------------------------------------------------------
    this.total_tax_payable_Baht =
      _totalSplit[0] == undefined ? 0 : _totalSplit[0]; // ยอดเงินภาษีที่ต้องชำระทั้งสิ้น
    this.total_tax_payable_Baht_show =
      _totalSplit[0] == undefined ? 0 : _totalSplit[0]; // ยอดเงินภาษีที่ต้องชำระทั้งสิ้น

    this.total_tax_payable_Satang =
      _totalSplit[1] == undefined ? 0 : _totalSplit[1]; // ยอดเงินภาษีที่ต้องชำระทั้งสิ้น
    this.total_tax_payable_Satang_show =
      _totalSplit[1] == undefined
        ? "00"
        : _totalSplit[1].length == 1
          ? _totalSplit[1] + "0"
          : _totalSplit[1];

    // --------------------------------------------------------------

    this.setChangeyear(res.yearly);
    this.setChangemonth(res.monthly);

    this.data = {
      taxForm03Id: res.taxForm03Id,
      //refRetailId: res.refRetailId,
      refRetailId:
        this._refRetailId == "" ? res.refRetailId : this._refRetailId,

      refStationId: res.refStationId,
      docNo: res.docNo,
      docDate: res.docDate,

      yearly: res.yearly,
      monthly: res.monthly.toString(),
      monthName: res.monthName,

      oilTypeId: "", // รายการน้ำมัน/ก๊าซปิโครเลียม
      oilTypeIdName: "", // รายการน้ำมัน/ก๊าซปิโครเลียม
      balance: "", // ยอดยกมา
      inQty: "", // ปริมาณนำเข้า
      outQty: "", // ปริมาณจำหน่าย
      remain: "", // คงเหลือยกไป

      statusAcc: res.statusAcc,
      statusAccDesc: res.statusAccDesc,
      status: res.status,
      statusName: res.statusName,

      addTimes: res.addTimes,
      sequence: res.sequence,
      extraMoney: res.extraMoney,
      extraRate: res.extraRate,

      extraStartDate: res.extraStartDate,
      extraEndDate: res.extraEndDate,

      taxTotal: res.taxTotal,

      accountList: accountList, // เอกสารประกอบ

      summaryList: res.summaryList,
      detailList: res.detailList,

      refRetail: {
        retailId: res.refRetail.retailId,
        mainStatus: res.refRetail.mainStatus,
        mainStatusName: res.refRetail.mainStatusName,
        ownerName: res.refRetail.ownerName,
        taxNo: res.refRetail.taxNo,

        customerType: res.refRetail.customerType,
        idCard: res.refRetail.idCard,
        idCardAt: res.refRetail.idCardAt,
        corpNo: res.refRetail.corpNo,
        corpDate: res.refRetail.corpDate,

        houseNo: res.refRetail.houseNo,
        address: res.refRetail.address,
        soi: res.refRetail.soi,
        road: res.refRetail.road,

        tambonId: res.refRetail.tambonId,
        amphurId: res.refRetail.amphurId,
        provinceId: res.refRetail.provinceId,
        postcode: res.refRetail.postcode,
        fullAddress: res.refRetail.fullAddress,
        mobile: res.refRetail.mobile,
        email: res.refRetail.email,

        houseFileNum: res.refRetail.houseFileNum,
        cerFileNum: res.refRetail.cerFileNum,
        agentFileNum: res.refRetail.agentFileNum,

        houseFileList: res.refRetail.houseFileList,
        cerFileList: res.refRetail.cerFileList,
        agentFileList: res.refRetail.agentFileList,
      },
      refStation: {
        retailStationId: res.refStation.retailStationId,
        retailId: res.refStation.retailId,
        stationCode: res.refStation.stationCode,
        stationName: res.refStation.stationName,

        businessTypeList: res.refStation.businessTypeList,

        houseNo: res.refStation.houseNo,
        address: res.refStation.address,
        soi: res.refStation.soi,
        road: res.refStation.road,
        tambonId: res.refStation.tambonId,
        tambonName: res.refStation.tambonName,

        amphurId: res.refStation.amphurId,
        amphurName: res.refStation.amphurName,

        provinceId: res.refStation.provinceId,
        provinceName: res.refStation.provinceName,
        postcode: res.refStation.postcode,
        fullAddress: res.refStation.fullAddress,
        mobile: res.refStation.mobile,
        email: res.refStation.email,
        status: res.refStation.status,
        statusName: res.refStation.statusName,
        houseFileNum: res.refStation.houseFileNum,
        mapFileNum: res.refStation.mapFileNum,

        houseFileList: res.refStation.houseFileList,
        mapFileList: res.refStation.mapFileList,
      },

      // -------------------------------------- //
      finesMonth: res.extraMonth == undefined ? 0 : res.extraMonth,
      finesPerMonth: res.extraRate == undefined ? 0 : res.extraRate,

      finesStartDate:
        res.extraStartDate == undefined ? "-" : res.extraStartDate,
      finesEndDate: res.extraEndDate == undefined ? "-" : res.extraEndDate,
      // ------------------------------------------------
      finesTotal: res.finesTotal == undefined ? 0 : res.finesTotal,

      refRetailStationId: res.refRetailStationId,
    };

    console.log("this.data ---");
    console.log(this.data);

    this.getListStationByTaxNo(res.refRetail.taxNo);

    this.drpOilTypeList.forEach((_l) => {
      this.data.detailList.forEach((_e) => {
        if (_e.oilTypeId == _l.oilTypeId) {
          _l.select = false;
        }
      });
    });
  }

  onSubmit() {
    // let extraStartDate;
    // if (this.data.extraStartDate != null) {
    //   extraStartDate = Utils.dateTostring(this.data.extraStartDate);
    // } else {
    //   extraStartDate = "";
    // }
    // let extraEndDate;
    // if (this.data.extraEndDate != null) {
    //   extraEndDate = Utils.dateTostring(this.data.extraEndDate);
    // } else {
    //   extraEndDate = "";
    // }

    // let finesStartDate;
    // if (this.data.finesStartDate != null) {
    //   finesStartDate = Utils.dateTostring(this.data.finesStartDate);
    // } else {
    //   finesStartDate = "";
    // }

    // let finesEndDate;
    // if (this.data.finesEndDate != null) {
    //   finesEndDate = Utils.dateTostring(this.data.finesEndDate);
    // } else {
    //   finesEndDate = "";
    // }

    //this.data.finesStartDate = res.result.finesStartDate;
    //this.data.finesEndDate = res.result.finesEndDate;

    let accountList = [];
    this.data.accountList.forEach((val) => {
      let d = {
        fileName: val.pathFileName,
        fileType: val.type,
        pathFile: val.pathFile,
      };
      accountList.push(d);
    });

    let re = /\,/gi;

    console.log("this.data");
    console.log(this.data);

    let summaryList = [];
    this.data.summaryList.forEach((val) => {
      let d = {
        sequence: val.sequence,
        oilTypeId: val.oilTypeId,
        oilTypeName: val.oilTypeName,
        qty: val.qty,
        rate: val.rate,
        amount: val.amount,
        amountBaht: val.amountBaht,
        amountSatang: val.amountSatang,
        taxTotal: parseFloat(
          val.amountBaht.toString() + "." + val.amountSatang.toString()
        ),
      };
      summaryList.push(d);
    });

    let detailList = [];
    this.data.detailList.forEach((val) => {
      let d = {
        sequence: val.sequence,
        oilTypeId: val.oilTypeId,
        oilTypeName: val.oilTypeName,
        balance: val.balance,
        inQty: val.inQty,
        outQty: val.outQty,
        remain: val.remain,
      };
      detailList.push(d);
    });

    //     response ที่ได้จาก service เมื่อกี้
    // addTimes: 0  >> addTimes
    // finesEndDate: "-" >> extraEndDate
    // finesPerMonth: 1.5  >> extraRate
    // finesStartDate: "-"  >>extraStartDate
    // finesTotal: 0 >> extraMoney
    // monthly: 3
    // refRetailStationId: "QMoVJvBPc4g7NImm4mssQw"
    // statusAcc: "1"
    // statusAccDesc: "ภายในกำหนดเวลา"
    // yearly: 2564

    let data = {
      refTaxNo: this.data.refRetail.taxNo,

      taxForm03Id: this.data.taxForm03Id,
      refRetailId: this.data.refRetailId,
      refStationId: this.data.refStationId,

      memberName: this.currentUser.memberName,
      yearly: this.data.yearly,
      monthly: this.data.monthly,

      monthName: this.data.monthName,
      docNo: this.data.docNo,
      status: this.data.status,
      statusAcc: this.data.statusAcc,
      statusAccDesc: this.data.statusAccDesc,

      addTimes: this.data.addTimes,
      sequence: this.data.sequence,

      //finesMonth: res.extraMonth == undefined ? 0 : res.extraMonth,
      //finesPerMonth: res.extraRate == undefined ? 0 : res.extraRate,

      extraMonth: this.data.finesMonth,
      extraRate: this.data.finesPerMonth, //

      extraStartDate: this.data.finesStartDate, // วันที่
      extraEndDate: this.data.finesEndDate,

      extraMoney: parseFloat(this.data.extraMoney.toString().replace(re, "")),
      taxTotal: parseFloat(this.data.taxTotal.toString().replace(re, "")),

      active: true,
      accountList: accountList,
      summaryList: summaryList,
      detailList: detailList,
    };

    console.log("data");
    console.log(data);

    this.ngxService.start();

    this.approvalTaxForm03Service.saveData(data).subscribe((res) => {
      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

      console.log("res");
      console.log(res);
      if (res.result) {
        // สำเนาหนังสือรับรองของกระทรวงพาณิชย์
        this.data.accountList.forEach((val) => {
          this.moveFile(val.pathFile);
        });

        this.delect_accountList.forEach((val) => {
          this.deleteFileTemp(val.pathFile);
          this.deleteFile(val.pathFile);
        });

        // Stop the foreground loading after 5s
        setTimeout(() => {
          this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
          Swal.fire({
            icon: "success",
            title: "สำเร็จ",
            text: "บันทึกข้อมูลสำเร็จ",
            showConfirmButton: false,
            timer: 1500,
          });

          setTimeout(() => {
            this.cancel();
          }, 1500);
        }, 300);
      } else {
        Swal.fire("ไม่สำเร็จ", res.message, "error");
      }
    });
  }

  clickFileImage_accountList() {
    let element: HTMLElement = document.getElementById(
      "fileImage_accountList"
    ) as HTMLElement;
    element.click();
  }

  fileOverBase_accountList(event) {
    if (event) {
      this.ngxService.start();
      this.uploader_accountList.getUploadListener().subscribe((res) => {
        this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
        let fileListImage = JSON.parse(res.respond.replace(/'/g, '"'));
        let filetype = fileListImage[0].split(".");
        if (filetype[1].toUpperCase() == "PDF") {
          filetype[1] = "P";
        } else if (
          filetype[1].toUpperCase() == "DOCX" ||
          filetype[1].toUpperCase() == "DOC"
        ) {
          filetype[1] = "W";
        } else if (
          filetype[1].toUpperCase() == "XLS" ||
          filetype[1].toUpperCase() == "XLSX"
        ) {
          filetype[1] = "E";
        }

        const fomatdata = {
          pathFileName: res.item.file.name,
          fileType: filetype[1],
          pathFile: fileListImage[0],
          fileNum: "",
          ShowFile:
            REST_URL.MAIN_UPLOADFILE.getFileTemp + "/" + fileListImage[0],
        };
        let listUrl = this.data.accountList.find((val) => {
          if (val === fileListImage[0]) {
            return true;
          } else {
            return false;
          }
        });
        if (listUrl !== fileListImage[0]) {
          this.data.accountList.push(fomatdata);
        }

        console.log("this.data.accountList");
        console.log(this.data.accountList);
      });
    }
  }

  public removeFiles_accountList(PathUid, i) {
    this.data.accountList.splice(i, 1);
    this.delect_accountList.push(PathUid);
  }

  moveFile(data) {
    let d = {
      fileName: data,
      location: "taxform03",
    };

    //this.ngxService.start();
    this.uploadService.moveFile(d).subscribe((res) => {
      //this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
    });
  }

  deleteFile(data) {
    let d = {
      fileName: data,
      location: "taxform03",
    };

    //this.ngxService.start();
    this.uploadService.deleteFile(d).subscribe((res) => {
      //this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
    });
  }

  deleteFileTemp(data) {
    let d = {
      fileName: data,
    };

    //this.ngxService.start();
    this.uploadService.deleteFileTemp(d).subscribe((res) => {
      //this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
    });
  }

  cancel() {
    console.log("this._cancel");
    console.log(this._cancel);
    this.router.navigate([this._cancel]);
  }

  add_detailList() {
    if (this.data.refStationId == "") {
      Swal.fire({
        icon: "error",
        title: "กรุณาระบุชื่อสถานการค้าปลีก",
        // text: "Modal with a custom image.",
        confirmButtonText: "ตกลง",
      });
    } else if (this.data.oilTypeId == "") {
      Swal.fire({
        icon: "error",
        title: "กรุณาระบุข้อมูลน้ำมัน/ก๊าซปิโตรเลียม",
        // text: "Modal with a custom image.",
        confirmButtonText: "ตกลง",
      });
    } else {
      let oilType = this.drpOilTypeList.find((val) => {
        if (val.oilTypeId == this.data.oilTypeId) {
          val.select = false;
          return val;
        }
      });

      // console.log("oilType");
      // console.log(oilType);

      // res.result.forEach(element => {
      //   element.select = true;
      // });

      //console.log("this.drpOilTypeList");
      //console.log(this.drpOilTypeList);

      // detailList

      let re = /\,/gi;
      let balance = parseFloat(
        this.data.balance == "" ? "0" : this.data.balance.replace(re, "")
      );
      let inQty = parseFloat(
        this.data.inQty == "" ? "0" : this.data.inQty.replace(re, "")
      );
      let outQty = parseFloat(
        this.data.outQty == "" ? "0" : this.data.outQty.replace(re, "")
      );

      let remain = parseFloat(
        this.data.remain == "" ? "0" : this.data.remain.replace(re, "")
      );

      //ปริมาณจำหน่าย (ลิตร)
      // คงเหลือยกไป (ลิตร)
      if (remain < 0) {
        Swal.fire({
          icon: "warning",
          title: "แจ้งเตือน",
          text: "ปริมาณนำเข้ามากกว่ายอดคงเหลือ",
          showConfirmButton: false,
          timer: 3000,
        });
      } else {
        let de_t = {
          sequence: this.data.detailList.length + 1,
          oilTypeId: oilType.oilTypeId,
          oilTypeName: oilType.oilTypeName,

          balance: balance, // คงเหลือยกมา
          inQty: inQty, // ปริมาณนำเข้า
          receiver: balance + inQty, // รวมรับ

          outQty: outQty, // ปริมาณการจำหน่าย
          pay: outQty, // รวมจ่าย
          remain: remain, // 	คงเหลือยกไป
        };

        this.data.detailList.push(de_t);

        // detailList

        // console.log("this.data.detailList");
        // console.log(this.data.detailList);

        // summaryList

        let Baht = outQty * oilType.oilTypeRate;
        Baht = parseFloat(Baht.toFixed(2));

        //console.log("outQty");
        //console.log(outQty);

        //console.log("oilType.oilTypeRate");
        //console.log(oilType.oilTypeRate);

        //console.log("Baht");
        //console.log(Baht);

        let BahtSplit = Baht.toLocaleString().split(".");

        //console.log("Baht");
        //console.log(Baht);

        //console.log("BahtSplit");
        //console.log(BahtSplit);

        //let _Satang = "";

        let de_s = {
          sequence: this.data.summaryList.length + 1,
          oilTypeId: oilType.oilTypeId, // ชนิดน้ำมันก๊าซ
          oilTypeName: oilType.oilTypeName, // ชนิดน้ำมันก๊าซ
          qty: outQty,
          rate: oilType.oilTypeRate, // อัตรา
          amount: Baht, // จำนวนเงิน
          // -----------------------------------------------
          amountBaht:
            BahtSplit[0] == undefined
              ? 0
              : parseFloat(BahtSplit[0].replace(re, "")), // บาท
          amountBaht_show: BahtSplit[0] == undefined ? 0 : BahtSplit[0], // บาท
          // -------------------------
          amountSatang:
            BahtSplit[1] == undefined
              ? 0
              : parseFloat(BahtSplit[1].replace(re, "")), // สตางต์
          amountSatang_show: BahtSplit[1] == undefined ? "00" : BahtSplit[1], // สตางต์
        };
        this.data.summaryList.push(de_s);

        console.log("this.data.summaryList");
        console.log(this.data.summaryList);

        //console.log("Baht");
        //console.log(Baht);

        this.calSum();

        this.data.oilTypeId = "";
        this.data.balance = "";
        this.data.inQty = "";
        this.data.outQty = "";
        this.data.remain = "";
      }
    }
  }

  calSum() {
    let oilType = this.drpOilTypeList.find((val) => {
      if (val.oilTypeId == this.data.oilTypeId) {
        val.select = false;
        return val;
      }
    });

    //console.log("oilType");
    //console.log(oilType);

    let re = /\,/gi;
    //

    let sum_amount = 0;

    this.data.summaryList.forEach((item) => {
      sum_amount =
        sum_amount + parseFloat(item.amount.toString().replace(re, ""));
    });

    sum_amount = parseFloat(sum_amount.toFixed(2)); // รวมบาท

    console.log("sum_amount");
    console.log(sum_amount);

    let _sum_amountSplit = sum_amount.toLocaleString().split(".");

    console.log("_sum_amountSplit");
    console.log(_sum_amountSplit);

    // ----------------------------------------------------------------------------
    this.sum_amountBaht =
      _sum_amountSplit[0] == undefined ? 0 : _sum_amountSplit[0]; // รวม(บาท)
    this.sum_amountBaht_show =
      _sum_amountSplit[0] == undefined ? 0 : _sum_amountSplit[0];

    this.sum_amountSatang =
      _sum_amountSplit[1] == undefined ? 0 : _sum_amountSplit[1]; // รวม(สตางค์)
    this.sum_amountSatang_show =
      _sum_amountSplit[1] == undefined ? "00" : _sum_amountSplit[1];

    // ----------------------------------------------------------------------------

    // this.data.finesMonth = res.result.finesMonth;
    // this.data.finesPerMonth = res.result.finesPerMonth;

    let _finesMonth = 0;

    // let Reate =
    //   (sum_amount *
    //     parseFloat(this.data.finesPerMonth.toString().replace(re, ""))) /
    //   100;

    //console.log("sum_amount");
    //console.log(sum_amount);

    //console.log("Reate");
    //console.log(Reate);

    //sum_amount // รวม
    //finesPerMonth // อัตรา
    //finesMonth // จำนวนเดือน

    _finesMonth =
      ((sum_amount *
        parseFloat(this.data.finesPerMonth.toString().replace(re, ""))) /
        100) *
      parseFloat(this.data.finesMonth.toString().replace(re, ""));

    _finesMonth = parseFloat(_finesMonth.toFixed(2)); // บวกเงินเพิ่ม (ร้อยละ 1.5 ต่อเดือนของค่าภาษีที่ต้่องชำระหรือชำระขาดตั้งแต่ 16/01/2564 ถึง 09/03/2564)

    //console.log("_finesMonth");
    //console.log(_finesMonth);

    ///console.log("this.data");
    //console.log(this.data);

    /// เช็คว่า ค่า ยอดเพิ่ม ไม่เกิน ยอดรวม /
    if (_finesMonth > sum_amount) {
      _finesMonth = sum_amount;
    }

    let BahtSplit = _finesMonth.toLocaleString().split(".");

    //console.log("_finesMonth");
    //console.log(_finesMonth);

    //console.log("sum_amount");
    //console.log(sum_amount);

    //console.log("BahtSplit");
    //console.log(BahtSplit);

    this.plus_extra_money_Baht = BahtSplit[0] == undefined ? 0 : BahtSplit[0];
    this.plus_extra_money_Satang = BahtSplit[1] == undefined ? 0 : BahtSplit[1];
    this.plus_extra_money_Satang_show =
      BahtSplit[1] == undefined
        ? "00"
        : BahtSplit[1].length == 1
          ? BahtSplit[1] + "0"
          : BahtSplit[1];

    //console.log("_finesMonth");
    //console.log(_finesMonth);

    //console.log("this.data.summaryList");
    //console.log(this.data.summaryList);

    /////////////////////////////////////// รวมทั้งหมด
    let _subAll = sum_amount + _finesMonth;

    //console.log("_subAll");
    //console.log(_subAll);

    _subAll = parseFloat(_subAll.toFixed(2)); // ยอดเงินภาษีที่ต้องชำระทั้งสิ้น

    let _subAllSplit = _subAll.toLocaleString().split(".");

    this.total_tax_payable_Baht = parseFloat(_subAllSplit[0]); // ยอดเงินภาษีที่ต้องชำระทั้งสิ้น
    this.total_tax_payable_Baht_show = _subAllSplit[0];

    this.total_tax_payable_Satang = parseFloat(_subAllSplit[1]); // ยอดเงินภาษีที่ต้องชำระทั้งสิ้น

    this.total_tax_payable_Satang_show =
      _subAllSplit[1] == undefined
        ? "00"
        : _subAllSplit[1].length == 1
          ? _subAllSplit[1] + "0"
          : _subAllSplit[1];
    /////////////////////////////////////// รวมทั้งหมด

    this.data.extraMoney = _finesMonth.toLocaleString(); // ยดดรวมบวกเงินเพิ่ม
    this.data.taxTotal = _subAll.toLocaleString(); // ยดดรวมทั้งสิ้น
  }

  numberOnly(event): boolean {
    const charCode = event.which ? event.which : event.keyCode;

    if (charCode > 31 && (charCode < 45 || charCode > 57)) {
      return false;
    }
    return true;
  }

  sumP4() {
    let re = /\,/gi;
    let balance = parseFloat(
      this.data.balance == "" ? "0" : this.data.balance.replace(re, "")
    );
    let inQty = parseFloat(
      this.data.inQty == "" ? "0" : this.data.inQty.replace(re, "")
    );
    let outQty = parseFloat(
      this.data.outQty == "" ? "0" : this.data.outQty.replace(re, "")
    );

    this.data.remain = (balance + inQty - outQty).toLocaleString(undefined, {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2,
    });
  }
  toMoney(amount) {
    if (typeof amount == 'string') {
      amount = parseFloat(amount.split(",").join(""));
    }
    return amount.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, "$&,");
  }
  remove_detailList(i) {
    Swal.fire({
      title: "คุณต้องการลบใช่หรือไม่",
      // text: 'You will not be able to recover this imaginary file!',
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "ตกลง",
      cancelButtonText: "ยกเลิก",
    }).then((result) => {
      if (result.value) {
        this.drpOilTypeList.forEach((_l) => {
          _l.select = true;
        });

        this.data.detailList.splice(i, 1);
        this.data.summaryList.splice(i, 1);

        this.data.detailList.forEach((_e) => {
          this.drpOilTypeList.forEach((_l) => {
            if (_e.oilTypeId == _l.oilTypeId) {
              _l.select = false;
            } else {
              _l.select = true;
            }
          });
        });

        console.log("this.data.detailList");
        console.log(this.data.detailList);

        console.log("this.drpOilTypeList");
        console.log(this.drpOilTypeList);

        let sum_amountBaht = 0;
        let sum_amountSatang = 0;
        this.data.summaryList.forEach((item) => {
          sum_amountBaht = sum_amountBaht + parseFloat(item.amountBaht);
          sum_amountSatang = sum_amountSatang + parseFloat(item.amountSatang);
        });

        this.sum_amountBaht = sum_amountBaht; // รวม(บาท)
        this.sum_amountSatang = sum_amountSatang; // รวม(สตางค์)

        this.total_tax_payable_Baht =
          sum_amountBaht + parseFloat(this.plus_extra_money_Baht); // ยอดเงินภาษีที่ต้องชำระทั้งสิ้น
        this.total_tax_payable_Satang =
          sum_amountSatang + parseFloat(this.plus_extra_money_Satang); // ยอดเงินภาษีที่ต้องชำระทั้งสิ้น
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        // Swal.fire("Cancelled", "Your imaginary file is safe :)", "error");
      }
    });
  }
}
