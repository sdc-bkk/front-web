import { Routes, RouterModule } from "@angular/router";
import { ApprovalTaxForm03ListDataComponent } from "./approval-tax-form-03-list-data/approval-tax-form-03-list-data.component";
import { ApprovalTaxForm03ListComponent } from "./approval-tax-form-03-list/approval-tax-form-03-list.component";
import { ApprovalTaxForm03UpdateComponent } from "./approval-tax-form-03-update/approval-tax-form-03-update.component";
import { ApprovalTaxForm03ViewComponent } from "./approval-tax-form-03-view/approval-tax-form-03-view.component";
import { ApprovalTaxForm03Component } from "./approval-tax-form-03.component";

const TEMPLATE_ROUTES: Routes = [
  {
    path: "",
    component: ApprovalTaxForm03Component,
    children: [
      {
        path: "",
        component: ApprovalTaxForm03ListComponent,
      },
      {
        path: "list-data/:yearly/:monthly",
        component: ApprovalTaxForm03ListDataComponent,
      },
      {
        path: "update/:yearly/:monthly/:id",
        component: ApprovalTaxForm03UpdateComponent,
      },
      {
        path: "view/:yearly/:monthly/:id",
        component: ApprovalTaxForm03ViewComponent,
      },
    ],
  },

  // 404 Page Not Found
  { path: "**", redirectTo: "/" },
];

export const ApprovalTaxForm03Routes = RouterModule.forChild(TEMPLATE_ROUTES);
