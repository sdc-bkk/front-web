import {
  Component,
  ViewEncapsulation,
  OnInit,
  TemplateRef,
  ViewChild,
} from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Observable, Subject } from "rxjs/Rx";
import { finalize, map, startWith, takeUntil } from "rxjs/operators";
import { ConfigService } from "../../../../shared/services/config/config.service";
// import createNumberMask from 'text-mask-addons/dist/createNumberMask';
import Utils from "../../../../shared/util/utils";
import {
  NgbModal,
  NgbModalConfig,
  NgbModalRef,
} from "@ng-bootstrap/ng-bootstrap";
import Swal from "sweetalert2";
import { FormControl } from "@angular/forms";
import { AddressService } from "../../../address.service";
import { FileUploader } from "../../../../shared/components/file-upload/file-uploader.class";
import { REST_URL } from "../../../../shared/services/config/rest-url";
import { SoiService } from "../../../soi.service";
import { RoadService } from "../../../road.service";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { UploadService } from "../../../upload.service";
import { BusinessTypeService } from "../../../business-type.service";
import { CustomerTypeService } from "../../../customer-type.service";
import { FrontretailService } from "../../../frontretail.service";
import createNumberMask from "text-mask-addons/dist/createNumberMask";
import { RequestRefundService } from "../request-refund.service";
import { ApprovalTaxForm03Service } from "../../approval-tax-form-03/approval-tax-form-03.service";

@Component({
  selector: "./request-refund-update-inner-wrapper",
  templateUrl: "./request-refund-update.component.html",
  styleUrls: ["./request-refund-update.component.scss"],
  encapsulation: ViewEncapsulation.Emulated,
})
export class RequestRefundUpdateComponent implements OnInit {

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private configModal: NgbModalConfig,
    public config: ConfigService,
    public ngxService: NgxUiLoaderService,
    private addressService: AddressService,
    private requestRefundService: RequestRefundService,
    private soiService: SoiService,
    private roadService: RoadService,
    private uploadService: UploadService,
    private businessTypeService: BusinessTypeService,
    private customerTypeService: CustomerTypeService,
    private frontretailService: FrontretailService,
    private approvalTaxForm03Service: ApprovalTaxForm03Service
  ) {
    configModal.backdrop = "static";
  }

  public currencyMask = createNumberMask({
    prefix: "",
    suffix: "",
    includeThousandsSeparator: true,
    thousandsSeparatorSymbol: ",",
    allowDecimal: true,
    decimalSymbol: ".",
    decimalLimit: 2,
    integerLimit: null,
    requireDecimal: false,
    allowNegative: false,
    allowLeadingZeroes: false,
  });

  public maskTaxNo = [
    /[0-9]/,
    "-",
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    "-",
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    "-",
    /\d/,
    /\d/,
    /\d/,
  ];

  public maskMobile = [
    /[0-9]/,
    /\d/,
    /\d/,
    "-",
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    /\d/,
  ];

  public maskHouseNo = [
    /[0-9]/,
    /\d/,
    /\d/,
    /\d/,
    "-",
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    "-",
    /\d/,
  ];

  /////////////// End Variable ////////////////
  id: any = "0";
  yearly: any = "0";
  monthly: any = "0";
  private unsubscribeAll = new Subject();
  //////////////// Value /////////////////////

  taxNoValidation: any = false;

  data = {
    taxFormRefundId: "",
    refRetailId: "",
    refStationId: "",
    yearly: "",
    monthly: "",
    monthName: "",
    amount: "",
    receiptNo: "",
    receiptDate: "",
    docNo: "",
    docDate: "",
    docNo1: "",
    docNo2: "",
    place: "",
    status: "1",
    statusName: "",
    acceptBy: "",
    acceptDate: "",
    refundBy: "",
    refundDate: "",
    rejectBy: "",
    rejectDate: "",
    refOffice: "",
    refOfficeName: "",
    billPaymentFileList: [], //ใบเสร็จรับเงิน หรือหลักฐานการชำระเงิน
    authorizeFileList: [],  //หนังสือมอบอำนาจกรณียื่นคำร้องแทน
    idCardFileList: [], //	บัตรประจำตัวประชาชน
    bookBankFileList: [], //หน้าสมุดบัญชีธนาคาร
    cerFileList: [
      // สำเนาหนังสือรับรองของกระทรวจพาณิชย์ (ที่ออกให้ไม่เกิน 6 เดือน)
      // {
      //     "fileName": "cerFileList.pdf",
      //     "fileType": "P",
      //     "pathFile": "14158953-f5fe-4a66-a680-14aa099da5a5.pdf"
      // }
    ],
    rejectRemark: "",
    docRefundDate: "",
    active: false,
    createdBy: "",
    createdDate: "",
    updatedBy: "",
    updatedDate: "",

    // เลิกกิจการ
    cancelDate: "",
    // เลิกกิจการ
    // ย้ายสถานที่ตั้งสถานการค้าปลีก
    changeDate: "", // ตั้งแต่วันที่
    addressOld: "", // ที่อยู่เดิม
    address: "", // ที่อยู่ใหม่
    houseNo: "",
    soi: "", // ซอย
    road: "", // ถนน
    tambonId: "", // ตำบลไอดี
    tambonName: "", // ตำบลชื่อ
    amphurId: "", // อำเภอชื่อ
    amphurName: "", // อำเภอชื่อ
    provinceId: "", // จังหวัดชื่อ
    provinceName: "", // จังหวัดชื่อ
    postcode: "", // รหัสไปรษณีย์
    mobileOld: "", // เบอร์โทรศัพท์เก่า
    mobile: "", // เบอร์โทรศัพท์
    stationCode: "", // สถานการค้ารหัส
    stationNameOld: "", // สถานการค้าปลีกเก่า
    stationName: "", // สถานการค้าชื่อ
    ownerNameOld: "", // สถานกาค้าปลีกเก่า
    ownerName: "", // สถานกาค้าปลีก
    taxNoOld: "", // รหัสผู้เสียภาษีอากรเก่า
    taxNo: "", // รหัสผู้เสียภาษีอากรใหม่
    customerTypeOld: "", // รหัสพนักงานเก่า
    customerType: "", // รหัสประเภทพนักงาน
    idCardOld: "", // ประจำตัวประชาชนเก่า
    idCard: "", // ประจำตัวประชาชน
    idCardAtOld: "", // ออก ณ ที่ เก่า
    idCardAt: "", // ออก ณ ที่
    corpNoOld: "", // เลขทะเบียนนิติบุคคลที่ เก่า
    corpNo: "", // เลขทะเบียนนิติบุคคลที่
    corpDateOld: "", // เมื่อวันที่ เก่า
    corpDate: "", // เมื่อวันที่ ใหม่
    other: "", // อื่นๆ
    taxRemain: "", // ภาษีค้างชำระตามบัญชีแนบ
    taxOilGas: "", // น้ำมัน/ก๊าซปิโตรเลียมที่ยังไม่ได้ชำระภาษีตามบัญชีแนบ
    fileNumE: "", // สำเนาหนังสือรับรองของกระทรวงพาณิชย์
    fileNumA: "",
    fileNumR: "",
    fileNumO: "",
    addressRetail: "",
    soiRetail: "",
    roadRetail: "",
    tambonIdRetail: "",
    tambonNameRetail: "",
    amphurIdRetail: "",
    amphurNameRetail: "",
    provinceIdRetail: "",
    provinceNameRetail: "",
    postcodeRetail: "",
    mobileRetail: "",
    cause: "",


    addTimes: "",
    sequence: "",
    extraMoney: "",
    extraRate: "",
    extraStartDate: "",
    extraEndDate: "",
    taxTotal: "",

    statusAcc: "",
    statusAccDesc: "",

    finesMonth: "",
    finesPerMonth: "",
    finesStartDate: "",
    finesEndDate: "",
    finesTotal: "",
    refRetailStationId: "",

    summaryList: [
      // {
      //     "taxForm03Id": "QbH2TVK3ahnjQK2StvFI5A",
      //     "sequence": 1,
      //     "oilTypeId": "RQ-eWw45LnFawRr7Dtfphg",
      //     "oilTypeName": "แก๊สโซฮอล์ 91",
      //     "qty": 1000.0,
      //     "rate": 1.2,
      //     "amount": 1200.0,
      //     "amountBaht": 1200,
      //     "amountSatang": 0
      // },
    ],

  };

  // + -
  panelOpenState = true;
  panelOpenState_1 = true;

  checkAll_stationList: any = false;
  _refRetailId: any = "";
  drpListStation: any = []; // ชื่อสถานการค้าปลีก
  drpBusinessStatusList: any = []; // ความประสงค์

  stationList: any = {
    taxForm01StationId: "",
    taxForm01RetailId: "",
    refStationId: "",
    stationCode: "",
    stationName: "",
    branchName: "", //สาขา
    houseNo: "",
    address: "",
    soi: "",
    road: "",
    tambonId: "",
    tambonName: "",
    amphurId: "",
    amphurName: "",
    provinceId: "10",
    provinceName: "กรุงเทพมหานคร",
    postcode: "",
    mobile: "",
    email: "",
    status: "",
    statusName: "",
    houseFileNum: "",
    mapFileNum: "",
    billPaymentFileList: [], //ใบเสร็จรับเงิน หรือหลักฐานการชำระเงิน
    authorizeFileList: [], //หนังสือมอบอำนาจกรณียื่นคำร้องแทน
    idCardFileList: [], //	บัตรประจำตัวประชาชน
    bookBankFileList: [], //หน้าสมุดบัญชีธนาคาร
    fullAddress: "",
  };

  data_getDataByMemberName = {
    active: true,
    address: "",

    agentFileNum: "",

    amphurId: "",
    amphurName: "",

    cerFileNum: "",

    corpDate: "",
    corpNo: "",
    createdBy: "",
    createdDate: "",
    customerType: "",
    email: "",
    firstRecord: true,
    fullAddress: "",

    houseFileNum: "",

    houseNo: "",
    idCard: "",
    idCardAt: "",
    mainStatus: "",
    mainStatusName: "",
    mobile: "",
    ownerName: "",
    postcode: "",

    provinceId: "",
    provinceName: "",

    retailId: "",
    road: "",
    soi: "",

    tambonId: "",
    tambonName: "",

    taxNo: "",
    updatedBy: "",
    updatedDate: "",
  };

  selected_number = 0;
  selected = new FormControl(this.selected_number);

  drpListProvince: []; // จังหวัด
  drpListDistrict: []; // อำเภอ
  drpListTambon: []; // ตำบล
  drpListProvince_: []; // จังหวัด
  drpListDistrict_: []; // อำเภอ
  drpListTambon_: []; // ตำบล
  displayMount: any;

  Month: any = new Date().getMonth() + 1;
  year: any = new Date().getFullYear() + 543;

  drpyear: any = []; // ปี
  drpmonth: any[] = [];

  @ViewChild("approval_tax_form_01modal")
  public approval_tax_form_01modal: TemplateRef<any>;
  approval_tax_form_01modal_reference: NgbModalRef;

  drpListSoi: any[] = []; // ซอย
  myControl_Soi = new FormControl();
  myControl_Soi_F = new FormControl();
  options_Soi: any[] = [];
  filteredOptions_Soi: Observable<string[]>;
  filteredOptions_Soi_F: Observable<string[]>;

  drpListRoad: any[] = []; // ซอย
  myControl_Road = new FormControl();
  myControl_Road_F = new FormControl();
  options_Road: any[] = [];
  filteredOptions_Road: Observable<string[]>;
  filteredOptions_Road_F: Observable<string[]>;

  @ViewChild("address_modal")
  public address_modal: TemplateRef<any>;
  address_modal_reference: NgbModalRef;
  Type: string = "data"; // data, modal

  drpListByPostCodeMain: any[] = []; // ผู้ประกอบการ
  typeData: any = false; // ผู้ประกอบการ
  drpListByPostCodeMain_: any[] = []; // สถานการค้าปลีก
  typeData_modal: any = false; // สถานการค้าปลีก

  // ใบเสร็จรับเงิน หรือหลักฐานการชำระเงิน
  delect_billPaymentFileList: any[] = [];

  uploader_billPaymentFileList: FileUploader = new FileUploader({
    autoUpload: true,
    url: REST_URL.MAIN_UPLOADFILE.uploadFile,
  });
  // ใบเสร็จรับเงิน หรือหลักฐานการชำระเงิน

  // หนังสือมอบอำนาจกรณียื่นคำร้องแทน
  delect_authorizeFileList: any[] = [];

  uploader_authorizeFileList: FileUploader = new FileUploader({
    autoUpload: true,
    url: REST_URL.MAIN_UPLOADFILE.uploadFile,
  });
  // หนังสือมอบอำนาจกรณียื่นคำร้องแทน

  // หนังสือมอบอำนาจและสำเนาบัตรประจำตัวประชาชนของผู้รับมอบอำนาจ
  delect_agentFileList: any[] = [];

  uploader_agentFileList: FileUploader = new FileUploader({
    autoUpload: true,
    url: REST_URL.MAIN_UPLOADFILE.uploadFile,
  });

  // หนังสือมอบอำนาจและสำเนาบัตรประจำตัวประชาชนของผู้รับมอบอำนาจ

  // บัตรประชาชน
  delect_idCardFileList: any[] = [];

  uploader_idCardFileList: FileUploader = new FileUploader({
    autoUpload: true,
    url: REST_URL.MAIN_UPLOADFILE.uploadFile,
  });
  // บัตรประชาชน

  // บัญชีธนาคาร
  delect_bookBankFileList: any[] = [];

  uploader_bookBankFileList: FileUploader = new FileUploader({
    autoUpload: true,
    url: REST_URL.MAIN_UPLOADFILE.uploadFile,
  });
  // บัญชีธนาคาร

  drpbusinessType: any = []; // ประเภทธุรกิจ
  drpcustomerType: any = []; // ประเภทผู้เสียภาษี

  currentUser: any;

  getDataByMemberNameStatus: any = false;


  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem("ba_currentUser"));
    this.yearly = this.route.snapshot.params["yearly"] || "0";
    this.monthly = this.route.snapshot.params["monthly"] || "0";
    this.id = this.route.snapshot.params["id"] || "0";
    this.getDrpListProvince_();
    this.getSoiDrpList(); // ซอย
    this.getRoadDrpList(); // ถนน

    this.drpyear.push(this.year - 1);
    this.drpyear.push(this.year);

    if (this.yearly != "0") {
      this.data.yearly = this.yearly;
    }

    if (this.monthly != "0") {
      this.data.monthly = this.monthly;
    }

    if (this.Month == 1) {
      let d = {
        id: "1",
        name: "มกราคม",
      };
      this.drpmonth.push(d);

      this.displayMount = "มกราคม";
    } else if (this.Month == 2) {
      let d = {
        id: "1",
        name: "มกราคม",
      };
      this.drpmonth.push(d);
      let d1 = {
        id: "2",
        name: "กุมภาพันธ์",
      };
      this.drpmonth.push(d1);

      this.displayMount = "กุมภาพันธ์";
    } else if (this.Month == 3) {
      let d = {
        id: "1",
        name: "มกราคม",
      };
      this.drpmonth.push(d);
      let d1 = {
        id: "2",
        name: "กุมภาพันธ์",
      };
      this.drpmonth.push(d1);
      let d2 = {
        id: "3",
        name: "มีนาคม",
      };
      this.drpmonth.push(d2);

      this.displayMount = "มีนาคม";
    } else if (this.Month == 4) {
      let d = {
        id: "1",
        name: "มกราคม",
      };
      this.drpmonth.push(d);
      let d1 = {
        id: "2",
        name: "กุมภาพันธ์",
      };
      this.drpmonth.push(d1);
      let d2 = {
        id: "3",
        name: "มีนาคม",
      };
      this.drpmonth.push(d2);
      let d3 = {
        id: "4",
        name: "เมษายน",
      };
      this.drpmonth.push(d3);

      this.displayMount = "เมษายน";
    } else if (this.Month == 5) {
      let d = {
        id: "1",
        name: "มกราคม",
      };
      this.drpmonth.push(d);
      let d1 = {
        id: "2",
        name: "กุมภาพันธ์",
      };
      this.drpmonth.push(d1);
      let d2 = {
        id: "3",
        name: "มีนาคม",
      };
      this.drpmonth.push(d2);
      let d3 = {
        id: "4",
        name: "เมษายน",
      };
      this.drpmonth.push(d3);
      let d4 = {
        id: "5",
        name: "พฤษภาคม",
      };
      this.drpmonth.push(d4);

      this.displayMount = "พฤษภาคม";
    } else if (this.Month == 6) {
      let d = {
        id: "1",
        name: "มกราคม",
      };
      this.drpmonth.push(d);
      let d1 = {
        id: "2",
        name: "กุมภาพันธ์",
      };
      this.drpmonth.push(d1);
      let d2 = {
        id: "3",
        name: "มีนาคม",
      };
      this.drpmonth.push(d2);
      let d3 = {
        id: "4",
        name: "เมษายน",
      };
      this.drpmonth.push(d3);
      let d4 = {
        id: "5",
        name: "พฤษภาคม",
      };
      this.drpmonth.push(d4);
      let d5 = {
        id: "6",
        name: "มิถุนายน",
      };
      this.drpmonth.push(d5);

      this.displayMount = "มิถุนายน";
    } else if (this.Month == 7) {
      let d = {
        id: "1",
        name: "มกราคม",
      };
      this.drpmonth.push(d);
      let d1 = {
        id: "2",
        name: "กุมภาพันธ์",
      };
      this.drpmonth.push(d1);
      let d2 = {
        id: "3",
        name: "มีนาคม",
      };
      this.drpmonth.push(d2);
      let d3 = {
        id: "4",
        name: "เมษายน",
      };
      this.drpmonth.push(d3);
      let d4 = {
        id: "5",
        name: "พฤษภาคม",
      };
      this.drpmonth.push(d4);
      let d5 = {
        id: "6",
        name: "มิถุนายน",
      };
      this.drpmonth.push(d5);
      let d6 = {
        id: "7",
        name: "กรกฎาคม",
      };
      this.drpmonth.push(d6);

      this.displayMount = "กรกฎาคม";
    } else if (this.Month == 8) {
      let d = {
        id: "1",
        name: "มกราคม",
      };
      this.drpmonth.push(d);
      let d1 = {
        id: "2",
        name: "กุมภาพันธ์",
      };
      this.drpmonth.push(d1);
      let d2 = {
        id: "3",
        name: "มีนาคม",
      };
      this.drpmonth.push(d2);
      let d3 = {
        id: "4",
        name: "เมษายน",
      };
      this.drpmonth.push(d3);
      let d4 = {
        id: "5",
        name: "พฤษภาคม",
      };
      this.drpmonth.push(d4);
      let d5 = {
        id: "6",
        name: "มิถุนายน",
      };
      this.drpmonth.push(d5);
      let d6 = {
        id: "7",
        name: "กรกฎาคม",
      };
      this.drpmonth.push(d6);
      let d7 = {
        id: "8",
        name: "สิงหาคม",
      };
      this.drpmonth.push(d7);

      this.displayMount = "สิงหาคม";
    } else if (this.Month == 9) {
      let d = {
        id: "1",
        name: "มกราคม",
      };
      this.drpmonth.push(d);
      let d1 = {
        id: "2",
        name: "กุมภาพันธ์",
      };
      this.drpmonth.push(d1);
      let d2 = {
        id: "3",
        name: "มีนาคม",
      };
      this.drpmonth.push(d2);
      let d3 = {
        id: "4",
        name: "เมษายน",
      };
      this.drpmonth.push(d3);
      let d4 = {
        id: "5",
        name: "พฤษภาคม",
      };
      this.drpmonth.push(d4);
      let d5 = {
        id: "6",
        name: "มิถุนายน",
      };
      this.drpmonth.push(d5);
      let d6 = {
        id: "7",
        name: "กรกฎาคม",
      };
      this.drpmonth.push(d6);
      let d7 = {
        id: "8",
        name: "สิงหาคม",
      };
      this.drpmonth.push(d7);
      let d8 = {
        id: "9",
        name: "กันยายน",
      };
      this.drpmonth.push(d8);

      this.displayMount = "กันยายน";
    } else if (this.Month == 10) {
      let d = {
        id: "1",
        name: "มกราคม",
      };
      this.drpmonth.push(d);
      let d1 = {
        id: "2",
        name: "กุมภาพันธ์",
      };
      this.drpmonth.push(d1);
      let d2 = {
        id: "3",
        name: "มีนาคม",
      };
      this.drpmonth.push(d2);
      let d3 = {
        id: "4",
        name: "เมษายน",
      };
      this.drpmonth.push(d3);
      let d4 = {
        id: "5",
        name: "พฤษภาคม",
      };
      this.drpmonth.push(d4);
      let d5 = {
        id: "6",
        name: "มิถุนายน",
      };
      this.drpmonth.push(d5);
      let d6 = {
        id: "7",
        name: "กรกฎาคม",
      };
      this.drpmonth.push(d6);
      let d7 = {
        id: "8",
        name: "สิงหาคม",
      };
      this.drpmonth.push(d7);
      let d8 = {
        id: "9",
        name: "กันยายน",
      };
      this.drpmonth.push(d8);
      let d9 = {
        id: "10",
        name: "ตุลาคม",
      };
      this.drpmonth.push(d9);

      this.displayMount = "ตุลาคม";
    } else if (this.Month == 11) {
      let d = {
        id: "1",
        name: "มกราคม",
      };
      this.drpmonth.push(d);
      let d1 = {
        id: "2",
        name: "กุมภาพันธ์",
      };
      this.drpmonth.push(d1);
      let d2 = {
        id: "3",
        name: "มีนาคม",
      };
      this.drpmonth.push(d2);
      let d3 = {
        id: "4",
        name: "เมษายน",
      };
      this.drpmonth.push(d3);
      let d4 = {
        id: "5",
        name: "พฤษภาคม",
      };
      this.drpmonth.push(d4);
      let d5 = {
        id: "6",
        name: "มิถุนายน",
      };
      this.drpmonth.push(d5);
      let d6 = {
        id: "7",
        name: "กรกฎาคม",
      };
      this.drpmonth.push(d6);
      let d7 = {
        id: "8",
        name: "สิงหาคม",
      };
      this.drpmonth.push(d7);
      let d8 = {
        id: "9",
        name: "กันยายน",
      };
      this.drpmonth.push(d8);
      let d9 = {
        id: "10",
        name: "ตุลาคม",
      };
      this.drpmonth.push(d9);
      let d10 = {
        id: "11",
        name: "พฤศจิกายน ",
      };
      this.drpmonth.push(d10);

      this.displayMount = "พฤศจิกายน";
    } else if (this.Month == 12) {
      let d = {
        id: "1",
        name: "มกราคม",
      };
      this.drpmonth.push(d);
      let d1 = {
        id: "2",
        name: "กุมภาพันธ์",
      };
      this.drpmonth.push(d1);
      let d2 = {
        id: "3",
        name: "มีนาคม",
      };
      this.drpmonth.push(d2);
      let d3 = {
        id: "4",
        name: "เมษายน",
      };
      this.drpmonth.push(d3);
      let d4 = {
        id: "5",
        name: "พฤษภาคม",
      };
      this.drpmonth.push(d4);
      let d5 = {
        id: "6",
        name: "มิถุนายน",
      };
      this.drpmonth.push(d5);
      let d6 = {
        id: "7",
        name: "กรกฎาคม",
      };
      this.drpmonth.push(d6);
      let d7 = {
        id: "8",
        name: "สิงหาคม",
      };
      this.drpmonth.push(d7);
      let d8 = {
        id: "9",
        name: "กันยายน",
      };
      this.drpmonth.push(d8);
      let d9 = {
        id: "10",
        name: "ตุลาคม",
      };
      this.drpmonth.push(d9);
      let d10 = {
        id: "11",
        name: "พฤศจิกายน ",
      };
      this.drpmonth.push(d10);
      let d11 = {
        id: "12",
        name: "ธันวาคม ",
      };
      this.drpmonth.push(d11);

      this.displayMount = "ธันวาคม";
    }
    // ปี
    this.data.yearly = this.year;

    // เดือน
    this.data.monthly = this.Month.toString();

    this.displayMount = this.displayMount + " " + this.year.toString();


    this.filteredOptions_Soi = this.myControl_Soi.valueChanges.pipe(
      startWith(""),
      map((value) => this._filter_Soi(value))
    );

    this.filteredOptions_Road = this.myControl_Road.valueChanges.pipe(
      startWith(""),
      map((value) => this._filter_Road(value))
    );

    this.filteredOptions_Soi_F = this.myControl_Soi_F.valueChanges.pipe(
      startWith(""),
      map((value) => this._filter_Soi(value))
    );

    this.filteredOptions_Road_F = this.myControl_Road_F.valueChanges.pipe(
      startWith(""),
      map((value) => this._filter_Road(value))
    );


    let d = {
      username: "",
      taxForm01RetailId: this.id,
    };

    //this.getDrpListStation(d); // ชื่อสถานการค้าปลีก

    this.getDataByMemberName();
    if (this.id != "0") {
      this.getData(this.id);
    }
  }

  public getDataByMemberName() {
    const payload = {
      memberName: this.currentUser.memberName,
    };

    this.ngxService.start();

    this.frontretailService.getDataByMemberName(payload).subscribe((res) => {
      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

      //this.getDataByMemberNameStatus = res.status;

      if (res.status == "false") {
        //this.data.firstRecord = false;
        this.getDataByMemberNameStatus = false;

        this.data_getDataByMemberName = {
          active: true,
          address: "",

          agentFileNum: "",

          amphurId: "",
          amphurName: "",

          cerFileNum: "",

          corpDate: "",
          corpNo: "",
          createdBy: "",
          createdDate: "",
          customerType: "",
          email: "",
          firstRecord: true,
          fullAddress: "",

          houseFileNum: "",

          houseNo: "",
          idCard: "",
          idCardAt: "",
          mainStatus: "",
          mainStatusName: "",
          mobile: "",
          ownerName: "",
          postcode: "",

          provinceId: "",
          provinceName: "",

          retailId: "",
          road: "",
          soi: "",

          tambonId: "",
          tambonName: "",

          taxNo: "",
          updatedBy: "",
          updatedDate: "",
        };
      } else {
        res.result.firstRecord = true;
        this.getDataByMemberNameStatus = true;

        //this.setForm(res.result);

        this._refRetailId = res.result.retailId;
        this.data.refRetailId = this._refRetailId;

        res.result.amphurName =
          res.result.amphurName == undefined ? "" : res.result.amphurName;
        res.result.tambonName =
          res.result.tambonName == undefined ? "" : res.result.tambonName;
        res.result.provinceName =
          res.result.provinceName == undefined ? "" : res.result.provinceName;

        this.data_getDataByMemberName = res.result;

        this.data.taxNoOld = res.result.taxNo;
        this.data.ownerNameOld = res.result.ownerName;
        this.data.customerTypeOld = res.result.customerType;
        this.data.idCardOld = res.result.idCard;
        this.data.idCardAtOld = res.result.idCardAt;
        this.data.corpNoOld = res.result.corpNo;
        this.data.corpDateOld = res.result.corpDate;

        this.getListStationByTaxNo(this.data_getDataByMemberName.taxNo);

      }
    });
  }

  public getDataByTaxNo(item) {
    let re = /\-/gi;
    let result = item.replace(re, "");

    const payload = {
      taxNo: result,
    };

    this.ngxService.start();

    this.frontretailService.getDataByTaxNo(payload).subscribe((res) => {
      if (res.status == "true") {
        this.taxNoValidation = true;

        Swal.fire({
          icon: "warning",
          title: res.resultMessage,
          // text: "Modal with a custom image.",
          confirmButtonText: "ตกลง",
        });
      } else {
        this.taxNoValidation = false;

        // this.data.taxNo = "";
      }

      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
    });
  }

  drpListStationByTaxNo: any = [];

  public getListStationByTaxNo(item) {
    const payload = {
      taxNo: item,
    };

    this.ngxService.start();

    this.frontretailService.getListStationByTaxNo2(payload).subscribe((res) => {
      console.log("getListStationByTaxNo :  ",res.result);
      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
      if (res.result) {
        this.drpListStationByTaxNo = res.result;
      } else {
        this.drpListStationByTaxNo = [];
      }

      //this.getDataByMemberNameStatus = res.status;
    });
  }

  public getDataStation(item) {

    const payload = {
      id: item,
    };

    this.ngxService.start();

    this.frontretailService.getDataStation(payload).subscribe((res) => {
      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

      if (res.result) {
        this.data.addressOld = res.result.fullAddress;
        this.data.mobileOld = res.result.mobile;
        this.data.stationNameOld = res.result.stationName;
        this.data.stationCode = res.result.stationCode; // รหัสสถานประกอบการ
        this.data.stationNameOld = res.result.stationName; // ชื่อสถานประกอบการ เดิม

        // this.data.addressStation = res.result.address;
        // this.data.soiStation = res.result.soi;
        // this.data.roadStation = res.result.road;
        // this.data.tambonIdStation = res.result.tambonId;
        // this.data.tambonNameStation = res.result.tambonName;
        // this.data.amphurIdStation = res.result.amphurId;
        // this.data.amphurNameStation = res.result.amphurName;
        // this.data.provinceIdStation = res.result.provinceId;
        // this.data.provinceNameStation = res.result.provinceName;
        // this.data.postcodeStation = res.result.postcode;
        // this.data.mobileStation = res.result.mobile;


      } else {
        this.data.addressOld = "";
        this.data.mobileOld = "";
        this.data.stationNameOld = "";
        this.data.stationCode = ""; // รหัสสถานประกอบการ
        this.data.stationNameOld = ""; // ชื่อสถานประกอบการ เดิม

        // this.data.addressStation = "";
        // this.data.soiStation = "";
        // this.data.roadStation = "";
        // this.data.tambonIdStation = "";
        // this.data.tambonNameStation = "";
        // this.data.amphurIdStation = "";
        // this.data.amphurNameStation = "";
        // this.data.provinceIdStation = "";
        // this.data.provinceNameStation = "";
        // this.data.postcodeStation = "";
        // this.data.mobileStation = "";
      }
    });
  }

  private _filter_Soi(value: string): string[] {
    const filterValue = value.toLowerCase();

    // return this.options_Soi.filter(
    //   (option) => option.toLowerCase().indexOf(filterValue) === 0
    // );
    let d = [];
    d = this.options_Soi.filter((val) => {
      if (val.toLowerCase().includes(filterValue)) {
        return val;
      }
    });

    // d.forEach()
    // let a = this.options_Soi.filter(
    //   (option) => option.toLowerCase().indexOf(d) === 0
    // );

    return d;
  }

  // ซอย
  public getSoiDrpList() {
    this.soiService.getSoiDrpList().subscribe((res) => {

      if (res.result) {
        this.drpListSoi = res.result;

        res.result.forEach((r) => {
          //this.optionsProvince.push(r.provinceThai);
          this.options_Soi.push(r.soiName);
        });
      } else {
        this.drpListSoi = [];
        this.options_Soi = [];
      }
    });
  }

  private _filter_Road(value: string): string[] {
    const filterValue = value.toLowerCase();

    let d = [];
    d = this.options_Road.filter((val) => {
      if (val.toLowerCase().includes(filterValue)) {
        return val;
      }
    });

    return d;
  }

  // ถนน
  public getRoadDrpList() {
    this.roadService.getRoadDrpList().subscribe((res) => {

      if (res.result) {
        this.drpListRoad = res.result;

        res.result.forEach((r) => {
          //this.optionsProvince.push(r.provinceThai);
          this.options_Road.push(r.roadName);
        });

      } else {
        this.drpListRoad = [];
        this.options_Road = [];
      }
    });
  }

  ngOnDestroy() {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }

  public getData(item) {
    const payload = {
      taxFormRefundId: item,
    };

    this.ngxService.start();
    this.requestRefundService.getData(payload).subscribe((res) => {

      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

      if (res.result) {
        this.setForm(res.result);
      }
    });
  }

  setForm(res: any) {
    this.setChangeyear(res.yearly);
    this.setChangemonth(res.monthly);
    this.drpBusinessStatusList.forEach((val) => {
      res.businessStatusList.forEach((element) => {
        if (element.id == val.businessStatusId) {
          val.selected = true;
        }
      });
    });

    // ใบเสร็จรับเงิน หรือหลักฐานการชำระเงิน
    let billPaymentFileList = [];
    if (res.billPaymentFileList) {
      res.billPaymentFileList.forEach((el) => {
        el.ShowFile = REST_URL.MAIN_UPLOADFILE.getFile + "/" + el.pathFile + "/" + "taxform01";
        billPaymentFileList.push(el);
      });
    }
    // ใบเสร็จรับเงิน หรือหลักฐานการชำระเงิน

    // หนังสือมอบอำนาจ

    let authorizeFileList = [];
    if (res.authorizeFileList != undefined) {
      res.authorizeFileList.forEach((el) => {
        el.ShowFile =
          REST_URL.MAIN_UPLOADFILE.getFile +
          "/" +
          el.pathFile +
          "/" +
          "taxform01";
        authorizeFileList.push(el);
      });
    }

    if (res.stationList != undefined) {
      res.stationList.forEach((el) => {
        el.selected = false;
        el.commit = true;

        if (el.billPaymentFileList != undefined) {
          el.billPaymentFileList.forEach((e) => {
            e.ShowFile =
              REST_URL.MAIN_UPLOADFILE.getFile +
              "/" +
              e.pathFile +
              "/" +
              "taxform01";
          });
        }

        if (el.mapFileList != undefined) {
          el.mapFileList.forEach((e) => {
            e.ShowFile =
              REST_URL.MAIN_UPLOADFILE.getFile +
              "/" +
              e.pathFile +
              "/" +
              "taxform01";
            //mapFileList.push(el);
          });
        }
      });
    }
    // หนังสือมอบอำนาจ

    //	บัตรประจำตัวประชาชน
    let bookBankFileList = [];
    if (res.bookBankFileList != undefined) {
      res.bookBankFileList.forEach((element) => {
        let m = {
          id: element.contentId,
          pathFile: element.pathFile,
          ShowFile:
            REST_URL.MAIN_UPLOADFILE.getFile +
            "/" +
            element.pathFile +
            "/" +
            "taxform02",
          pathFileName: element.fileName,
          Status: 200,
          type: element.fileType,
          fileName: element.fileName,
        };
        bookBankFileList.push(m);
      });
    }
    // บัตรประจำตัวประชาชน

    //		หน้าสมุดบัญชีธนาคาร
    let idCardFileList = [];
    if (res.idCardFileList != undefined) {
      res.idCardFileList.forEach((element) => {
        let m = {
          id: element.contentId,
          pathFile: element.pathFile,
          ShowFile:
            REST_URL.MAIN_UPLOADFILE.getFile +
            "/" +
            element.pathFile +
            "/" +
            "taxform02",
          pathFileName: element.fileName,
          Status: 200,
          type: element.fileType,
          fileName: element.fileName,
        };
        idCardFileList.push(m);
      });
    }
    // 	หน้าสมุดบัญชีธนาคาร


    let cancelDate = null;
    if (res.cancelDate != "") {
      cancelDate = Utils.stringToDate(res.cancelDate);
    } else {
      cancelDate = "";
    }

    let changeDate = null;
    if (res.changeDate != "") {
      changeDate = Utils.stringToDate(res.changeDate);
    } else {
      changeDate = "";
    }

    let docDate = null;
    if (res.docDate != "") {
      docDate = Utils.stringToDate(res.docDate);
    } else {
      docDate = "";
    }

    // let receiptDate = null;
    // if (res.receiptDate != "") {
    //   debugger
    //   receiptDate = Utils.stringToDate(res.receiptDate);
    // } else {
    //   receiptDate = "";
    // }

    this.data = {
      taxFormRefundId: res.taxFormRefundId,
      refRetailId: res.refRetailId,
      refStationId: res.refStationId,

      yearly: res.yearly,
      monthly: res.monthly.toString(),
      monthName: res.monthName,
      amount: res.amount,
      receiptNo: res.receiptNo,
      receiptDate: res.receiptDate ? Utils.stringToDate(res.receiptDate) : "",
      docNo: res.docNo,
      docDate: docDate,
      docNo1: res.docNo1,
      docNo2: res.docNo2,
      place: res.place,
      status: res.status,
      statusName: res.statusName,
      acceptBy: res.acceptBy,
      acceptDate: res.acceptDate,
      refundBy: res.refundBy,
      refundDate: res.refundDate,
      rejectBy: res.rejectBy,
      rejectDate: res.rejectDate,
      refOffice: res.refOffice,
      refOfficeName: res.refOfficeName,

      billPaymentFileList: billPaymentFileList, //ใบเสร็จรับเงิน หรือหลักฐานการชำระเงิน
      authorizeFileList: authorizeFileList, // หนังสือมอบอำนาจ
      idCardFileList: idCardFileList,  //บัตรประจำตัวประชาชน
      bookBankFileList: bookBankFileList, //	หน้าสมุดบัญชีธนาคาร

      cerFileList: [
        // สำเนาหนังสือรับรองของกระทรวจพาณิชย์ (ที่ออกให้ไม่เกิน 6 เดือน)
        // {
        //     "fileName": "cerFileList.pdf",
        //     "fileType": "P",
        //     "pathFile": "14158953-f5fe-4a66-a680-14aa099da5a5.pdf"
        // }
      ],

      rejectRemark: res.rejectRemark,
      docRefundDate: res.docRefundDate,
      active: false,
      createdBy: res.createdBy,
      createdDate: res.createdDate,
      updatedBy: res.updatedBy,
      updatedDate: res.updatedDate,


      // เลิกกิจการ
      cancelDate: res.cancelDate,
      // เลิกกิจการ
      // ย้ายสถานที่ตั้งสถานการค้าปลีก
      changeDate: res.changeDate, // ตั้งแต่วันที่
      addressOld: res.addressOld, // ที่อยู่เดิม
      address: res.address, // ที่อยู่ใหม่
      houseNo: res.houseNo,
      soi: res.soi, // ซอย
      road: res.road, // ถนน
      tambonId: res.tambonId, // ตำบลไอดี
      tambonName: res.tambonName, // ตำบลชื่อ
      amphurId: res.amphurId, // อำเภอชื่อ
      amphurName: res.amphurName, // อำเภอชื่อ
      provinceId: res.provinceId, // จังหวัดชื่อ
      provinceName: res.provinceName, // จังหวัดชื่อ
      postcode: res.postcode, // รหัสไปรษณีย์
      mobileOld: res.mobileOld, // เบอร์โทรศัพท์เก่า
      mobile: res.mobile, // เบอร์โทรศัพท์
      stationCode: res.stationCode, // สถานการค้ารหัส
      stationNameOld: res.stationNameOld, // สถานการค้าปลีกเก่า
      stationName: res.stationName, // สถานการค้าชื่อ
      ownerNameOld: res.ownerNameOld, // สถานกาค้าปลีกเก่า
      ownerName: res.ownerName, // สถานกาค้าปลีก
      taxNoOld: res.taxNoOld, // รหัสผู้เสียภาษีอากรเก่า
      taxNo: res.taxNo, // รหัสผู้เสียภาษีอากรใหม่
      customerTypeOld: res.customerTypeOld, // รหัสพนักงานเก่า
      customerType: res.customerType, // รหัสประเภทพนักงาน
      idCardOld: res.idCardOld, // ประจำตัวประชาชนเก่า
      idCard: res.idCard, // ประจำตัวประชาชน
      idCardAtOld: res.idCardAtOld, // ออก ณ ที่ เก่า
      idCardAt: res.idCardAt, // ออก ณ ที่
      corpNoOld: res.corpNoOld, // เลขทะเบียนนิติบุคคลที่ เก่า
      corpNo: res.corpNo, // เลขทะเบียนนิติบุคคลที่
      corpDateOld: res.corpDateOld, // เมื่อวันที่ เก่า
      corpDate: res.corpDate, // เมื่อวันที่ ใหม่
      other: res.other, // อื่นๆ
      taxRemain: res.taxRemain, // ภาษีค้างชำระตามบัญชีแนบ
      taxOilGas: res.taxOilGas, // น้ำมัน/ก๊าซปิโตรเลียมที่ยังไม่ได้ชำระภาษีตามบัญชีแนบ
      fileNumE: res.fileNumE, // สำเนาหนังสือรับรองของกระทรวงพาณิชย์
      fileNumA: res.fileNumA,
      fileNumR: res.fileNumR,
      fileNumO: res.fileNumO,
      addressRetail: res.addressRetail,
      soiRetail: res.soiRetail,
      roadRetail: res.roadRetail,
      tambonIdRetail: res.tambonIdRetail,
      tambonNameRetail: res.tambonNameRetail,
      amphurIdRetail: res.amphurIdRetail,
      amphurNameRetail: res.amphurNameRetail,
      provinceIdRetail: res.provinceIdRetail,
      provinceNameRetail: res.provinceNameRetail,
      postcodeRetail: res.postcodeRetail,
      mobileRetail: res.mobileRetail,
      cause: res.cause,

      addTimes: "",
      sequence: "",
      extraMoney: "",
      extraRate: "",
      extraStartDate: "",
      extraEndDate: "",
      taxTotal: "",

      statusAcc: "",
      statusAccDesc: "",

      finesMonth: "",
      finesPerMonth: "",
      finesStartDate: "",
      finesEndDate: "",
      finesTotal: "",
      refRetailStationId: "",
      summaryList: [
        // {
        //     "taxForm03Id": "QbH2TVK3ahnjQK2StvFI5A",
        //     "sequence": 1,
        //     "oilTypeId": "RQ-eWw45LnFawRr7Dtfphg",
        //     "oilTypeName": "แก๊สโซฮอล์ 91",
        //     "qty": 1000.0,
        //     "rate": 1.2,
        //     "amount": 1200.0,
        //     "amountBaht": 1200,
        //     "amountSatang": 0
        // },
      ],


    };
    console.log("this.data", this.data);
    this.getDataStation(
      this.data.refStationId == undefined ? "" : this.data.refStationId
    );
  }

  public setChangemonth(item) {
    let m_name = Utils.ConvertMonthTH(item);

    if (this.data.yearly != "") {
      let m = this.displayMount.split("/");

      this.displayMount = m_name + "/" + m[1];
    } else {
      this.displayMount = m_name;
    }
  }

  public setChangeyear(item) {
    this.drpmonth = [];

    if (item == this.year) {
      if (this.Month == 1) {
        let d = {
          id: "1",
          name: "มกราคม",
        };
        this.drpmonth.push(d);

        this.displayMount = "มกราคม";
      } else if (this.Month == 2) {
        let d = {
          id: "1",
          name: "มกราคม",
        };
        this.drpmonth.push(d);
        let d1 = {
          id: "2",
          name: "กุมภาพันธ์",
        };
        this.drpmonth.push(d1);
      } else if (this.Month == 3) {
        let d = {
          id: "1",
          name: "มกราคม",
        };
        this.drpmonth.push(d);
        let d1 = {
          id: "2",
          name: "กุมภาพันธ์",
        };
        this.drpmonth.push(d1);
        let d2 = {
          id: "3",
          name: "มีนาคม",
        };
        this.drpmonth.push(d2);
      } else if (this.Month == 4) {
        let d = {
          id: "1",
          name: "มกราคม",
        };
        this.drpmonth.push(d);
        let d1 = {
          id: "2",
          name: "กุมภาพันธ์",
        };
        this.drpmonth.push(d1);
        let d2 = {
          id: "3",
          name: "มีนาคม",
        };
        this.drpmonth.push(d2);
        let d3 = {
          id: "4",
          name: "เมษายน",
        };
        this.drpmonth.push(d3);
      } else if (this.Month == 5) {
        let d = {
          id: "1",
          name: "มกราคม",
        };
        this.drpmonth.push(d);
        let d1 = {
          id: "2",
          name: "กุมภาพันธ์",
        };
        this.drpmonth.push(d1);
        let d2 = {
          id: "3",
          name: "มีนาคม",
        };
        this.drpmonth.push(d2);
        let d3 = {
          id: "4",
          name: "เมษายน",
        };
        this.drpmonth.push(d3);
        let d4 = {
          id: "5",
          name: "พฤษภาคม",
        };
        this.drpmonth.push(d4);
      } else if (this.Month == 6) {
        let d = {
          id: "1",
          name: "มกราคม",
        };
        this.drpmonth.push(d);
        let d1 = {
          id: "2",
          name: "กุมภาพันธ์",
        };
        this.drpmonth.push(d1);
        let d2 = {
          id: "3",
          name: "มีนาคม",
        };
        this.drpmonth.push(d2);
        let d3 = {
          id: "4",
          name: "เมษายน",
        };
        this.drpmonth.push(d3);
        let d4 = {
          id: "5",
          name: "พฤษภาคม",
        };
        this.drpmonth.push(d4);
        let d5 = {
          id: "6",
          name: "มิถุนายน",
        };
        this.drpmonth.push(d5);
      } else if (this.Month == 7) {
        let d = {
          id: "1",
          name: "มกราคม",
        };
        this.drpmonth.push(d);
        let d1 = {
          id: "2",
          name: "กุมภาพันธ์",
        };
        this.drpmonth.push(d1);
        let d2 = {
          id: "3",
          name: "มีนาคม",
        };
        this.drpmonth.push(d2);
        let d3 = {
          id: "4",
          name: "เมษายน",
        };
        this.drpmonth.push(d3);
        let d4 = {
          id: "5",
          name: "พฤษภาคม",
        };
        this.drpmonth.push(d4);
        let d5 = {
          id: "6",
          name: "มิถุนายน",
        };
        this.drpmonth.push(d5);
        let d6 = {
          id: "7",
          name: "กรกฎาคม",
        };
        this.drpmonth.push(d6);
      } else if (this.Month == 8) {
        let d = {
          id: "1",
          name: "มกราคม",
        };
        this.drpmonth.push(d);
        let d1 = {
          id: "2",
          name: "กุมภาพันธ์",
        };
        this.drpmonth.push(d1);
        let d2 = {
          id: "3",
          name: "มีนาคม",
        };
        this.drpmonth.push(d2);
        let d3 = {
          id: "4",
          name: "เมษายน",
        };
        this.drpmonth.push(d3);
        let d4 = {
          id: "5",
          name: "พฤษภาคม",
        };
        this.drpmonth.push(d4);
        let d5 = {
          id: "6",
          name: "มิถุนายน",
        };
        this.drpmonth.push(d5);
        let d6 = {
          id: "7",
          name: "กรกฎาคม",
        };
        this.drpmonth.push(d6);
        let d7 = {
          id: "8",
          name: "สิงหาคม",
        };
        this.drpmonth.push(d7);
      } else if (this.Month == 9) {
        let d = {
          id: "1",
          name: "มกราคม",
        };
        this.drpmonth.push(d);
        let d1 = {
          id: "2",
          name: "กุมภาพันธ์",
        };
        this.drpmonth.push(d1);
        let d2 = {
          id: "3",
          name: "มีนาคม",
        };
        this.drpmonth.push(d2);
        let d3 = {
          id: "4",
          name: "เมษายน",
        };
        this.drpmonth.push(d3);
        let d4 = {
          id: "5",
          name: "พฤษภาคม",
        };
        this.drpmonth.push(d4);
        let d5 = {
          id: "6",
          name: "มิถุนายน",
        };
        this.drpmonth.push(d5);
        let d6 = {
          id: "7",
          name: "กรกฎาคม",
        };
        this.drpmonth.push(d6);
        let d7 = {
          id: "8",
          name: "สิงหาคม",
        };
        this.drpmonth.push(d7);
        let d8 = {
          id: "9",
          name: "กันยายน",
        };
        this.drpmonth.push(d8);
      } else if (this.Month == 10) {
        let d = {
          id: "1",
          name: "มกราคม",
        };
        this.drpmonth.push(d);
        let d1 = {
          id: "2",
          name: "กุมภาพันธ์",
        };
        this.drpmonth.push(d1);
        let d2 = {
          id: "3",
          name: "มีนาคม",
        };
        this.drpmonth.push(d2);
        let d3 = {
          id: "4",
          name: "เมษายน",
        };
        this.drpmonth.push(d3);
        let d4 = {
          id: "5",
          name: "พฤษภาคม",
        };
        this.drpmonth.push(d4);
        let d5 = {
          id: "6",
          name: "มิถุนายน",
        };
        this.drpmonth.push(d5);
        let d6 = {
          id: "7",
          name: "กรกฎาคม",
        };
        this.drpmonth.push(d6);
        let d7 = {
          id: "8",
          name: "สิงหาคม",
        };
        this.drpmonth.push(d7);
        let d8 = {
          id: "9",
          name: "กันยายน",
        };
        this.drpmonth.push(d8);
        let d9 = {
          id: "10",
          name: "ตุลาคม",
        };
        this.drpmonth.push(d9);
      } else if (this.Month == 11) {
        let d = {
          id: "1",
          name: "มกราคม",
        };
        this.drpmonth.push(d);
        let d1 = {
          id: "2",
          name: "กุมภาพันธ์",
        };
        this.drpmonth.push(d1);
        let d2 = {
          id: "3",
          name: "มีนาคม",
        };
        this.drpmonth.push(d2);
        let d3 = {
          id: "4",
          name: "เมษายน",
        };
        this.drpmonth.push(d3);
        let d4 = {
          id: "5",
          name: "พฤษภาคม",
        };
        this.drpmonth.push(d4);
        let d5 = {
          id: "6",
          name: "มิถุนายน",
        };
        this.drpmonth.push(d5);
        let d6 = {
          id: "7",
          name: "กรกฎาคม",
        };
        this.drpmonth.push(d6);
        let d7 = {
          id: "8",
          name: "สิงหาคม",
        };
        this.drpmonth.push(d7);
        let d8 = {
          id: "9",
          name: "กันยายน",
        };
        this.drpmonth.push(d8);
        let d9 = {
          id: "10",
          name: "ตุลาคม",
        };
        this.drpmonth.push(d9);
        let d10 = {
          id: "11",
          name: "พฤศจิกายน ",
        };
        this.drpmonth.push(d10);
      } else if (this.Month == 12) {
        let d = {
          id: "1",
          name: "มกราคม",
        };
        this.drpmonth.push(d);
        let d1 = {
          id: "2",
          name: "กุมภาพันธ์",
        };
        this.drpmonth.push(d1);
        let d2 = {
          id: "3",
          name: "มีนาคม",
        };
        this.drpmonth.push(d2);
        let d3 = {
          id: "4",
          name: "เมษายน",
        };
        this.drpmonth.push(d3);
        let d4 = {
          id: "5",
          name: "พฤษภาคม",
        };
        this.drpmonth.push(d4);
        let d5 = {
          id: "6",
          name: "มิถุนายน",
        };
        this.drpmonth.push(d5);
        let d6 = {
          id: "7",
          name: "กรกฎาคม",
        };
        this.drpmonth.push(d6);
        let d7 = {
          id: "8",
          name: "สิงหาคม",
        };
        this.drpmonth.push(d7);
        let d8 = {
          id: "9",
          name: "กันยายน",
        };
        this.drpmonth.push(d8);
        let d9 = {
          id: "10",
          name: "ตุลาคม",
        };
        this.drpmonth.push(d9);
        let d10 = {
          id: "11",
          name: "พฤศจิกายน ",
        };
        this.drpmonth.push(d10);
        let d11 = {
          id: "12",
          name: "ธันวาคม ",
        };
        this.drpmonth.push(d11);
      }
    } else {
      this.drpmonth = [];

      let d = {
        id: "1",
        name: "มกราคม",
      };
      this.drpmonth.push(d);
      let d1 = {
        id: "2",
        name: "กุมภาพันธ์",
      };
      this.drpmonth.push(d1);
      let d2 = {
        id: "3",
        name: "มีนาคม",
      };
      this.drpmonth.push(d2);
      let d3 = {
        id: "4",
        name: "เมษายน",
      };
      this.drpmonth.push(d3);
      let d4 = {
        id: "5",
        name: "พฤษภาคม",
      };
      this.drpmonth.push(d4);
      let d5 = {
        id: "6",
        name: "มิถุนายน",
      };
      this.drpmonth.push(d5);
      let d6 = {
        id: "7",
        name: "กรกฎาคม",
      };
      this.drpmonth.push(d6);
      let d7 = {
        id: "8",
        name: "สิงหาคม",
      };
      this.drpmonth.push(d7);
      let d8 = {
        id: "9",
        name: "กันยายน",
      };
      this.drpmonth.push(d8);
      let d9 = {
        id: "10",
        name: "ตุลาคม",
      };
      this.drpmonth.push(d9);
      let d10 = {
        id: "11",
        name: "พฤศจิกายน ",
      };
      this.drpmonth.push(d10);
      let d11 = {
        id: "12",
        name: "ธันวาคม ",
      };
      this.drpmonth.push(d11);
    }

    //
    if (this.data.monthly != "") {
      let m = this.displayMount.split("/");

      this.displayMount = m[0] + "/" + item.toString();
    } else {
      this.displayMount = item.toString();
    }
  }

  public getStatusAndFinesByMonth() {
    const payload = {
      refRetailStationId: this.data.refStationId,
      yearly: this.data.yearly,
      monthly: this.data.monthly,
      memberName: this.currentUser.memberName,
    };

    this.ngxService.start();

    this.approvalTaxForm03Service.getStatusAndFinesByMonth(payload).subscribe((res) => {
      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
      if (res.result) {
        // save
        this.data.addTimes = res.result.addTimes;
        this.data.statusAcc = res.result.statusAcc;
        this.data.statusAccDesc = res.result.statusAccDesc;
        // save
        this.data.finesMonth = res.result.finesMonth;
        this.data.finesPerMonth = res.result.finesPerMonth;
        //
        this.data.finesStartDate = res.result.finesStartDate;
        this.data.finesEndDate = res.result.finesEndDate;
        //
        this.data.finesTotal = res.result.finesTotal;


        // let BahtSplit = res.result.finesTotal.toLocaleString().split(".");

        // this.plus_extra_money_Baht =
        //   BahtSplit[0] == undefined ? 0 : BahtSplit[0];
        // this.plus_extra_money_Satang =
        //   BahtSplit[1] == undefined ? 0 : BahtSplit[1];
        // this.plus_extra_money_Satang_show =
        //   BahtSplit[1] == undefined ? "00" : BahtSplit[1];
        //this.data.monthly = res.result.monthly;
        this.data.refRetailStationId = res.result.refRetailStationId;

        //           addTimes: 0
        // finesEndDate: "มกราคม 2564"
        // finesPerMonth: 1.5
        // finesStartDate: "ตุลาคม 2563"
        // finesTotal: 15
        // monthly: 2
        // refRetailStationId: "KHnf_uO5if_gfzHJp5prsA"
        // statusAcc: "1"
        // statusAccDesc: "ภายในกำหนดเวลา"
        // yearly: 2564
        if (this.data.summaryList.length != 0) {
          this.calSum();
        }
      } else {
      }
    });
  }

  calSum() {



    let re = /\,/gi;
    //

    let sum_amount = 0;

    this.data.summaryList.forEach((item) => {
      sum_amount =
        sum_amount + parseFloat(item.amount.toString().replace(re, ""));
    });

    sum_amount = parseFloat(sum_amount.toFixed(2)); // รวมบาท

  }

  clickFileImage_billPaymentFileList() {
    let element: HTMLElement = document.getElementById(
      "fileImage_billPaymentFileList"
    ) as HTMLElement;
    element.click();

    // this.blockUI.stop(); // Stop blocking
  }

  fileOverBase_billPaymentFileList(event) {
    console.log("event");
    console.log(event);

    if (event) {
      this.ngxService.start();
      this.uploader_billPaymentFileList.getUploadListener().subscribe((res) => {
        this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

        let fileListImage = JSON.parse(res.respond.replace(/'/g, '"'));
        let filetype = fileListImage[0].split(".");
        if (filetype[1].toUpperCase() == "PDF") {
          filetype[1] = "P";
        } else if (
          filetype[1].toUpperCase() == "DOCX" ||
          filetype[1].toUpperCase() == "DOC"
        ) {
          filetype[1] = "W";
        } else if (
          filetype[1].toUpperCase() == "XLS" ||
          filetype[1].toUpperCase() == "XLSX"
        ) {
          filetype[1] = "E";
        }

        const fomatdata = {
          fileName: res.item.file.name,
          fileType: filetype[1],
          pathFile: fileListImage[0],
          fileCate: "",
          ShowFile:
            REST_URL.MAIN_UPLOADFILE.getFileTemp + "/" + fileListImage[0],
        };

        let listUrl = this.data.billPaymentFileList.find((val) => {
          if (val.pathFile === fileListImage[0]) {
            return true;
          } else {
            return false;
          }
        });

        if (listUrl) {
          if (listUrl.pathFile !== fileListImage[0]) {
            this.data.billPaymentFileList.push(fomatdata);
          }
        } else {
          this.data.billPaymentFileList.push(fomatdata);
        }

        console.log("this.data.billPaymentFileList");
        console.log(this.data.billPaymentFileList);
      });
    }
  }

  public removeFiles_billPaymentFileList(PathUid, i) {
    this.data.billPaymentFileList.splice(i, 1);
    this.delect_billPaymentFileList.push(PathUid);
  }


  clickFileImage_authorizeFileList() {
    let element: HTMLElement = document.getElementById(
      "fileImage_authorizeFileList"
    ) as HTMLElement;
    element.click();

    // this.blockUI.stop(); // Stop blocking
  }

  fileOverBase_authorizeFileList(event) {
    console.log("event");
    console.log(event);
    if (event) {
      this.ngxService.start();
      this.uploader_authorizeFileList.getUploadListener().subscribe((res) => {
        this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

        let fileListImage = JSON.parse(res.respond.replace(/'/g, '"'));
        let filetype = fileListImage[0].split(".");
        if (filetype[1].toUpperCase() == "PDF") {
          filetype[1] = "P";
        } else if (
          filetype[1].toUpperCase() == "DOCX" ||
          filetype[1].toUpperCase() == "DOC"
        ) {
          filetype[1] = "W";
        } else if (
          filetype[1].toUpperCase() == "XLS" ||
          filetype[1].toUpperCase() == "XLSX"
        ) {
          filetype[1] = "E";
        }

        const fomatdata = {
          fileName: res.item.file.name,
          fileType: filetype[1],
          pathFile: fileListImage[0],
          fileCate: "",
          ShowFile:
            REST_URL.MAIN_UPLOADFILE.getFileTemp + "/" + fileListImage[0],
        };

        let listUrl = this.data.authorizeFileList.find((val) => {
          if (val.pathFile === fileListImage[0]) {
            return true;
          } else {
            return false;
          }
        });

        if (listUrl) {
          if (listUrl.pathFile !== fileListImage[0]) {
            this.data.authorizeFileList.push(fomatdata);
          }
        } else {
          this.data.authorizeFileList.push(fomatdata);
        }

        console.log("this.data.authorizeFileList");
        console.log(this.data.authorizeFileList);
      });
    }
  }

  public removeFiles_authorizeFileList(PathUid, i) {
    this.data.authorizeFileList.splice(i, 1);
    this.delect_authorizeFileList.push(PathUid);
  }

  clickFileImage_idCardFileList() {
    let element: HTMLElement = document.getElementById(
      "fileImage_idCardFileList"
    ) as HTMLElement;
    element.click();

    // this.blockUI.stop(); // Stop blocking
  }

  fileOverBase_idCardFileList(event) {
    console.log("event");
    console.log(event);
    if (event) {
      this.ngxService.start();
      this.uploader_idCardFileList.getUploadListener().subscribe((res) => {
        this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

        let fileListImage = JSON.parse(res.respond.replace(/'/g, '"'));
        let filetype = fileListImage[0].split(".");
        if (filetype[1].toUpperCase() == "PDF") {
          filetype[1] = "P";
        } else if (
          filetype[1].toUpperCase() == "DOCX" ||
          filetype[1].toUpperCase() == "DOC"
        ) {
          filetype[1] = "W";
        } else if (
          filetype[1].toUpperCase() == "XLS" ||
          filetype[1].toUpperCase() == "XLSX"
        ) {
          filetype[1] = "E";
        }

        const fomatdata = {
          fileName: res.item.file.name,
          fileType: filetype[1],
          pathFile: fileListImage[0],
          fileCate: "",
          ShowFile:
            REST_URL.MAIN_UPLOADFILE.getFileTemp + "/" + fileListImage[0],
        };

        let listUrl = this.data.idCardFileList.find((val) => {
          if (val.pathFile === fileListImage[0]) {
            return true;
          } else {
            return false;
          }
        });

        if (listUrl) {
          if (listUrl.pathFile !== fileListImage[0]) {
            this.data.idCardFileList.push(fomatdata);
          }
        } else {
          this.data.idCardFileList.push(fomatdata);
        }

        console.log("this.data.idCardFileList");
        console.log(this.data.idCardFileList);
      });
    }
  }

  public removeFiles_idCardFileList(PathUid, i) {
    this.data.idCardFileList.splice(i, 1);
    this.delect_idCardFileList.push(PathUid);
  }

  clickFileImage_bookBankFileList() {
    let element: HTMLElement = document.getElementById(
      "fileImage_bookBankFileList"
    ) as HTMLElement;
    element.click();

    // this.blockUI.stop(); // Stop blocking
  }

  fileOverBase_bookBankFileList(event) {
    console.log("event");
    console.log(event);
    if (event) {
      this.ngxService.start();
      this.uploader_bookBankFileList.getUploadListener().subscribe((res) => {
        this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

        let fileListImage = JSON.parse(res.respond.replace(/'/g, '"'));
        let filetype = fileListImage[0].split(".");
        if (filetype[1].toUpperCase() == "PDF") {
          filetype[1] = "P";
        } else if (
          filetype[1].toUpperCase() == "DOCX" ||
          filetype[1].toUpperCase() == "DOC"
        ) {
          filetype[1] = "W";
        } else if (
          filetype[1].toUpperCase() == "XLS" ||
          filetype[1].toUpperCase() == "XLSX"
        ) {
          filetype[1] = "E";
        }

        const fomatdata = {
          fileName: res.item.file.name,
          fileType: filetype[1],
          pathFile: fileListImage[0],
          fileCate: "",
          ShowFile:
            REST_URL.MAIN_UPLOADFILE.getFileTemp + "/" + fileListImage[0],
        };

        let listUrl = this.data.bookBankFileList.find((val) => {
          if (val.pathFile === fileListImage[0]) {
            return true;
          } else {
            return false;
          }
        });

        if (listUrl) {
          if (listUrl.pathFile !== fileListImage[0]) {
            this.data.bookBankFileList.push(fomatdata);
          }
        } else {
          this.data.bookBankFileList.push(fomatdata);
        }

        console.log("this.data.bookBankFileList");
        console.log(this.data.bookBankFileList);
      });
    }
  }

  public removeFiles_bookBankFileList(PathUid, i) {
    this.data.bookBankFileList.splice(i, 1);
    this.delect_bookBankFileList.push(PathUid);
  }


  validation_form() {
    if (this.data.billPaymentFileList.length == 0) {
      Swal.fire({
        icon: "error",
        title: "กรุณาแนบไฟล์",
        text: "ใบเสร็จรับเงิน หรือหลักฐานการชำระเงิน",
        confirmButtonText: "ตกลง",
      });
    }
    // else if (this.data.fileNumE == "") {
    //   Swal.fire({
    //     icon: "error",
    //     title: "กรุณาระบุจำนวน (แผ่น)",
    //     text: "สำเนาหนังสือรับรองของกระทรวงพาณิชย์ (ที่ออกให้ไม่เกิน 6 เดือน)",
    //     confirmButtonText: "ตกลง",
    //   });
    // } 
    else if (this.data.idCardFileList.length == 0) {
      Swal.fire({
        icon: "error",
        title: "กรุณาแนบไฟล์",
        text: "บัตรประจำตัวประชาชน",
        confirmButtonText: "ตกลง",
      });
    } else if (this.data.bookBankFileList.length == 0) {
      Swal.fire({
        icon: "error",
        title: "กรุณาแนบไฟล์",
        text: "	หน้าสมุดบัญชีธนาคาร",
        confirmButtonText: "ตกลง",
      });
    } else {
      this.onSubmit();
    }
  }

  onSubmit() {

    // let cancelDate;
    // if (this.data.cancelDate != null) {
    //   cancelDate = Utils.dateTostring(this.data.cancelDate);
    // } else {
    //   cancelDate = "";
    // }
    // let changeDate;
    // if (this.data.changeDate != null) {
    //   changeDate = Utils.dateTostring(this.data.changeDate);
    // } else {
    //   changeDate = "";
    // }

    // let corpDateOld;
    // if (this.data.corpDateOld != null) {
    //   corpDateOld = Utils.dateTostring(this.data.corpDateOld);
    // } else {
    //   corpDateOld = "";
    // }

    // let corpDate;
    // if (this.data.corpDate != null) {
    //   corpDate = Utils.dateTostring(this.data.corpDate);
    // } else {
    //   corpDate = "";
    // }

    // 	ใบเสร็จรับเงิน หรือหลักฐานการชำระเงิน
    let billPaymentFileList = [];
    this.data.billPaymentFileList.forEach((val) => {
      let d = {
        taxFormRefundId: "",
        fileName: val.fileName,
        // fileType: val.type,
        pathFile: val.pathFile,
      };
      billPaymentFileList.push(d);
    });



    // หนังสือมอบอำนาจ
    let authorizeFileList = [];
    this.data.authorizeFileList.forEach((val) => {
      let d = {
        taxFormRefundId: "",
        fileName: val.fileName,
        // fileType: val.type,
        pathFile: val.pathFile,
      };
      authorizeFileList.push(d);
    });

    // บัตรประชาชน
    let idCardFileList = [];
    this.data.idCardFileList.forEach((val) => {
      let d = {
        taxFormRefundId: "",
        fileName: val.fileName,
        // fileType: val.type,
        pathFile: val.pathFile,
      };
      idCardFileList.push(d);
    });

    //  บัญชีธนาคาร ๆ
    let bookBankFileList = [];
    this.data.bookBankFileList.forEach((val) => {
      let d = {
        taxFormRefundId: "",
        fileName: val.fileName,
        // fileType: val.type,
        pathFile: val.pathFile,
      };
      bookBankFileList.push(d);
    });
    let amt =this.data.amount.split(",").join("");
    let data = {};
    data = {
      taxFormRefundId: this.id,
      refRetailId: this.data.refRetailId,
      refStationId: this.data.refStationId,
      yearly: this.data.yearly,
      monthly: this.data.monthly,
      amount: parseFloat(amt),
      receiptNo: this.data.receiptNo,
      receiptDate: this.data.receiptDate ? Utils.dateTostring(this.data.receiptDate) : "",
      billPaymentFileList: billPaymentFileList,
      authorizeFileList: authorizeFileList,
      idCardFileList: idCardFileList,
      bookBankFileList: bookBankFileList
    };

    console.log("data");
    console.log(data);

    this.ngxService.start();

    this.requestRefundService.saveData(data).subscribe((res) => {
      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

      console.log("res");
      console.log(res);
      if (res.result) {
        // ใบเสร็จ
        this.data.billPaymentFileList.forEach((val) => {
          this.moveFile(val.pathFile);
        });

        this.delect_billPaymentFileList.forEach((val) => {
          this.deleteFileTemp(val.pathFile);
          this.deleteFile(val.pathFile);
        });

        // หนังสือมอบอำนาจ
        this.data.authorizeFileList.forEach((val) => {
          this.moveFile(val.pathFile);
        });

        this.delect_authorizeFileList.forEach((val) => {
          this.deleteFileTemp(val.pathFile);
          this.deleteFile(val.pathFile);
        });

        // บัตรประชาชน
        this.data.idCardFileList.forEach((val) => {
          this.moveFile(val.pathFile);
        });

        this.delect_idCardFileList.forEach((val) => {
          this.deleteFileTemp(val.pathFile);
          this.deleteFile(val.pathFile);
        });

        // บัญชีธนาคาร
        this.data.bookBankFileList.forEach((val) => {
          this.moveFile(val.pathFile);
        });

        this.delect_bookBankFileList.forEach((val) => {
          this.deleteFileTemp(val.pathFile);
          this.deleteFile(val.pathFile);
        });

        // Stop the foreground loading after 5s
        setTimeout(() => {
          this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
          Swal.fire({
            icon: "success",
            title: "สำเร็จ",
            text: "บันทึกข้อมูลสำเร็จ",
            showConfirmButton: false,
            timer: 1500,
          });

          setTimeout(() => {
            this.cancel();
          }, 1500);
        }, 300);
      } else {
        Swal.fire("ไม่สำเร็จ", res.message, "error");
      }
    });
  }


  moveFile(data) {
    let d = {
      fileName: data,
      location: "taxform01",
    };

    this.ngxService.start();
    this.uploadService.moveFile(d).subscribe((res) => {
      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
    });
  }

  deleteFile(data) {
    let d = {
      fileName: data,
      location: "taxform01",
    };

    this.ngxService.start();
    this.uploadService.deleteFile(d).subscribe((res) => {
      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
    });
  }

  deleteFileTemp(data) {
    let d = {
      fileName: data,
    };

    this.ngxService.start();
    this.uploadService.deleteFileTemp(d).subscribe((res) => {
      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
    });
  }

  cancel() {
    this.router.navigate(["/fontOffice/approval-setting/request-refund"]);
  }


  public getDrpListProvince_() {
    this.addressService.getDrpListProvince().subscribe((res) => {

      if (res.result) {
        this.drpListProvince_ = res.result;
        this.stationList.provinceId = "10";
        this.getDrpListDistrict_("10");
      } else {
        this.drpListProvince_ = [];
      }
    });
  }

  // อำเภอ
  public getDrpListDistrict(provinceId) {
    this.addressService.getDrpListDistrict(provinceId).subscribe((res) => {

      if (res.result) {
        this.drpListDistrict = res.result;
      } else {
        this.drpListDistrict = [];
      }
    });
  }

  public getDrpListDistrict_(provinceId) {
    this.addressService.getDrpListDistrict(provinceId).subscribe((res) => {
      if (res.result) {
        this.drpListDistrict_ = res.result;
      } else {
        this.drpListDistrict_ = [];
      }
    });
  }

  // ตำบล
  public getDrpListTambon(districtId) {
    this.addressService.getDrpListTambon(districtId).subscribe((res) => {
      if (res.result) {
        this.drpListTambon = res.result;
      } else {
        this.drpListTambon = [];
      }
    });
  }

  public getDrpListTambon_(districtId) {
    this.addressService.getDrpListTambon(districtId).subscribe((res) => {

      if (res.result) {
        this.drpListTambon_ = res.result;
      } else {
        this.drpListTambon_ = [];
      }
    });
  }


  public countselectedCheck_stationList(event, item) {
    item.selected = event;
  }

  createStationList() {
    this.stationList = {
      taxForm01StationId: "0",
      taxForm01RetailId: "",
      refStationId: "",
      stationCode: "",
      stationName: "",
      branchName: "", //สาขา
      houseNo: "",
      address: "",
      soi: "",
      road: "",
      tambonId: "",
      tambonName: "",
      amphurId: "",
      amphurName: "",
      // provinceId: "10",
      // provinceName: "กรุงเทพมหานคร",
      provinceId: "",
      provinceName: "",
      postcode: "",
      mobile: "",
      email: "",
      status: "",
      statusName: "",
      houseFileNum: "",
      mapFileNum: "",
      billPaymentFileList: [], //ใบเสร็จรับเงิน หรือหลักฐานการชำระเงิน
      authorizeFileList: [],  //หนังสือมอบอำนาจ
      idCardFileList: [],   //บัตรประจำตัวประชาชน
      bookBankFileList: [], //	หน้าสมุดบัญชีธนาคาร

      commit: false,
    };
  }

  public getDrpListByPostCodeMain(postCode, type) {

    // drpListByPostCodeMain_: any[] = [];// สถานการค้าปลีก
    // typeData_modal: any = false; // สถานการค้าปลีก

    if (postCode.length == 5) {
      if (type == "data") {
        this.Type = type; // data, modal
        this.typeData = true;

        let d = {
          postCodeMain: postCode,
        };

        this.ngxService.start();

        this.addressService.getDrpListByPostCodeMain(d).subscribe((res) => {
          this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

          if (res.result) {
            this.drpListByPostCodeMain = res.result;
          } else {
            this.drpListByPostCodeMain = [];
          }
        });
      } else {
        this.Type = type; // data, modal
        this.typeData_modal = true;

        let d = {
          postCodeMain: postCode,
        };

        this.ngxService.start();

        this.addressService.getDrpListByPostCodeMain(d).subscribe((res) => {
          this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId


          if (res.result) {
            this.drpListByPostCodeMain_ = res.result;
          } else {
            this.drpListByPostCodeMain_ = [];
          }

          // Open Modal
          // this.address_modal_reference = this.modalService.open(
          //   this.address_modal,
          //   {
          //     size: "lg",
          //   }
          // );
        });
      }
    }
  }

  numberOnly(event): boolean {
    const charCode = event.which ? event.which : event.keyCode;

    if (charCode > 31 && (charCode < 45 || charCode > 57)) {
      return false;
    }
    return true;
  }
  sumP4() {
    let re = /\,/gi;
    let balance = parseFloat(
      this.data.amount == "" ? "0" : this.data.amount.replace(re, "")
    );
  }
}
