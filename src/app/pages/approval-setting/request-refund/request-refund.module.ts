import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { NgxsModule } from "@ngxs/store";
import { AddressApi } from "../../../api/address-api";
import { BusinessTypeApi } from "../../../api/business-type-api";
import { CustomerTypeApi } from "../../../api/customer-type-api";
import { FrontretailApi } from "../../../api/frontretail-api";
import { Frontaxform01Api } from "../../../api/fronttaxform01-api";
import { RequestRefundApi } from "../../../api/request-refund-api";
import { RoadApi } from "../../../api/road-api";
import { SoiApi } from "../../../api/soi-api";
import { UploadApi } from "../../../api/upload-api";
import { SharedModule } from "../../../shared/shared.module";
import { AddressService } from "../../address.service";
import { BusinessTypeService } from "../../business-type.service";
import { CustomerTypeService } from "../../customer-type.service";
import { FrontretailService } from "../../frontretail.service";
import { RoadService } from "../../road.service";
import { SoiService } from "../../soi.service";
import { UploadService } from "../../upload.service";
import { RequestRefundListComponent } from "./request-refund-list/request-refund-list.component";
import { RequestRefundUpdateComponent } from "./request-refund-update/request-refund-update.component";
import { RequestRefundViewComponent } from "./request-refund-view/request-refund-view.component";
import { RequestRefundComponent } from "./request-refund.component";
import { RequestRefundRoutes } from "./request-refund.routes";
import { RequestRefundService } from "./request-refund.service";

@NgModule({
  declarations: [
    RequestRefundComponent,
    RequestRefundListComponent,
    RequestRefundUpdateComponent,
    RequestRefundViewComponent,
  ],
  providers: [
    RequestRefundApi,
    RequestRefundService,
    AddressService,
    AddressApi,
    RoadService,
    RoadApi,
    SoiService,
    SoiApi,
    BusinessTypeService,
    BusinessTypeApi,
    CustomerTypeService,
    CustomerTypeApi,
    UploadApi,
    UploadService,
    FrontretailApi,
    FrontretailService,
  ],
  imports: [CommonModule, SharedModule, RequestRefundRoutes],
})
export class RequestRefundModule {}
