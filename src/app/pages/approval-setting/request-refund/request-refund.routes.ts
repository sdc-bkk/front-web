import { Routes, RouterModule } from "@angular/router";
import { RequestRefundListComponent } from "./request-refund-list/request-refund-list.component";
import { RequestRefundUpdateComponent } from "./request-refund-update/request-refund-update.component";
import { RequestRefundViewComponent } from "./request-refund-view/request-refund-view.component";
import { RequestRefundComponent } from "./request-refund.component";

const TEMPLATE_ROUTES: Routes = [
  {
    path: "",
    component: RequestRefundComponent,
    children: [
      {
        path: "",
        component: RequestRefundListComponent,
      },
      {
        path: "update/:id",
        component: RequestRefundUpdateComponent,
      },
      {
        path: "view/:id",
        component: RequestRefundViewComponent,
      },
    ],
  },

  // 404 Page Not Found
  { path: "**", redirectTo: "/" },
];

export const RequestRefundRoutes = RouterModule.forChild(TEMPLATE_ROUTES);
