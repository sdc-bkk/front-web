import {
  Component,
  ViewEncapsulation,
  OnInit,
  TemplateRef,
  ViewChild,
} from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Observable, Subject } from "rxjs/Rx";
import { finalize, map, startWith, takeUntil } from "rxjs/operators";
import { ConfigService } from "../../../../shared/services/config/config.service";
// import createNumberMask from 'text-mask-addons/dist/createNumberMask';
import Utils from "../../../../shared/util/utils";
import {
  NgbModal,
  NgbModalConfig,
  NgbModalRef,
} from "@ng-bootstrap/ng-bootstrap";
import Swal from "sweetalert2";
import { FormControl } from "@angular/forms";
import { AddressService } from "../../../address.service";
import { FileUploader } from "../../../../shared/components/file-upload/file-uploader.class";
import { REST_URL } from "../../../../shared/services/config/rest-url";
import { SoiService } from "../../../soi.service";
import { RoadService } from "../../../road.service";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { UploadService } from "../../../upload.service";
import { BusinessTypeService } from "../../../business-type.service";
import { CustomerTypeService } from "../../../customer-type.service";
import { FrontretailService } from "../../../frontretail.service";
import createNumberMask from "text-mask-addons/dist/createNumberMask";
import { RequestRefundService } from "../request-refund.service";

@Component({
  selector: ".request-refund-view-inner-wrapper",
  templateUrl: "./request-refund-view.component.html",
  styleUrls: ["./request-refund-view.component.scss"],
  encapsulation: ViewEncapsulation.Emulated,
})
export class RequestRefundViewComponent implements OnInit {
  public currencyMask = createNumberMask({
    prefix: "",
    suffix: "",
    includeThousandsSeparator: true,
    thousandsSeparatorSymbol: ",",
    allowDecimal: true,
    decimalSymbol: ".",
    decimalLimit: 2,
    integerLimit: null,
    requireDecimal: false,
    allowNegative: false,
    allowLeadingZeroes: false,
  });

  public maskTaxNo = [
    /[0-9]/,
    "-",
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    "-",
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    "-",
    /\d/,
    /\d/,
    /\d/,
  ];

  public maskMobile = [
    /[0-9]/,
    /\d/,
    /\d/,
    "-",
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    /\d/,
  ];

  /////////////// End Variable ////////////////

  id: any = "0";
  private unsubscribeAll = new Subject();
  //////////////// Value /////////////////////

  taxNoValidation: any = false;

  data = {
    memberName: "",
    firstRecord: false,
    taxForm01RetailId: "0",
    refRetailId: "",
    docNo: "",
    mainStatus: "",
    ownerName: "",
    taxNo: "",
    customerType: "",
    idCard: "",
    idCardAt: "",
    corpNo: "",
    corpDate: "",
    houseNo: "",
    address: "",
    soi: "",
    road: "",
    tambonId: "",
    tambonName: "",
    amphurId: "",
    amphurName: "",
    provinceId: "10",
    provinceName: "",
    postcode: "",
    mobile: "",
    email: "",
    houseFileNum: "",
    cerFileNum: "",
    agentFileNum: "",
    active: true,
    billPaymentFileList: [
    
    ],
    cerFileList: [
      // สำเนาหนังสือรับรองของกระทรวจพาณิชย์ (ที่ออกให้ไม่เกิน 6 เดือน)
      // {
      //     "fileName": "cerFileList.pdf",
      //     "fileType": "P",
      //     "pathFile": "14158953-f5fe-4a66-a680-14aa099da5a5.pdf"
      // }
    ],
    agentFileList: [
      // หนังสือมอบอำนาจและสำเนาบัตรประจำตัวประชาชนของผู้รับมอบอำนาจ
      // {
      //     "fileName": "idCardFileList.pdf",
      //     "fileType": "P",
      //     "pathFile": "14158953-f5fe-4a66-a680-14aa099da5a5.pdf"
      // }
    ],
    stationList: [
      // ผู้ประกอบการ
    ],
    fullAddress: "",
  };

  checkAll_stationList: any = false;

  stationList: any = {
    taxForm01StationId: "",
    taxForm01RetailId: "",
    refStationId: "",
    stationCode: "",
    stationName: "",
    branchName: "",
    houseNo: "",
    address: "",
    soi: "",
    road: "",
    tambonId: "",
    tambonName: "",
    amphurId: "",
    amphurName: "",
    provinceId: "10",
    provinceName: "กรุงเทพมหานคร",
    postcode: "",
    mobile: "",
    email: "",
    status: "",
    statusName: "",
    houseFileNum: "",
    mapFileNum: "",
    businessTypeList: [
      // {
      //     "id": "upyVEXl6pVTiOUJxmMEcvA",
      //     "name": "น้ำมันเชื้อเพลิง"
      // },
      // {
      //     "id": "wFlYj87tnX307XVrn9r6kw",
      //     "name": "ปิโตรเลียม"
      // }
    ],
    billPaymentFileList: [
      // {
      //     "fileName": "Cover.jpg",
      //     "fileType": "I",
      //     "pathFile": "14158953-f5fe-4a66-a680-14aa099da5a5.jpg"
      // }
    ],
    mapFileList: [
      // {
      //     "fileName": "Cover.jpg",
      //     "fileType": "I",
      //     "pathFile": "14158953-f5fe-4a66-a680-14aa099da5a5.jpg"
      // }
    ],
  };

  selected_number = 0;
  selected = new FormControl(this.selected_number);

  drpListProvince: []; // จังหวัด
  drpListDistrict: []; // อำเภอ
  drpListTambon: []; // ตำบล

  drpListProvince_: []; // จังหวัด
  drpListDistrict_: []; // อำเภอ
  drpListTambon_: []; // ตำบล

  @ViewChild("approval_tax_form_01modal")
  public approval_tax_form_01modal: TemplateRef<any>;
  approval_tax_form_01modal_reference: NgbModalRef;

  drpListSoi: any[] = []; // ซอย
  myControl_Soi = new FormControl();
  myControl_Soi_F = new FormControl();
  options_Soi: any[] = [];
  filteredOptions_Soi: Observable<string[]>;
  filteredOptions_Soi_F: Observable<string[]>;

  drpListRoad: any[] = []; // ซอย
  myControl_Road = new FormControl();
  myControl_Road_F = new FormControl();
  options_Road: any[] = [];
  filteredOptions_Road: Observable<string[]>;
  filteredOptions_Road_F: Observable<string[]>;

  @ViewChild("address_modal")
  public address_modal: TemplateRef<any>;
  address_modal_reference: NgbModalRef;
  Type: string = "data"; // data, modal

  drpListByPostCodeMain: any[] = []; // ผู้ประกอบการ
  typeData: any = false; // ผู้ประกอบการ
  drpListByPostCodeMain_: any[] = []; // สถานการค้าปลีก
  typeData_modal: any = false; // สถานการค้าปลีก

  // เอกสารแนบสำเนาทะเบียนบ้าน
  delect_billPaymentFileList: any[] = [];

  uploader_billPaymentFileList: FileUploader = new FileUploader({
    autoUpload: true,
    url: REST_URL.MAIN_UPLOADFILE.uploadFile,
  });
  // เอกสารแนบสำเนาทะเบียนบ้าน

  // หนังสือมอบอำนาจและสำเนาบัตรประจำตัวประชาชนของผู้รับมอบอำนาจ
  delect_agentFileList: any[] = [];

  uploader_agentFileList: FileUploader = new FileUploader({
    autoUpload: true,
    url: REST_URL.MAIN_UPLOADFILE.uploadFile,
  });

  // หนังสือมอบอำนาจและสำเนาบัตรประจำตัวประชาชนของผู้รับมอบอำนาจ

  // สำเนาหนังสือรับรองของกระทรวจพาณิชย์ (ที่ออกให้ไม่เกิน 6 เดือน)
  delect_cerFileList: any[] = [];

  uploader_cerFileList: FileUploader = new FileUploader({
    autoUpload: true,
    url: REST_URL.MAIN_UPLOADFILE.uploadFile,
  });
  // สำเนาหนังสือรับรองของกระทรวจพาณิชย์ (ที่ออกให้ไม่เกิน 6 เดือน)

  // เอกสารแนบสำเนาทะเบียนบ้าน สถานประกอบการ
  delect_billPaymentFileList_modal: any[] = [];

  uploader_billPaymentFileList_modal: FileUploader = new FileUploader({
    autoUpload: true,
    url: REST_URL.MAIN_UPLOADFILE.uploadFile,
  });
  // เอกสารแนบสำเนาทะเบียนบ้าน สถานประกอบการ

  // แผนที่ตั้ง
  delect_mapFileList_modal: any[] = [];

  uploader_mapFileList_modal: FileUploader = new FileUploader({
    autoUpload: true,
    url: REST_URL.MAIN_UPLOADFILE.uploadFile,
  });
  // แผนที่ตั้ง

  drpbusinessType: any = []; // ประเภทธุรกิจ
  drpcustomerType: any = []; // ประเภทผู้เสียภาษี

  currentUser: any;

  getDataByMemberNameStatus: any = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private configModal: NgbModalConfig,
    public config: ConfigService,
    private modalService: NgbModal,
    public ngxService: NgxUiLoaderService,
    private addressService: AddressService,
    private requestRefundService: RequestRefundService,
    private soiService: SoiService,
    private roadService: RoadService,
    private uploadService: UploadService,
    private businessTypeService: BusinessTypeService,
    private customerTypeService: CustomerTypeService,
    private frontretailService: FrontretailService
  ) {
    configModal.backdrop = "static";
  }

  ngOnInit() {
    // this.currentUser = JSON.parse(localStorage.getItem("ba_currentUser"));

    // this.id = this.route.snapshot.params["id"] || "0";
    // this.getDrpListProvince();
    // this.getDrpListProvince_();
    // this.getSoiDrpList(); // ซอย
    // this.getRoadDrpList(); // ถนน
    // this.getCustomerTypeDrpList(); // ประเภทผู้เสียภาษี
    // this.getDrpBusinessTypeList();

    // this.filteredOptions_Soi = this.myControl_Soi.valueChanges.pipe(
    //   startWith(""),
    //   map((value) => this._filter_Soi(value))
    // );

    // this.filteredOptions_Road = this.myControl_Road.valueChanges.pipe(
    //   startWith(""),
    //   map((value) => this._filter_Road(value))
    // );

    // this.filteredOptions_Soi_F = this.myControl_Soi_F.valueChanges.pipe(
    //   startWith(""),
    //   map((value) => this._filter_Soi(value))
    // );

    // this.filteredOptions_Road_F = this.myControl_Road_F.valueChanges.pipe(
    //   startWith(""),
    //   map((value) => this._filter_Road(value))
    // );

    // //this.getDataByMemberName();
    // if (this.id == "0") {
    //   this.getDataByMemberName();
    // } else {
    //   this.getData(this.id);
    // }
  }

  public getDataByMemberName() {
    const payload = {
      memberName: this.currentUser.memberName,
    };

    this.ngxService.start();

    this.frontretailService.getDataByMemberName(payload).subscribe((res) => {
      // console.log("res getDataByMemberName");
      // console.log(res);
      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

      //this.getDataByMemberNameStatus = res.status;

      if (res.status == "false") {
        this.data.firstRecord = true;
        this.getDataByMemberNameStatus = true;
      } else {
        res.result.firstRecord = false;
        this.getDataByMemberNameStatus = false;

        this.setForm(res.result);
      }
    });
  }

  public getDataByTaxNo(item) {
    let re = /\-/gi;
    let result = item.replace(re, "");

    const payload = {
      taxNo: result,
    };

    this.ngxService.start();

    this.frontretailService.getDataByTaxNo(payload).subscribe((res) => {
      // console.log("res getDataByTaxNo");
      // console.log(res);
      if (res.status == "true") {
        this.taxNoValidation = true;

        Swal.fire({
          icon: "warning",
          title: res.resultMessage,
          // text: "Modal with a custom image.",
          confirmButtonText: "ตกลง",
        });
      } else {
        this.taxNoValidation = false;

        // this.data.taxNo = "";
      }

      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
    });
  }

  private _filter_Soi(value: string): string[] {
    const filterValue = value.toLowerCase();

    // return this.options_Soi.filter(
    //   (option) => option.toLowerCase().indexOf(filterValue) === 0
    // );
    let d = [];
    d = this.options_Soi.filter((val) => {
      if (val.toLowerCase().includes(filterValue)) {
        return val;
      }
    });

    // console.log("d");
    // console.log(d);

    // console.log("this.options_Soi");
    // console.log(this.options_Soi);

    // console.log("this.filterValue");
    // console.log(filterValue);

    // d.forEach()
    // let a = this.options_Soi.filter(
    //   (option) => option.toLowerCase().indexOf(d) === 0
    // );

    return d;
  }

  // ซอย
  public getSoiDrpList() {
    this.soiService.getSoiDrpList().subscribe((res) => {
      // console.log("res getDrpListProvince");
      // console.log(res);

      if (res.result) {
        this.drpListSoi = res.result;

        res.result.forEach((r) => {
          //this.optionsProvince.push(r.provinceThai);
          this.options_Soi.push(r.soiName);
        });
      } else {
        this.drpListSoi = [];
        this.options_Soi = [];
      }
    });
  }

  private _filter_Road(value: string): string[] {
    const filterValue = value.toLowerCase();

    let d = [];
    d = this.options_Road.filter((val) => {
      if (val.toLowerCase().includes(filterValue)) {
        return val;
      }
    });

    return d;
  }

  // ถนน
  public getRoadDrpList() {
    this.roadService.getRoadDrpList().subscribe((res) => {
      // console.log("res getDrpListProvince");
      // console.log(res);

      if (res.result) {
        this.drpListRoad = res.result;

        res.result.forEach((r) => {
          //this.optionsProvince.push(r.provinceThai);
          this.options_Road.push(r.roadName);
        });
        // console.log("res this.options_Road");
        // console.log(this.options_Road);
      } else {
        this.drpListRoad = [];
        this.options_Road = [];
      }
    });
  }

  ngOnDestroy() {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }

  // public getData(item) {
  //   const payload = {
  //     taxForm01RetailId: item,
  //   };

  //   this.ngxService.start();

  //   this.RequestRefundService.getData(payload).subscribe((res) => {
  //     //console.log("res getData");
  //     //console.log(res);

  //     this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

  //     if (res.result) {
  //       this.setForm(res.result);
  //     }
  //   });
  // }

  setForm(res: any) {
    console.log("res setForm");
    console.log(res);

    // สำเนาทะเบียนบ้านสำเนาบัตรประจำตัวประชาชนและสำเนาบัตรประจำตัวผู้เสียภาษีอาการ
    let billPaymentFileList = [];
    if (res.billPaymentFileList != undefined) {
      res.billPaymentFileList.forEach((el) => {
        el.ShowFile =
          REST_URL.MAIN_UPLOADFILE.getFile +
          "/" +
          el.pathFile +
          "/" +
          "taxform01";
          billPaymentFileList.push(el);
      });
    }

    // สำเนาหนังสือรับรองของกระทรวจพาณิชย์ (ที่ออกให้ไม่เกิน 6 เดือน)
    let cerFileList = [];
    if (res.cerFileList != undefined) {
      res.cerFileList.forEach((el) => {
        el.ShowFile =
          REST_URL.MAIN_UPLOADFILE.getFile +
          "/" +
          el.pathFile +
          "/" +
          "taxform01";
        cerFileList.push(el);
      });
    }

    // หนังสือมอบอำนาจและสำเนาบัตรประจำตัวประชาชนของผู้รับมอบอำนาจ
    let agentFileList = [];
    if (res.agentFileList != undefined) {
      res.agentFileList.forEach((el) => {
        el.ShowFile =
          REST_URL.MAIN_UPLOADFILE.getFile +
          "/" +
          el.pathFile +
          "/" +
          "taxform01";
        agentFileList.push(el);
      });
    }

    if (res.stationList != undefined) {
      res.stationList.forEach((el) => {
        el.selected = false;
        el.commit = true;

        if (el.billPaymentFileList != undefined) {
          el.billPaymentFileList.forEach((e) => {
            e.ShowFile =
              REST_URL.MAIN_UPLOADFILE.getFile +
              "/" +
              e.pathFile +
              "/" +
              "taxform01";
          });
        }

        if (el.mapFileList != undefined) {
          el.mapFileList.forEach((e) => {
            e.ShowFile =
              REST_URL.MAIN_UPLOADFILE.getFile +
              "/" +
              e.pathFile +
              "/" +
              "taxform01";
            //mapFileList.push(el);
          });
        }
      });
    }

    //this.data.firstRecord = res.firstRecord;

    this.data = {
      memberName: this.currentUser.memberName,

      firstRecord: res.firstRecord,

      taxForm01RetailId:
        res.taxForm01RetailId == undefined ? "" : res.taxForm01RetailId,

      refRetailId: res.refRetailId,

      docNo: res.docNo == undefined ? "" : res.docNo,
      mainStatus: res.mainStatus,
      ownerName: res.ownerName,
      taxNo: res.taxNo,
      customerType: res.customerType,
      idCard: res.idCard,
      idCardAt: res.idCardAt,
      corpNo: res.corpNo,
      corpDate: res.corpDate,
      houseNo: res.houseNo,

      address: res.address,
      soi: res.soi,
      road: res.road,
      tambonId: res.tambonId,
      tambonName: res.tambonName == undefined ? "" : res.tambonName,
      amphurId: res.amphurId,
      amphurName: res.amphurName == undefined ? "" : res.amphurName,

      provinceId: res.provinceId,
      provinceName: res.provinceName == undefined ? "" : res.provinceName,
      postcode: res.postcode,
      mobile: res.mobile,
      email: res.email,
      houseFileNum: res.houseFileNum,
      cerFileNum: res.cerFileNum,
      agentFileNum: res.agentFileNum,
      active: res.active,

      // สำเนาทะเบียนบ้านสำเนาบัตรประจำตัวประชาชนและสำเนาบัตรประจำตัวผู้เสียภาษีอาการ
      billPaymentFileList: billPaymentFileList,
      // สำเนาหนังสือรับรองของกระทรวจพาณิชย์ (ที่ออกให้ไม่เกิน 6 เดือน)
      cerFileList: cerFileList,
      // หนังสือมอบอำนาจและสำเนาบัตรประจำตัวประชาชนของผู้รับมอบอำนาจ
      agentFileList: agentFileList,
      // ผู้ประกอบการ
      stationList: res.stationList == undefined ? [] : res.stationList,
      fullAddress: res.fullAddress,
    };

    console.log("this.data");
    console.log(this.data);
  }

  cancel() {
    this.router.navigate(["/fontOffice/approval-setting/request-refund"]);
  }

  next() {
    this.selected_number++;
    this.selected = new FormControl(this.selected_number);
  }

  prenext() {
    this.selected_number--;
    this.selected = new FormControl(this.selected_number);
  }

  // drpListProvince_: []; // จังหวัด
  // drpListDistrict_: []; // อำเภอ
  // drpListTambon_: []; // ตำบล

  // จังหวัด
  public getDrpListProvince() {
    this.addressService.getDrpListProvince().subscribe((res) => {
      // console.log("res getDrpListProvince");
      // console.log(res);

      if (res.result) {
        this.drpListProvince = res.result;
        this.data.provinceId = "10";
        this.getDrpListDistrict("10");
      } else {
        this.drpListProvince = [];
      }
    });
  }

  public getDrpListProvince_() {
    this.addressService.getDrpListProvince().subscribe((res) => {
      //console.log("res getDrpListProvince_");
      //console.log(res);

      if (res.result) {
        this.drpListProvince_ = res.result;
        this.stationList.provinceId = "10";
        this.getDrpListDistrict_("10");
      } else {
        this.drpListProvince_ = [];
      }
    });
  }

  // อำเภอ
  public getDrpListDistrict(provinceId) {
    this.addressService.getDrpListDistrict(provinceId).subscribe((res) => {
      // console.log("res getDrpListDistrict");
      // console.log(res);

      if (res.result) {
        this.drpListDistrict = res.result;
      } else {
        this.drpListDistrict = [];
      }
    });
  }

  public getDrpListDistrict_(provinceId) {
    this.addressService.getDrpListDistrict(provinceId).subscribe((res) => {
      // console.log("res getDrpListDistrict");
      // console.log(res);

      if (res.result) {
        this.drpListDistrict_ = res.result;
      } else {
        this.drpListDistrict_ = [];
      }
    });
  }

  // ตำบล
  public getDrpListTambon(districtId) {
    this.addressService.getDrpListTambon(districtId).subscribe((res) => {
      //console.log("res getDrpListTambon");
      //console.log(res);

      if (res.result) {
        this.drpListTambon = res.result;
      } else {
        this.drpListTambon = [];
      }
    });
  }

  public getDrpListTambon_(districtId) {
    this.addressService.getDrpListTambon(districtId).subscribe((res) => {
      //console.log("res getDrpListTambon");
      //console.log(res);

      if (res.result) {
        this.drpListTambon_ = res.result;
      } else {
        this.drpListTambon_ = [];
      }
    });
  }

  // Modal 01

  public selectAll_stationList(event) {
    this.checkAll_stationList = event;

    if (event) {
      this.data.stationList.map((val) => {
        val.selected = true;
      });
    } else {
      this.data.stationList.map((val) => {
        val.selected = false;
      });
    }
  }

  public countselectedCheck_stationList(event, item) {
    item.selected = event;
  }

  createStationList() {
    this.stationList = {
      taxForm01StationId: "0",
      taxForm01RetailId: "",
      refStationId: "",
      stationCode: "",
      stationName: "",
      branchName: "",
      houseNo: "",
      address: "",
      soi: "",
      road: "",
      tambonId: "",
      tambonName: "",
      amphurId: "",
      amphurName: "",
      // provinceId: "10",
      // provinceName: "กรุงเทพมหานคร",
      provinceId: "",
      provinceName: "",
      postcode: "",
      mobile: "",
      email: "",
      status: "",
      statusName: "",
      houseFileNum: "",
      mapFileNum: "",
      businessTypeList: [
        // {
        //     "id": "upyVEXl6pVTiOUJxmMEcvA",
        //     "name": "น้ำมันเชื้อเพลิง"
        // },
        // {
        //     "id": "wFlYj87tnX307XVrn9r6kw",
        //     "name": "ปิโตรเลียม"
        // }
      ],
      // สำเนาทะเบียนบ้านสำเนาบัตรประจำตัวประชาชนและสำเนาบัตรประจำตัวผู้เสียภาษีอาการ
      billPaymentFileList: [
        // {
        //     "fileName": "Cover.jpg",
        //     "fileType": "I",
        //     "pathFile": "14158953-f5fe-4a66-a680-14aa099da5a5.jpg"
        // }
      ],
      // แผนที่ตั้ง
      mapFileList: [
        // {
        //     "fileName": "Cover.jpg",
        //     "fileType": "I",
        //     "pathFile": "14158953-f5fe-4a66-a680-14aa099da5a5.jpg"
        // }
      ],
      commit: false,
    };
  }

  open_approval_tax_form_01modal() {
    this.uploader_billPaymentFileList_modal = new FileUploader({
      autoUpload: true,
      url: REST_URL.MAIN_UPLOADFILE.uploadFile,
    });

    this.uploader_mapFileList_modal = new FileUploader({
      autoUpload: true,
      url: REST_URL.MAIN_UPLOADFILE.uploadFile,
    });

    this.drpbusinessType.forEach((val_2) => {
      val_2.selected = false;
    });

    //this.getDrpBusinessTypeList();
    this.typeData_modal = false;

    this.createStationList();

    this.approval_tax_form_01modal_reference = this.modalService.open(
      this.approval_tax_form_01modal,
      {
        size: "lg",
      }
    );
  }

  edit_approval_tax_form_01modal(item, i) {
    this.drpbusinessType.forEach((element) => {
      element.selected = false;
    });

    console.log("item");
    console.log(item);

    this.createStationList();

    this.uploader_billPaymentFileList_modal = new FileUploader({
      autoUpload: true,
      url: REST_URL.MAIN_UPLOADFILE.uploadFile,
    });

    this.uploader_mapFileList_modal = new FileUploader({
      autoUpload: true,
      url: REST_URL.MAIN_UPLOADFILE.uploadFile,
    });

    //this.getDrpBusinessTypeList();
    this.typeData_modal = false;

    this.stationList = {
      taxForm01StationId: "0",
      taxForm01RetailId: "",
      refStationId: "",
      stationCode: "",
      stationName: "",
      branchName: "",
      houseNo: "",
      address: "",
      soi: "",
      road: "",
      tambonId: "",
      tambonName: "",
      amphurId: "",
      amphurName: "",
      provinceId: "10",
      provinceName: "กรุงเทพมหานคร",
      postcode: "",
      mobile: "",
      email: "",
      status: "",
      statusName: "",
      houseFileNum: "",
      mapFileNum: "",
      businessTypeList: [
        // {
        //     "id": "upyVEXl6pVTiOUJxmMEcvA",
        //     "name": "น้ำมันเชื้อเพลิง"
        // },
        // {
        //     "id": "wFlYj87tnX307XVrn9r6kw",
        //     "name": "ปิโตรเลียม"
        // }
      ],
      // สำเนาทะเบียนบ้านสำเนาบัตรประจำตัวประชาชนและสำเนาบัตรประจำตัวผู้เสียภาษีอาการ
      billPaymentFileList: [
        // {
        //     "fileName": "Cover.jpg",
        //     "fileType": "I",
        //     "pathFile": "14158953-f5fe-4a66-a680-14aa099da5a5.jpg"
        // }
      ],
      // แผนที่ตั้ง
      mapFileList: [
        // {
        //     "fileName": "Cover.jpg",
        //     "fileType": "I",
        //     "pathFile": "14158953-f5fe-4a66-a680-14aa099da5a5.jpg"
        // }
      ],
    };

    this.stationList = this.data.stationList[i];

    this.data.stationList[i].businessTypeList.forEach((element) => {
      this.drpbusinessType.map((_element) => {
        if (_element.businessTypeId == element.id) {
          return (_element.selected = true);
        }
      });
    });

    this.approval_tax_form_01modal_reference = this.modalService.open(
      this.approval_tax_form_01modal,
      {
        size: "lg",
      }
    );
  }

  remove_approval_tax_form_01(i) {
    Swal.fire({
      title: "คุณต้องการลบใช่หรือไม่",
      // text: 'You will not be able to recover this imaginary file!',
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "ตกลง",
      cancelButtonText: "ยกเลิก",
    }).then((result) => {
      if (result.value) {
        this.data.stationList.splice(i, 1);
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        // Swal.fire("Cancelled", "Your imaginary file is safe :)", "error");
      }
    });
  }

  remove_all_approval_tax_form_01() {
    Swal.fire({
      title: "คุณต้องการลบใช่หรือไม่",
      // text: 'You will not be able to recover this imaginary file!',
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "ตกลง",
      cancelButtonText: "ยกเลิก",
    }).then((result) => {
      if (result.value) {
        let i = 0;
        this.data.stationList.forEach((v) => {
          if (v.selected) {
            this.data.stationList.splice(i, 1);
          }
          i++;
        });
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        // Swal.fire("Cancelled", "Your imaginary file is safe :)", "error");
      }
    });
  }

  public submit_approval_tax_form_01() {
    let businessTypeList = [];
    this.drpbusinessType.forEach((element) => {
      if (element.selected) {
        let d = {
          id: element.businessTypeId,
          name: element.businessTypeName,
        };
        businessTypeList.push(d);
      }
    });

    this.stationList.businessTypeList = businessTypeList;

    if (this.stationList.taxForm01StationId == "0") {
      if (this.stationList.stationName == "") {
        Swal.fire({
          icon: "error",
          title: "กรุณาระบุชื่อสถานการค้าปลีก",
          // text: "Modal with a custom image.",
          confirmButtonText: "ตกลง",
        });
      } else {
        this.data.stationList.push(this.stationList);
      }
    }

    console.log("this.data.stationList");
    console.log(this.data.stationList);

    this.approval_tax_form_01modal_reference.close();
  }

  public closeModel() {
    this.approval_tax_form_01modal_reference.close();
  }

  // เอกสารแนบสำเนาทะเบียนบ้าน
  clickFileImage_billPaymentFileList_modal() {
    let element: HTMLElement = document.getElementById(
      "fileImage_billPaymentFileList_modal"
    ) as HTMLElement;
    element.click();

    // this.blockUI.stop(); // Stop blocking
  }

  fileOverBase_billPaymentFileList_modal(event) {
    // console.log("event");
    // console.log(event);

    if (event) {
      this.ngxService.start();
      this.uploader_billPaymentFileList_modal.getUploadListener().subscribe((res) => {
        this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

        // console.log("res uploader_billPaymentFileList");
        // console.log(res);

        // console.log("this.data.billPaymentFileList");
        // console.log(this.data.billPaymentFileList);

        let fileListImage = JSON.parse(res.respond.replace(/'/g, '"'));
        let filetype = fileListImage[0].split(".");
        if (filetype[1].toUpperCase() == "PDF") {
          filetype[1] = "P";
        } else if (
          filetype[1].toUpperCase() == "DOCX" ||
          filetype[1].toUpperCase() == "DOC"
        ) {
          filetype[1] = "W";
        } else if (
          filetype[1].toUpperCase() == "XLS" ||
          filetype[1].toUpperCase() == "XLSX"
        ) {
          filetype[1] = "E";
        }

        const fomatdata = {
          fileName: res.item.file.name,
          fileType: filetype[1],
          pathFile: fileListImage[0],
          fileNum: "",
          ShowFile:
            REST_URL.MAIN_UPLOADFILE.getFileTemp + "/" + fileListImage[0],

          // id: "0",
          // pathFile: fileListImage[0],
          // NameUrl:
          //   REST_URL.MAIN_UPLOADFILE.getFileTemp + "/" + fileListImage[0],
          // pathFileName: res.item.file.name,
          // Status: res.status,
          // type: filetype[1],
        };

        // let listUrl = this.stationList.billPaymentFileList.find((val) => {
        //   if (val === fileListImage[0]) {
        //     return true;
        //   } else {
        //     return false;
        //   }
        // });

        let listUrl = this.stationList.billPaymentFileList.find((val) => {
          if (val.pathFile === fileListImage[0]) {
            return true;
          } else {
            return false;
          }
        });

        console.log("listUrl");
        console.log(listUrl);

        if (listUrl) {
          if (listUrl.pathFile !== fileListImage[0]) {
            this.stationList.billPaymentFileList.push(fomatdata);
          }
        } else {
          this.stationList.billPaymentFileList.push(fomatdata);
        }

        //console.log("this.stationList.billPaymentFileList");
        //console.log(this.stationList.billPaymentFileList);
      });
    }
  }

  public removeFiles_billPaymentFileList_modal(PathUid, i) {
    this.stationList.billPaymentFileList.splice(i, 1);
    this.delect_billPaymentFileList_modal.push(PathUid);
  }
  // เอกสารแนบสำเนาทะเบียนบ้าน

  // แผนที่ตั้ง
  // delect_mapFileList_modal: any[] = [];

  // uploader_mapFileList_modal: FileUploader = new FileUploader({
  //   autoUpload: true,
  //   url: REST_URL.MAIN_UPLOADFILE.uploadFile,
  // });

  clickFileImage_mapFileList_modal() {
    let element: HTMLElement = document.getElementById(
      "fileImage_mapFileList_modal"
    ) as HTMLElement;
    element.click();

    // this.blockUI.stop(); // Stop blocking
  }

  fileOverBase_mapFileList_modal(event) {
    // console.log("event");
    // console.log(event);

    if (event) {
      this.ngxService.start();
      this.uploader_mapFileList_modal.getUploadListener().subscribe((res) => {
        this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

        let fileListImage = JSON.parse(res.respond.replace(/'/g, '"'));
        let filetype = fileListImage[0].split(".");
        if (filetype[1].toUpperCase() == "PDF") {
          filetype[1] = "P";
        } else if (
          filetype[1].toUpperCase() == "DOCX" ||
          filetype[1].toUpperCase() == "DOC"
        ) {
          filetype[1] = "W";
        } else if (
          filetype[1].toUpperCase() == "XLS" ||
          filetype[1].toUpperCase() == "XLSX"
        ) {
          filetype[1] = "E";
        }

        const fomatdata = {
          fileName: res.item.file.name,
          fileType: filetype[1],
          pathFile: fileListImage[0],
          fileNum: "",
          ShowFile:
            REST_URL.MAIN_UPLOADFILE.getFileTemp + "/" + fileListImage[0],

          // id: "0",
          // pathFile: fileListImage[0],
          // NameUrl:
          //   REST_URL.MAIN_UPLOADFILE.getFileTemp + "/" + fileListImage[0],
          // pathFileName: res.item.file.name,
          // Status: res.status,
          // type: filetype[1],
        };
        let listUrl = this.stationList.mapFileList.find((val) => {
          if (val.pathFile === fileListImage[0]) {
            return true;
          } else {
            return false;
          }
        });

        if (listUrl) {
          if (listUrl.pathFile !== fileListImage[0]) {
            this.stationList.mapFileList.push(fomatdata);
          }
        } else {
          this.stationList.mapFileList.push(fomatdata);
        }

        //console.log("this.stationList.mapFileList");
        //console.log(this.stationList.mapFileList);
      });
    }
  }

  public removeFiles_mapFileList_modal(PathUid, i) {
    this.stationList.mapFileList.splice(i, 1);
    this.delect_mapFileList_modal.push(PathUid);
  }

  // แผนที่ตั้ง

  // Modal 01

  // @ViewChild("address_modal")
  // public address_modal: TemplateRef<any>;
  // address_modal_reference: NgbModalRef;

  public getDrpListByPostCodeMain(postCode, type) {
    // console.log("postCode.length");
    // console.log(postCode.length);

    // drpListByPostCodeMain_: any[] = [];// สถานการค้าปลีก
    // typeData_modal: any = false; // สถานการค้าปลีก

    if (postCode.length == 5) {
      if (type == "data") {
        this.Type = type; // data, modal
        this.typeData = true;

        let d = {
          postCodeMain: postCode,
        };

        this.ngxService.start();

        this.addressService.getDrpListByPostCodeMain(d).subscribe((res) => {
          this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

          //console.log("res getDrpListByPostCodeMain");
          //console.log(res);

          if (res.result) {
            this.drpListByPostCodeMain = res.result;
          } else {
            this.drpListByPostCodeMain = [];
          }
        });
      } else {
        this.Type = type; // data, modal
        this.typeData_modal = true;

        let d = {
          postCodeMain: postCode,
        };

        this.ngxService.start();

        this.addressService.getDrpListByPostCodeMain(d).subscribe((res) => {
          this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

          //console.log("res getDrpListByPostCodeMain");
          //console.log(res);

          if (res.result) {
            this.drpListByPostCodeMain_ = res.result;
          } else {
            this.drpListByPostCodeMain_ = [];
          }

          // Open Modal
          // this.address_modal_reference = this.modalService.open(
          //   this.address_modal,
          //   {
          //     size: "lg",
          //   }
          // );
        });
      }
    }
  }

  // open_address_modal() {
  //   this.Type = "data"; // data, modal
  //   this.getDrpListByPostCodeMain(this.data.postcode);
  // }

  // open_address_stationList_modal() {
  //   this.Type = "modal"; // data, modal
  //   this.getDrpListByPostCodeMain(this.stationList.postcode);
  // }

  select_address_modal(item) {
    //console.log("this.Type");
    //console.log(this.Type);

    if (this.Type == "data") {
      this.typeData = false;
      // จังหวัด
      this.data.provinceId = item.provinceId;
      this.data.provinceName = item.provinceThai;
      // อำเภอ
      this.data.amphurId = item.districtId;
      this.data.amphurName = item.districtThai;
      // ตำบล
      this.data.tambonId = item.tambonId;
      this.data.tambonName = item.tambonThai;
    }

    if (this.Type == "modal") {
      this.typeData_modal = false;
      // จังหวัด
      this.stationList.provinceId = item.provinceId;
      this.stationList.provinceName = item.provinceThai;
      // อำเภอ
      this.stationList.amphurId = item.districtId;
      this.stationList.amphurName = item.districtThai;
      // ตำบล
      this.stationList.tambonId = item.tambonId;
      this.stationList.tambonName = item.tambonThai;
    }

    //this.address_modal_reference.close();
  }

  numberOnly(event): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  // เอกสารแนบสำเนาทะเบียนบ้าน
  // ImageUidPreviewUrl_billPaymentFileList: any[] = [];
  // ImageUidPreview_billPaymentFileList: any[] = [];
  // ImageUidPreviewUrlDelect_billPaymentFileList: any[] = [];

  // uploader_billPaymentFileList: FileUploader = new FileUploader({
  //   autoUpload: true,
  //   url: REST_URL.MAIN_UPLOADFILE.uploadFile,
  // });

  clickFileImage_billPaymentFileList() {
    let element: HTMLElement = document.getElementById(
      "fileImage_billPaymentFileList"
    ) as HTMLElement;
    element.click();

    // this.blockUI.stop(); // Stop blocking
  }

  fileOverBase_billPaymentFileList(event) {
    // console.log("event");
    // console.log(event);

    if (event) {
      this.ngxService.start();
      this.uploader_billPaymentFileList.getUploadListener().subscribe((res) => {
        this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

        // console.log("res uploader_billPaymentFileList");
        // console.log(res);

        // console.log("this.data.billPaymentFileList");
        // console.log(this.data.billPaymentFileList);

        let fileListImage = JSON.parse(res.respond.replace(/'/g, '"'));
        let filetype = fileListImage[0].split(".");
        if (filetype[1].toUpperCase() == "PDF") {
          filetype[1] = "P";
        } else if (
          filetype[1].toUpperCase() == "DOCX" ||
          filetype[1].toUpperCase() == "DOC"
        ) {
          filetype[1] = "W";
        } else if (
          filetype[1].toUpperCase() == "XLS" ||
          filetype[1].toUpperCase() == "XLSX"
        ) {
          filetype[1] = "E";
        }

        const fomatdata = {
          fileName: res.item.file.name,
          fileType: filetype[1],
          pathFile: fileListImage[0],
          fileNum: "",
          ShowFile:
            REST_URL.MAIN_UPLOADFILE.getFileTemp + "/" + fileListImage[0],

          // id: "0",
          // pathFile: fileListImage[0],
          // NameUrl:
          //   REST_URL.MAIN_UPLOADFILE.getFileTemp + "/" + fileListImage[0],
          // pathFileName: res.item.file.name,
          // Status: res.status,
          // type: filetype[1],
        };
        let listUrl = this.data.billPaymentFileList.find((val) => {
          if (val.pathFile === fileListImage[0]) {
            return true;
          } else {
            return false;
          }
        });

        if (listUrl) {
          if (listUrl.pathFile !== fileListImage[0]) {
            this.data.billPaymentFileList.push(fomatdata);
          }
        } else {
          this.data.billPaymentFileList.push(fomatdata);
        }

        //console.log("this.data.billPaymentFileList");
        //console.log(this.data.billPaymentFileList);
      });
    }
  }

  public removeFiles_billPaymentFileList(PathUid, i) {
    this.data.billPaymentFileList.splice(i, 1);
    this.delect_billPaymentFileList.push(PathUid);
  }

  // เอกสารแนบสำเนาทะเบียนบ้าน

  // หนังสือมอบอำนาจและสำเนาบัตรประจำตัวประชาชนของผู้รับมอบอำนาจ
  // delect_agentFileList: any[] = [];

  // uploader_agentFileList: FileUploader = new FileUploader({
  //   autoUpload: true,
  //   url: REST_URL.MAIN_UPLOADFILE.uploadFile,
  // });

  clickFileImage_agentFileList() {
    let element: HTMLElement = document.getElementById(
      "fileImage_agentFileList"
    ) as HTMLElement;
    element.click();

    // this.blockUI.stop(); // Stop blocking
  }

  fileOverBase_agentFileList(event) {
    // console.log("event");
    // console.log(event);

    if (event) {
      this.ngxService.start();
      this.uploader_agentFileList.getUploadListener().subscribe((res) => {
        this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

        let fileListImage = JSON.parse(res.respond.replace(/'/g, '"'));
        let filetype = fileListImage[0].split(".");
        if (filetype[1].toUpperCase() == "PDF") {
          filetype[1] = "P";
        } else if (
          filetype[1].toUpperCase() == "DOCX" ||
          filetype[1].toUpperCase() == "DOC"
        ) {
          filetype[1] = "W";
        } else if (
          filetype[1].toUpperCase() == "XLS" ||
          filetype[1].toUpperCase() == "XLSX"
        ) {
          filetype[1] = "E";
        }

        const fomatdata = {
          fileName: res.item.file.name,
          fileType: filetype[1],
          pathFile: fileListImage[0],
          fileNum: "",
          ShowFile:
            REST_URL.MAIN_UPLOADFILE.getFileTemp + "/" + fileListImage[0],
        };
        let listUrl = this.data.agentFileList.find((val) => {
          if (val.pathFile === fileListImage[0]) {
            return true;
          } else {
            return false;
          }
        });

        if (listUrl) {
          if (listUrl.pathFile !== fileListImage[0]) {
            this.data.agentFileList.push(fomatdata);
          }
        } else {
          this.data.agentFileList.push(fomatdata);
        }

        // console.log("this.data.agentFileList");
        // console.log(this.data.agentFileList);
      });
    }
  }

  public removeFiles_agentFileList(PathUid, i) {
    this.data.agentFileList.splice(i, 1);
    this.delect_agentFileList.push(PathUid);
  }

  // หนังสือมอบอำนาจและสำเนาบัตรประจำตัวประชาชนของผู้รับมอบอำนาจ

  // สำเนาหนังสือรับรองของกระทรวจพาณิชย์ (ที่ออกให้ไม่เกิน 6 เดือน)
  // delect_cerFileList: any[] = [];

  // uploader_cerFileList: FileUploader = new FileUploader({
  //   autoUpload: true,
  //   url: REST_URL.MAIN_UPLOADFILE.uploadFile,
  // });

  clickFileImage_cerFileList() {
    let element: HTMLElement = document.getElementById(
      "fileImage_cerFileList"
    ) as HTMLElement;
    element.click();

    // this.blockUI.stop(); // Stop blocking
  }

  fileOverBase_cerFileList(event) {
    // console.log("event");
    // console.log(event);

    if (event) {
      this.ngxService.start();

      this.uploader_cerFileList.getUploadListener().subscribe((res) => {
        this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

        let fileListImage = JSON.parse(res.respond.replace(/'/g, '"'));
        let filetype = fileListImage[0].split(".");
        if (filetype[1].toUpperCase() == "PDF") {
          filetype[1] = "P";
        } else if (
          filetype[1].toUpperCase() == "DOCX" ||
          filetype[1].toUpperCase() == "DOC"
        ) {
          filetype[1] = "W";
        } else if (
          filetype[1].toUpperCase() == "XLS" ||
          filetype[1].toUpperCase() == "XLSX"
        ) {
          filetype[1] = "E";
        }

        const fomatdata = {
          fileName: res.item.file.name,
          fileType: filetype[1],
          pathFile: fileListImage[0],
          fileNum: "",
          ShowFile:
            REST_URL.MAIN_UPLOADFILE.getFileTemp + "/" + fileListImage[0],
        };
        let listUrl = this.data.cerFileList.find((val) => {
          if (val.pathFile === fileListImage[0]) {
            return true;
          } else {
            return false;
          }
        });

        if (listUrl) {
          if (listUrl.pathFile !== fileListImage[0]) {
            this.data.cerFileList.push(fomatdata);
          }
        } else {
          this.data.cerFileList.push(fomatdata);
        }

        // console.log("this.data.cerFileList");
        // console.log(this.data.cerFileList);
      });
    }
  }

  public removeFiles_cerFileList(PathUid, i) {
    this.data.cerFileList.splice(i, 1);
    this.delect_cerFileList.push(PathUid);
  }

  // สำเนาหนังสือรับรองของกระทรวจพาณิชย์ (ที่ออกให้ไม่เกิน 6 เดือน)

  // ประเภทธุรกิจ
  public getDrpBusinessTypeList() {
    this.businessTypeService.getDrpBusinessTypeList().subscribe((res) => {
      if (res.result) {
        res.result.map((item) => {
          item.selected = false;
          return item;
        });
        this.drpbusinessType = res.result;

        //console.log("res this.drpbusinessType ");
        //console.log(this.drpbusinessType);
      } else {
        this.drpbusinessType = [];
      }
    });
  }

  // ประเภทธุรกิจ
  public getCustomerTypeDrpList() {
    this.customerTypeService.getCustomerTypeDrpList().subscribe((res) => {
      //console.log("res getCustomerTypeDrpList");
      //console.log(res);

      if (res.result) {
        this.drpcustomerType = res.result;

        if (this.id == "0") {
          this.data.customerType = "บุคคลธรรมดา";
        }
      } else {
        this.drpcustomerType = [];
      }
    });
  }
}
