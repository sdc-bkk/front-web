import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import { RequestRefundApi } from "../../../api/request-refund-api";

@Injectable()
export class RequestRefundService {
  constructor(private requestRefundApi: RequestRefundApi) {}

  getList(data): Observable<any> {
    return this.requestRefundApi.getList(data);
  }

  getData(data): Observable<any> {
    return this.requestRefundApi.getData(data);
  }

  saveData(data): Observable<any> {
    return this.requestRefundApi.saveData(data);
  }

  getDataStation(data): Observable<any> {
    return this.requestRefundApi.getDataStation(data);
  }

  getDrpListStatus(): Observable<any> {
    return this.requestRefundApi.getDrpListStatus();
  }
  

  deleteData(data): Observable<any> {
    return this.requestRefundApi.deleteData(data);
  }

  // sendApprove(data): Observable<any> {
  //   return this.frontaxform01Api.sendApprove(data);
  // }

  // getDrpListRetail(data): Observable<any> {
  //   return this.frontaxform01Api.getDrpListRetail(data);
  // }

  // getDrpListStation(data): Observable<any> {
  //   return this.frontaxform01Api.getDrpListStation(data);
  // }

  // getListStatusLog(data): Observable<any> {
  //   return this.frontaxform01Api.getListStatusLog(data);
  // }

  
}
