import { ViewChild, TemplateRef } from "@angular/core";
import { Component, ViewEncapsulation, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Subject } from "rxjs";
import { finalize, takeUntil } from "rxjs/operators";
import { ConfigService } from "../../../../shared/services/config/config.service";
import { DbMenuService } from "../../../../shared/localstorage/db-menu-service";
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";
import Swal from "sweetalert2";
import { NgxUiLoaderService } from "ngx-ui-loader";
import Utils from "../../../../shared/util/utils";
import { RequestRefundService } from "../request-refund.service";

@Component({
  selector: "./request-refund-list-inner-wrapper",
  templateUrl: "./request-refund-list.component.html",
  styleUrls: ["./request-refund-list.component.scss"],
  encapsulation: ViewEncapsulation.Emulated,
})
export class RequestRefundListComponent implements OnInit {
  /////////////////// Config ///////////////////
  public allExpandState = false;
  panelOpenState: boolean = false;
  allRowsSelected: boolean;
  selectedCheck = 0;
  isSearchActive: boolean = false;
  ///////////////// End Config /////////////////

  ///////////////// Variable /////////////////
  public model: any = {};
  public loading = false;
  private unsubscribeAll = new Subject();

  /////////////// End Variable ////////////////

  //////////////// Value /////////////////////
  dataList: any[] = [];
  dataListTemp: any[] = [];
  maxitemPerPage: number = this.config.paging.ITEMPERPAGE;
  maxSize: number = this.config.paging.MAXSIZE;
  totalItemdataList: number;
  searchValue = {
    docNo: "",
    startDate: "",
    endDate: "",
    status: "",
    memberName: "",
    active: "",
  };
  checkAll: any;
  check_agree: any = false;
  public currentMenuDataBase: any;

  resultPage: any = {
    pageNo: 1,
    totalItem: 0,
    itemPerPage: 0,
    collectionSize: 0,
  };

  loader: any;

  drpContentTypeList: any = [];
  currentUser: any;

  @ViewChild("statusDesign_modal")
  public statusDesign_modal: TemplateRef<any>;
  statusDesign_modalReference: NgbModalRef;

  statusDesignModal: any;

  drpListStatus: any[] = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public config: ConfigService,
    private dbMenuService: DbMenuService,
    private modalService: NgbModal,
    public ngxService: NgxUiLoaderService,
    private requestRefundService: RequestRefundService
  ) { }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem("ba_currentUser"));
    this.getList(1);
    this.getDrpListStatus();
  }

  ngOnDestroy() { }


  public getDrpListStatus() {
    this.requestRefundService.getDrpListStatus().subscribe((res) => {
      if (res.result) {
        this.drpListStatus = res.result;
      } else {
        this.drpListStatus = [];
      }
    });
  }


  /* checkbox select all */
  public selectAll(event) {
    this.allRowsSelected = event;
    if (event) {
      this.dataList.map((val) => {
        if (val.mainStatus == "0" || val.mainStatus == "2") {
          val.selected = true;

          let hasData = this.dataListTemp.find((dataList) => {
            return dataList.taxFormRefundId == val.taxFormRefundId;
          });

          if (!hasData) {
            return this.dataListTemp.push(val);
          }
        }
      });
    } else {
      this.dataListTemp = this.dataListTemp.reduce((result, val) => {
        let hasData = this.dataList.find((data) => {
          return data.taxFormRefundId == val.taxFormRefundId;
        });

        if (!hasData) {
          result = [...result, val];
        }

        return result;
      }, []);

      this.dataList.map((val) => {
        val.selected = false;
        return val;
      });
    }
    this.selectedCheck = this.dataListTemp.length;

    console.log("this.dataListTemp");
    console.log(this.dataListTemp);
  }

  public countselectedCheck(event, item) {
    this.dataListTemp = this.dataListTemp.filter(
      (temp) => temp.taxFormRefundId != item.taxFormRefundId
    );
    if (event) {
      this.selectedCheck++;
      this.dataListTemp.push(item);
    } else {
      this.selectedCheck--;
    }

    let CountCheckTrue = 0;
    this.dataList.map((item) => {
      let dataList = this.dataListTemp.find(
        (temp) => item.taxFormRefundId == temp.taxFormRefundId
      );

      if (dataList) {
        item.selected = true;
        CountCheckTrue++;
      } else {
        item.selected = false;
      }
      return item;
    });

    if (CountCheckTrue == this.dataList.length) {
      this.allRowsSelected = true;
    } else {
      this.allRowsSelected = false;
    }
  }

  public searchActive() {
    if (this.isSearchActive == false) {
      this.isSearchActive = true;
    } else {
      this.isSearchActive = false;
    }
    this.config.setIsSearchActive(this.isSearchActive);
  }
  public triggerClose(event) {
    this.isSearchActive = !this.isSearchActive;
  }

  public refreshDataSearch() {
    this.searchValue = {
      startDate: "",
      endDate: "",
      status: "",
      docNo: "",
      memberName: "",
      active: "",
    };

    this.config.searchValue = this.searchValue;
    this.getList(1);
  }

  public getList(page) {

    let startDate;
    if (this.searchValue.startDate != null) {
      startDate = Utils.dateTostring(this.searchValue.startDate);
    } else {
      startDate = "";
    }

    let endDate;
    if (this.searchValue.endDate != null) {
      endDate = Utils.dateTostring(this.searchValue.endDate);
    } else {
      endDate = "";
    }

    let data = {
      page: page,
      itemPerPage: this.config.paging.ITEMPERPAGE,
      orderBy: "createdDate",
      sort: "desc",

      startDate: startDate,
      endDate: endDate,
      status: this.searchValue.status,
      docNo: this.searchValue.docNo,
      memberName: this.currentUser.memberName,

    };
    console.log("data");
    console.log(data);

    this.config.resultPage.pageNo = page;

    this.ngxService.start();

    this.requestRefundService.getList(data).subscribe((res) => {
      console.log("res requestRefundService  getList");
      console.log(res);

      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

      if (res.result) {
        let selected = 0;
        res.result.map((item) => {
          let dataList = this.dataListTemp.find(
            (temp) => item.taxFormRefundId == temp.taxFormRefundId
          );
          if (dataList) {
            selected++;
            item.selected = true;
          } else {
            item.selected = false;
          }

          // if (item.startDate != "" && item.endDate != "") {
          //   item.fullDate = item.startDate + " - " + item.endDate;
          // } else {
          //   item.fullDate = "";
          // }
          return item;
        });

        this.dataList = res.result;
        this.totalItemdataList = res.resultPage.totalItem;
        this.config.resultPage.totalItem = res.resultPage.totalItem;

        if (selected == res.result.length && selected > 0) {
          this.allRowsSelected = true;
        } else {
          this.allRowsSelected = false;
        }

        if (res.result.lengt == 0) {
          this.allRowsSelected = false;
        }

        this.resultPage.totalItem = res.resultPage.totalItem;
        if (this.totalItemdataList < selected) {
          this.selectedCheck = res.resultPage.totalItem;
        } else {
          this.selectedCheck = this.dataListTemp.length;
        }
      } else {
        this.dataList = [];
        this.totalItemdataList = 0;
        this.config.resultPage.totalItem = 0;
        this.selectedCheck = 0;
      }
    });
  }

  public add() {
    this.router.navigate([
      "/fontOffice/approval-setting/request-refund/update/",
      0,
    ]);
  }

  public update(id) {
    this.router.navigate([
      "/fontOffice/approval-setting/request-refund/update/",
      id,
    ]);
  }

  public delete(item, mes) {
    if (mes != "row") {
      if (this.dataListTemp.length != 0) {
        Swal.fire({
          title: "คุณต้องการลบใช่หรือไม่",
          text:
            "จำนวนรายการที่ต้องการลบ " + this.dataListTemp.length + " รายการ",
          icon: "warning",
          showCancelButton: true,
          confirmButtonText: "ตกลง",
          cancelButtonText: "ยกเลิก",
        }).then((result) => {
          if (result.value) {
            let d = {
              idList: [],
              // updatedBy: "1",
            };

            this.dataListTemp.forEach((v) => {
              d.idList.push(v.taxFormRefundId);
            });

            this.ngxService.start();

            this.requestRefundService.deleteData(d).subscribe((res) => {
              this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

              console.log("res delete");
              console.log(res);

              if (res.result) {
                Swal.fire("สำเร็จ", "ลบข้อมูลสำเร็จ", "success");
                this.dataListTemp = [];
                // ดักตอนลบ
                this.getList(1);
              } else {
                Swal.fire("ไม่สำเร็จ", res.resultMsg, "error");
                this.dataListTemp = [];
                // ดักตอนลบ
                this.getList(1);
              }
            });

            // Swal.fire("ไม่สำเร็จ", "ลบข้อมูลไม่สำเร็จ", "error");
            // For more information about handling dismissals please visit
            // https://sweetalert2.github.io/#handling-dismissals
          } else if (result.dismiss === Swal.DismissReason.cancel) {
            // Swal.fire("Cancelled", "Your imaginary file is safe :)", "error");
          }
        });
      } else {
        Swal.fire("กรุณาเลือกรายการ", "", "error");
      }
    } else {
      Swal.fire({
        title: "คุณต้องการลบใช่หรือไม่",
        text: item.stationName,
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "ตกลง",
        cancelButtonText: "ยกเลิก",
      }).then((result) => {

        if (result.value) {
          if (item.status == 3 || item.status == 1) {
            let d = {
              idList: [],
              // updatedBy: "1",
            };
            d.idList.push(item.taxFormRefundId);
            console.log("delete", d);
            this.ngxService.start();
            this.requestRefundService.deleteData(d).subscribe((res) => {
              this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

              console.log("res delete");
              console.log(res);

              if (res.result) {
                Swal.fire("สำเร็จ", "ลบข้อมูลสำเร็จ", "success");
                this.dataListTemp = [];
                // ดักตอนลบ
                this.getList(1);
              } else {
                Swal.fire("ไม่สำเร็จ", res.resultMsg, "error");
                this.dataListTemp = [];
                // ดักตอนลบ
                this.getList(1);
              }
            });
          }

          // Swal.fire("ไม่สำเร็จ", "ลบข้อมูลไม่สำเร็จ", "error");
          // For more information about handling dismissals please visit
          // https://sweetalert2.github.io/#handling-dismissals
        } else if (result.dismiss === Swal.DismissReason.cancel) {
          // Swal.fire("Cancelled", "Your imaginary file is safe :)", "error");
        }
      });
    }
  }

  public view(id) {
    console.log("id : ",id);
    this.router.navigate([
      "/fontOffice/approval-setting/request-refund/view/",
      id,
    ]);
  }

toMoney(amount){
  return  amount.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, "$&,");
}

}
