import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import { PaymentTaxOilApi } from "../../../api/payment-tax-oil-api";

@Injectable({
  providedIn: 'root'
})
export class PaymentTaxOilService {
  constructor(private paymentTaxOilApi: PaymentTaxOilApi) {}

  getList(data): Observable<any> {
    return this.paymentTaxOilApi.getList(data);
  }

}
