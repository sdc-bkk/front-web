import { Routes, RouterModule } from "@angular/router";
import { PaymentTaxOilComponent } from "./payment-tax-oil.component";
import { PaymentTaxOilListComponent } from './payment-tax-oil-list/payment-tax-oil-list.component';

const TEMPLATE_ROUTES: Routes = [
  {
    path: "",
    component: PaymentTaxOilComponent,
    children: [
      {
        path: "",
        component: PaymentTaxOilListComponent,
      },
    ],
  },

  // 404 Page Not Found
  { path: "**", redirectTo: "/" },
];

export const PaymentTaxOilRoutes = RouterModule.forChild(TEMPLATE_ROUTES);
