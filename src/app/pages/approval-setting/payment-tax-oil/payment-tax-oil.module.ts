import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { NgxsModule } from "@ngxs/store";
import { SharedModule } from "../../../shared/shared.module";
import { PaymentTaxOilComponent } from "./payment-tax-oil.component";
import { PaymentTaxOilRoutes } from "./payment-tax-oil.routes";
import { PaymentTaxOilService } from "./payment-tax-oil.service";
import { PaymentTaxOilListComponent } from './payment-tax-oil-list/payment-tax-oil-list.component';
import { PaymentTaxOilApi } from "../../../api/payment-tax-oil-api";


@NgModule({
    declarations: [PaymentTaxOilComponent, PaymentTaxOilListComponent],
    providers: [
        PaymentTaxOilService,
        PaymentTaxOilApi,
    ],
    imports: [
        CommonModule,
        SharedModule,
        PaymentTaxOilRoutes,
    ],
})
export class PaymentTaxOilModule { }
