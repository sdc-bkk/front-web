import { ViewChild, TemplateRef } from "@angular/core";
import { Component, ViewEncapsulation, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Subject } from "rxjs";
import { finalize, takeUntil } from "rxjs/operators";
import { ConfigService } from "../../../../shared/services/config/config.service";
import { DbMenuService } from "../../../../shared/localstorage/db-menu-service";
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";
import Swal from "sweetalert2";
import { ApprovalTaxForm01Service } from "../approval-tax-form-01.service";
import { NgxUiLoaderService } from "ngx-ui-loader";
import Utils from "../../../../shared/util/utils";

@Component({
  selector: ".approval-tax-form-01-list-inner-wrapper",
  templateUrl: "./approval-tax-form-01-list.component.html",
  styleUrls: ["./approval-tax-form-01-list.component.scss"],
  encapsulation: ViewEncapsulation.Emulated,
})
export class ApprovalTaxForm01ListComponent implements OnInit {
  /////////////////// Config ///////////////////
  public allExpandState = false;
  panelOpenState: boolean = false;
  allRowsSelected: boolean;
  selectedCheck = 0;
  isSearchActive: boolean = false;
  ///////////////// End Config /////////////////

  ///////////////// Variable /////////////////
  public model: any = {};
  public loading = false;
  private unsubscribeAll = new Subject();

  /////////////// End Variable ////////////////

  //////////////// Value /////////////////////
  dataList: any[] = [];
  dataListTemp: any[] = [];
  maxitemPerPage: number = this.config.paging.ITEMPERPAGE;
  maxSize: number = this.config.paging.MAXSIZE;
  totalItemdataList: number;
  searchValue = {
    startDate: "",
    endDate: "",
    status: "",
    docNo: "",
    memberName: "",
    active: "",
  };
  checkAll: any;
  check_agree: any = false;
  public currentMenuDataBase: any;

  resultPage: any = {
    pageNo: 1,
    totalItem: 0,
    itemPerPage: 0,
    collectionSize: 0,
  };

  loader: any;

  drpContentTypeList: any = [];

  // ส่งแบบ
  @ViewChild("sendDesign_modal")
  public sendDesign_modal: TemplateRef<any>;
  sendDesign_modalReference: NgbModalRef;

  sendDesignData_modal: any = [];
  // ส่งแบบ

  // ไม่ผ่าน

  @ViewChild("statusNotApprove_modal")
  public statusNotApprove_modal: TemplateRef<any>;
  statusNotApprove_modalReference: NgbModalRef;

  statusNotApproveData_modal: any = {
    reportid: "",
    regisdate: "",
    texno: "",
    companyname: "",
    pn01: "",
    r1: "",
    statusName: "",
  };

  // ไม่ผ่าน

  report_pn01 = "/api/report/";

  // getLetterAuditPDF: any =
  // "exciseaudit/api" + REST_URL.REPORT_PERMISSION.getLetterAuditPDF + "/";

  currentUser: any;

  @ViewChild("statusDesign_modal")
  public statusDesign_modal: TemplateRef<any>;
  statusDesign_modalReference: NgbModalRef;

  statusDesignModal: any;

  drpListStatus: any[] = [];

  getTaxForm01PDF: any = "taxexportwebservice/api/formExport/getTaxForm01PDF/";

  getTaxFormData01PDF: any =
    "taxexportwebservice/api/formExport/getTaxFormData01PDF/";

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public config: ConfigService,
    private dbMenuService: DbMenuService,
    private modalService: NgbModal,
    public ngxService: NgxUiLoaderService,
    private approvalTaxForm01Service: ApprovalTaxForm01Service
  ) {}

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem("ba_currentUser"));

    this.getTaxForm01PDF = this.config.urllinkReport + this.getTaxForm01PDF;

    this.getTaxFormData01PDF =
      this.config.urllinkReport + this.getTaxFormData01PDF;

    this.getDrpListStatus();
    this.getList(1);
  }

  ngOnDestroy() {}

  public getDrpListStatus() {
    this.approvalTaxForm01Service.getDrpListStatus().subscribe((res) => {
      //console.log("res getDrpListTambon");
      //console.log(res);

      if (res.result) {
        this.drpListStatus = res.result;
      } else {
        this.drpListStatus = [];
      }
    });
  }

  /* checkbox select all */
  public selectAll(event) {
    this.allRowsSelected = event;
    if (event) {
      this.dataList.map((val) => {
        if (val.mainStatus == "0" || val.mainStatus == "2") {
          val.selected = true;

          let hasData = this.dataListTemp.find((dataList) => {
            return dataList.taxForm01RetailId == val.taxForm01RetailId;
          });

          if (!hasData) {
            return this.dataListTemp.push(val);
          }
        }
      });
    } else {
      this.dataListTemp = this.dataListTemp.reduce((result, val) => {
        let hasData = this.dataList.find((data) => {
          return data.taxForm01RetailId == val.taxForm01RetailId;
        });

        if (!hasData) {
          result = [...result, val];
        }

        return result;
      }, []);

      this.dataList.map((val) => {
        val.selected = false;
        return val;
      });
    }
    this.selectedCheck = this.dataListTemp.length;

    console.log("this.dataListTemp");
    console.log(this.dataListTemp);
  }

  public countselectedCheck(event, item) {
    this.dataListTemp = this.dataListTemp.filter(
      (temp) => temp.taxForm01RetailId != item.taxForm01RetailId
    );
    if (event) {
      this.selectedCheck++;
      this.dataListTemp.push(item);
    } else {
      this.selectedCheck--;
    }

    let CountCheckTrue = 0;
    this.dataList.map((item) => {
      let dataList = this.dataListTemp.find(
        (temp) => item.taxForm01RetailId == temp.taxForm01RetailId
      );

      if (dataList) {
        item.selected = true;
        CountCheckTrue++;
      } else {
        item.selected = false;
      }
      return item;
    });

    if (CountCheckTrue == this.dataList.length) {
      this.allRowsSelected = true;
    } else {
      this.allRowsSelected = false;
    }
  }

  public searchActive() {
    if (this.isSearchActive == false) {
      this.isSearchActive = true;
    } else {
      this.isSearchActive = false;
    }
    this.config.setIsSearchActive(this.isSearchActive);
  }
  public triggerClose(event) {
    this.isSearchActive = !this.isSearchActive;
  }

  public refreshDataSearch() {
    this.searchValue = {
      startDate: "",
      endDate: "",
      status: "",
      docNo: "",
      memberName: "",
      active: "",
    };

    this.config.searchValue = this.searchValue;
    this.getList(1);
  }

  public getList(page) {
    let active = null;
    if (this.searchValue.active == "true") {
      active = true;
    } else if (this.searchValue.active == "false") {
      active = false;
    }

    let startDate;
    if (this.searchValue.startDate != null) {
      startDate = Utils.dateTostring(this.searchValue.startDate);
    } else {
      startDate = "";
    }

    let endDate;
    if (this.searchValue.endDate != null) {
      endDate = Utils.dateTostring(this.searchValue.endDate);
    } else {
      endDate = "";
    }

    let data = {
      page: page,
      itemPerPage: this.config.paging.ITEMPERPAGE,
      orderBy: "createdDate",
      sort: "desc",

      startDate: startDate,
      endDate: endDate,
      status: this.searchValue.status,
      docNo: this.searchValue.docNo,
      memberName: this.currentUser.memberName,
      active: active,

      // name: this.searchValue.name,
      // status: this.searchValue.status,
      // reqNo: this.searchValue.reqNo,
      // memberName: this.currentUser.memberName,
      // active: active,
    };
    // console.log("data");
    // console.log(data);

    this.config.resultPage.pageNo = page;

    this.ngxService.start();

    this.approvalTaxForm01Service.getList(data).subscribe((res) => {
      console.log("res approvalTaxForm01Service  getList");
      console.log(res);

      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

      if (res.result) {
        let selected = 0;
        res.result.map((item) => {
          let dataList = this.dataListTemp.find(
            (temp) => item.taxForm01RetailId == temp.taxForm01RetailId
          );
          if (dataList) {
            selected++;
            item.selected = true;
          } else {
            item.selected = false;
          }

          // if (item.startDate != "" && item.endDate != "") {
          //   item.fullDate = item.startDate + " - " + item.endDate;
          // } else {
          //   item.fullDate = "";
          // }
          return item;
        });

        this.dataList = res.result;
        this.totalItemdataList = res.resultPage.totalItem;
        this.config.resultPage.totalItem = res.resultPage.totalItem;

        if (selected == res.result.length && selected > 0) {
          this.allRowsSelected = true;
        } else {
          this.allRowsSelected = false;
        }

        if (res.result.lengt == 0) {
          this.allRowsSelected = false;
        }

        this.resultPage.totalItem = res.resultPage.totalItem;
        if (this.totalItemdataList < selected) {
          this.selectedCheck = res.resultPage.totalItem;
        } else {
          this.selectedCheck = this.dataListTemp.length;
        }
      } else {
        this.dataList = [];
        this.totalItemdataList = 0;
        this.config.resultPage.totalItem = 0;
        this.selectedCheck = 0;
      }
    });
  }

  public add() {
    this.router.navigate([
      "/fontOffice/approval-setting/approval-tax-form-01/update/",
      0,
    ]);
  }

  public update(id) {
    this.router.navigate([
      "/fontOffice/approval-setting/approval-tax-form-01/update/",
      id,
    ]);
  }

  public view(id) {
    this.router.navigate([
      "/fontOffice/approval-setting/approval-tax-form-01/view/",
      id,
    ]);
  }

  downloadPDF_pn01(taxForm01RetailId): any {
    // window.open("../../../../../assets/pdf/form_oil_tax1.pdf", "_blank");
    window.open(this.report_pn01 + taxForm01RetailId, "_blank");
  }

  downloadPDF_or1(): any {
    window.open("../../../../../assets/pdf/or_1.pdf", "_blank");
  }

  // ส่งแบบ
  public open_sendDesign(modalItems) {
    this.check_agree = false;
    let sendDesignData_modal = [];
    // tslint:disable-next-line:triple-equals
    if (this.dataListTemp != undefined && this.dataListTemp.length != 0) {
      sendDesignData_modal = this.dataListTemp;

      this.sendDesignData_modal = sendDesignData_modal;

      console.log("this.sendDesignData_modal");
      console.log(this.sendDesignData_modal);

      this.sendDesign_modalReference = this.modalService.open(modalItems, {
        size: "lg",
        windowClass: "modal-xl",
      });
    } else {
      Swal.fire({
        icon: "warning",
        title: "แจ้งเตือน",
        text: "กรุณาเลือกรายการที่จะส่งแบบ",
        showConfirmButton: false,
        timer: 3000,
      });
    }
  }

  public submit_sendDesign() {
    let d = this.sendDesignData_modal.find((val) => {
      return val.mainStatus != "0" || val.mainStatus != "2";
    });

    console.log("d");
    console.log(d);

    if (!d) {
      //console.log("!d");

      Swal.fire({
        icon: "warning",
        title: "แจ้งเตือน",
        text: "ไม่สามารถส่งแบบได้เนื้องจากมีการส่งแบบไปแล้ว",
        showConfirmButton: false,
        timer: 3000,
      });
    } else {
      //console.log("d");

      let idList = [];
      this.sendDesignData_modal.forEach((v) => {
        idList.push(v.taxForm01RetailId);
      });

      let d = {
        taxForm01RetailId: "",
        actionBy: this.currentUser.memberName,
        remark: "",
        idList: idList,
      };

      this.approvalTaxForm01Service.sendApprove(d).subscribe((res) => {
        console.log("res");
        console.log(res);
        if (res.result) {
          Swal.fire({
            icon: "success",
            title: "สำเร็จ",
            text: "บันทึกข้อมูลสำเร็จ",
            showConfirmButton: false,
            timer: 1500,
          });

          this.dataListTemp = [];

          setTimeout(() => {
            //this.dataListTemp = [];
            this.sendDesign_modalReference.close();
            this.getList(1);
          }, 1500);
        } else {
          Swal.fire("ไม่สำเร็จ", res.message, "error");
        }
      });
    }

    // if (
    //   this.sendDesignData_modal[0].mainStatus == 0 ||
    //   this.sendDesignData_modal[0].mainStatus == 2
    // ) {

    // } else {
    //   Swal.fire({
    //     icon: "warning",
    //     title: "แจ้งเตือน",
    //     text: "ไม่สามารถส่งแบบได้เนื้องจากมีการส่งแบบไปแล้ว",
    //     showConfirmButton: false,
    //     timer: 3000,
    //   });
    // }
  }

  public close_sendDesign() {
    this.sendDesign_modalReference.close();
  }
  // ส่งแบบ

  // ไม่ผ่าน
  public open_statusNotApprove(modalItems) {
    this.statusNotApproveData_modal = {
      regisdate: "10/11/2563",
      companyname: "ปั้ม PTT ห้วยขวาง",
      kname: "ห้วยขวาง",
      detaill: "เอกสารไม่ครบ",
    };

    this.statusNotApprove_modalReference = this.modalService.open(modalItems, {
      size: "lg",
      windowClass: "modal-xl",
    });
  }

  public submit_statusNotApprove() {
    this.statusNotApprove_modalReference.close();
  }

  public close_statusNotApprove() {
    this.statusNotApprove_modalReference.close();
  }
  // ไม่ผ่าน

  // สถานะ
  public open_statusDesign(item) {
    this.getListStatusLog(item.taxForm01RetailId);

    // this.statusDesignModal = item;
  }

  public submit_statusDesign() {
    this.statusDesign_modalReference.close();
  }

  public close_statusDesign() {
    this.statusDesign_modalReference.close();
  }
  // สถานะ

  public delete(item, mes) {
    if (mes != "row") {
      if (this.dataListTemp.length != 0) {
        Swal.fire({
          title: "คุณต้องการลบใช่หรือไม่",
          text:
            "จำนวนรายการที่ต้องการลบ " + this.dataListTemp.length + " รายการ",
          icon: "warning",
          showCancelButton: true,
          confirmButtonText: "ตกลง",
          cancelButtonText: "ยกเลิก",
        }).then((result) => {
          if (result.value) {
            let d = {
              idList: [],
              // updatedBy: "1",
            };

            this.dataListTemp.forEach((v) => {
              d.idList.push(v.taxFormRefundId);
            });

            this.ngxService.start();

            this.approvalTaxForm01Service.deleteData(d).subscribe((res) => {
              this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
       
              if (res.result) {
                Swal.fire("สำเร็จ", "ลบข้อมูลสำเร็จ", "success");
                this.dataListTemp = [];
                // ดักตอนลบ
                this.getList(1);
              } else {
                Swal.fire("ไม่สำเร็จ", res.resultMsg, "error");
                this.dataListTemp = [];
                // ดักตอนลบ
                this.getList(1);
              }
            });

            // Swal.fire("ไม่สำเร็จ", "ลบข้อมูลไม่สำเร็จ", "error");
            // For more information about handling dismissals please visit
            // https://sweetalert2.github.io/#handling-dismissals
          } else if (result.dismiss === Swal.DismissReason.cancel) {
            // Swal.fire("Cancelled", "Your imaginary file is safe :)", "error");
          }
        });
      } else {
        Swal.fire("กรุณาเลือกรายการ", "", "error");
      }
    } else {
      Swal.fire({
        title: "คุณต้องการลบใช่หรือไม่",
        text: item.ownerName,
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "ตกลง",
        cancelButtonText: "ยกเลิก",
      }).then((result) => {
        if (result.value) {
          if (item.mainStatus == 0) {
            let d = {
              idList: [],
              // updatedBy: "1",
            };
            d.idList.push(item.taxFormRefundId);

            this.ngxService.start();
            this.approvalTaxForm01Service.deleteData(d).subscribe((res) => {
              this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
            
              if (res.result) {
                Swal.fire("สำเร็จ", "ลบข้อมูลสำเร็จ", "success");
                this.dataListTemp = [];
                // ดักตอนลบ
                this.getList(1);
              } else {
                Swal.fire("ไม่สำเร็จ", res.resultMsg, "error");
                this.dataListTemp = [];
                // ดักตอนลบ
                this.getList(1);
              }
            });
          }

          // Swal.fire("ไม่สำเร็จ", "ลบข้อมูลไม่สำเร็จ", "error");
          // For more information about handling dismissals please visit
          // https://sweetalert2.github.io/#handling-dismissals
        } else if (result.dismiss === Swal.DismissReason.cancel) {
          // Swal.fire("Cancelled", "Your imaginary file is safe :)", "error");
        }
      });
    }
  }

  public getListStatusLog(item) {
    const payload = {
      taxForm01RetailId: item,
    };

    this.ngxService.start();

    this.approvalTaxForm01Service.getListStatusLog(payload).subscribe((res) => {
      console.log("res getListStatusLog");
      console.log(res);

      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

      if (res.result) {
        res.result.forEach((element) => {
          element.select = true;
        });

        this.statusDesignModal = res.result;
      } else {
        this.statusDesignModal = res.result;
      }

      this.statusDesign_modalReference = this.modalService.open(
        this.statusDesign_modal,
        {
          size: "lg",
          windowClass: "modal-xl",
        }
      );
    });
  }
}
