import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { NgxsModule } from "@ngxs/store";
import { AddressApi } from "../../../api/address-api";
import { BusinessTypeApi } from "../../../api/business-type-api";
import { CustomerTypeApi } from "../../../api/customer-type-api";
import { FrontretailApi } from "../../../api/frontretail-api";
import { Frontaxform01Api } from "../../../api/fronttaxform01-api";
import { RoadApi } from "../../../api/road-api";
import { SoiApi } from "../../../api/soi-api";
import { UploadApi } from "../../../api/upload-api";
import { SharedModule } from "../../../shared/shared.module";
import { AddressService } from "../../address.service";
import { BusinessTypeService } from "../../business-type.service";
import { CustomerTypeService } from "../../customer-type.service";
import { FrontretailService } from "../../frontretail.service";
import { RoadService } from "../../road.service";
import { SoiService } from "../../soi.service";
import { UploadService } from "../../upload.service";
import { ApprovalTaxForm01ListComponent } from "./approval-tax-form-01-list/approval-tax-form-01-list.component";
import { ApprovalTaxForm01UpdateComponent } from "./approval-tax-form-01-update/approval-tax-form-01-update.component";
import { ApprovalTaxForm01ViewComponent } from "./approval-tax-form-01-view/approval-tax-form-01-view.component";
import { ApprovalTaxForm01Component } from "./approval-tax-form-01.component";
import { ApprovalTaxForm01Routes } from "./approval-tax-form-01.routes";
import { ApprovalTaxForm01Service } from "./approval-tax-form-01.service";

@NgModule({
  declarations: [
    ApprovalTaxForm01Component,
    ApprovalTaxForm01ListComponent,
    ApprovalTaxForm01UpdateComponent,
    ApprovalTaxForm01ViewComponent,
  ],
  providers: [
    Frontaxform01Api,
    ApprovalTaxForm01Service,
    AddressService,
    AddressApi,
    RoadService,
    RoadApi,
    SoiService,
    SoiApi,
    BusinessTypeService,
    BusinessTypeApi,
    CustomerTypeService,
    CustomerTypeApi,
    UploadApi,
    UploadService,
    FrontretailApi,
    FrontretailService,
  ],
  imports: [CommonModule, SharedModule, ApprovalTaxForm01Routes],
})
export class ApprovalTaxForm01Module {}
