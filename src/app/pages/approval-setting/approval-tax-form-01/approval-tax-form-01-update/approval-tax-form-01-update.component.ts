import {
  Component,
  ViewEncapsulation,
  OnInit,
  TemplateRef,
  ViewChild,
} from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Observable, Subject } from "rxjs/Rx";
import { finalize, map, startWith, takeUntil } from "rxjs/operators";
import { ConfigService } from "../../../../shared/services/config/config.service";
// import createNumberMask from 'text-mask-addons/dist/createNumberMask';
import Utils from "../../../../shared/util/utils";
import {
  NgbModal,
  NgbModalConfig,
  NgbModalRef,
} from "@ng-bootstrap/ng-bootstrap";
import Swal from "sweetalert2";
import { FormControl } from "@angular/forms";
import { AddressService } from "../../../address.service";
import { ApprovalTaxForm01Service } from "../approval-tax-form-01.service";
import { FileUploader } from "../../../../shared/components/file-upload/file-uploader.class";
import { REST_URL } from "../../../../shared/services/config/rest-url";
import { SoiService } from "../../../soi.service";
import { RoadService } from "../../../road.service";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { UploadService } from "../../../upload.service";
import { BusinessTypeService } from "../../../business-type.service";
import { CustomerTypeService } from "../../../customer-type.service";
import { FrontretailService } from "../../../frontretail.service";
import createNumberMask from "text-mask-addons/dist/createNumberMask";

@Component({
  selector: ".approval-tax-form-01-update-inner-wrapper",
  templateUrl: "./approval-tax-form-01-update.component.html",
  styleUrls: ["./approval-tax-form-01-update.component.scss"],
  encapsulation: ViewEncapsulation.Emulated,
})
export class ApprovalTaxForm01UpdateComponent implements OnInit {
  public currencyMask = createNumberMask({
    prefix: "",
    suffix: "",
    includeThousandsSeparator: true,
    thousandsSeparatorSymbol: ",",
    allowDecimal: true,
    decimalSymbol: ".",
    decimalLimit: 2,
    integerLimit: null,
    requireDecimal: false,
    allowNegative: false,
    allowLeadingZeroes: false,
  });

  public maskTaxNo = [
    /[0-9]/,
    "-",
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    "-",
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    "-",
    /\d/,
    /\d/,
    /\d/,
  ];

  public maskMobile = [
    /[0-9]/,
    /\d/,
    /\d/,
    "-",
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    /\d/,
  ];

  public maskHouseNo = [
    /[0-9]/,
    /\d/,
    /\d/,
    /\d/,
    "-",
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    "-",
    /\d/,
  ];

  /////////////// End Variable ////////////////

  id: any = "0";
  private unsubscribeAll = new Subject();
  //////////////// Value /////////////////////

  taxNoValidation: any = false;

  data = {
    memberName: "",
    firstRecord: false,
    taxForm01RetailId: "0",
    refRetailId: "",
    docNo: "",
    mainStatus: "",
    ownerName: "",
    taxNo: "",
    customerType: "",
    idCard: "",
    idCardAt: "",
    corpNo: "",
    corpDate: "",
    houseNo: "",
    address: "",
    soi: "",
    road: "",
    tambonId: "",
    tambonName: "",
    amphurId: "",
    amphurName: "",
    provinceId: "10",
    provinceName: "",
    postcode: "",
    mobile: "",
    email: "",
    houseFileNum: "",
    cerFileNum: "",
    agentFileNum: "",
    active: true,
    houseFileList: [
      // สำเนาทะเบียนบ้านสำเนาบัตรประจำตัวประชาชนและสำเนาบัตรประจำตัวผู้เสียภาษีอาการ
      // {
      //     "fileName": "houseFileList.jpg",
      //     "fileType": "I",
      //     "pathFile": "14158953-f5fe-4a66-a680-14aa099da5a5.pdf"
      // }
    ],
    cerFileList: [
      // สำเนาหนังสือรับรองของกระทรวจพาณิชย์ (ที่ออกให้ไม่เกิน 6 เดือน)
      // {
      //     "fileName": "cerFileList.pdf",
      //     "fileType": "P",
      //     "pathFile": "14158953-f5fe-4a66-a680-14aa099da5a5.pdf"
      // }
    ],
    agentFileList: [
      // หนังสือมอบอำนาจและสำเนาบัตรประจำตัวประชาชนของผู้รับมอบอำนาจ
      // {
      //     "fileName": "idCardFileList.pdf",
      //     "fileType": "P",
      //     "pathFile": "14158953-f5fe-4a66-a680-14aa099da5a5.pdf"
      // }
    ],
    stationList: [
      // ผู้ประกอบการ
    ],
    fullAddress: "",
    //houseNumber: "",
    signerName:""
  };

  checkAll_stationList: any = false;

  stationList: any = {
    taxForm01StationId: "",
    taxForm01RetailId: "",
    refStationId: "",
    stationCode: "",
    stationName: "",
    branchName: "", //สาขา
    houseNo: "",
    address: "",
    soi: "",
    road: "",
    tambonId: "",
    tambonName: "",
    amphurId: "",
    amphurName: "",
    provinceId: "10",
    provinceName: "กรุงเทพมหานคร",
    postcode: "",
    mobile: "",
    email: "",
    status: "",
    statusName: "",
    houseFileNum: "",
    mapFileNum: "",
    businessTypeList: [
      // {
      //     "id": "upyVEXl6pVTiOUJxmMEcvA",
      //     "name": "น้ำมันเชื้อเพลิง"
      // },
      // {
      //     "id": "wFlYj87tnX307XVrn9r6kw",
      //     "name": "ปิโตรเลียม"
      // }
    ],
    houseFileList: [
      // {
      //     "fileName": "Cover.jpg",
      //     "fileType": "I",
      //     "pathFile": "14158953-f5fe-4a66-a680-14aa099da5a5.jpg"
      // }
    ],
    mapFileList: [
      // {
      //     "fileName": "Cover.jpg",
      //     "fileType": "I",
      //     "pathFile": "14158953-f5fe-4a66-a680-14aa099da5a5.jpg"
      // }
    ],
    fullAddress: "",
  };

  selected_number = 0;
  selected = new FormControl(this.selected_number);

  drpListProvince: []; // จังหวัด
  drpListDistrict: []; // อำเภอ
  drpListTambon: []; // ตำบล

  drpListProvince_: []; // จังหวัด
  drpListDistrict_: []; // อำเภอ
  drpListTambon_: []; // ตำบล

  @ViewChild("approval_tax_form_01modal")
  public approval_tax_form_01modal: TemplateRef<any>;
  approval_tax_form_01modal_reference: NgbModalRef;

  drpListSoi: any[] = []; // ซอย
  myControl_Soi = new FormControl();
  myControl_Soi_F = new FormControl();
  options_Soi: any[] = [];
  filteredOptions_Soi: Observable<string[]>;
  filteredOptions_Soi_F: Observable<string[]>;

  drpListRoad: any[] = []; // ซอย
  myControl_Road = new FormControl();
  myControl_Road_F = new FormControl();
  options_Road: any[] = [];
  filteredOptions_Road: Observable<string[]>;
  filteredOptions_Road_F: Observable<string[]>;

  @ViewChild("address_modal")
  public address_modal: TemplateRef<any>;
  address_modal_reference: NgbModalRef;
  Type: string = "data"; // data, modal

  drpListByPostCodeMain: any[] = []; // ผู้ประกอบการ
  typeData: any = false; // ผู้ประกอบการ
  drpListByPostCodeMain_: any[] = []; // สถานการค้าปลีก
  typeData_modal: any = false; // สถานการค้าปลีก

  // เอกสารแนบสำเนาทะเบียนบ้าน
  delect_houseFileList: any[] = [];

  uploader_houseFileList: FileUploader = new FileUploader({
    autoUpload: true,
    url: REST_URL.MAIN_UPLOADFILE.uploadFile,
  });
  // เอกสารแนบสำเนาทะเบียนบ้าน

  // หนังสือมอบอำนาจและสำเนาบัตรประจำตัวประชาชนของผู้รับมอบอำนาจ
  delect_agentFileList: any[] = [];

  uploader_agentFileList: FileUploader = new FileUploader({
    autoUpload: true,
    url: REST_URL.MAIN_UPLOADFILE.uploadFile,
  });

  // หนังสือมอบอำนาจและสำเนาบัตรประจำตัวประชาชนของผู้รับมอบอำนาจ

  // สำเนาหนังสือรับรองของกระทรวจพาณิชย์ (ที่ออกให้ไม่เกิน 6 เดือน)
  delect_cerFileList: any[] = [];

  uploader_cerFileList: FileUploader = new FileUploader({
    autoUpload: true,
    url: REST_URL.MAIN_UPLOADFILE.uploadFile,
  });
  // สำเนาหนังสือรับรองของกระทรวจพาณิชย์ (ที่ออกให้ไม่เกิน 6 เดือน)

  // เอกสารแนบสำเนาทะเบียนบ้าน สถานประกอบการ
  delect_houseFileList_modal: any[] = [];

  uploader_houseFileList_modal: FileUploader = new FileUploader({
    autoUpload: true,
    url: REST_URL.MAIN_UPLOADFILE.uploadFile,
  });
  // เอกสารแนบสำเนาทะเบียนบ้าน สถานประกอบการ

  // แผนที่ตั้ง
  delect_mapFileList_modal: any[] = [];

  uploader_mapFileList_modal: FileUploader = new FileUploader({
    autoUpload: true,
    url: REST_URL.MAIN_UPLOADFILE.uploadFile,
  });
  // แผนที่ตั้ง

  drpbusinessType: any = []; // ประเภทธุรกิจ
  drpcustomerType: any = []; // ประเภทผู้เสียภาษี

  currentUser: any;

  getDataByMemberNameStatus: any = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private configModal: NgbModalConfig,
    public config: ConfigService,
    private modalService: NgbModal,
    public ngxService: NgxUiLoaderService,
    private addressService: AddressService,
    private approvalTaxForm01Service: ApprovalTaxForm01Service,
    private soiService: SoiService,
    private roadService: RoadService,
    private uploadService: UploadService,
    private businessTypeService: BusinessTypeService,
    private customerTypeService: CustomerTypeService,
    private frontretailService: FrontretailService
  ) {
    configModal.backdrop = "static";
  }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem("ba_currentUser"));

    this.id = this.route.snapshot.params["id"] || "0";
    this.getDrpListProvince();
    this.getDrpListProvince_();
    this.getSoiDrpList(); // ซอย
    this.getRoadDrpList(); // ถนน
    this.getCustomerTypeDrpList(); // ประเภทผู้เสียภาษี
    this.getDrpBusinessTypeList();

    this.filteredOptions_Soi = this.myControl_Soi.valueChanges.pipe(
      startWith(""),
      map((value) => this._filter_Soi(value))
    );

    this.filteredOptions_Road = this.myControl_Road.valueChanges.pipe(
      startWith(""),
      map((value) => this._filter_Road(value))
    );

    this.filteredOptions_Soi_F = this.myControl_Soi_F.valueChanges.pipe(
      startWith(""),
      map((value) => this._filter_Soi(value))
    );

    this.filteredOptions_Road_F = this.myControl_Road_F.valueChanges.pipe(
      startWith(""),
      map((value) => this._filter_Road(value))
    );

    //this.getDataByMemberName();
    if (this.id == "0") {
      this.getDataByMemberName();
    } else {
      this.getData(this.id);
    }
  }

  public getDataByMemberName() {
    const payload = {
      memberName: this.currentUser.memberName,
    };

    this.ngxService.start();

    this.frontretailService.getDataByMemberName(payload).subscribe((res) => {
      console.log("res getDataByMemberName");
      console.log(res);
      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

      //this.getDataByMemberNameStatus = res.status;

      if (res.status == "false") {
        this.data.firstRecord = true;
        this.getDataByMemberNameStatus = true;
      } else {
        res.result.firstRecord = false;
        this.getDataByMemberNameStatus = false;

        res.result.refRetailId = res.result.retailId;

        this.setForm(res.result);
      }
    });
  }

  public getDataByTaxNo(item) {
    let re = /\-/gi;
    let result = item.replace(re, "");

    const payload = {
      taxNo: result,
    };

    this.ngxService.start();

    this.frontretailService.getDataByTaxNo(payload).subscribe((res) => {
      // console.log("res getDataByTaxNo");
      // console.log(res);
      if (res.status == "true") {
        this.taxNoValidation = true;

        Swal.fire({
          icon: "warning",
          title: res.resultMessage,
          // text: "Modal with a custom image.",
          confirmButtonText: "ตกลง",
        });
      } else {
        this.taxNoValidation = false;

        // this.data.taxNo = "";
      }

      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
    });
  }

  private _filter_Soi(value: string): string[] {
    const filterValue = value.toLowerCase();

    // return this.options_Soi.filter(
    //   (option) => option.toLowerCase().indexOf(filterValue) === 0
    // );
    let d = [];
    d = this.options_Soi.filter((val) => {
      if (val.toLowerCase().includes(filterValue)) {
        return val;
      }
    });

    // console.log("d");
    // console.log(d);

    // console.log("this.options_Soi");
    // console.log(this.options_Soi);

    // console.log("this.filterValue");
    // console.log(filterValue);

    // d.forEach()
    // let a = this.options_Soi.filter(
    //   (option) => option.toLowerCase().indexOf(d) === 0
    // );

    return d;
  }

  // ซอย
  public getSoiDrpList() {
    this.soiService.getSoiDrpList().subscribe((res) => {
      // console.log("res getDrpListProvince");
      // console.log(res);

      if (res.result) {
        this.drpListSoi = res.result;

        res.result.forEach((r) => {
          //this.optionsProvince.push(r.provinceThai);
          this.options_Soi.push(r.soiName);
        });
      } else {
        this.drpListSoi = [];
        this.options_Soi = [];
      }
    });
  }

  private _filter_Road(value: string): string[] {
    const filterValue = value.toLowerCase();

    let d = [];
    d = this.options_Road.filter((val) => {
      if (val.toLowerCase().includes(filterValue)) {
        return val;
      }
    });

    return d;
  }

  // ถนน
  public getRoadDrpList() {
    this.roadService.getRoadDrpList().subscribe((res) => {
      // console.log("res getDrpListProvince");
      // console.log(res);

      if (res.result) {
        this.drpListRoad = res.result;

        res.result.forEach((r) => {
          //this.optionsProvince.push(r.provinceThai);
          this.options_Road.push(r.roadName);
        });
        // console.log("res this.options_Road");
        // console.log(this.options_Road);
      } else {
        this.drpListRoad = [];
        this.options_Road = [];
      }
    });
  }

  ngOnDestroy() {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }

  public getData(item) {
    const payload = {
      taxForm01RetailId: item,
    };

    this.ngxService.start();

    this.approvalTaxForm01Service.getData(payload).subscribe((res) => {
      //console.log("res getData");
      //console.log(res);

      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

      if (res.result) {
        this.setForm(res.result);
      }
    });
  }

  setForm(res: any) {
    console.log("res setForm");
    console.log(res);

    // สำเนาทะเบียนบ้านสำเนาบัตรประจำตัวประชาชนและสำเนาบัตรประจำตัวผู้เสียภาษีอาการ
    let houseFileList = [];
    if (res.houseFileList != undefined) {
      res.houseFileList.forEach((el) => {
        el.ShowFile =
          REST_URL.MAIN_UPLOADFILE.getFile +
          "/" +
          el.pathFile +
          "/" +
          "docform";
        houseFileList.push(el);
      });
    }

    // สำเนาหนังสือรับรองของกระทรวจพาณิชย์ (ที่ออกให้ไม่เกิน 6 เดือน)
    let cerFileList = [];
    if (res.cerFileList != undefined) {
      res.cerFileList.forEach((el) => {
        el.ShowFile =
          REST_URL.MAIN_UPLOADFILE.getFile +
          "/" +
          el.pathFile +
          "/" +
          "docform";
        cerFileList.push(el);
      });
    }

    // หนังสือมอบอำนาจและสำเนาบัตรประจำตัวประชาชนของผู้รับมอบอำนาจ
    let agentFileList = [];
    if (res.agentFileList != undefined) {
      res.agentFileList.forEach((el) => {
        el.ShowFile =
          REST_URL.MAIN_UPLOADFILE.getFile +
          "/" +
          el.pathFile +
          "/" +
          "docform";
        agentFileList.push(el);
      });
    }

    if (res.stationList != undefined) {
      res.stationList.forEach((el) => {
        el.selected = false;
        el.commit = true;

        if (el.houseFileList != undefined) {
          el.houseFileList.forEach((e) => {
            e.ShowFile =
              REST_URL.MAIN_UPLOADFILE.getFile +
              "/" +
              e.pathFile +
              "/" +
              "docform";
          });
        }

        if (el.mapFileList != undefined) {
          el.mapFileList.forEach((e) => {
            e.ShowFile =
              REST_URL.MAIN_UPLOADFILE.getFile +
              "/" +
              e.pathFile +
              "/" +
              "docform";
            //mapFileList.push(el);
          });
        }
      });
    }

    //this.data.firstRecord = res.firstRecord;

    let corpDate = null;
    if (res.corpDate != "") {
      corpDate = Utils.stringToDate(res.corpDate);
    } else {
      corpDate = "";
    }
    console.log("corpDate : ", corpDate);
    this.data = {
      memberName: this.currentUser.memberName,

      firstRecord: res.firstRecord,

      taxForm01RetailId:
        res.taxForm01RetailId == undefined ? "" : res.taxForm01RetailId,

      refRetailId: res.refRetailId,
      //refRetailId: res.retailId,

      docNo: res.docNo == undefined ? "" : res.docNo,
      mainStatus: res.mainStatus,
      ownerName: res.ownerName,
      taxNo: res.taxNo,
      customerType: res.customerType,
      idCard: res.idCard,
      idCardAt: res.idCardAt,
      //
      corpNo: res.corpNo,

      corpDate: res.firstRecord ? corpDate : res.corpDate,
      //
      houseNo: res.houseNo,

      address: res.address,
      soi: res.soi,
      road: res.road,
      tambonId: res.tambonId,
      tambonName: res.tambonName == undefined ? "" : res.tambonName,
      amphurId: res.amphurId,
      amphurName: res.amphurName == undefined ? "" : res.amphurName,

      provinceId: res.provinceId,
      provinceName: res.provinceName == undefined ? "" : res.provinceName,
      postcode: res.postcode,
      mobile: res.mobile,
      email: res.email,
      houseFileNum: res.houseFileNum,
      cerFileNum: res.cerFileNum,
      agentFileNum: res.agentFileNum,
      active: res.active,

      // สำเนาทะเบียนบ้านสำเนาบัตรประจำตัวประชาชนและสำเนาบัตรประจำตัวผู้เสียภาษีอาการ
      houseFileList: houseFileList,
      // สำเนาหนังสือรับรองของกระทรวจพาณิชย์ (ที่ออกให้ไม่เกิน 6 เดือน)
      cerFileList: cerFileList,
      // หนังสือมอบอำนาจและสำเนาบัตรประจำตัวประชาชนของผู้รับมอบอำนาจ
      agentFileList: agentFileList,
      // ผู้ประกอบการ
      stationList: res.stationList == undefined ? [] : res.stationList,
      fullAddress: res.fullAddress,
      signerName:res.signerName
    };

    console.log("this.data");
    console.log(this.data);
  }

  validation_form() {
    console.log("save data : this.data");
    console.log(this.data);

    let _houseFileList = true;
    let _houseFileListNum = true;
    let _mapFileList = true;
    let _mapFileListNum = true;
    let _stationName = "";
    this.data.stationList.forEach((val) => {
      if (val.houseFileList.length == 0) {
        _houseFileList = false;

        if (_stationName == "") {
          _stationName = val.stationName;
        }
      }

      if (val.houseFileNum == "") {
        _houseFileListNum = false;

        if (_stationName == "") {
          _stationName = val.stationName;
        }
      }

      if (val.mapFileList.length == 0) {
        _mapFileList = false;

        if (_stationName == "") {
          _stationName = val.stationName;
        }
      }

      if (val.mapFileNum == "") {
        _mapFileListNum = false;

        if (_stationName == "") {
          _stationName = val.stationName;
        }
      }

      // let houseFileList = [];
      // if (val.houseFileList != undefined) {
      //   val.houseFileList.forEach((val_1) => {
      //     let d = {
      //       fileName: val_1.fileName,
      //       fileType: val_1.fileType,
      //       pathFile: val_1.pathFile,
      //     };
      //     houseFileList.push(d);
      //   });
      // }

      // let mapFileList = [];
      // if (val.mapFileList != undefined) {
      //   val.mapFileList.forEach((val_1) => {
      //     let d = {
      //       fileName: val_1.fileName,
      //       fileType: val_1.fileType,
      //       pathFile: val_1.pathFile,
      //     };
      //     mapFileList.push(d);
      //   });
      // }

      // let d = {
      //   houseFileNum: val.houseFileNum == "" ? 0 : parseFloat(val.houseFileNum),
      //   mapFileNum: val.mapFileNum == "" ? 0 : parseFloat(val.mapFileNum),
      // };
    });

    if (this.data.taxNo == "") {
      Swal.fire({
        icon: "error",
        title: "กรุณาระบุเลขประจำตัวผู้เสียภาษีอากร",
        // text: "Modal with a custom image.",
        confirmButtonText: "ตกลง",
      });
    } else if (this.data.houseFileList.length == 0) {
      Swal.fire({
        icon: "error",
        title: "กรุณาแนบไฟล์",
        text:
          "สำเนาทะเบียนบ้าน สำเนาบัตรประจำตัวประชาชน สำเนาบัตรประจำตัวผู้เสียภาษีอาการ",
        confirmButtonText: "ตกลง",
      });
    } else if (this.data.houseFileNum == "") {
      Swal.fire({
        icon: "error",
        title: "กรุณาระบุจำนวน (แผ่น)",
        text:
          "สำเนาทะเบียนบ้าน สำเนาบัตรประจำตัวประชาชน สำเนาบัตรประจำตัวผู้เสียภาษีอาการ",
        confirmButtonText: "ตกลง",
      });
    } else if (this.data.cerFileList.length == 0) {
      Swal.fire({
        icon: "error",
        title: "กรุณาแนบไฟล์",
        text: "สำเนาหนังสือรับรองของกระทรวงพาณิชย์ (ที่ออกให้ไม่เกิน 6 เดือน)",
        confirmButtonText: "ตกลง",
      });
    } else if (this.data.cerFileNum == "") {
      Swal.fire({
        icon: "error",
        title: "กรุณาระบุจำนวน (แผ่น)",
        text: "สำเนาหนังสือรับรองของกระทรวงพาณิชย์ (ที่ออกให้ไม่เกิน 6 เดือน)",
        confirmButtonText: "ตกลง",
      });
    } else if (!_houseFileList) {
      Swal.fire({
        icon: "error",
        title: "กรุณาแนบไฟล์",
        text:
          "สำเนาทะเบียนบ้าน สำเนาบัตรประจำตัวประชาชน สำเนาบัตรประจำตัวผู้เสียภาษีอาการ",
        confirmButtonText: "ตกลง",
      });
    } else if (!_houseFileListNum) {
      Swal.fire({
        icon: "error",
        title: "กรุณาระบุจำนวน (แผ่น)",
        text:
          "สำเนาทะเบียนบ้าน สำเนาบัตรประจำตัวประชาชน สำเนาบัตรประจำตัวผู้เสียภาษีอาการ",
        confirmButtonText: "ตกลง",
      });
    }
    // else if (!_mapFileList) {
    //   Swal.fire({
    //     icon: "error",
    //     title: "กรุณาแนบไฟล์",
    //     text: "แผนที่ตั้งสถานการค้าปลีก",
    //     confirmButtonText: "ตกลง",
    //   });
    // } else if (!_mapFileListNum) {
    //   Swal.fire({
    //     icon: "error",
    //     title: "กรุณาระบุจำนวน (แผ่น)",
    //     text: "แผนที่ตั้งสถานการค้าปลีก",
    //     confirmButtonText: "ตกลง",
    //   });
    // } 

    else {
      if (this.id == "0") {
        let re = /\-/gi;
        let result = this.data.taxNo.replace(re, "");
        const payload = {
          taxNo: result,
        };
        this.ngxService.start();
        this.frontretailService.getDataByTaxNo(payload).subscribe((res) => {
          //console.log("res getDataByTaxNo");
          //console.log(res);
          if (res.status == "true") {
            this.taxNoValidation = true;
            Swal.fire({
              icon: "error",
              //title: "กรุณาลงทะเบียนเลขประจำตัวผู้เสียภาษีอากร",
              title: res.resultMsg,
              // text: "Modal with a custom image.",
              confirmButtonText: "ตกลง",
            });
          } else {
            this.taxNoValidation = false;
            this.onSubmit();
            // this.data.taxNo = "";
          }
          this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
        });
      } else {
        this.onSubmit();
      }
    }
  }

  onSubmit() {
    let recoma = /\,/gi;
    //console.log("this.data");
    //console.log(this.data);

    //this.data.firstRecord == true ? corpDate : res.corpDate,
    let corpDate;
    if (this.data.firstRecord) {
      if (this.data.corpDate != null) {
        corpDate = Utils.dateTostring(this.data.corpDate);
      } else {
        corpDate = "";
      }
    } else {
      corpDate = this.data.corpDate;
    }

    // สำเนาทะเบียนบ้านสำเนาบัตรประจำตัวประชาชนและสำเนาบัตรประจำตัวผู้เสียภาษีอาการ
    let houseFileList = [];
    this.data.houseFileList.forEach((val) => {
      let d = {
        fileName: val.fileName,
        fileType: val.fileType,
        pathFile: val.pathFile,
      };
      houseFileList.push(d);
    });

    // สำเนาหนังสือรับรองของกระทรวจพาณิชย์ (ที่ออกให้ไม่เกิน 6 เดือน)
    let cerFileList = [];
    this.data.cerFileList.forEach((val) => {
      let d = {
        fileName: val.fileName,
        fileType: val.fileType,
        pathFile: val.pathFile,
      };
      cerFileList.push(d);
    });

    // หนังสือมอบอำนาจและสำเนาบัตรประจำตัวประชาชนของผู้รับมอบอำนาจ
    let agentFileList = [];
    this.data.agentFileList.forEach((val) => {
      let d = {
        fileName: val.fileName,
        fileType: val.fileType,
        pathFile: val.pathFile,
      };
      agentFileList.push(d);
    });

    // ผู้ประกอบการ
    let stationList = [];

    console.log("this.data.stationList");
    console.log(this.data.stationList);
    this.data.stationList.forEach((val) => {
      let houseFileList = [];
      if (val.houseFileList != undefined) {
        val.houseFileList.forEach((val_1) => {
          let d = {
            fileName: val_1.fileName,
            fileType: val_1.fileType,
            pathFile: val_1.pathFile,
          };
          houseFileList.push(d);
        });
      }

      let mapFileList = [];
      if (val.mapFileList != undefined) {
        val.mapFileList.forEach((val_1) => {
          let d = {
            fileName: val_1.fileName,
            fileType: val_1.fileType,
            pathFile: val_1.pathFile,
          };
          mapFileList.push(d);
        });
      }

      // console.log("this.drpbusinessType");
      // console.log(this.drpbusinessType);

      // console.log("this.data.stationList");
      // console.log(this.data.stationList);

      let d = {
        taxForm01StationId: val.taxForm01StationId,
        taxForm01RetailId: val.taxForm01RetailId,
        refStationId: val.refStationId,
        stationCode: val.stationCode,

        stationName: val.stationName,
        branchName: val.branchName,
        houseNo: val.houseNo,

        address: val.address,
        moo:val.moo,
        soi: val.soi,
        road: val.road,
        tambonId: val.tambonId,
        tambonName: val.tambonName,
        amphurId: val.amphurId,
        amphurName: val.amphurName,
        provinceId: val.provinceId,
        provinceName: val.provinceName,
        postcode: val.postcode,
        mobile: val.mobile,

        email: val.email,

        status: val.status,
        statusName: val.statusName,

        houseFileNum:
          val.houseFileNum == ""
            ? 0
            : parseFloat(val.houseFileNum.toString().replace(recoma, "")),
        mapFileNum:
          val.mapFileNum == ""
            ? 0
            : parseFloat(val.mapFileNum.toString().replace(recoma, "")),

        businessTypeList: val.businessTypeList,
        houseFileList: houseFileList,
        mapFileList: mapFileList,
        fullAddress: val.fullAddress,
      };
      stationList.push(d);
    });

    let re = /\-/gi;
    let taxNo = this.data.taxNo.replace(re, "");

    let idCard = this.data.idCard.replace(re, "");

    let data = {};
    data = {
      memberName: this.currentUser.memberName,
      firstRecord: this.data.firstRecord,

      taxForm01RetailId: this.data.taxForm01RetailId,

      refRetailId: this.data.refRetailId,

      docNo: this.data.docNo,
      mainStatus: this.data.mainStatus,
      ownerName: this.data.ownerName,

      taxNo: taxNo,

      customerType: this.data.customerType,
      idCard: idCard,
      idCardAt: this.data.idCardAt,
      corpNo: this.data.corpNo,

      corpDate: corpDate,
      houseNo: this.data.houseNo,

      address: this.data.address,
      soi: this.data.soi,
      road: this.data.road,
      tambonId: this.data.tambonId,
      tambonName: this.data.tambonName,
      amphurId: this.data.amphurId,
      amphurName: this.data.amphurName,
      provinceId: this.data.provinceId,
      provinceName: this.data.provinceName,
      postcode: this.data.postcode,

      mobile: this.data.mobile,
      email: this.data.email,

      houseFileNum:
        this.data.houseFileNum == ""
          ? 0
          : parseFloat(this.data.houseFileNum.toString().replace(recoma, "")),
      cerFileNum:
        this.data.cerFileNum == ""
          ? 0
          : parseFloat(this.data.cerFileNum.toString().replace(recoma, "")),
      agentFileNum:
        this.data.agentFileNum == ""
          ? 0
          : parseFloat(this.data.agentFileNum.toString().replace(recoma, "")),
      active: this.data.active,

      // สำเนาทะเบียนบ้านสำเนาบัตรประจำตัวประชาชนและสำเนาบัตรประจำตัวผู้เสียภาษีอาการ
      houseFileList: houseFileList,
      // สำเนาหนังสือรับรองของกระทรวจพาณิชย์ (ที่ออกให้ไม่เกิน 6 เดือน)
      cerFileList: cerFileList,
      // หนังสือมอบอำนาจและสำเนาบัตรประจำตัวประชาชนของผู้รับมอบอำนาจ
      agentFileList: agentFileList,
      // ผู้ประกอบการ
      stationList: stationList,
      fullAddress: this.data.fullAddress,
    };

    console.log("data");
    console.log(data);

    this.ngxService.start();

    this.approvalTaxForm01Service.saveData(data).subscribe((res) => {
      //console.log("res");
      //console.log(res);
      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
      if (res.result) {
        // เอกสารแนบสำเนาทะเบียนบ้าน
        this.data.houseFileList.forEach((val) => {
          this.moveFile(val.pathFile);
        });

        this.delect_houseFileList.forEach((val) => {
          this.deleteFileTemp(val);
          this.deleteFile(val);
        });
        // เอกสารแนบสำเนาทะเบียนบ้าน

        // สำเนาหนังสือรับรองของกระทรวจพาณิชย์ (ที่ออกให้ไม่เกิน 6 เดือน)
        this.data.cerFileList.forEach((val) => {
          this.moveFile(val.pathFile);
        });

        this.delect_cerFileList.forEach((val) => {
          this.deleteFileTemp(val);
          this.deleteFile(val);
        });
        // สำเนาหนังสือรับรองของกระทรวจพาณิชย์ (ที่ออกให้ไม่เกิน 6 เดือน)

        // หนังสือมอบอำนาจและสำเนาบัตรประจำตัวประชาชนของผู้รับมอบอำนาจ
        this.data.agentFileList.forEach((val) => {
          this.moveFile(val.pathFile);
        });

        this.delect_agentFileList.forEach((val) => {
          this.deleteFileTemp(val);
          this.deleteFile(val);
        });
        // หนังสือมอบอำนาจและสำเนาบัตรประจำตัวประชาชนของผู้รับมอบอำนาจ

        // delect_houseFileList_modal
        // delect_mapFileList_modal

        // houseFileList: [], // สำเนาทะเบียนบ้านสำเนาบัตรประจำตัวประชาชนและสำเนาบัตรประจำตัวผู้เสียภาษีอาการ
        // mapFileList: [], // แผนที่ตั้ง

        // สำเนาทะเบียนบ้านสำเนาบัตรประจำตัวประชาชนและสำเนาบัตรประจำตัวผู้เสียภาษีอาการ
        this.data.stationList.forEach((vl) => {
          vl.houseFileList.forEach((l) => {
            this.moveFile(l.pathFile);
          });

          vl.mapFileList.forEach((l) => {
            this.moveFile(l.pathFile);
          });
        });

        this.delect_houseFileList_modal.forEach((val) => {
          this.deleteFileTemp(val);
          this.deleteFile(val);
        });

        this.delect_mapFileList_modal.forEach((val) => {
          this.deleteFileTemp(val);
          this.deleteFile(val);
        });

        this.delect_agentFileList.forEach((val) => {
          this.deleteFileTemp(val);
          this.deleteFile(val);
        });

        // สำเนาทะเบียนบ้านสำเนาบัตรประจำตัวประชาชนและสำเนาบัตรประจำตัวผู้เสียภาษีอาการ

        Swal.fire({
          icon: "success",
          title: "สำเร็จ",
          text: "บันทึกข้อมูลสำเร็จ",
          showConfirmButton: false,
          timer: 1500,
        });

        setTimeout(() => {
          this.cancel();
        }, 1500);
      } else {
        Swal.fire("ไม่สำเร็จ", res.message, "error");
      }
    });
  }

  moveFile(data) {
    let d = {
      fileName: data,
      location: "docform",
    };

    this.ngxService.start();
    this.uploadService.moveFile(d).subscribe((res) => {
      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
    });
  }

  deleteFile(data) {
    let d = {
      fileName: data,
      location: "docform",
    };

    this.ngxService.start();
    this.uploadService.deleteFile(d).subscribe((res) => {
      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
    });
  }

  deleteFileTemp(data) {
    let d = {
      fileName: data,
    };

    this.ngxService.start();
    this.uploadService.deleteFileTemp(d).subscribe((res) => {
      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
    });
  }

  cancel() {
    this.router.navigate(["/fontOffice/approval-setting/approval-tax-form-01"]);
  }

  next() {
    this.selected_number++;
    this.selected = new FormControl(this.selected_number);
  }

  prenext() {
    this.selected_number--;
    this.selected = new FormControl(this.selected_number);
  }

  // drpListProvince_: []; // จังหวัด
  // drpListDistrict_: []; // อำเภอ
  // drpListTambon_: []; // ตำบล

  // จังหวัด
  public getDrpListProvince() {
    this.addressService.getDrpListProvince().subscribe((res) => {
      // console.log("res getDrpListProvince");
      // console.log(res);

      if (res.result) {
        this.drpListProvince = res.result;
        this.data.provinceId = "10";
        this.getDrpListDistrict("10");
      } else {
        this.drpListProvince = [];
      }
    });
  }

  public getDrpListProvince_() {
    this.addressService.getDrpListProvince().subscribe((res) => {
      //console.log("res getDrpListProvince_");
      //console.log(res);

      if (res.result) {
        this.drpListProvince_ = res.result;
        this.stationList.provinceId = "10";
        this.getDrpListDistrict_("10");
      } else {
        this.drpListProvince_ = [];
      }
    });
  }

  // อำเภอ
  public getDrpListDistrict(provinceId) {
    this.addressService.getDrpListDistrict(provinceId).subscribe((res) => {
      // console.log("res getDrpListDistrict");
      // console.log(res);

      if (res.result) {
        this.drpListDistrict = res.result;
      } else {
        this.drpListDistrict = [];
      }
    });
  }

  public getDrpListDistrict_(provinceId) {
    this.addressService.getDrpListDistrict(provinceId).subscribe((res) => {
      // console.log("res getDrpListDistrict");
      // console.log(res);

      if (res.result) {
        this.drpListDistrict_ = res.result;
      } else {
        this.drpListDistrict_ = [];
      }
    });
  }

  // ตำบล
  public getDrpListTambon(districtId) {
    this.addressService.getDrpListTambon(districtId).subscribe((res) => {
      //console.log("res getDrpListTambon");
      //console.log(res);

      if (res.result) {
        this.drpListTambon = res.result;
      } else {
        this.drpListTambon = [];
      }
    });
  }

  public getDrpListTambon_(districtId) {
    this.addressService.getDrpListTambon(districtId).subscribe((res) => {
      //console.log("res getDrpListTambon");
      //console.log(res);

      if (res.result) {
        this.drpListTambon_ = res.result;
      } else {
        this.drpListTambon_ = [];
      }
    });
  }

  // Modal 01

  public selectAll_stationList(event) {
    this.checkAll_stationList = event;

    if (event) {
      this.data.stationList.map((val) => {
        val.selected = true;
      });
    } else {
      this.data.stationList.map((val) => {
        val.selected = false;
      });
    }
  }

  public countselectedCheck_stationList(event, item) {
    item.selected = event;
  }

  createStationList() {
    this.stationList = {
      taxForm01StationId: "0",
      taxForm01RetailId: "",
      refStationId: "",
      stationCode: "",
      stationName: "",
      branchName: "", //สาขา
      houseNo: "",
      address: "",
      soi: "",
      road: "",
      tambonId: "",
      tambonName: "",
      amphurId: "",
      amphurName: "",
      // provinceId: "10",
      // provinceName: "กรุงเทพมหานคร",
      provinceId: "",
      provinceName: "",
      postcode: "",
      mobile: "",
      email: "",
      status: "",
      statusName: "",
      houseFileNum: "",
      mapFileNum: "",
      businessTypeList: [
        // {
        //     "id": "upyVEXl6pVTiOUJxmMEcvA",
        //     "name": "น้ำมันเชื้อเพลิง"
        // },
        // {
        //     "id": "wFlYj87tnX307XVrn9r6kw",
        //     "name": "ปิโตรเลียม"
        // }
      ],
      // สำเนาทะเบียนบ้านสำเนาบัตรประจำตัวประชาชนและสำเนาบัตรประจำตัวผู้เสียภาษีอาการ
      houseFileList: [
        // {
        //     "fileName": "Cover.jpg",
        //     "fileType": "I",
        //     "pathFile": "14158953-f5fe-4a66-a680-14aa099da5a5.jpg"
        // }
      ],
      // แผนที่ตั้ง
      mapFileList: [
        // {
        //     "fileName": "Cover.jpg",
        //     "fileType": "I",
        //     "pathFile": "14158953-f5fe-4a66-a680-14aa099da5a5.jpg"
        // }
      ],
      commit: false,
    };
  }

  open_approval_tax_form_01modal() {
    this.uploader_houseFileList_modal = new FileUploader({
      autoUpload: true,
      url: REST_URL.MAIN_UPLOADFILE.uploadFile,
    });

    this.uploader_mapFileList_modal = new FileUploader({
      autoUpload: true,
      url: REST_URL.MAIN_UPLOADFILE.uploadFile,
    });

    this.drpbusinessType.forEach((val_2) => {
      val_2.selected = false;
    });

    //this.getDrpBusinessTypeList();
    this.typeData_modal = false;

    this.createStationList();

    this.approval_tax_form_01modal_reference = this.modalService.open(
      this.approval_tax_form_01modal,
      {
        size: "lg",
      }
    );
  }

  edit_approval_tax_form_01modal(item, i) {
    this.drpbusinessType.forEach((element) => {
      element.selected = false;
    });

    console.log("item");
    console.log(item);

    this.createStationList();

    this.uploader_houseFileList_modal = new FileUploader({
      autoUpload: true,
      url: REST_URL.MAIN_UPLOADFILE.uploadFile,
    });

    this.uploader_mapFileList_modal = new FileUploader({
      autoUpload: true,
      url: REST_URL.MAIN_UPLOADFILE.uploadFile,
    });

    //this.getDrpBusinessTypeList();
    this.typeData_modal = false;

    this.stationList = {
      taxForm01StationId: "0",
      taxForm01RetailId: "",
      refStationId: "",
      stationCode: "",
      stationName: "",
      branchName: "", // สาขา
      houseNo: "",
      address: "",
      soi: "",
      road: "",
      tambonId: "",
      tambonName: "",
      amphurId: "",
      amphurName: "",
      provinceId: "",
      provinceName: "",
      postcode: "",
      mobile: "",
      email: "",
      status: "",
      statusName: "",
      houseFileNum: "",
      mapFileNum: "",
      businessTypeList: [
        // {
        //     "id": "upyVEXl6pVTiOUJxmMEcvA",
        //     "name": "น้ำมันเชื้อเพลิง"
        // },
        // {
        //     "id": "wFlYj87tnX307XVrn9r6kw",
        //     "name": "ปิโตรเลียม"
        // }
      ],
      // สำเนาทะเบียนบ้านสำเนาบัตรประจำตัวประชาชนและสำเนาบัตรประจำตัวผู้เสียภาษีอาการ
      houseFileList: [
        // {
        //     "fileName": "Cover.jpg",
        //     "fileType": "I",
        //     "pathFile": "14158953-f5fe-4a66-a680-14aa099da5a5.jpg"
        // }
      ],
      // แผนที่ตั้ง
      mapFileList: [
        // {
        //     "fileName": "Cover.jpg",
        //     "fileType": "I",
        //     "pathFile": "14158953-f5fe-4a66-a680-14aa099da5a5.jpg"
        // }
      ],
    };

    this.stationList = this.data.stationList[i];

    this.data.stationList[i].businessTypeList.forEach((element) => {
      this.drpbusinessType.map((_element) => {
        if (_element.businessTypeId == element.id) {
          return (_element.selected = true);
        }
      });
    });

    this.approval_tax_form_01modal_reference = this.modalService.open(
      this.approval_tax_form_01modal,
      {
        size: "lg",
      }
    );
  }

  remove_approval_tax_form_01(i) {
    Swal.fire({
      title: "คุณต้องการลบใช่หรือไม่",
      // text: 'You will not be able to recover this imaginary file!',
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "ตกลง",
      cancelButtonText: "ยกเลิก",
    }).then((result) => {
      if (result.value) {
        this.data.stationList.splice(i, 1);
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        // Swal.fire("Cancelled", "Your imaginary file is safe :)", "error");
      }
    });
  }

  remove_all_approval_tax_form_01() {
    Swal.fire({
      title: "คุณต้องการลบใช่หรือไม่",
      // text: 'You will not be able to recover this imaginary file!',
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "ตกลง",
      cancelButtonText: "ยกเลิก",
    }).then((result) => {
      if (result.value) {
        let i = 0;
        this.data.stationList.forEach((v) => {
          if (v.selected) {
            this.data.stationList.splice(i, 1);
          }
          i++;
        });
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        // Swal.fire("Cancelled", "Your imaginary file is safe :)", "error");
      }
    });
  }

  public submit_approval_tax_form_01() {
    let businessTypeList = [];
    this.drpbusinessType.forEach((element) => {
      if (element.selected) {
        let d = {
          id: element.businessTypeId,
          name: element.businessTypeName,
        };
        businessTypeList.push(d);
      }
    });

    this.stationList.businessTypeList = businessTypeList;

    if (this.stationList.stationName == "") {
      Swal.fire({
        icon: "error",
        title: "กรุณาระบุชื่อสถานการค้าปลีก",
        // text: "Modal with a custom image.",
        confirmButtonText: "ตกลง",
      });
    } else {
      if (!this.stationList.commit) {
        this.stationList.commit = true;
        this.data.stationList.push(this.stationList);
      }
      this.approval_tax_form_01modal_reference.close();
    }

    console.log("this.data.stationList");
    console.log(this.data.stationList);
  }

  public closeModel() {
    this.approval_tax_form_01modal_reference.close();
  }

  // เอกสารแนบสำเนาทะเบียนบ้าน
  clickFileImage_houseFileList_modal() {
    let element: HTMLElement = document.getElementById(
      "fileImage_houseFileList_modal"
    ) as HTMLElement;
    element.click();

    // this.blockUI.stop(); // Stop blocking
  }

  fileOverBase_houseFileList_modal(event) {
    // console.log("event");
    // console.log(event);

    if (event) {
      this.ngxService.start();
      this.uploader_houseFileList_modal.getUploadListener().subscribe((res) => {
        this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

        // console.log("res uploader_houseFileList");
        // console.log(res);

        // console.log("this.data.houseFileList");
        // console.log(this.data.houseFileList);

        let fileListImage = JSON.parse(res.respond.replace(/'/g, '"'));
        let filetype = fileListImage[0].split(".");
        if (filetype[1].toUpperCase() == "PDF") {
          filetype[1] = "P";
        } else if (
          filetype[1].toUpperCase() == "DOCX" ||
          filetype[1].toUpperCase() == "DOC"
        ) {
          filetype[1] = "W";
        } else if (
          filetype[1].toUpperCase() == "XLS" ||
          filetype[1].toUpperCase() == "XLSX"
        ) {
          filetype[1] = "E";
        }

        const fomatdata = {
          fileName: res.item.file.name,
          fileType: filetype[1],
          pathFile: fileListImage[0],
          fileNum: "",
          ShowFile:
            REST_URL.MAIN_UPLOADFILE.getFileTemp + "/" + fileListImage[0],

          // id: "0",
          // pathFile: fileListImage[0],
          // NameUrl:
          //   REST_URL.MAIN_UPLOADFILE.getFileTemp + "/" + fileListImage[0],
          // pathFileName: res.item.file.name,
          // Status: res.status,
          // type: filetype[1],
        };

        // let listUrl = this.stationList.houseFileList.find((val) => {
        //   if (val === fileListImage[0]) {
        //     return true;
        //   } else {
        //     return false;
        //   }
        // });

        let listUrl = this.stationList.houseFileList.find((val) => {
          if (val.pathFile === fileListImage[0]) {
            return true;
          } else {
            return false;
          }
        });

        console.log("listUrl");
        console.log(listUrl);

        if (listUrl) {
          if (listUrl.pathFile !== fileListImage[0]) {
            this.stationList.houseFileList.push(fomatdata);
          }
        } else {
          this.stationList.houseFileList.push(fomatdata);
        }

        //console.log("this.stationList.houseFileList");
        //console.log(this.stationList.houseFileList);
      });
    }
  }

  public removeFiles_houseFileList_modal(PathUid, i) {
    this.stationList.houseFileList.splice(i, 1);
    this.delect_houseFileList_modal.push(PathUid);
  }
  // เอกสารแนบสำเนาทะเบียนบ้าน

  // แผนที่ตั้ง
  // delect_mapFileList_modal: any[] = [];

  // uploader_mapFileList_modal: FileUploader = new FileUploader({
  //   autoUpload: true,
  //   url: REST_URL.MAIN_UPLOADFILE.uploadFile,
  // });

  clickFileImage_mapFileList_modal() {
    let element: HTMLElement = document.getElementById(
      "fileImage_mapFileList_modal"
    ) as HTMLElement;
    element.click();

    // this.blockUI.stop(); // Stop blocking
  }

  fileOverBase_mapFileList_modal(event) {
    // console.log("event");
    // console.log(event);

    if (event) {
      this.ngxService.start();
      this.uploader_mapFileList_modal.getUploadListener().subscribe((res) => {
        this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

        let fileListImage = JSON.parse(res.respond.replace(/'/g, '"'));
        let filetype = fileListImage[0].split(".");
        if (filetype[1].toUpperCase() == "PDF") {
          filetype[1] = "P";
        } else if (
          filetype[1].toUpperCase() == "DOCX" ||
          filetype[1].toUpperCase() == "DOC"
        ) {
          filetype[1] = "W";
        } else if (
          filetype[1].toUpperCase() == "XLS" ||
          filetype[1].toUpperCase() == "XLSX"
        ) {
          filetype[1] = "E";
        }

        const fomatdata = {
          fileName: res.item.file.name,
          fileType: filetype[1],
          pathFile: fileListImage[0],
          fileNum: "",
          ShowFile:
            REST_URL.MAIN_UPLOADFILE.getFileTemp + "/" + fileListImage[0],

          // id: "0",
          // pathFile: fileListImage[0],
          // NameUrl:
          //   REST_URL.MAIN_UPLOADFILE.getFileTemp + "/" + fileListImage[0],
          // pathFileName: res.item.file.name,
          // Status: res.status,
          // type: filetype[1],
        };
        let listUrl = this.stationList.mapFileList.find((val) => {
          if (val.pathFile === fileListImage[0]) {
            return true;
          } else {
            return false;
          }
        });

        if (listUrl) {
          if (listUrl.pathFile !== fileListImage[0]) {
            this.stationList.mapFileList.push(fomatdata);
          }
        } else {
          this.stationList.mapFileList.push(fomatdata);
        }

        //console.log("this.stationList.mapFileList");
        //console.log(this.stationList.mapFileList);
      });
    }
  }

  public removeFiles_mapFileList_modal(PathUid, i) {
    this.stationList.mapFileList.splice(i, 1);
    this.delect_mapFileList_modal.push(PathUid);
  }

  // แผนที่ตั้ง

  // Modal 01

  // @ViewChild("address_modal")
  // public address_modal: TemplateRef<any>;
  // address_modal_reference: NgbModalRef;

  public getDrpListByPostCodeMain(postCode, type) {
    // console.log("postCode.length");
    // console.log(postCode.length);

    // drpListByPostCodeMain_: any[] = [];// สถานการค้าปลีก
    // typeData_modal: any = false; // สถานการค้าปลีก

    if (postCode.length == 5) {
      if (type == "data") {
        this.Type = type; // data, modal
        this.typeData = true;

        let d = {
          postCodeMain: postCode,
        };

        this.ngxService.start();

        this.addressService.getDrpListByPostCodeMain(d).subscribe((res) => {
          this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

          //console.log("res getDrpListByPostCodeMain");
          //console.log(res);

          if (res.result) {
            this.drpListByPostCodeMain = res.result;
          } else {
            this.drpListByPostCodeMain = [];
          }
        });
      } else {
        this.Type = type; // data, modal
        this.typeData_modal = true;

        let d = {
          postCodeMain: postCode,
        };

        this.ngxService.start();

        this.addressService.getDrpListByPostCodeMain(d).subscribe((res) => {
          this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

          //console.log("res getDrpListByPostCodeMain");
          //console.log(res);

          if (res.result) {
            this.drpListByPostCodeMain_ = res.result;
          } else {
            this.drpListByPostCodeMain_ = [];
          }

          // Open Modal
          // this.address_modal_reference = this.modalService.open(
          //   this.address_modal,
          //   {
          //     size: "lg",
          //   }
          // );
        });
      }
    }
  }

  // open_address_modal() {
  //   this.Type = "data"; // data, modal
  //   this.getDrpListByPostCodeMain(this.data.postcode);
  // }

  // open_address_stationList_modal() {
  //   this.Type = "modal"; // data, modal
  //   this.getDrpListByPostCodeMain(this.stationList.postcode);
  // }

  select_address_modal(item) {
    //console.log("this.Type");
    //console.log(this.Type);

    if (this.Type == "data") {
      this.typeData = false;
      // จังหวัด
      this.data.provinceId = item.provinceId;
      this.data.provinceName = item.provinceThai;
      // อำเภอ
      this.data.amphurId = item.districtId;
      this.data.amphurName = item.districtThai;
      // ตำบล
      this.data.tambonId = item.tambonId;
      this.data.tambonName = item.tambonThai;
    }

    if (this.Type == "modal") {
      this.typeData_modal = false;
      // จังหวัด
      this.stationList.provinceId = item.provinceId;
      this.stationList.provinceName = item.provinceThai;
      // อำเภอ
      this.stationList.amphurId = item.districtId;
      this.stationList.amphurName = item.districtThai;
      // ตำบล
      this.stationList.tambonId = item.tambonId;
      this.stationList.tambonName = item.tambonThai;
    }

    //this.address_modal_reference.close();
  }

  numberOnly(event): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  // เอกสารแนบสำเนาทะเบียนบ้าน
  // ImageUidPreviewUrl_houseFileList: any[] = [];
  // ImageUidPreview_houseFileList: any[] = [];
  // ImageUidPreviewUrlDelect_houseFileList: any[] = [];

  // uploader_houseFileList: FileUploader = new FileUploader({
  //   autoUpload: true,
  //   url: REST_URL.MAIN_UPLOADFILE.uploadFile,
  // });

  clickFileImage_houseFileList() {
    let element: HTMLElement = document.getElementById(
      "fileImage_houseFileList"
    ) as HTMLElement;
    element.click();

    // this.blockUI.stop(); // Stop blocking
  }

  fileOverBase_houseFileList(event) {
    // console.log("event");
    // console.log(event);

    if (event) {
      this.ngxService.start();
      this.uploader_houseFileList.getUploadListener().subscribe((res) => {
        this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

        // console.log("res uploader_houseFileList");
        // console.log(res);

        // console.log("this.data.houseFileList");
        // console.log(this.data.houseFileList);

        let fileListImage = JSON.parse(res.respond.replace(/'/g, '"'));
        let filetype = fileListImage[0].split(".");
        if (filetype[1].toUpperCase() == "PDF") {
          filetype[1] = "P";
        } else if (
          filetype[1].toUpperCase() == "DOCX" ||
          filetype[1].toUpperCase() == "DOC"
        ) {
          filetype[1] = "W";
        } else if (
          filetype[1].toUpperCase() == "XLS" ||
          filetype[1].toUpperCase() == "XLSX"
        ) {
          filetype[1] = "E";
        }

        const fomatdata = {
          fileName: res.item.file.name,
          fileType: filetype[1],
          pathFile: fileListImage[0],
          fileNum: "",
          ShowFile:
            REST_URL.MAIN_UPLOADFILE.getFileTemp + "/" + fileListImage[0],

          // id: "0",
          // pathFile: fileListImage[0],
          // NameUrl:
          //   REST_URL.MAIN_UPLOADFILE.getFileTemp + "/" + fileListImage[0],
          // pathFileName: res.item.file.name,
          // Status: res.status,
          // type: filetype[1],
        };
        let listUrl = this.data.houseFileList.find((val) => {
          if (val.pathFile === fileListImage[0]) {
            return true;
          } else {
            return false;
          }
        });

        if (listUrl) {
          if (listUrl.pathFile !== fileListImage[0]) {
            this.data.houseFileList.push(fomatdata);
          }
        } else {
          this.data.houseFileList.push(fomatdata);
        }

        //console.log("this.data.houseFileList");
        //console.log(this.data.houseFileList);
      });
    }
  }

  public removeFiles_houseFileList(PathUid, i) {
    this.data.houseFileList.splice(i, 1);
    this.delect_houseFileList.push(PathUid);
  }

  // เอกสารแนบสำเนาทะเบียนบ้าน

  // หนังสือมอบอำนาจและสำเนาบัตรประจำตัวประชาชนของผู้รับมอบอำนาจ
  // delect_agentFileList: any[] = [];

  // uploader_agentFileList: FileUploader = new FileUploader({
  //   autoUpload: true,
  //   url: REST_URL.MAIN_UPLOADFILE.uploadFile,
  // });

  clickFileImage_agentFileList() {
    let element: HTMLElement = document.getElementById(
      "fileImage_agentFileList"
    ) as HTMLElement;
    element.click();

    // this.blockUI.stop(); // Stop blocking
  }

  fileOverBase_agentFileList(event) {
    // console.log("event");
    // console.log(event);

    if (event) {
      this.ngxService.start();
      this.uploader_agentFileList.getUploadListener().subscribe((res) => {
        this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

        let fileListImage = JSON.parse(res.respond.replace(/'/g, '"'));
        let filetype = fileListImage[0].split(".");
        if (filetype[1].toUpperCase() == "PDF") {
          filetype[1] = "P";
        } else if (
          filetype[1].toUpperCase() == "DOCX" ||
          filetype[1].toUpperCase() == "DOC"
        ) {
          filetype[1] = "W";
        } else if (
          filetype[1].toUpperCase() == "XLS" ||
          filetype[1].toUpperCase() == "XLSX"
        ) {
          filetype[1] = "E";
        }

        const fomatdata = {
          fileName: res.item.file.name,
          fileType: filetype[1],
          pathFile: fileListImage[0],
          fileNum: "",
          ShowFile:
            REST_URL.MAIN_UPLOADFILE.getFileTemp + "/" + fileListImage[0],
        };
        let listUrl = this.data.agentFileList.find((val) => {
          if (val.pathFile === fileListImage[0]) {
            return true;
          } else {
            return false;
          }
        });

        if (listUrl) {
          if (listUrl.pathFile !== fileListImage[0]) {
            this.data.agentFileList.push(fomatdata);
          }
        } else {
          this.data.agentFileList.push(fomatdata);
        }

        // console.log("this.data.agentFileList");
        // console.log(this.data.agentFileList);
      });
    }
  }

  public removeFiles_agentFileList(PathUid, i) {
    this.data.agentFileList.splice(i, 1);
    this.delect_agentFileList.push(PathUid);
  }

  // หนังสือมอบอำนาจและสำเนาบัตรประจำตัวประชาชนของผู้รับมอบอำนาจ

  // สำเนาหนังสือรับรองของกระทรวจพาณิชย์ (ที่ออกให้ไม่เกิน 6 เดือน)
  // delect_cerFileList: any[] = [];

  // uploader_cerFileList: FileUploader = new FileUploader({
  //   autoUpload: true,
  //   url: REST_URL.MAIN_UPLOADFILE.uploadFile,
  // });

  clickFileImage_cerFileList() {
    let element: HTMLElement = document.getElementById(
      "fileImage_cerFileList"
    ) as HTMLElement;
    element.click();

    // this.blockUI.stop(); // Stop blocking
  }

  fileOverBase_cerFileList(event) {
    // console.log("event");
    // console.log(event);

    if (event) {
      this.ngxService.start();

      this.uploader_cerFileList.getUploadListener().subscribe((res) => {
        this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

        let fileListImage = JSON.parse(res.respond.replace(/'/g, '"'));
        let filetype = fileListImage[0].split(".");
        if (filetype[1].toUpperCase() == "PDF") {
          filetype[1] = "P";
        } else if (
          filetype[1].toUpperCase() == "DOCX" ||
          filetype[1].toUpperCase() == "DOC"
        ) {
          filetype[1] = "W";
        } else if (
          filetype[1].toUpperCase() == "XLS" ||
          filetype[1].toUpperCase() == "XLSX"
        ) {
          filetype[1] = "E";
        }

        const fomatdata = {
          fileName: res.item.file.name,
          fileType: filetype[1],
          pathFile: fileListImage[0],
          fileNum: "",
          ShowFile:
            REST_URL.MAIN_UPLOADFILE.getFileTemp + "/" + fileListImage[0],
        };
        let listUrl = this.data.cerFileList.find((val) => {
          if (val.pathFile === fileListImage[0]) {
            return true;
          } else {
            return false;
          }
        });

        if (listUrl) {
          if (listUrl.pathFile !== fileListImage[0]) {
            this.data.cerFileList.push(fomatdata);
          }
        } else {
          this.data.cerFileList.push(fomatdata);
        }

        // console.log("this.data.cerFileList");
        // console.log(this.data.cerFileList);
      });
    }
  }

  public removeFiles_cerFileList(PathUid, i) {
    console.log("removeFiles_cerFileList")
    this.data.cerFileList.splice(i, 1);
    this.delect_cerFileList.push(PathUid);
  }

  // สำเนาหนังสือรับรองของกระทรวจพาณิชย์ (ที่ออกให้ไม่เกิน 6 เดือน)

  // ประเภทธุรกิจ
  public getDrpBusinessTypeList() {
    this.businessTypeService.getDrpBusinessTypeList().subscribe((res) => {
      if (res.result) {
        res.result.map((item) => {
          item.selected = false;
          return item;
        });
        this.drpbusinessType = res.result;

        //console.log("res this.drpbusinessType ");
        //console.log(this.drpbusinessType);
      } else {
        this.drpbusinessType = [];
      }
    });
  }

  // ประเภทธุรกิจ
  public getCustomerTypeDrpList() {
    this.customerTypeService.getCustomerTypeDrpList().subscribe((res) => {
      //console.log("res getCustomerTypeDrpList");
      //console.log(res);

      if (res.result) {
        this.drpcustomerType = res.result;

        if (this.id == "0") {
          this.data.customerType = "บุคคลธรรมดา";
        }
      } else {
        this.drpcustomerType = [];
      }
    });
  }
}
