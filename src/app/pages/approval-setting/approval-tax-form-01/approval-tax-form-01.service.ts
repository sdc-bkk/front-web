import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import { Frontaxform01Api } from "../../../api/fronttaxform01-api";

@Injectable()
export class ApprovalTaxForm01Service {
  constructor(private frontaxform01Api: Frontaxform01Api) {}

  getList(data): Observable<any> {
    return this.frontaxform01Api.getList(data);
  }

  getData(data): Observable<any> {
    return this.frontaxform01Api.getData(data);
  }

  saveData(data): Observable<any> {
    return this.frontaxform01Api.saveData(data);
  }

  getDataStation(data): Observable<any> {
    return this.frontaxform01Api.getDataStation(data);
  }

  deleteData(data): Observable<any> {
    return this.frontaxform01Api.deleteData(data);
  }

  sendApprove(data): Observable<any> {
    return this.frontaxform01Api.sendApprove(data);
  }

  getDrpListRetail(data): Observable<any> {
    return this.frontaxform01Api.getDrpListRetail(data);
  }

  getDrpListStation(data): Observable<any> {
    return this.frontaxform01Api.getDrpListStation(data);
  }

  getListStatusLog(data): Observable<any> {
    return this.frontaxform01Api.getListStatusLog(data);
  }

  getDrpListStatus(): Observable<any> {
    return this.frontaxform01Api.getDrpListStatus();
  }
}
