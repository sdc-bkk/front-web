import { Routes, RouterModule } from "@angular/router";
import { ApprovalTaxForm01ListComponent } from "./approval-tax-form-01-list/approval-tax-form-01-list.component";
import { ApprovalTaxForm01UpdateComponent } from "./approval-tax-form-01-update/approval-tax-form-01-update.component";
import { ApprovalTaxForm01ViewComponent } from "./approval-tax-form-01-view/approval-tax-form-01-view.component";
import { ApprovalTaxForm01Component } from "./approval-tax-form-01.component";

const TEMPLATE_ROUTES: Routes = [
  {
    path: "",
    component: ApprovalTaxForm01Component,
    children: [
      {
        path: "",
        component: ApprovalTaxForm01ListComponent,
      },
      {
        path: "update/:id",
        component: ApprovalTaxForm01UpdateComponent,
      },
      {
        path: "view/:id",
        component: ApprovalTaxForm01ViewComponent,
      },
    ],
  },

  // 404 Page Not Found
  { path: "**", redirectTo: "/" },
];

export const ApprovalTaxForm01Routes = RouterModule.forChild(TEMPLATE_ROUTES);
