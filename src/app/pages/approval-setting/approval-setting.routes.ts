import { Routes, RouterModule } from "@angular/router";
import { ApprovalSettingComponent } from "./approval-setting.component";

const ROUTES: Routes = [
  {
    path: "",
    component: ApprovalSettingComponent,
    children: [
      // - อนุมัติ ภน.01
      {
        path: "approval-tax-form-01",
        loadChildren: () =>
          import("./approval-tax-form-01/approval-tax-form-01.module").then(
            (m) => m.ApprovalTaxForm01Module
          ),
      },
      // - อนุมัติ ภน.02
      {
        path: "approval-tax-form-02",
        loadChildren: () =>
          import("./approval-tax-form-02/approval-tax-form-02.module").then(
            (m) => m.ApprovalTaxForm02Module
          ),
      },

      // - อนุมัติ ภน.03
      {
        path: "approval-tax-form-03",
        loadChildren: () =>
          import("./approval-tax-form-03/approval-tax-form-03.module").then(
            (m) => m.ApprovalTaxForm03Module
          ),
      },

      {
        path: "request-refund",
        loadChildren: () =>
          import("./request-refund/request-refund.module").then((m) => m.RequestRefundModule),
      },

      {
        path: "company",
        loadChildren: () =>
          import("./company/company.module").then((m) => m.CompanyModule),
      },

      {
        path: "payment-tax-oil",
        loadChildren: () =>
          import("./payment-tax-oil/payment-tax-oil.module").then((m) => m.PaymentTaxOilModule),
      },

      {
        path: "history-payment-tax-oil",
        loadChildren: () =>
          import("./history-payment-tax-oil/history-payment-tax-oil.module").then((m) => m.HistoryPaymentTaxOilModule),
      },

      {
        path: "",
        redirectTo: "fontOffice/approval-setting/approval-tax-form-01",
      },
    ],
  },

  // 404 Page Not Found
  { path: "**", redirectTo: "/" },
];

export const ApprovalSettingRoutes = RouterModule.forChild(ROUTES);
