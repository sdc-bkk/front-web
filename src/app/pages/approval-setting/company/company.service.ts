import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import { Frontaxform03Api } from "../../../api/fronttaxform03-api";
import { FrontretailApi } from "../../../api/frontretail-api";

@Injectable()
export class CompanyService {
  constructor(private frontretailApi: FrontretailApi) {}

  getListStationByTaxNo(data): Observable<any> {
    return this.frontretailApi.getListStationByTaxNo(data);
  }
}
