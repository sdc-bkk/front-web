import { Routes, RouterModule } from "@angular/router";
import { CompanyListComponent } from "./company-list/company-list.component";
import { CompanyComponent } from "./company.component";

const TEMPLATE_ROUTES: Routes = [
  {
    path: "",
    component: CompanyComponent,
    children: [
      {
        path: "",
        component: CompanyListComponent,
      },
    ],
  },

  // 404 Page Not Found
  { path: "**", redirectTo: "/" },
];

export const CompanyRoutes = RouterModule.forChild(TEMPLATE_ROUTES);
