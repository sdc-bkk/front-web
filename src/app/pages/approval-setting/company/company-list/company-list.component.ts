import { ViewChild, TemplateRef } from "@angular/core";
import { Component, ViewEncapsulation, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Subject } from "rxjs";
import { finalize, reduce, takeUntil } from "rxjs/operators";
import { ConfigService } from "../../../../shared/services/config/config.service";
import { DbMenuService } from "../../../../shared/localstorage/db-menu-service";
import Swal from "sweetalert2";
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";
import { FileUploader } from "../../../../shared/components/file-upload/file-uploader.class";
import { REST_URL } from "../../../../shared/services/config/rest-url";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { DbUserLocalstorage } from "../../../../shared/services/localstorage/db-user-localstorage";
import { CompanyService } from "../company.service";

@Component({
  selector: ".company-list-inner-wrapper",
  templateUrl: "./company-list.component.html",
  styleUrls: ["./company-list.component.scss"],
  encapsulation: ViewEncapsulation.Emulated,
})
export class CompanyListComponent implements OnInit {
  /////////////////// Config ///////////////////
  public allExpandState = false;
  panelOpenState: boolean = false;
  allRowsSelected: boolean;
  selectedCheck = 0;
  isSearchActive: boolean = false;
  ///////////////// End Config /////////////////

  ///////////////// Variable /////////////////
  public model: any = {};
  public loading = false;
  private unsubscribeAll = new Subject();
  /////////////// End Variable ////////////////

  //////////////// Value /////////////////////
  dataList: any[] = [];
  dataListTemp: any[] = [];
  maxitemPerPage: number = this.config.paging.ITEMPERPAGE;
  maxSize: number = this.config.paging.MAXSIZE;
  totalItemdataList: number;
  searchValue = {
    stationName: "",
    officeName: ""
  };
  checkAll: any;
  check_agree: any = false;
  public currentMenuDataBase: any;

  resultPage: any = {
    pageNo: 1,
    totalItem: 0,
    itemPerPage: 0,
    collectionSize: 0,
  };

  loader: any;

  currentUser: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public config: ConfigService,
    private dbUserLocalstorage: DbUserLocalstorage,
    private dbMenuService: DbMenuService,
    private modalService: NgbModal,
    public ngxService: NgxUiLoaderService,
    public companyService: CompanyService
  ) { }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem("ba_currentUser"));

    this.getList(1);
  }

  ngOnDestroy() { }

  public searchActive() {
    if (this.isSearchActive == false) {
      this.isSearchActive = true;
    } else {
      this.isSearchActive = false;
    }
    this.config.setIsSearchActive(this.isSearchActive);
  }
  public triggerClose(event) {
    this.isSearchActive = !this.isSearchActive;
  }

  public refreshDataSearch() {
    this.searchValue = {
      stationName: "",
      officeName: "",
    };

    this.config.searchValue = this.searchValue;
    this.getList(1);
  }


  public getList(page) {
    let data = {
      page: page,
      itemPerPage: this.config.paging.ITEMPERPAGE,
      orderBy: "createdDate",
      sort: "desc",

      stationName: this.searchValue.stationName,
      officeName: this.searchValue.officeName,
      memberName: this.currentUser.memberName
    };

    console.log("data");
    console.log(data);

    this.config.resultPage.pageNo = page;

    this.ngxService.start();

    this.companyService.getListStationByTaxNo(data).subscribe((res) => {
      console.log("res approvalTaxForm03Service  getList");
      console.log(res);

      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

      if (res.result) {

        let selected = 0;
        res.result.map((item) => {

          if (item.updatedDate != "") {
            item.createdDate = item.updatedDate
          }

          let dataList = this.dataListTemp.find(
            (temp) => item.yearly == temp.yearly
          );
          if (dataList) {
            selected++;
            item.selected = true;
          } else {
            item.selected = false;
          }
          return item;
        });

        this.dataList = res.result;
        this.totalItemdataList = res.resultPage.totalItem;
        this.config.resultPage.totalItem = res.resultPage.totalItem;

        if (selected == res.result.length && selected > 0) {
          this.allRowsSelected = true;
        } else {
          this.allRowsSelected = false;
        }

        if (res.result.lengt == 0) {
          this.allRowsSelected = false;
        }

        this.resultPage.totalItem = res.resultPage.totalItem;
        if (this.totalItemdataList < selected) {
          this.selectedCheck = res.resultPage.totalItem;
        } else {
          this.selectedCheck = this.dataListTemp.length;
        }
      } else {
        this.dataList = [];
        this.totalItemdataList = 0;
        this.config.resultPage.totalItem = 0;
        this.selectedCheck = 0;
      }
    });
  }
}
