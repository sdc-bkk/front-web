import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { NgxsModule } from "@ngxs/store";
import { SharedModule } from "../../../shared/shared.module";
import { DbUserLocalstorage } from "../../../shared/services/localstorage/db-user-localstorage";
import { AddressApi } from "../../../api/address-api";
import { BusinessStatusApi } from "../../../api/business-status-api";
import { Frontaxform01Api } from "../../../api/fronttaxform01-api";
import { Frontaxform02Api } from "../../../api/fronttaxform02-api";
import { RoadApi } from "../../../api/road-api";
import { SoiApi } from "../../../api/soi-api";
import { AddressService } from "../../address.service";
import { BusinessStatusService } from "../../business-status.service";
import { RoadService } from "../../road.service";
import { SoiService } from "../../soi.service";
import { ApprovalTaxForm01Service } from "../approval-tax-form-01/approval-tax-form-01.service";
import { ApprovalTaxForm02Service } from "../approval-tax-form-02/approval-tax-form-02.service";
import { OilTypeApi } from "../../../api/oil-type-api";
import { OilTypeService } from "../../oil-type.service";
import { Unity } from "../../Unity";
import { UploadApi } from "../../../api/upload-api";
import { UploadService } from "../../upload.service";
import { FrontretailApi } from "../../../api/frontretail-api";
import { FrontretailService } from "../../frontretail.service";
import { Frontaxform03Api } from "../../../api/fronttaxform03-api";
import { CompanyService } from "./company.service";
import { CompanyRoutes } from "./company.routes";
import { CompanyComponent } from "./company.component";
import { CompanyListComponent } from "./company-list/company-list.component";

@NgModule({
  declarations: [CompanyComponent, CompanyListComponent],
  providers: [
    Frontaxform01Api,
    ApprovalTaxForm01Service,
    BusinessStatusService,
    BusinessStatusApi,
    AddressService,
    AddressApi,
    RoadService,
    RoadApi,
    SoiService,
    SoiApi,
    OilTypeService,
    OilTypeApi,
    Frontaxform02Api,
    ApprovalTaxForm02Service,
    Frontaxform03Api,
    CompanyService,
    DbUserLocalstorage,
    Unity,
    UploadApi,
    UploadService,
    FrontretailApi,
    FrontretailService,
  ],
  imports: [CommonModule, SharedModule, CompanyRoutes],
})
export class CompanyModule {}
