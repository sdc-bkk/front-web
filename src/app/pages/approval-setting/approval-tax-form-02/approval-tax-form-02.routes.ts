import { Routes, RouterModule } from "@angular/router";
import { ApprovalTaxForm02ListComponent } from "./approval-tax-form-02-list/approval-tax-form-02-list.component";
import { ApprovalTaxForm02UpdateComponent } from "./approval-tax-form-02-update/approval-tax-form-02-update.component";
import { ApprovalTaxForm02ViewComponent } from "./approval-tax-form-02-view/approval-tax-form-02-view.component";
import { ApprovalTaxForm02Component } from "./approval-tax-form-02.component";

const TEMPLATE_ROUTES: Routes = [
  {
    path: "",
    component: ApprovalTaxForm02Component,
    children: [
      {
        path: "",
        component: ApprovalTaxForm02ListComponent,
      },
      {
        path: "update/:id",
        component: ApprovalTaxForm02UpdateComponent,
      },
      {
        path: "view/:id",
        component: ApprovalTaxForm02ViewComponent,
      },
    ],
  },

  // 404 Page Not Found
  { path: "**", redirectTo: "/" },
];

export const ApprovalTaxForm02Routes = RouterModule.forChild(TEMPLATE_ROUTES);
