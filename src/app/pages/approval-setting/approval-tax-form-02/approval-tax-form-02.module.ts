import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { NgxsModule } from "@ngxs/store";
import { AddressApi } from "../../../api/address-api";
import { BusinessStatusApi } from "../../../api/business-status-api";
import { FrontretailApi } from "../../../api/frontretail-api";
import { Frontaxform01Api } from "../../../api/fronttaxform01-api";
import { Frontaxform02Api } from "../../../api/fronttaxform02-api";
import { RoadApi } from "../../../api/road-api";
import { SoiApi } from "../../../api/soi-api";
import { UploadApi } from "../../../api/upload-api";
import { SharedModule } from "../../../shared/shared.module";
import { AddressService } from "../../address.service";
import { BusinessStatusService } from "../../business-status.service";
import { FrontretailService } from "../../frontretail.service";
import { RoadService } from "../../road.service";
import { SoiService } from "../../soi.service";
import { UploadService } from "../../upload.service";
import { ApprovalTaxForm01Service } from "../approval-tax-form-01/approval-tax-form-01.service";
import { ApprovalTaxForm02ListComponent } from "./approval-tax-form-02-list/approval-tax-form-02-list.component";
import { ApprovalTaxForm02UpdateComponent } from "./approval-tax-form-02-update/approval-tax-form-02-update.component";
import { ApprovalTaxForm02ViewComponent } from "./approval-tax-form-02-view/approval-tax-form-02-view.component";
import { ApprovalTaxForm02Component } from "./approval-tax-form-02.component";
import { ApprovalTaxForm02Routes } from "./approval-tax-form-02.routes";
import { ApprovalTaxForm02Service } from "./approval-tax-form-02.service";

@NgModule({
  declarations: [
    ApprovalTaxForm02Component,
    ApprovalTaxForm02ListComponent,
    ApprovalTaxForm02UpdateComponent,
    ApprovalTaxForm02ViewComponent,
  ],
  providers: [
    Frontaxform01Api,
    ApprovalTaxForm01Service,
    BusinessStatusService,
    BusinessStatusApi,
    AddressService,
    AddressApi,
    RoadService,
    RoadApi,
    SoiService,
    SoiApi,
    Frontaxform02Api,
    ApprovalTaxForm02Service,
    UploadApi,
    UploadService,
    FrontretailApi,
    FrontretailService,
  ],
  imports: [CommonModule, SharedModule, ApprovalTaxForm02Routes],
})
export class ApprovalTaxForm02Module {}
