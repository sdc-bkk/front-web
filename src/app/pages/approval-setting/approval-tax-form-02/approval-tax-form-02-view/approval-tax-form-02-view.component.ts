import {
  Component,
  ViewEncapsulation,
  OnInit,
  TemplateRef,
  ViewChild,
} from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Observable, Subject } from "rxjs/Rx";
import { finalize, map, startWith, takeUntil } from "rxjs/operators";
import { ConfigService } from "../../../../shared/services/config/config.service";
// import createNumberMask from 'text-mask-addons/dist/createNumberMask';
import Utils from "../../../../shared/util/utils";
import {
  NgbModal,
  NgbModalConfig,
  NgbModalRef,
} from "@ng-bootstrap/ng-bootstrap";
import Swal from "sweetalert2";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { ApprovalTaxForm01Service } from "../../approval-tax-form-01/approval-tax-form-01.service";
import { ApprovalTaxForm02Service } from "../approval-tax-form-02.service";
import { BusinessStatusService } from "../../../business-status.service";
import { FormControl } from "@angular/forms";
import { AddressService } from "../../../address.service";
import { RoadService } from "../../../road.service";
import { SoiService } from "../../../soi.service";
import { FileUploader } from "../../../../shared/components/file-upload/file-uploader.class";
import { REST_URL } from "../../../../shared/services/config/rest-url";
import { UploadService } from "../../../upload.service";
import { FrontretailService } from "../../../frontretail.service";
import createNumberMask from "text-mask-addons/dist/createNumberMask";

@Component({
  selector: ".approval-tax-form-02-view-inner-wrapper",
  templateUrl: "./approval-tax-form-02-view.component.html",
  styleUrls: ["./approval-tax-form-02-view.component.scss"],
  encapsulation: ViewEncapsulation.Emulated,
})
export class ApprovalTaxForm02ViewComponent implements OnInit {
  public currencyMask = createNumberMask({
    prefix: "",
    suffix: "",
    includeThousandsSeparator: true,
    thousandsSeparatorSymbol: ",",
    allowDecimal: true,
    decimalSymbol: ".",
    decimalLimit: 2,
    integerLimit: null,
    requireDecimal: false,
    allowNegative: false,
    allowLeadingZeroes: false,
  });

  /////////////// End Variable ////////////////

  id: any = "0";
  dataList: any[] = [];
  private unsubscribeAll = new Subject();
  //////////////// Value /////////////////////
  data = {
    businessStatus: "",
    taxForm02Id: "0",
    refRetailId: "",
    refStationId: "",
    docNo: "",
    docDate: "",
    forType: "",
    businessStatusId: "",
    businessStatusCode: "",
    businessStatusName: "",

    // เลิกกิจการ
    cancelDate: "",
    // เลิกกิจการ
    // ย้ายสถานที่ตั้งสถานการค้าปลีก
    changeDate: "", // ตั้งแต่วันที่
    addressOld: "", // ที่อยู่เดิม
    address: "", // ที่อยู่ใหม่
    houseNo: "",
    soi: "", // ซอย
    road: "", // ถนน
    tambonId: "", // ตำบลไอดี
    tambonName: "", // ตำบลชื่อ
    amphurId: "", // อำเภอชื่อ
    amphurName: "", // อำเภอชื่อ
    provinceId: "", // จังหวัดชื่อ
    provinceName: "", // จังหวัดชื่อ
    postcode: "", // รหัสไปรษณีย์
    mobileOld: "", // เบอร์โทรศัพท์เก่า
    mobile: "", // เบอร์โทรศัพท์
    stationCode: "", // สถานการค้ารหัส
    stationNameOld: "", // สถานการค้าปลีกเก่า
    stationName: "", // สถานการค้าชื่อ
    ownerNameOld: "", // สถานกาค้าปลีกเก่า
    ownerName: "", // สถานกาค้าปลีก
    taxNoOld: "", // รหัสผู้เสียภาษีอากรเก่า
    taxNo: "", // รหัสผู้เสียภาษีอากรใหม่
    customerTypeOld: "", // รหัสพนักงานเก่า
    customerType: "", // รหัสประเภทพนักงาน
    idCardOld: "", // ประจำตัวประชาชนเก่า
    idCard: "", // ประจำตัวประชาชน
    idCardAtOld: "", // ออก ณ ที่ เก่า
    idCardAt: "", // ออก ณ ที่
    corpNoOld: "", // เลขทะเบียนนิติบุคคลที่ เก่า
    corpNo: "", // เลขทะเบียนนิติบุคคลที่
    corpDateOld: "", // เมื่อวันที่ เก่า
    corpDate: "", // เมื่อวันที่ ใหม่
    other: "", // อื่นๆ
    taxRemain: "", // ภาษีค้างชำระตามบัญชีแนบ
    taxOilGas: "", // น้ำมัน/ก๊าซปิโตรเลียมที่ยังไม่ได้ชำระภาษีตามบัญชีแนบ
    status: "",
    statusName: "",
    fileNumE: "", // สำเนาหนังสือรับรองของกระทรวงพาณิชย์
    fileNumA: "",
    fileNumR: "",
    fileNumO: "",
    active: true,
    addressRetail: "",
    soiRetail: "",
    roadRetail: "",
    tambonIdRetail: "",
    tambonNameRetail: "",
    amphurIdRetail: "",
    amphurNameRetail: "",
    provinceIdRetail: "",
    provinceNameRetail: "",
    postcodeRetail: "",
    mobileRetail: "",
    cause: "",

    businessStatusList: [
      // {
      //   id: "upyVEXl6pVTiOUJxmMEcvA",
      //   name: "เลิกกิจการ",
      //   code: "ล",
      //   other: "A",
      // },
      // {
      //   id: "REG6Yo-twQ0m1-CqsbLt9g",
      //   name: "ย้ายสถานที่ตั้งสถานการค้าปลีก",
      //   code: "ต",
      //   other: "B",
      // },
    ],

    commerceFileList: [],
    authorizeFileList: [],
    taxRemainFileList: [],
    otherFileList: [],
  };

  // + -
  panelOpenState = true;
  panelOpenState_1 = true;

  drpListStation: any = []; // ชื่อสถานการค้าปลีก

  drpBusinessStatusList: any = []; // ความประสงค์

  drpListSoi: any[] = []; // ซอย
  myControl_Soi = new FormControl();
  options_Soi: any[] = [];
  filteredOptions_Soi: Observable<string[]>;

  drpListRoad: any[] = []; // ถนน
  myControl_Road = new FormControl();
  options_Road: any[] = [];
  filteredOptions_Road: Observable<string[]>;

  drpListByPostCodeMain: any[] = []; // ผู้ประกอบการ
  typeData: any = false; // ผู้ประกอบการ

  // สำเนาหนังสือรับรองของกระทรวงพาณิชย์
  delect_commerceFileList: any[] = [];

  uploader_commerceFileList: FileUploader = new FileUploader({
    autoUpload: true,
    url: REST_URL.MAIN_UPLOADFILE.uploadFile,
  });
  // สำเนาหนังสือรับรองของกระทรวงพาณิชย์

  // หนังสือมอบอำนาจ
  delect_authorizeFileList: any[] = [];

  uploader_authorizeFileList: FileUploader = new FileUploader({
    autoUpload: true,
    url: REST_URL.MAIN_UPLOADFILE.uploadFile,
  });
  // หนังสือมอบอำนาจ

  // บัญชีรายการค้างชำระภาษี
  delect_taxRemainFileList: any[] = [];

  uploader_taxRemainFileList: FileUploader = new FileUploader({
    autoUpload: true,
    url: REST_URL.MAIN_UPLOADFILE.uploadFile,
  });
  // บัญชีรายการค้างชำระภาษี

  // เอกสารอื่น ๆ
  delect_otherFileList: any[] = [];

  uploader_otherFileList: FileUploader = new FileUploader({
    autoUpload: true,
    url: REST_URL.MAIN_UPLOADFILE.uploadFile,
  });
  // เอกสารอื่น ๆ

  currentUser: any;

  getDataByMemberNameStatus: any = false;

  taxNoValidation: any = false;

  data_getDataByMemberName = {
    active: true,
    address: "",

    agentFileNum: "",

    amphurId: "",
    amphurName: "",

    cerFileNum: "",

    corpDate: "",
    corpNo: "",
    createdBy: "",
    createdDate: "",
    customerType: "",
    email: "",
    firstRecord: true,
    fullAddress: "",

    houseFileNum: "",

    houseNo: "",
    idCard: "",
    idCardAt: "",
    mainStatus: "",
    mainStatusName: "",
    mobile: "",
    ownerName: "",
    postcode: "",

    provinceId: "",
    provinceName: "",

    retailId: "",
    road: "",
    soi: "",

    tambonId: "",
    tambonName: "",

    taxNo: "",
    updatedBy: "",
    updatedDate: "",
  };

  drpListStationByTaxNo: any = [];

  stationList = {
    taxForm01StationId: "0",
    taxForm01RetailId: "",
    refStationId: "",
    stationCode: "",
    stationName: "",
    houseNo: "",
    address: "",
    addressfull: "",
    soi: "",
    road: "",
    tambonId: "",
    tambonName: "",
    amphurId: "",
    amphurName: "",
    provinceId: "10",
    provinceName: "กรุงเทพมหานคร",
    postcode: "",
    mobile: "",
    email: "",
    status: "",
    statusName: "",
    houseFileNum: "",
    mapFileNum: "",
    businessTypeList: [
      // {
      //     "id": "upyVEXl6pVTiOUJxmMEcvA",
      //     "name": "น้ำมันเชื้อเพลิง"
      // },
      // {
      //     "id": "wFlYj87tnX307XVrn9r6kw",
      //     "name": "ปิโตรเลียม"
      // }
    ],
    // สำเนาทะเบียนบ้านสำเนาบัตรประจำตัวประชาชนและสำเนาบัตรประจำตัวผู้เสียภาษีอาการ
    houseFileList: [
      // {
      //     "fileName": "Cover.jpg",
      //     "fileType": "I",
      //     "pathFile": "14158953-f5fe-4a66-a680-14aa099da5a5.jpg"
      // }
    ],
    // แผนที่ตั้ง
    mapFileList: [
      // {
      //     "fileName": "Cover.jpg",
      //     "fileType": "I",
      //     "pathFile": "14158953-f5fe-4a66-a680-14aa099da5a5.jpg"
      // }
    ],
  };

  _refRetailId: any = "";

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private configModal: NgbModalConfig,
    public config: ConfigService,
    private modalService: NgbModal,
    public ngxService: NgxUiLoaderService,
    private addressService: AddressService,
    private soiService: SoiService,
    private roadService: RoadService,

    private approvalTaxForm01Service: ApprovalTaxForm01Service,
    private businessStatusService: BusinessStatusService,
    private approvalTaxForm02Service: ApprovalTaxForm02Service,
    private uploadService: UploadService,
    private frontretailService: FrontretailService
  ) {
    configModal.backdrop = "static";
  }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem("ba_currentUser"));
    this.id = this.route.snapshot.params["id"] || "0";

    this.getDataByMemberName();
    if (this.id != "0") {
      this.getData(this.id);
    }
  }

  public getDataByMemberName() {
    const payload = {
      memberName: this.currentUser.memberName,
    };

    this.ngxService.start();

    this.frontretailService.getDataByMemberName(payload).subscribe((res) => {
      // console.log("res getDataByMemberName");
      // console.log(res);
      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

      //this.getDataByMemberNameStatus = res.status;

      if (res.status == "false") {
        //this.data.firstRecord = false;
        this.getDataByMemberNameStatus = false;

        this.data_getDataByMemberName = {
          active: true,
          address: "",

          agentFileNum: "",

          amphurId: "",
          amphurName: "",

          cerFileNum: "",

          corpDate: "",
          corpNo: "",
          createdBy: "",
          createdDate: "",
          customerType: "",
          email: "",
          firstRecord: true,
          fullAddress: "",

          houseFileNum: "",

          houseNo: "",
          idCard: "",
          idCardAt: "",
          mainStatus: "",
          mainStatusName: "",
          mobile: "",
          ownerName: "",
          postcode: "",

          provinceId: "",
          provinceName: "",

          retailId: "",
          road: "",
          soi: "",

          tambonId: "",
          tambonName: "",

          taxNo: "",
          updatedBy: "",
          updatedDate: "",
        };
      } else {
        res.result.firstRecord = true;
        this.getDataByMemberNameStatus = true;

        //this.setForm(res.result);

        this._refRetailId = res.result.retailId;
        this.data.refRetailId = this._refRetailId;

        res.result.amphurName =
          res.result.amphurName == undefined ? "" : res.result.amphurName;
        res.result.tambonName =
          res.result.tambonName == undefined ? "" : res.result.tambonName;
        res.result.provinceName =
          res.result.provinceName == undefined ? "" : res.result.provinceName;

        this.data_getDataByMemberName = res.result;

        this.getListStationByTaxNo(this.data_getDataByMemberName.taxNo);

        // console.log("this.data_getDataByMemberName");
        // console.log(this.data_getDataByMemberName);
      }
    });
  }

  public getListStationByTaxNo(item) {
    const payload = {
      taxNo: item,
    };

    this.ngxService.start();

    this.frontretailService.getListStationByTaxNo(payload).subscribe((res) => {
      // console.log("res getListStationByTaxNo");
      // console.log(res);
      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
      if (res.result) {
        this.drpListStationByTaxNo = res.result;
      } else {
        this.drpListStationByTaxNo = [];
      }

      //this.getDataByMemberNameStatus = res.status;
    });
  }

  ngOnDestroy() {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }

  public getData(item) {
    const payload = {
      taxForm02Id: item,
    };

    this.ngxService.start();

    this.approvalTaxForm02Service.getData(payload).subscribe((res) => {
      console.log("res approvalTaxForm02Service getData");
      console.log(res);

      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

      if (res.result) {
        this.setForm(res.result);
      }
    });
  }

  setForm(res: any) {
    console.log("res setForm");
    console.log(res);

    // console.log("this.drpBusinessStatusList");
    // console.log(this.drpBusinessStatusList);

    let commerceFileList = [];
    // สำเนาหนังสือรับรองของกระทรวงพาณิชย์
    if (res.commerceFileList != undefined) {
      res.commerceFileList.forEach((element) => {
        let m = {
          id: element.contentId,
          pathFile: element.pathFile,
          ShowFile:
            REST_URL.MAIN_UPLOADFILE.getFile +
            "/" +
            element.pathFile +
            "/" +
            "taxform02",
          pathFileName: element.fileName,
          Status: 200,
          type: element.fileType,
        };

        commerceFileList.push(m);
      });
    }
    // สำเนาหนังสือรับรองของกระทรวงพาณิชย์

    let authorizeFileList = [];
    // หนังสือมอบอำนาจ

    if (res.authorizeFileList != undefined) {
      res.authorizeFileList.forEach((element) => {
        let m = {
          id: element.contentId,
          pathFile: element.pathFile,
          ShowFile:
            REST_URL.MAIN_UPLOADFILE.getFile +
            "/" +
            element.pathFile +
            "/" +
            "taxform02",
          pathFileName: element.fileName,
          Status: 200,
          type: element.fileType,
        };

        authorizeFileList.push(m);
      });
    }
    // หนังสือมอบอำนาจ

    let taxRemainFileList = [];
    // บัญชีรายการค้างชำระภาษี

    if (res.taxRemainFileList != undefined) {
      res.taxRemainFileList.forEach((element) => {
        let m = {
          id: element.contentId,
          pathFile: element.pathFile,
          ShowFile:
            REST_URL.MAIN_UPLOADFILE.getFile +
            "/" +
            element.pathFile +
            "/" +
            "taxform02",
          pathFileName: element.fileName,
          Status: 200,
          type: element.fileType,
        };

        taxRemainFileList.push(m);
      });
    }
    // บัญชีรายการค้างชำระภาษี

    let otherFileList = [];
    // เอกสารอื่น ๆ

    if (res.otherFileList != undefined) {
      res.otherFileList.forEach((element) => {
        let m = {
          id: element.contentId,
          pathFile: element.pathFile,
          ShowFile:
            REST_URL.MAIN_UPLOADFILE.getFile +
            "/" +
            element.pathFile +
            "/" +
            "taxform02",
          pathFileName: element.fileName,
          Status: 200,
          type: element.fileType,
        };

        otherFileList.push(m);
      });
    }

    // เอกสารอื่น ๆ
    let businessStatus = "";
    res.businessStatusList.forEach((e, index) => {
      if (index == 0) {
        businessStatus = e.name;
      } else {
        businessStatus = businessStatus + ", " + e.name;
      }
    });

    this.data = {
      businessStatus: businessStatus,

      taxForm02Id: res.taxForm02Id,
      refRetailId: res.refRetailId,
      refStationId: res.refStationId,
      docNo: res.docNo,
      docDate: res.docDate,
      forType: res.forType,
      businessStatusId: res.businessStatusId,
      businessStatusCode: res.businessStatusCode,
      businessStatusName: res.businessStatusName,

      // เลิกกิจการ
      cancelDate: res.cancelDate,
      // เลิกกิจการ
      // ย้ายสถานที่ตั้งสถานการค้าปลีก
      changeDate: res.changeDate, // ตั้งแต่วันที่
      addressOld: res.addressOld, // ที่อยู่เดิม
      address: res.address, // ที่อยู่ใหม่
      houseNo: res.houseNo,
      soi: res.soi, // ซอย
      road: res.road, // ถนน
      tambonId: res.tambonId, // ตำบลไอดี
      tambonName: res.tambonName, // ตำบลชื่อ
      amphurId: res.amphurId, // อำเภอชื่อ
      amphurName: res.amphurName, // อำเภอชื่อ
      provinceId: res.provinceId, // จังหวัดชื่อ
      provinceName: res.provinceName, // จังหวัดชื่อ
      postcode: res.postcode, // รหัสไปรษณีย์
      mobileOld: res.mobileOld, // เบอร์โทรศัพท์เก่า
      mobile: res.mobile, // เบอร์โทรศัพท์
      stationCode: res.stationCode, // สถานการค้ารหัส
      stationNameOld: res.stationNameOld, // สถานการค้าปลีกเก่า
      stationName: res.stationName == "" ? res.stationNameOld : res.stationName, // สถานการค้าชื่อ
      ownerNameOld: res.ownerNameOld, // สถานกาค้าปลีกเก่า
      ownerName: res.ownerName, // สถานกาค้าปลีก
      taxNoOld: res.taxNoOld, // รหัสผู้เสียภาษีอากรเก่า
      taxNo: res.taxNo, // รหัสผู้เสียภาษีอากรใหม่
      customerTypeOld: res.customerTypeOld, // รหัสพนักงานเก่า
      customerType: res.customerType, // รหัสประเภทพนักงาน
      idCardOld: res.idCardOld, // ประจำตัวประชาชนเก่า
      idCard: res.idCard, // ประจำตัวประชาชน
      idCardAtOld: res.idCardAtOld, // ออก ณ ที่ เก่า
      idCardAt: res.idCardAt, // ออก ณ ที่
      corpNoOld: res.corpNoOld, // เลขทะเบียนนิติบุคคลที่ เก่า
      corpNo: res.corpNo, // เลขทะเบียนนิติบุคคลที่
      corpDateOld: res.corpDateOld, // เมื่อวันที่ เก่า
      corpDate: res.corpDate, // เมื่อวันที่ ใหม่
      other: res.other, // อื่นๆ
      taxRemain: res.taxRemain, // ภาษีค้างชำระตามบัญชีแนบ
      taxOilGas: res.taxOilGas, // น้ำมัน/ก๊าซปิโตรเลียมที่ยังไม่ได้ชำระภาษีตามบัญชีแนบ
      status: res.status,
      statusName: res.statusName,
      fileNumE: res.fileNumE, // สำเนาหนังสือรับรองของกระทรวงพาณิชย์
      fileNumA: res.fileNumA,
      fileNumR: res.fileNumR,
      fileNumO: res.fileNumO,
      active: res.active,
      addressRetail: res.addressRetail,
      soiRetail: res.soiRetail,
      roadRetail: res.roadRetail,
      tambonIdRetail: res.tambonIdRetail,
      tambonNameRetail: res.tambonNameRetail,
      amphurIdRetail: res.amphurIdRetail,
      amphurNameRetail: res.amphurNameRetail,
      provinceIdRetail: res.provinceIdRetail,
      provinceNameRetail: res.provinceNameRetail,
      postcodeRetail: res.postcodeRetail,
      mobileRetail: res.mobileRetail,
      cause: res.cause,

      businessStatusList: res.businessStatusList,

      commerceFileList: commerceFileList,
      authorizeFileList: authorizeFileList,
      taxRemainFileList: taxRemainFileList,
      otherFileList: otherFileList,
    };
  }

  cancel() {
    this.router.navigate(["/fontOffice/approval-setting/approval-tax-form-02"]);
  }
}
