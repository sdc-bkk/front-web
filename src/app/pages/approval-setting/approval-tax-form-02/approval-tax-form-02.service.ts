import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import { Frontaxform02Api } from "../../../api/fronttaxform02-api";

@Injectable()
export class ApprovalTaxForm02Service {
  constructor(private frontaxform02Api: Frontaxform02Api) {}

  getList(data): Observable<any> {
    return this.frontaxform02Api.getList(data);
  }

  getData(data): Observable<any> {
    return this.frontaxform02Api.getData(data);
  }

  saveData(data): Observable<any> {
    return this.frontaxform02Api.saveData(data);
  }

  deleteData(data): Observable<any> {
    return this.frontaxform02Api.deleteData(data);
  }

  sendApprove(data): Observable<any> {
    return this.frontaxform02Api.sendApprove(data);
  }

  getListStatusLog(data): Observable<any> {
    return this.frontaxform02Api.getListStatusLog(data);
  }
}
