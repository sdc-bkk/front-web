import {
  Component,
  ViewEncapsulation,
  OnInit,
  TemplateRef,
  ViewChild,
} from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Observable, Subject } from "rxjs/Rx";
import { finalize, map, startWith, takeUntil } from "rxjs/operators";
import { ConfigService } from "../../../../shared/services/config/config.service";
// import createNumberMask from 'text-mask-addons/dist/createNumberMask';
import Utils from "../../../../shared/util/utils";
import {
  NgbModal,
  NgbModalConfig,
  NgbModalRef,
} from "@ng-bootstrap/ng-bootstrap";
import Swal from "sweetalert2";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { ApprovalTaxForm01Service } from "../../approval-tax-form-01/approval-tax-form-01.service";
import { ApprovalTaxForm02Service } from "../approval-tax-form-02.service";
import { BusinessStatusService } from "../../../business-status.service";
import { FormControl } from "@angular/forms";
import { AddressService } from "../../../address.service";
import { RoadService } from "../../../road.service";
import { SoiService } from "../../../soi.service";
import { FileUploader } from "../../../../shared/components/file-upload/file-uploader.class";
import { REST_URL } from "../../../../shared/services/config/rest-url";
import { UploadService } from "../../../upload.service";
import { FrontretailService } from "../../../frontretail.service";
import createNumberMask from "text-mask-addons/dist/createNumberMask";

@Component({
  selector: ".approval-tax-form-02-update-inner-wrapper",
  templateUrl: "./approval-tax-form-02-update.component.html",
  styleUrls: ["./approval-tax-form-02-update.component.scss"],
  encapsulation: ViewEncapsulation.Emulated,
})
export class ApprovalTaxForm02UpdateComponent implements OnInit {
  public currencyMask = createNumberMask({
    prefix: "",
    suffix: "",
    includeThousandsSeparator: true,
    thousandsSeparatorSymbol: ",",
    allowDecimal: true,
    decimalSymbol: ".",
    decimalLimit: 2,
    integerLimit: null,
    requireDecimal: false,
    allowNegative: false,
    allowLeadingZeroes: false,
  });

  /////////////// End Variable ////////////////

  id: any = "0";
  dataList: any[] = [];
  private unsubscribeAll = new Subject();
  //////////////// Value /////////////////////
  getfileUrl=REST_URL.MAIN_UPLOADFILE.getFile;
  data = {
    taxForm02Id: "0",
    refRetailId: "",
    refStationId: "",
    docNo: "",
    docDate: "",
    forType: "",
    businessStatusId: "",
    businessStatusCode: "",
    businessStatusName: "",

    // เลิกกิจการ
    cancelDate: "",
    // เลิกกิจการ
    // ย้ายสถานที่ตั้งสถานการค้าปลีก
    changeDate: "", // ตั้งแต่วันที่
    addressOld: "", // ที่อยู่เดิม
    address: "", // ที่อยู่ใหม่
    houseNo: "",
    soi: "", // ซอย
    road: "", // ถนน
    tambonId: "", // ตำบลไอดี
    tambonName: "", // ตำบลชื่อ
    amphurId: "", // อำเภอชื่อ
    amphurName: "", // อำเภอชื่อ
    provinceId: "", // จังหวัดชื่อ
    provinceName: "", // จังหวัดชื่อ
    postcode: "", // รหัสไปรษณีย์
    mobileOld: "", // เบอร์โทรศัพท์เก่า
    mobile: "", // เบอร์โทรศัพท์
    stationCode: "", // สถานการค้ารหัส
    stationNameOld: "", // สถานการค้าปลีกเก่า
    stationName: "", // สถานการค้าชื่อ
    ownerNameOld: "", // สถานกาค้าปลีกเก่า
    ownerName: "", // สถานกาค้าปลีก
    taxNoOld: "", // รหัสผู้เสียภาษีอากรเก่า
    taxNo: "", // รหัสผู้เสียภาษีอากรใหม่
    customerTypeOld: "", // รหัสพนักงานเก่า
    customerType: "", // รหัสประเภทพนักงาน
    idCardOld: "", // ประจำตัวประชาชนเก่า
    idCard: "", // ประจำตัวประชาชน
    idCardAtOld: "", // ออก ณ ที่ เก่า
    idCardAt: "", // ออก ณ ที่
    corpNoOld: "", // เลขทะเบียนนิติบุคคลที่ เก่า
    corpNo: "", // เลขทะเบียนนิติบุคคลที่
    corpDateOld: "", // เมื่อวันที่ เก่า
    corpDate: "", // เมื่อวันที่ ใหม่
    other: "", // อื่นๆ
    taxRemain: "", // ภาษีค้างชำระตามบัญชีแนบ
    taxOilGas: "", // น้ำมัน/ก๊าซปิโตรเลียมที่ยังไม่ได้ชำระภาษีตามบัญชีแนบ
    status: "",
    statusName: "",
    fileNumE: "", // สำเนาหนังสือรับรองของกระทรวงพาณิชย์
    fileNumA: "",
    fileNumR: "",
    fileNumO: "",
    active: true,
    addressRetail: "",
    soiRetail: "",
    roadRetail: "",
    tambonIdRetail: "",
    tambonNameRetail: "",
    amphurIdRetail: "",
    amphurNameRetail: "",
    provinceIdRetail: "",
    provinceNameRetail: "",
    postcodeRetail: "",
    mobileRetail: "",
    cause: "",

    businessStatusList: [
      // {
      //   id: "upyVEXl6pVTiOUJxmMEcvA",
      //   name: "เลิกกิจการ",
      //   code: "ล",
      //   other: "A",
      // },
      // {
      //   id: "REG6Yo-twQ0m1-CqsbLt9g",
      //   name: "ย้ายสถานที่ตั้งสถานการค้าปลีก",
      //   code: "ต",
      //   other: "B",
      // },
    ],

    commerceFileList: [],
    authorizeFileList: [],
    taxRemainFileList: [],
    otherFileList: [],

    addressStation: "",
    soiStation: "",
    roadStation: "",
    tambonIdStation: "",
    tambonNameStation: "",
    amphurIdStation: "",
    amphurNameStation: "",
    provinceIdStation: "",
    provinceNameStation: "",
    postcodeStation: "",
    mobileStation: "",
  };

  // + -
  panelOpenState = true;
  panelOpenState_1 = true;

  drpListStation: any = []; // ชื่อสถานการค้าปลีก

  drpBusinessStatusList: any = []; // ความประสงค์

  drpListSoi: any[] = []; // ซอย
  myControl_Soi = new FormControl();
  options_Soi: any[] = [];
  filteredOptions_Soi: Observable<string[]>;

  myControl_Soi_Retail = new FormControl();
  options_Soi_Retail: any[] = [];
  filteredOptions_Soi_Retail: Observable<string[]>;

  drpListRoad: any[] = []; // ถนน
  myControl_Road = new FormControl();
  options_Road: any[] = [];
  filteredOptions_Road: Observable<string[]>;

  myControl_Road_Retail = new FormControl();
  options_Road_Retail: any[] = [];
  filteredOptions_Road_Retail: Observable<string[]>;

  drpListByPostCodeMain: any[] = []; // ผู้ประกอบการ
  drpListByPostCodeMainRetail: any[] = []; // ผู้ประกอบการ
  typeData: any = false; // ผู้ประกอบการ

  // สำเนาหนังสือรับรองของกระทรวงพาณิชย์
  delect_commerceFileList: any[] = [];

  uploader_commerceFileList: FileUploader = new FileUploader({
    autoUpload: true,
    url: REST_URL.MAIN_UPLOADFILE.uploadFile,
  });
  // สำเนาหนังสือรับรองของกระทรวงพาณิชย์

  // หนังสือมอบอำนาจ
  delect_authorizeFileList: any[] = [];

  uploader_authorizeFileList: FileUploader = new FileUploader({
    autoUpload: true,
    url: REST_URL.MAIN_UPLOADFILE.uploadFile,
  });
  // หนังสือมอบอำนาจ

  // บัญชีรายการค้างชำระภาษี
  delect_taxRemainFileList: any[] = [];

  uploader_taxRemainFileList: FileUploader = new FileUploader({
    autoUpload: true,
    url: REST_URL.MAIN_UPLOADFILE.uploadFile,
  });
  // บัญชีรายการค้างชำระภาษี

  // เอกสารอื่น ๆ
  delect_otherFileList: any[] = [];

  uploader_otherFileList: FileUploader = new FileUploader({
    autoUpload: true,
    url: REST_URL.MAIN_UPLOADFILE.uploadFile,
  });
  // เอกสารอื่น ๆ

  currentUser: any;

  getDataByMemberNameStatus: any = false;

  taxNoValidation: any = false;

  data_getDataByMemberName = {
    active: true,
    address: "",

    agentFileNum: "",

    amphurId: "",
    amphurName: "",

    cerFileNum: "",

    corpDate: "",
    corpNo: "",
    createdBy: "",
    createdDate: "",
    customerType: "",
    email: "",
    firstRecord: true,
    fullAddress: "",

    houseFileNum: "",

    houseNo: "",
    idCard: "",
    idCardAt: "",
    mainStatus: "",
    mainStatusName: "",
    mobile: "",
    ownerName: "",
    postcode: "",

    provinceId: "",
    provinceName: "",

    retailId: "",
    road: "",
    soi: "",

    tambonId: "",
    tambonName: "",

    taxNo: "",
    updatedBy: "",
    updatedDate: "",
  };

  drpListStationByTaxNo: any = [];

  stationList = {
    taxForm01StationId: "0",
    taxForm01RetailId: "",
    refStationId: "",
    stationCode: "",
    stationName: "",
    houseNo: "",
    address: "",
    addressfull: "",
    soi: "",
    road: "",
    tambonId: "",
    tambonName: "",
    amphurId: "",
    amphurName: "",
    provinceId: "10",
    provinceName: "กรุงเทพมหานคร",
    postcode: "",
    mobile: "",
    email: "",
    status: "",
    statusName: "",
    houseFileNum: "",
    mapFileNum: "",
    businessTypeList: [
      // {
      //     "id": "upyVEXl6pVTiOUJxmMEcvA",
      //     "name": "น้ำมันเชื้อเพลิง"
      // },
      // {
      //     "id": "wFlYj87tnX307XVrn9r6kw",
      //     "name": "ปิโตรเลียม"
      // }
    ],
    // สำเนาทะเบียนบ้านสำเนาบัตรประจำตัวประชาชนและสำเนาบัตรประจำตัวผู้เสียภาษีอาการ
    houseFileList: [
      // {
      //     "fileName": "Cover.jpg",
      //     "fileType": "I",
      //     "pathFile": "14158953-f5fe-4a66-a680-14aa099da5a5.jpg"
      // }
    ],
    // แผนที่ตั้ง
    mapFileList: [
      // {
      //     "fileName": "Cover.jpg",
      //     "fileType": "I",
      //     "pathFile": "14158953-f5fe-4a66-a680-14aa099da5a5.jpg"
      // }
    ],
  };

  _refRetailId: any = "";

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private configModal: NgbModalConfig,
    public config: ConfigService,
    private modalService: NgbModal,
    public ngxService: NgxUiLoaderService,
    private addressService: AddressService,
    private soiService: SoiService,
    private roadService: RoadService,

    private approvalTaxForm01Service: ApprovalTaxForm01Service,
    private businessStatusService: BusinessStatusService,
    private approvalTaxForm02Service: ApprovalTaxForm02Service,
    private uploadService: UploadService,
    private frontretailService: FrontretailService
  ) {
    configModal.backdrop = "static";
  }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem("ba_currentUser"));

    this.id = this.route.snapshot.params["id"] || "0";
    this.getSoiDrpList(); // ซอย
    this.getRoadDrpList(); // ถนน
    this.getDrpBusinessStatusList(); // ความประสงค์

    this.filteredOptions_Soi = this.myControl_Soi.valueChanges.pipe(
      startWith(""),
      map((value) => this._filter_Soi(value))
    );

    this.filteredOptions_Road = this.myControl_Road.valueChanges.pipe(
      startWith(""),
      map((value) => this._filter_Road(value))
    );

    this.filteredOptions_Soi_Retail = this.myControl_Soi_Retail.valueChanges.pipe(
      startWith(""),
      map((value) => this._filter_Soi_Retail(value))
    );

    this.filteredOptions_Road_Retail = this.myControl_Road_Retail.valueChanges.pipe(
      startWith(""),
      map((value) => this._filter_Road_Retail(value))
    );

    let d = {
      username: "",
      taxForm01RetailId: this.id,
    };

    //this.getDrpListStation(d); // ชื่อสถานการค้าปลีก

    this.getDataByMemberName();
    if (this.id != "0") {
      this.getData(this.id);
    }
  }

  public getDataByMemberName() {
    const payload = {
      memberName: this.currentUser.memberName,
    };

    this.ngxService.start();

    this.frontretailService.getDataByMemberName(payload).subscribe((res) => {
      console.log("res getDataByMemberName");
      console.log(res);
      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

      //this.getDataByMemberNameStatus = res.status;

      if (res.status == "false") {
        //this.data.firstRecord = false;
        this.getDataByMemberNameStatus = false;

        this.data_getDataByMemberName = {
          active: true,
          address: "",

          agentFileNum: "",

          amphurId: "",
          amphurName: "",

          cerFileNum: "",

          corpDate: "",
          corpNo: "",
          createdBy: "",
          createdDate: "",
          customerType: "",
          email: "",
          firstRecord: true,
          fullAddress: "",

          houseFileNum: "",

          houseNo: "",
          idCard: "",
          idCardAt: "",
          mainStatus: "",
          mainStatusName: "",
          mobile: "",
          ownerName: "",
          postcode: "",

          provinceId: "",
          provinceName: "",

          retailId: "",
          road: "",
          soi: "",

          tambonId: "",
          tambonName: "",

          taxNo: "",
          updatedBy: "",
          updatedDate: "",
        };
      } else {
        res.result.firstRecord = true;
        this.getDataByMemberNameStatus = true;

        //this.setForm(res.result);

        this._refRetailId = res.result.retailId;
        this.data.refRetailId = this._refRetailId;

        res.result.amphurName =
          res.result.amphurName == undefined ? "" : res.result.amphurName;
        res.result.tambonName =
          res.result.tambonName == undefined ? "" : res.result.tambonName;
        res.result.provinceName =
          res.result.provinceName == undefined ? "" : res.result.provinceName;

        this.data_getDataByMemberName = res.result;

        this.data.taxNoOld = res.result.taxNo;
        this.data.ownerNameOld = res.result.ownerName;
        this.data.customerTypeOld = res.result.customerType;
        this.data.idCardOld = res.result.idCard;
        this.data.idCardAtOld = res.result.idCardAt;
        this.data.corpNoOld = res.result.corpNo;
        this.data.corpDateOld = res.result.corpDate;

        this.getListStationByTaxNo(this.data_getDataByMemberName.taxNo);

        // console.log("this.data_getDataByMemberName");
        // console.log(this.data_getDataByMemberName);
      }
    });
  }

  public getListStationByTaxNo(item) {
    const payload = {
      taxNo: item,
    };

    this.ngxService.start();

    this.frontretailService.getListStationByTaxNo2(payload).subscribe((res) => {
      // console.log("res getListStationByTaxNo");
      // console.log(res);
      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
      if (res.result) {
        this.drpListStationByTaxNo = res.result;
      } else {
        this.drpListStationByTaxNo = [];
      }

      //this.getDataByMemberNameStatus = res.status;
    });
  }

  public getDataStation(item) {

    const payload = {
      id: item,
    };

    this.ngxService.start();

    this.frontretailService.getDataStation(payload).subscribe((res) => {
      console.log("res getDataStation");
      console.log(res);
      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

      if (res.result) {
        this.data.addressOld = res.result.fullAddress;
        this.data.mobileOld = res.result.mobile;
        this.data.stationNameOld = res.result.stationName;
        this.data.stationCode = res.result.stationCode; // รหัสสถานประกอบการ
        this.data.stationNameOld = res.result.stationName; // ชื่อสถานประกอบการ เดิม

        this.data.addressStation = res.result.address;
        this.data.soiStation = res.result.soi;
        this.data.roadStation = res.result.road;
        this.data.tambonIdStation = res.result.tambonId;
        this.data.tambonNameStation = res.result.tambonName;
        this.data.amphurIdStation = res.result.amphurId;
        this.data.amphurNameStation = res.result.amphurName;
        this.data.provinceIdStation = res.result.provinceId;
        this.data.provinceNameStation = res.result.provinceName;
        this.data.postcodeStation = res.result.postcode;
        this.data.mobileStation = res.result.mobile;

        console.log("this.data-----------");
        console.log(this.data);

       
      } else {
        this.data.addressOld = "";
        this.data.mobileOld = "";
        this.data.stationNameOld = "";
        this.data.stationCode = ""; // รหัสสถานประกอบการ
        this.data.stationNameOld = ""; // ชื่อสถานประกอบการ เดิม

        this.data.addressStation = "";
        this.data.soiStation = "";
        this.data.roadStation = "";
        this.data.tambonIdStation = "";
        this.data.tambonNameStation = "";
        this.data.amphurIdStation = "";
        this.data.amphurNameStation = "";
        this.data.provinceIdStation = "";
        this.data.provinceNameStation = "";
        this.data.postcodeStation = "";
        this.data.mobileStation = "";
      }
    });
  }

  public getDataByTaxNo(item) {
    let re = /\-/gi;
    let result = item.replace(re, "");

    const payload = {
      taxNo: result,
    };

    this.ngxService.start();

    this.frontretailService.getDataByTaxNo(payload).subscribe((res) => {
      //console.log("res getDataByTaxNo");
      //console.log(res);
      if (res.status == "true") {
        this.taxNoValidation = true;
      } else {
        this.taxNoValidation = false;

        Swal.fire({
          icon: "warning",
          title: res.resultMessage,
          // text: "Modal with a custom image.",
          confirmButtonText: "ตกลง",
        });

        // this.data.taxNo = "";
      }

      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
    });
  }

  ngOnDestroy() {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }

  private _filter_Soi(value: string): string[] {
    const filterValue = value.toLowerCase();
    let d = [];
    d = this.options_Soi.filter((val) => {
      if (val.toLowerCase().includes(filterValue)) {
        return val;
      }
    });

    return d;
  }

  private _filter_Soi_Retail(value: string): string[] {
    const filterValue = value.toLowerCase();
    let d = [];
    d = this.options_Soi_Retail.filter((val) => {
      if (val.toLowerCase().includes(filterValue)) {
        return val;
      }
    });

    return d;
  }

  // ซอย
  public getSoiDrpList() {
    this.soiService.getSoiDrpList().subscribe((res) => {
      if (res.result) {
        this.drpListSoi = res.result;

        res.result.forEach((r) => {
          this.options_Soi.push(r.soiName);
          this.options_Soi_Retail = this.options_Soi;
        });
      } else {
        this.drpListSoi = [];
        this.options_Soi = [];
        this.options_Soi_Retail = [];
      }
    });
  }

  private _filter_Road(value: string): string[] {
    const filterValue = value.toLowerCase();

    let d = [];
    d = this.options_Road.filter((val) => {
      if (val.toLowerCase().includes(filterValue)) {
        return val;
      }
    });

    return d;
  }

  private _filter_Road_Retail(value: string): string[] {
    const filterValue = value.toLowerCase();

    let d = [];
    d = this.options_Road_Retail.filter((val) => {
      if (val.toLowerCase().includes(filterValue)) {
        return val;
      }
    });

    return d;
  }

  // ถนน
  public getRoadDrpList() {
    this.roadService.getRoadDrpList().subscribe((res) => {
      if (res.result) {
        this.drpListRoad = res.result;

        res.result.forEach((r) => {
          this.options_Road.push(r.roadName);
          this.options_Road_Retail = this.options_Road;
        });
      } else {
        this.drpListRoad = [];
        this.options_Road = [];
        this.options_Road_Retail = [];
      }
    });
  }

  public getDrpBusinessStatusListCheck(event, templateCode) {
    // console.log("event");
    // console.log(event);
    // console.log("this.templateCode");
    // console.log(templateCode);

    if (templateCode == "A" && event == true) {
      console.log("IF");
      let hasData = this.drpBusinessStatusList.find((data) => {
        return data.templateCode == templateCode;
      });

      if (hasData) {
        this.drpBusinessStatusList.forEach((e) => {
          if (e.templateCode == templateCode) {
            e.IsSelect = false;
            e.selected = true;
          } else {
            e.IsSelect = true;
            e.selected = false;
          }
        });
      }
    } else if (templateCode == "A" && event == false) {
      console.log("else if");
      this.drpBusinessStatusList.forEach((r) => {
        r.IsSelect = false;
        r.selected = false;
      });
    }
  }

  public getDrpBusinessStatusList() {
    this.businessStatusService.getDrpBusinessStatusList().subscribe((res) => {
      console.log("res getDrpBusinessStatusList");
      console.log(res);

      if (res.result) {
        res.result.forEach((r) => {
          r.IsSelect = false;
          r.selected = false;
        });

        this.drpBusinessStatusList = res.result;

        // console.log("this.drpBusinessStatusList");
        // console.log(this.drpBusinessStatusList);
      } else {
        this.drpBusinessStatusList = [];
      }
    });
  }

  // public getDrpListStation(item) {
  //   const payload = {
  //     username: "",
  //     taxForm01RetailId: item.taxForm01RetailId,
  //   };

  //   this.approvalTaxForm01Service
  //     .getDrpListStation(payload)
  //     .subscribe((res) => {
  //       // console.log("res getDrpListStation");
  //       // console.log(res);

  //       if (res.result) {
  //         this.drpListStation = res.result;
  //       } else {
  //         this.drpListStation = [];
  //       }
  //     });
  // }

  public getData(item) {
    const payload = {
      taxForm02Id: item,
    };

    this.ngxService.start();

    this.approvalTaxForm02Service.getData(payload).subscribe((res) => {
      console.log("res approvalTaxForm02Service getData");
      console.log(res);

      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

      if (res.result) {
        this.setForm(res.result);
      }
    });
  }

  setForm(res: any) {
    console.log("res setForm");
    console.log(res);


    this.drpBusinessStatusList.forEach((val) => {
      res.businessStatusList.forEach((element) => {
        if (element.id == val.businessStatusId) {
          val.selected = true;
        }
      });
    });

    let hasdata = res.businessStatusList.find((val) => {
      return val.other == "A";
    });

   

    if (hasdata) {
      this.drpBusinessStatusList.forEach((val) => {
        if (val.templateCode == "A") {
        } else {
          val.IsSelect = true;
          val.selected = false;
        }
      });
    }

    let commerceFileList = [];
    // สำเนาหนังสือรับรองของกระทรวงพาณิชย์
    if (res.commerceFileList != undefined) {
      res.commerceFileList.forEach((element) => {
        let m = {
          id: element.contentId,
          pathFile: element.pathFile,
          ShowFile:
            REST_URL.MAIN_UPLOADFILE.getFile +
            "/" +
            element.pathFile +
            "/" +
            "docform",
          pathFileName: element.fileName,
          Status: 200,
          type: element.fileType,
        };

        commerceFileList.push(m);
      });
    }
    // สำเนาหนังสือรับรองของกระทรวงพาณิชย์

    let authorizeFileList = [];
    // หนังสือมอบอำนาจ

    if (res.authorizeFileList != undefined) {
      res.authorizeFileList.forEach((element) => {
        let m = {
          id: element.contentId,
          pathFile: element.pathFile,
          ShowFile:
            REST_URL.MAIN_UPLOADFILE.getFile +
            "/" +
            element.pathFile +
            "/" +
            "docform",
          pathFileName: element.fileName,
          Status: 200,
          type: element.fileType,
        };

        authorizeFileList.push(m);
      });
    }
    // หนังสือมอบอำนาจ

    let taxRemainFileList = [];
    // บัญชีรายการค้างชำระภาษี

    if (res.taxRemainFileList != undefined) {
      res.taxRemainFileList.forEach((element) => {
        let m = {
          id: element.contentId,
          pathFile: element.pathFile,
          ShowFile:
            REST_URL.MAIN_UPLOADFILE.getFile +
            "/" +
            element.pathFile +
            "/" +
            "docform",
          pathFileName: element.fileName,
          Status: 200,
          type: element.fileType,
        };

        taxRemainFileList.push(m);
      });
    }
    // บัญชีรายการค้างชำระภาษี

    let otherFileList = [];
    // เอกสารอื่น ๆ

    if (res.otherFileList != undefined) {
      res.otherFileList.forEach((element) => {
        let m = {
          id: element.contentId,
          pathFile: element.pathFile,
          ShowFile:
            REST_URL.MAIN_UPLOADFILE.getFile +
            "/" +
            element.pathFile +
            "/" +
            "docform",
          pathFileName: element.fileName,
          Status: 200,
          type: element.fileType,
        };

        otherFileList.push(m);
      });
    }

    // เอกสารอื่น ๆ

    let cancelDate = null;
    if (res.cancelDate != "") {
      cancelDate = Utils.stringToDate(res.cancelDate);
    } else {
      cancelDate = "";
    }

    let changeDate = null;
    if (res.changeDate != "") {
      changeDate = Utils.stringToDate(res.changeDate);
    } else {
      changeDate = "";
    }

    let docDate = null;
    if (res.docDate != "") {
      docDate = Utils.stringToDate(res.docDate);
    } else {
      docDate = "";
    }

    this.data = {
      taxForm02Id: res.taxForm02Id,
      refRetailId: res.refRetailId,
      refStationId: res.refStationId,
      docNo: res.docNo,
      docDate: docDate,
      forType: res.forType,
      businessStatusId: res.businessStatusId,
      businessStatusCode: res.businessStatusCode,
      businessStatusName: res.businessStatusName,

      // เลิกกิจการ
      cancelDate: cancelDate,
      // เลิกกิจการ
      // ย้ายสถานที่ตั้งสถานการค้าปลีก
      changeDate: changeDate, // ตั้งแต่วันที่
      addressOld: res.addressOld, // ที่อยู่เดิม
      address: res.address, // ที่อยู่ใหม่
      houseNo: res.houseNo,
      soi: res.soi, // ซอย
      road: res.road, // ถนน
      tambonId: res.tambonId, // ตำบลไอดี
      tambonName: res.tambonName, // ตำบลชื่อ
      amphurId: res.amphurId, // อำเภอชื่อ
      amphurName: res.amphurName, // อำเภอชื่อ
      provinceId: res.provinceId, // จังหวัดชื่อ
      provinceName: res.provinceName, // จังหวัดชื่อ
      postcode: res.postcode, // รหัสไปรษณีย์
      mobileOld: res.mobileOld, // เบอร์โทรศัพท์เก่า
      mobile: res.mobile, // เบอร์โทรศัพท์
      stationCode: res.stationCode, // สถานการค้ารหัส
      stationNameOld: res.stationNameOld, // สถานการค้าปลีกเก่า
      stationName: res.stationName, // สถานการค้าชื่อ
      ownerNameOld: res.ownerNameOld, // สถานกาค้าปลีกเก่า
      ownerName: res.ownerName, // สถานกาค้าปลีก
      taxNoOld: res.taxNoOld, // รหัสผู้เสียภาษีอากรเก่า
      taxNo: res.taxNo, // รหัสผู้เสียภาษีอากรใหม่
      customerTypeOld: res.customerTypeOld, // รหัสพนักงานเก่า
      customerType: res.customerType, // รหัสประเภทพนักงาน
      idCardOld: res.idCardOld, // ประจำตัวประชาชนเก่า
      idCard: res.idCard, // ประจำตัวประชาชน
      idCardAtOld: res.idCardAtOld, // ออก ณ ที่ เก่า
      idCardAt: res.idCardAt, // ออก ณ ที่
      corpNoOld: res.corpNoOld, // เลขทะเบียนนิติบุคคลที่ เก่า
      corpNo: res.corpNo, // เลขทะเบียนนิติบุคคลที่
      corpDateOld: res.corpDateOld, // เมื่อวันที่ เก่า
      corpDate: res.corpDate, // เมื่อวันที่ ใหม่
      other: res.other, // อื่นๆ
      taxRemain: res.taxRemain, // ภาษีค้างชำระตามบัญชีแนบ
      taxOilGas: res.taxOilGas, // น้ำมัน/ก๊าซปิโตรเลียมที่ยังไม่ได้ชำระภาษีตามบัญชีแนบ
      status: res.status,
      statusName: res.statusName,
      fileNumE: res.fileNumE, // สำเนาหนังสือรับรองของกระทรวงพาณิชย์
      fileNumA: res.fileNumA,
      fileNumR: res.fileNumR,
      fileNumO: res.fileNumO,
      active: res.active,
      addressRetail: res.addressRetail,
      soiRetail: res.soiRetail,
      roadRetail: res.roadRetail,
      tambonIdRetail: res.tambonIdRetail,
      tambonNameRetail: res.tambonNameRetail,
      amphurIdRetail: res.amphurIdRetail,
      amphurNameRetail: res.amphurNameRetail,
      provinceIdRetail: res.provinceIdRetail,
      provinceNameRetail: res.provinceNameRetail,
      postcodeRetail: res.postcodeRetail,
      mobileRetail: res.mobileRetail,
      cause: res.cause,

      businessStatusList: res.businessStatusList,

      commerceFileList: commerceFileList,
      authorizeFileList: authorizeFileList,
      taxRemainFileList: taxRemainFileList,
      otherFileList: otherFileList,

      addressStation: res.addressStation,
      soiStation: res.soiStation,
      roadStation: res.roadStation,
      tambonIdStation: res.tambonIdStation,
      tambonNameStation: res.tambonNameStation,
      amphurIdStation: res.amphurIdStation,
      amphurNameStation: res.amphurNameStation,
      provinceIdStation: res.provinceIdStation,
      provinceNameStation: res.provinceNameStation,
      postcodeStation: res.postcodeStation,
      mobileStation: res.mobileStation,


    };

    this.getDataStation(
      this.data.refStationId == undefined ? "" : this.data.refStationId
    );
  }

  validation_form() {
    if (this.data.commerceFileList.length == 0) {
      Swal.fire({
        icon: "error",
        title: "กรุณาแนบไฟล์",
        text: "สำเนาหนังสือรับรองของกระทรวงพาณิชย์ (ที่ออกให้ไม่เกิน 6 เดือน)",
        confirmButtonText: "ตกลง",
      });
    } else if (this.data.fileNumE == "") {
      Swal.fire({
        icon: "error",
        title: "กรุณาระบุจำนวน (แผ่น)",
        text: "สำเนาหนังสือรับรองของกระทรวงพาณิชย์ (ที่ออกให้ไม่เกิน 6 เดือน)",
        confirmButtonText: "ตกลง",
      });
    } else if (this.data.taxRemainFileList.length == 0) {
      Swal.fire({
        icon: "error",
        title: "กรุณาระบุจำนวน (แผ่น)",
        text:
          "บัญชีสินค้าน้ำมัน/ก๊าซปิโตรเลียมและบัญชีรายการค้างชำระภาษี(กรณี ขอเลิก/ย้าย/เปลี่ยนแปลงกิจการ)",
        confirmButtonText: "ตกลง",
      });
    } else if (this.data.fileNumR == "") {
      Swal.fire({
        icon: "error",
        title: "กรุณาระบุจำนวน (แผ่น)",
        text:
          "บัญชีสินค้าน้ำมัน/ก๊าซปิโตรเลียมและบัญชีรายการค้างชำระภาษี(กรณี ขอเลิก/ย้าย/เปลี่ยนแปลงกิจการ)",
        confirmButtonText: "ตกลง",
      });
    } else {
      this.onSubmit();
    }
  }

  onSubmit() {
    let cancelDate;
    if (this.data.cancelDate != null) {
      cancelDate = Utils.dateTostring(this.data.cancelDate);
    } else {
      cancelDate = "";
    }
    let changeDate;
    if (this.data.changeDate != null) {
      changeDate = Utils.dateTostring(this.data.changeDate);
    } else {
      changeDate = "";
    }

    let corpDateOld;
    if (this.data.corpDateOld != null) {
      corpDateOld = Utils.dateTostring(this.data.corpDateOld);
    } else {
      corpDateOld = "";
    }

    let corpDate;
    if (this.data.corpDate != null) {
      corpDate = Utils.dateTostring(this.data.corpDate);
    } else {
      corpDate = "";
    }

    // สำเนาหนังสือรับรองของกระทรวงพาณิชย์
    let commerceFileList = [];
    this.data.commerceFileList.forEach((val) => {
      let d = {
        fileName: val.pathFileName,
        fileType: val.type,
        pathFile: val.pathFile,
      };
      commerceFileList.push(d);
    });

    // หนังสือมอบอำนาจ
    let authorizeFileList = [];
    this.data.authorizeFileList.forEach((val) => {
      let d = {
        fileName: val.pathFileName,
        fileType: val.type,
        pathFile: val.pathFile,
      };
      authorizeFileList.push(d);
    });

    // บัญชีรายการค้างชำระภาษี
    let taxRemainFileList = [];
    this.data.taxRemainFileList.forEach((val) => {
      let d = {
        fileName: val.pathFileName,
        fileType: val.type,
        pathFile: val.pathFile,
      };
      taxRemainFileList.push(d);
    });

    // เอกสารอื่น ๆ
    let otherFileList = [];
    this.data.otherFileList.forEach((val) => {
      let d = {
        fileName: val.pathFileName,
        fileType: val.type,
        pathFile: val.pathFile,
      };
      otherFileList.push(d);
    });

    // ประสงค์
    // let d = this.drpBusinessStatusList.find((val) => {
    //   return val.templateCode == this.data.businessStatusCode;
    // });

    let re = /\,/gi;

    let taxRemain = "";
    taxRemain = this.data.taxRemain.toLocaleString();

    let taxOilGas = "";
    taxOilGas = this.data.taxOilGas.toLocaleString();

    let businessStatusList = [];
    this.drpBusinessStatusList.forEach((el) => {
      if (el.selected) {
        let d = {
          // businessStatusCode: el.businessStatusCode,
          // businessStatusId: el.businessStatusId,
          // businessStatusName: el.businessStatusName,
          // templateCode: el.templateCode,

          id: el.businessStatusId,
          name: el.businessStatusName,
          code: el.businessStatusCode,
          other: el.templateCode,
        };
        businessStatusList.push(d);
      }
    });

    let data = {};
    data = {
      taxForm02Id: this.id,
      refRetailId: this.data.refRetailId,
      refStationId: this.data.refStationId,
      docNo: this.data.docNo,
      forType: this.data.forType,
      businessStatusId: this.data.businessStatusId,
      businessStatusCode: this.data.businessStatusCode,
      businessStatusName: this.data.businessStatusName,

      cancelDate: cancelDate,
      changeDate: changeDate,

      addressOld: this.data.addressOld,
      houseNo: this.data.houseNo,
      address: this.data.address,

      soi: this.data.soi,
      road: this.data.road,
      tambonId: this.data.tambonId,
      tambonName: this.data.tambonName,
      amphurId: this.data.amphurId,
      amphurName: this.data.amphurName,
      provinceId: this.data.provinceId,
      provinceName: this.data.provinceName,
      postcode: this.data.postcode,
      mobileOld: this.data.mobileOld,
      mobile: this.data.mobile,

      stationCode: this.data.stationCode,
      stationNameOld: this.data.stationNameOld,
      stationName: this.data.stationName,
      ownerNameOld: this.data.ownerNameOld,
      ownerName: this.data.ownerName,
      taxNoOld: this.data.taxNoOld,
      taxNo: this.data.taxNo,
      customerTypeOld: this.data.customerTypeOld,

      customerType: this.data.customerType,
      idCardOld: this.data.idCardOld,
      idCard: this.data.idCard,
      idCardAtOld: this.data.idCardAtOld,
      idCardAt: this.data.idCardAt,
      corpNoOld: this.data.corpNoOld,
      corpNo: this.data.corpNo,
      corpDateOld: this.data.corpDateOld,
      corpDate: this.data.corpDate,
      other: this.data.other,
      status: this.data.status,
      statusName: this.data.statusName,
      taxRemain:
        this.data.taxRemain == ""
          ? 0
          : parseFloat(taxRemain.toString().replace(re, "")),
      taxOilGas:
        this.data.taxOilGas == ""
          ? 0
          : parseFloat(taxOilGas.toString().replace(re, "")),

      fileNumE: parseFloat(this.data.fileNumE.toString().replace(re, "")),
      fileNumA: parseFloat(this.data.fileNumA.toString().replace(re, "")),
      fileNumR: parseFloat(this.data.fileNumR.toString().replace(re, "")),
      fileNumO: parseFloat(this.data.fileNumO.toString().replace(re, "")),
      active: this.data.active,

      addressRetail: this.data.addressRetail,
      soiRetail: this.data.soiRetail,
      roadRetail: this.data.roadRetail,
      tambonIdRetail: this.data.tambonIdRetail,
      tambonNameRetail: this.data.tambonNameRetail,
      amphurIdRetail: this.data.amphurIdRetail,
      amphurNameRetail: this.data.amphurNameRetail,
      provinceIdRetail: this.data.provinceIdRetail,
      provinceNameRetail: this.data.provinceNameRetail,
      postcodeRetail: this.data.postcodeRetail,
      mobileRetail: this.data.mobileRetail,
      cause: this.data.cause,

      businessStatusList: businessStatusList,

      commerceFileList: commerceFileList,
      authorizeFileList: authorizeFileList,
      taxRemainFileList: taxRemainFileList,
      otherFileList: otherFileList,

      addressStation: this.data.addressStation,
      soiStation: this.data.soiStation,
      roadStation: this.data.roadStation,
      tambonIdStation: this.data.tambonIdStation,
      tambonNameStation: this.data.tambonNameStation,
      amphurIdStation: this.data.amphurIdStation,
      amphurNameStation: this.data.amphurNameStation,
      provinceIdStation: this.data.provinceIdStation,
      provinceNameStation: this.data.provinceNameStation,
      postcodeStation: this.data.postcodeStation,
      mobileStation: this.data.mobileStation,
    };

    console.log("saveData");
    console.log(data);

    this.ngxService.start();

    this.approvalTaxForm02Service.saveData(data).subscribe((res) => {
      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

      console.log("res saveData");
      console.log(res);
      if (res.result) {
        // สำเนาหนังสือรับรองของกระทรวงพาณิชย์
        this.data.commerceFileList.forEach((val) => {
          this.moveFile(val.pathFile);
        });

        this.delect_commerceFileList.forEach((val) => {
          this.deleteFileTemp(val);
          this.deleteFile(val);
        });

        // หนังสือมอบอำนาจ
        this.data.authorizeFileList.forEach((val) => {
          this.moveFile(val.pathFile);
        });

        this.delect_authorizeFileList.forEach((val) => {
          this.deleteFileTemp(val);
          this.deleteFile(val);
        });

        // บัญชีรายการค้างชำระภาษี
        this.data.taxRemainFileList.forEach((val) => {
          this.moveFile(val.pathFile);
        });

        this.delect_taxRemainFileList.forEach((val) => {
          this.deleteFileTemp(val);
          this.deleteFile(val);
        });

        // เอกสารอื่น ๆ
        this.data.otherFileList.forEach((val) => {
          this.moveFile(val.pathFile);
        });

        this.delect_otherFileList.forEach((val) => {
          this.deleteFileTemp(val);
          this.deleteFile(val);
        });

        //Stop the foreground loading after 5s
        setTimeout(() => {
          this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
          Swal.fire({
            icon: "success",
            title: "สำเร็จ",
            text: "บันทึกข้อมูลสำเร็จ",
            showConfirmButton: false,
            timer: 1500,
          });

          setTimeout(() => {
            this.cancel();
          }, 1500);
        }, 300);
      } else {
        Swal.fire("ไม่สำเร็จ", res.message, "error");
      }
    });
  }

  moveFile(data) {
    let d = {
      fileName: data,
      location: "docform",
    };

    //this.ngxService.start();
    this.uploadService.moveFile(d).subscribe((res) => {
      //this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
    });
  }

  deleteFile(data) {
    let d = {
      fileName: data,
      location: "docform",
    };

    //this.ngxService.start();
    this.uploadService.deleteFile(d).subscribe((res) => {
      //this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
    });
  }

  deleteFileTemp(data) {
    let d = {
      fileName: data,
    };

    //this.ngxService.start();
    this.uploadService.deleteFileTemp(d).subscribe((res) => {
      //this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
    });
  }

  cancel() {
    this.router.navigate(["/fontOffice/approval-setting/approval-tax-form-02"]);
  }

  public getDrpListByPostCodeMain(postCode) {
    if (postCode.length == 5) {
      this.typeData = true;

      let d = {
        postCodeMain: postCode,
      };

      this.ngxService.start();

      this.addressService.getDrpListByPostCodeMain(d).subscribe((res) => {
        this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

        console.log("res getDrpListByPostCodeMain");
        console.log(res);

        if (res.result) {
          this.drpListByPostCodeMain = res.result;
        } else {
          this.drpListByPostCodeMain = [];
        }
      });
    }
  }

  select_address_modal(item) {
    // ขอย้ายสถานการค้าปลีก
    // if (this.data.businessStatusCode == "B") {
    this.typeData = false;
    // จังหวัด
    this.data.provinceId = item.provinceId;
    this.data.provinceName = item.provinceThai;

    // อำเภอ
    this.data.amphurId = item.districtId;
    this.data.amphurName = item.districtThai;

    // ตำบล
    this.data.tambonId = item.tambonId;
    this.data.tambonName = item.tambonThai;

    // }
    // ขอย้ายสถานการค้าปลีก

    //this.address_modal_reference.close();
  }

  numberOnly(event): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  numberComaOnly(event): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 45 || charCode > 57)) {
      return false;
    }
    return true;
  }

  // สำเนาหนังสือรับรองของกระทรวงพาณิชย์

  // commerceFileList: [
  //สำเนาหนังสือรับรองของกระทรวงพาณิชย์
  // {
  //     "fileName": "houseFileList.jpg",
  //     "fileType": "I",
  //     "pathFile": "14158953-f5fe-4a66-a680-14aa099da5a5.pdf",
  //     "fileNum": 1
  // }
  // ],

  clickFileImage_commerceFileList() {
    let element: HTMLElement = document.getElementById(
      "fileImage_commerceFileList"
    ) as HTMLElement;
    element.click();
  }

  fileOverBase_commerceFileList(event) {
    if (event) {
      this.ngxService.start();
      this.uploader_commerceFileList.getUploadListener().subscribe((res) => {
        this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
        let fileListImage = JSON.parse(res.respond.replace(/'/g, '"'));
        let filetype = fileListImage[0].split(".");
        if (filetype[1].toUpperCase() == "PDF") {
          filetype[1] = "P";
        } else if (
          filetype[1].toUpperCase() == "DOCX" ||
          filetype[1].toUpperCase() == "DOC"
        ) {
          filetype[1] = "W";
        } else if (
          filetype[1].toUpperCase() == "XLS" ||
          filetype[1].toUpperCase() == "XLSX"
        ) {
          filetype[1] = "E";
        }

        const fomatdata = {
          pathFileName: res.item.file.name,
          fileType: filetype[1],
          pathFile: fileListImage[0],
          fileNum: "",
          ShowFile:
            REST_URL.MAIN_UPLOADFILE.getFileTemp + "/" + fileListImage[0],
        };
        let listUrl = this.data.commerceFileList.find((val) => {
          if (val === fileListImage[0]) {
            return true;
          } else {
            return false;
          }
        });
        if (listUrl !== fileListImage[0]) {
          this.data.commerceFileList.push(fomatdata);
        }

        console.log("this.data.commerceFileList");
        console.log(this.data.commerceFileList);
      });
    }
  }

  public removeFiles_commerceFileList(PathUid, i) {
    this.data.commerceFileList.splice(i, 1);
    this.delect_commerceFileList.push(PathUid);
  }

  // เอกสารแนบสำเนาทะเบียนบ้าน

  //หนังสือมอบอำนาจ
  // authorizeFileList: [
  //หนังสือมอบอำนาจ
  // {
  //     "fileName": "idCardFileList.pdf",
  //     "fileType": "P",
  //     "pathFile": "14158953-f5fe-4a66-a680-14aa099da5a5.pdf",
  //     "fileNum": 1
  // }
  // ],

  clickFileImage_authorizeFileList() {
    let element: HTMLElement = document.getElementById(
      "fileImage_authorizeFileList"
    ) as HTMLElement;
    element.click();
  }

  fileOverBase_authorizeFileList(event) {
    if (event) {
      this.ngxService.start();
      this.uploader_authorizeFileList.getUploadListener().subscribe((res) => {
        this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
        let fileListImage = JSON.parse(res.respond.replace(/'/g, '"'));
        let filetype = fileListImage[0].split(".");
        if (filetype[1].toUpperCase() == "PDF") {
          filetype[1] = "P";
        } else if (
          filetype[1].toUpperCase() == "DOCX" ||
          filetype[1].toUpperCase() == "DOC"
        ) {
          filetype[1] = "W";
        } else if (
          filetype[1].toUpperCase() == "XLS" ||
          filetype[1].toUpperCase() == "XLSX"
        ) {
          filetype[1] = "E";
        }

        const fomatdata = {
          pathFileName: res.item.file.name,
          fileType: filetype[1],
          pathFile: fileListImage[0],
          fileNum: "",
          ShowFile:
            REST_URL.MAIN_UPLOADFILE.getFileTemp + "/" + fileListImage[0],
        };
        let listUrl = this.data.authorizeFileList.find((val) => {
          if (val === fileListImage[0]) {
            return true;
          } else {
            return false;
          }
        });
        if (listUrl !== fileListImage[0]) {
          this.data.authorizeFileList.push(fomatdata);
        }

        console.log("this.data.authorizeFileList");
        console.log(this.data.authorizeFileList);
      });
    }
  }

  public removeFiles_authorizeFileList(PathUid, i) {
    this.data.authorizeFileList.splice(i, 1);
    this.delect_authorizeFileList.push(PathUid);
  }

  //หนังสือมอบอำนาจ

  //บัญชีรายการค้างชำระภาษี
  // taxRemainFileList: [
  //   บัญชีรายการค้างชำระภาษี
  //   {
  //       "fileName": "taxFileList.pdf",
  //       "fileType": "P",
  //       "pathFile": "14158953-f5fe-4a66-a680-14aa099da5a5.pdf",
  //       "fileNum": 1
  //   }
  // ],

  clickFileImage_taxRemainFileList() {
    let element: HTMLElement = document.getElementById(
      "fileImage_taxRemainFileList"
    ) as HTMLElement;
    element.click();
  }

  fileOverBase_taxRemainFileList(event) {
    if (event) {
      this.ngxService.start();
      this.uploader_taxRemainFileList.getUploadListener().subscribe((res) => {
        this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
        let fileListImage = JSON.parse(res.respond.replace(/'/g, '"'));
        let filetype = fileListImage[0].split(".");
        if (filetype[1].toUpperCase() == "PDF") {
          filetype[1] = "P";
        } else if (
          filetype[1].toUpperCase() == "DOCX" ||
          filetype[1].toUpperCase() == "DOC"
        ) {
          filetype[1] = "W";
        } else if (
          filetype[1].toUpperCase() == "XLS" ||
          filetype[1].toUpperCase() == "XLSX"
        ) {
          filetype[1] = "E";
        }

        const fomatdata = {
          pathFileName: res.item.file.name,
          fileType: filetype[1],
          pathFile: fileListImage[0],
          fileNum: "",
          ShowFile:
            REST_URL.MAIN_UPLOADFILE.getFileTemp + "/" + fileListImage[0],
        };
        let listUrl = this.data.taxRemainFileList.find((val) => {
          if (val === fileListImage[0]) {
            return true;
          } else {
            return false;
          }
        });
        if (listUrl !== fileListImage[0]) {
          this.data.taxRemainFileList.push(fomatdata);
        }

        console.log("this.data.taxRemainFileList");
        console.log(this.data.taxRemainFileList);
      });
    }
  }

  public removeFiles_taxRemainFileList(PathUid, i) {
    this.data.taxRemainFileList.splice(i, 1);
    this.delect_taxRemainFileList.push(PathUid);
  }

  //บัญชีรายการค้างชำระภาษี

  //   เอกสารอื่น ๆ
  // otherFileList: [
  //   เอกสารอื่น ๆ
  //   {
  //       "fileName": "cerFileList.pdf",
  //       "fileType": "P",
  //       "pathFile": "14158953-f5fe-4a66-a680-14aa099da5a5.pdf",
  //       "fileNum": 1
  //   }
  // ],

  clickFileImage_otherFileList() {
    let element: HTMLElement = document.getElementById(
      "fileImage_otherFileList"
    ) as HTMLElement;
    element.click();
  }

  fileOverBase_otherFileList(event) {
    if (event) {
      this.ngxService.start();
      this.uploader_otherFileList.getUploadListener().subscribe((res) => {
        this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
        let fileListImage = JSON.parse(res.respond.replace(/'/g, '"'));
        let filetype = fileListImage[0].split(".");
        if (filetype[1].toUpperCase() == "PDF") {
          filetype[1] = "P";
        } else if (
          filetype[1].toUpperCase() == "DOCX" ||
          filetype[1].toUpperCase() == "DOC"
        ) {
          filetype[1] = "W";
        } else if (
          filetype[1].toUpperCase() == "XLS" ||
          filetype[1].toUpperCase() == "XLSX"
        ) {
          filetype[1] = "E";
        }

        const fomatdata = {
          pathFileName: res.item.file.name,
          fileType: filetype[1],
          pathFile: fileListImage[0],
          fileNum: "",
          ShowFile:
            REST_URL.MAIN_UPLOADFILE.getFileTemp + "/" + fileListImage[0],
        };
        let listUrl = this.data.otherFileList.find((val) => {
          if (val === fileListImage[0]) {
            return true;
          } else {
            return false;
          }
        });
        if (listUrl !== fileListImage[0]) {
          this.data.otherFileList.push(fomatdata);
        }

        console.log("this.data.otherFileList");
        console.log(this.data.otherFileList);
      });
    }
  }

  public removeFiles_otherFileList(PathUid, i) {
    this.data.otherFileList.splice(i, 1);
    this.delect_otherFileList.push(PathUid);
  }

  //   เอกสารอื่น ๆ

  public getDrpListByPostCodeMain_Retail(postCode) {
    if (postCode.length == 5) {
      this.typeData = true;

      let d = {
        postCodeMain: postCode,
      };

      this.ngxService.start();

      this.addressService.getDrpListByPostCodeMain(d).subscribe((res) => {
        this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

        console.log("res drpListByPostCodeMainRetail");
        console.log(res);

        if (res.result) {
          this.drpListByPostCodeMainRetail = res.result;
        } else {
          this.drpListByPostCodeMainRetail = [];
        }
      });
    }
  }

  select_address_modal_Retail(item) {
    // ขอย้ายสถานการค้าปลีก
    // if (this.data.businessStatusCode == "B") {
    this.typeData = false;
    // จังหวัด
    this.data.provinceIdRetail = item.provinceId;
    this.data.provinceNameRetail = item.provinceThai;

    // อำเภอ
    this.data.amphurIdRetail = item.districtId;
    this.data.amphurNameRetail = item.districtThai;

    // ตำบล
    this.data.tambonIdRetail = item.tambonId;
    this.data.tambonNameRetail = item.tambonThai;

    // }
    // ขอย้ายสถานการค้าปลีก

    //this.address_modal_reference.close();
  }
}
