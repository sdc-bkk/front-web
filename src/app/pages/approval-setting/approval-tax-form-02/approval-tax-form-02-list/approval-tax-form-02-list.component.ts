import { ViewChild, TemplateRef } from "@angular/core";
import { Component, ViewEncapsulation, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Subject } from "rxjs";
import { finalize, takeUntil } from "rxjs/operators";
import { ConfigService } from "../../../../shared/services/config/config.service";
import { DbMenuService } from "../../../../shared/localstorage/db-menu-service";
import Swal from "sweetalert2";
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { ApprovalTaxForm02Service } from "../approval-tax-form-02.service";
import { AddressService } from "../../../address.service";
import { BusinessStatusService } from "../../../business-status.service";
import { FrontretailService } from "../../../frontretail.service";
import { ApprovalTaxForm01Service } from "../../approval-tax-form-01/approval-tax-form-01.service";

@Component({
  selector: ".approval-tax-form-02-list-inner-wrapper",
  templateUrl: "./approval-tax-form-02-list.component.html",
  styleUrls: ["./approval-tax-form-02-list.component.scss"],
  encapsulation: ViewEncapsulation.Emulated,
})
export class ApprovalTaxForm02ListComponent implements OnInit {
  /////////////////// Config ///////////////////
  public allExpandState = false;
  panelOpenState: boolean = false;
  allRowsSelected: boolean;
  selectedCheck = 0;
  isSearchActive: boolean = false;
  ///////////////// End Config /////////////////

  ///////////////// Variable /////////////////
  public model: any = {};
  public loading = false;
  private unsubscribeAll = new Subject();
  /////////////// End Variable ////////////////

  //////////////// Value /////////////////////
  dataList: any[] = [];
  dataListTemp: any[] = [];
  maxitemPerPage: number = this.config.paging.ITEMPERPAGE;
  maxSize: number = this.config.paging.MAXSIZE;
  totalItemdataList: number;
  searchValue = {
    docNo: "",
    stationName: "",
    provinceId: "",
    tambonId: "",
    amphurId: "",
    amphurName: "",
    businessStatusId: "",
    businessStatusName: "",
    status: "",
    username: "",
  };
  checkAll: any;
  public currentMenuDataBase: any;

  // ส่งแบบ
  @ViewChild("sendDesign_modal")
  public sendDesign_modal: TemplateRef<any>;
  sendDesign_modalReference: NgbModalRef;

  sendDesignData_modal: any = [];
  // ส่งแบบ

  // ไม่ผ่าน

  @ViewChild("statusNotApprove_modal")
  public statusNotApprove_modal: TemplateRef<any>;
  statusNotApprove_modalReference: NgbModalRef;

  statusNotApproveData_modal: any = {
    reportid: "",
    regisdate: "",
    texno: "",
    companyname: "",
    pn01: "",
    r1: "",
    statusName: "",
  };

  // ไม่ผ่าน

  @ViewChild("modalcheckAllSystem")
  public modalcheckAllSystem: TemplateRef<any>;
  modalReferencecheckAllSystem: NgbModalRef;

  modalcheckAllSystem_model: any = {
    name: "",
    county: "",
  };

  @ViewChild("modalReplyAllSystem")
  public modalReplyAllSystemAllSystem: TemplateRef<any>;
  modalReferenceReplyAllSystemAllSystem: NgbModalRef;

  modalReplyAllSystem_model: any = {
    name: "",
    county: "",
    detail: "",
  };

  resultPage: any = {
    pageNo: 1,
    totalItem: 0,
    itemPerPage: 0,
    collectionSize: 0,
  };

  drpListProvince: []; // จังหวัด
  drpListDistrict: []; // อำเภอ
  drpListTambon: []; // ตำบล
  drpBusinessStatus: any[] = []; // ความประสงค์

  currentUser: any;

  report_pn01 = "/api/report/";

  getDataByMemberNameStatus: any = false;

  getTaxForm02PDF: any = "taxexportwebservice/api/formExport/getTaxForm02PDF/";

  status_sendplan_modal: any = false;

  @ViewChild("statusDesign_modal")
  public statusDesign_modal: TemplateRef<any>;
  statusDesign_modalReference: NgbModalRef;

  drpListStatus: any[] = [];

  _stationName: any = "";
  statusDesignModal: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public config: ConfigService,
    private dbMenuService: DbMenuService,
    private modalService: NgbModal,
    public ngxService: NgxUiLoaderService,
    private approvalTaxForm02Service: ApprovalTaxForm02Service,
    private addressService: AddressService,
    private businessStatusService: BusinessStatusService,
    private frontretailService: FrontretailService,
    private approvalTaxForm01Service: ApprovalTaxForm01Service
  ) {}

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem("ba_currentUser"));

    this.getTaxForm02PDF = this.config.urllinkReport + this.getTaxForm02PDF;

    this.getDrpListStatus();

    console.log("this.currentUser");
    console.log(this.currentUser);
    this.getDataByMemberName();
    this.getDrpListProvince();
    this.getDrpBusinessStatusList();
    this.getList(1);
  }

  public getDrpListStatus() {
    this.approvalTaxForm01Service.getDrpListStatus().subscribe((res) => {
      // console.log("res getDrpListStatus ------- ");
      // console.log(res);

      if (res.result) {
        this.drpListStatus = res.result;
      } else {
        this.drpListStatus = [];
      }
    });
  }

  ngOnDestroy() {}

  public getDataByMemberName() {
    const payload = {
      memberName: this.currentUser.memberName,
    };

    this.ngxService.start();

    this.frontretailService.getDataByMemberName(payload).subscribe((res) => {
      console.log("res getDataByMemberName");
      console.log(res);
      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

      //this.getDataByMemberNameStatus = res.status;

      if (res.status == "false") {
        //this.data.firstRecord = false;
        this.getDataByMemberNameStatus = false;
      } else {
        res.result.firstRecord = true;
        this.getDataByMemberNameStatus = true;
      }
    });
  }

  public refreshDataSearch() {
    this.searchValue = {
      docNo: "",
      stationName: "",
      provinceId: "",
      tambonId: "",
      amphurId: "",
      amphurName: "",
      businessStatusId: "",
      businessStatusName: "",
      status: "",
      username: "",
    };

    this.config.searchValue = this.searchValue;
    this.getList(1);
  }

  public getList(page) {
    // let active = null;
    // if (this.searchValue.active == "true") {
    //   active = true;
    // } else if (this.searchValue.active == "false") {
    //   active = false;
    // }

    let data = {
      page: page,
      itemPerPage: this.config.paging.ITEMPERPAGE,
      orderBy: "updatedDate",
      sort: "desc",

      docNo: this.searchValue.docNo,
      stationName: this.searchValue.stationName,
      provinceId: this.searchValue.provinceId,
      tambonId: this.searchValue.tambonId,
      amphurId: this.searchValue.amphurId,
      amphurName: this.searchValue.amphurName,
      businessStatusId: this.searchValue.businessStatusId,
      businessStatusName: this.searchValue.businessStatusName,
      status: this.searchValue.status,
      username: this.searchValue.username,
      memberName: this.currentUser.memberName,
    };
    // console.log("data");
    // console.log(data);

    this.config.resultPage.pageNo = page;

    this.ngxService.start();

    this.approvalTaxForm02Service.getList(data).subscribe((res) => {
      console.log("res approvalTaxForm02Service  getList");
      console.log(res);

      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

      if (res.result) {
        let selected = 0;

        res.result.map((item) => {
          let dataList = this.dataListTemp.find(
            (temp) => item.taxForm02Id == temp.taxForm02Id
          );
          if (dataList) {
            selected++;
            item.selected = true;
          } else {
            item.selected = false;
          }

          return item;
        });

        this.dataList = res.result;
        this.totalItemdataList = res.resultPage.totalItem;
        this.config.resultPage.totalItem = res.resultPage.totalItem;

        if (selected == res.result.length && selected > 0) {
          this.allRowsSelected = true;
        } else {
          this.allRowsSelected = false;
        }

        if (res.result.lengt == 0) {
          this.allRowsSelected = false;
        }

        this.resultPage.totalItem = res.resultPage.totalItem;
        if (this.totalItemdataList < selected) {
          this.selectedCheck = res.resultPage.totalItem;
        } else {
          this.selectedCheck = this.dataListTemp.length;
        }
      } else {
        this.dataList = [];
        this.totalItemdataList = 0;
        this.config.resultPage.totalItem = 0;
        this.selectedCheck = 0;
      }
    });
  }

  public selectAll(event) {
    this.allRowsSelected = event;
    if (event) {
      this.dataList.map((val) => {
        if (val.status == "0" || val.status == "2") {
          val.selected = true;

          let hasData = this.dataListTemp.find((dataList) => {
            return dataList.taxForm02Id == val.taxForm02Id;
          });

          if (!hasData) {
            return this.dataListTemp.push(val);
          }
        }
      });
    } else {
      this.dataListTemp = this.dataListTemp.reduce((result, val) => {
        let hasData = this.dataList.find((data) => {
          return data.taxForm02Id == val.taxForm02Id;
        });

        if (!hasData) {
          result = [...result, val];
        }

        return result;
      }, []);

      this.dataList.map((val) => {
        val.selected = false;
        return val;
      });
    }
    this.selectedCheck = this.dataListTemp.length;
  }

  public countselectedCheck(event, item) {
    this.dataListTemp = this.dataListTemp.filter(
      (temp) => temp.taxForm02Id != item.taxForm02Id
    );
    if (event) {
      this.selectedCheck++;
      this.dataListTemp.push(item);
    } else {
      this.selectedCheck--;
    }

    let CountCheckTrue = 0;
    this.dataList.map((item) => {
      let dataList = this.dataListTemp.find(
        (temp) => item.taxForm02Id == temp.taxForm02Id
      );

      if (dataList) {
        item.selected = true;
        CountCheckTrue++;
      } else {
        item.selected = false;
      }
      return item;
    });

    if (CountCheckTrue == this.dataList.length) {
      this.allRowsSelected = true;
    } else {
      this.allRowsSelected = false;
    }
  }

  public searchActive() {
    if (this.isSearchActive == false) {
      this.isSearchActive = true;
    } else {
      this.isSearchActive = false;
    }
    this.config.setIsSearchActive(this.isSearchActive);
  }
  public triggerClose(event) {
    this.isSearchActive = !this.isSearchActive;
  }

  public opencheckAllSystem(modalItems) {
    this.modalcheckAllSystem_model = {
      name: "",
      county: "",
    };

    this.modalReferencecheckAllSystem = this.modalService.open(modalItems, {
      size: "lg",
      windowClass: "modal-xl",
    });
  }

  public openReplyAllSystem(modalItems) {
    this.modalReplyAllSystem_model = {
      name: "",
      county: "",
      detial: "",
    };
  }

  updatecheckAll(modalItems, item) {
    this.modalcheckAllSystem_model = {
      name: "",
      county: "",
    };

    let d = this.dataList.find((val) => {
      return val.id == item.id;
    });

    // console.log("d");
    // console.log(d);

    if (d) {
      let m = {
        id: d.id,
        name: d.name,
        county: d.county,
        active: true,
      };

      this.modalcheckAllSystem_model = m;

      this.modalReferencecheckAllSystem = this.modalService.open(modalItems, {
        size: "lg",
        windowClass: "modal-xl",
      });
    }
  }

  updateReplyAll(modalItems, item) {
    this.modalReplyAllSystem_model = {
      name: "",
      county: "",
      detial: "",
    };

    let d = this.dataList.find((val) => {
      return val.id == item.id;
    });

    // console.log("d");
    // console.log(d);

    if (d) {
      let m = {
        id: d.id,
        name: d.name,
        county: d.county,
        active: true,
      };

      this.modalReplyAllSystem_model = m;

      this.modalReferenceReplyAllSystemAllSystem = this.modalService.open(
        modalItems,
        {
          size: "lg",
          windowClass: "modal-xl",
        }
      );
    }
  }

  // ส่งแบบ
  public open_sendDesign() {
    this.status_sendplan_modal = false;

    let sendDesignData_modal = [];
    // tslint:disable-next-line:triple-equals
    if (this.dataListTemp != undefined && this.dataListTemp.length != 0) {
      sendDesignData_modal = this.dataListTemp;

      this.sendDesignData_modal = sendDesignData_modal;

      console.log("this.sendDesignData_modal");
      console.log(this.sendDesignData_modal);

      let businessStatusName = "";

      this.sendDesignData_modal.forEach((el) => {
        el.businessStatusList.forEach((ment, index) => {
          if (index == 0) {
            businessStatusName = ment.name;
          } else {
            businessStatusName = businessStatusName + ", " + ment.name;
          }
        });
        el.businessStatusName = businessStatusName;
      });

      // this.sendDesignData_modal.businessStatusName = businessStatusName;

      this.sendDesign_modalReference = this.modalService.open(
        this.sendDesign_modal,
        {
          size: "lg",
          windowClass: "modal-xl",
        }
      );
    } else {
      Swal.fire({
        icon: "warning",
        title: "แจ้งเตือน",
        text: "กรุณาเลือกรายการที่จะส่งแบบ",
        showConfirmButton: false,
        timer: 3000,
      });
    }
  }

  public submit_sendDesign() {
    let idList = [];
    this.sendDesignData_modal.forEach((e) => {
      idList.push(e.taxForm02Id);
    });

    let d = {
      idList: idList,
      actionBy: this.currentUser.memberName,
      remark: "ขอส่งแบบ ภน.02",
    };

    console.log("d");
    console.log(d);

    this.approvalTaxForm02Service.sendApprove(d).subscribe((res) => {
      // console.log("res");
      // console.log(res);
      if (res.result) {
        Swal.fire({
          icon: "success",
          title: "สำเร็จ",
          text: "บันทึกข้อมูลสำเร็จ",
          showConfirmButton: false,
          timer: 1500,
        });

        this.dataListTemp = [];

        setTimeout(() => {
          this.sendDesign_modalReference.close();
          this.getList(1);
        }, 1500);
      } else {
        Swal.fire("ไม่สำเร็จ", res.message, "error");
      }
    });
  }

  public close_sendDesign() {
    this.sendDesign_modalReference.close();
  }
  // ส่งแบบ

  // ไม่ผ่าน
  // public open_statusNotApprove(modalItems) {
  //   this.statusNotApproveData_modal = {
  //     regisdate: "10/11/2563",
  //     companyname: "ปั้ม PTT ห้วยขวาง",
  //     kname: "ห้วยขวาง",
  //     detaill: "เอกสารไม่ครบ",
  //   };

  //   this.statusNotApprove_modalReference = this.modalService.open(modalItems, {
  //     size: "lg",
  //     windowClass: "modal-xl",
  //   });
  // }

  // public submit_statusNotApprove() {
  //   this.statusNotApprove_modalReference.close();
  // }

  // public close_statusNotApprove() {
  //   this.statusNotApprove_modalReference.close();
  // }
  // ไม่ผ่าน

  public add() {
    this.router.navigate([
      "/fontOffice/approval-setting/approval-tax-form-02/update/",
      0,
    ]);
  }

  public update(id) {
    this.router.navigate([
      "/fontOffice/approval-setting/approval-tax-form-02/update/",
      id,
    ]);
  }

  public view(id) {
    this.router.navigate([
      "/fontOffice/approval-setting/approval-tax-form-02/view/",
      id,
    ]);
  }

  public delete(item, mes) {
    if (mes != "row") {
      if (this.dataListTemp.length != 0) {
        Swal.fire({
          title: "คุณต้องการลบใช่หรือไม่",
          text:
            "จำนวนรายการที่ต้องการลบ " + this.dataListTemp.length + " รายการ",
          icon: "warning",
          showCancelButton: true,
          confirmButtonText: "ตกลง",
          cancelButtonText: "ยกเลิก",
        }).then((result) => {
          if (result.value) {
            let d = {
              idList: [],
              updatedBy: "1",
            };

            this.dataListTemp.forEach((v) => {
              d.idList.push(v.taxForm02Id);
            });

            this.ngxService.start();

            this.approvalTaxForm02Service.deleteData(d).subscribe((res) => {
              this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
              // console.log("res delete");
              // console.log(res);
              if (res.result) {
                Swal.fire("สำเร็จ", "ลบข้อมูลสำเร็จ", "success");
                this.dataListTemp = [];
                // ดักตอนลบ
                this.getList(1);
              } else {
                Swal.fire("ไม่สำเร็จ", res.resultMsg, "error");
                this.dataListTemp = [];
                // ดักตอนลบ
                this.getList(1);
              }
            });

            // Swal.fire("ไม่สำเร็จ", "ลบข้อมูลไม่สำเร็จ", "error");
            // For more information about handling dismissals please visit
            // https://sweetalert2.github.io/#handling-dismissals
          } else if (result.dismiss === Swal.DismissReason.cancel) {
            // Swal.fire("Cancelled", "Your imaginary file is safe :)", "error");
          }
        });
      } else {
        Swal.fire("กรุณาเลือกรายการ", "", "error");
      }
    } else {
      Swal.fire({
        title: "คุณต้องการลบใช่หรือไม่",
        text: item.stationName,
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "ตกลง",
        cancelButtonText: "ยกเลิก",
      }).then((result) => {
        if (result.value) {
          let d = {
            idList: [],
            updatedBy: "1",
          };
          d.idList.push(item.taxForm02Id);

          console.log("item");
          console.log(item);

          this.ngxService.start();

          this.approvalTaxForm02Service.deleteData(d).subscribe((res) => {
            this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
            // console.log("res delete");
            // console.log(res);
            if (res.result) {
              Swal.fire("สำเร็จ", "ลบข้อมูลสำเร็จ", "success");
              this.dataListTemp = [];
              // ดักตอนลบ
              this.getList(1);
            } else {
              Swal.fire("ไม่สำเร็จ", res.resultMsg, "error");
              this.dataListTemp = [];
              // ดักตอนลบ
              this.getList(1);
            }
          });

          // Swal.fire("ไม่สำเร็จ", "ลบข้อมูลไม่สำเร็จ", "error");
          // For more information about handling dismissals please visit
          // https://sweetalert2.github.io/#handling-dismissals
        } else if (result.dismiss === Swal.DismissReason.cancel) {
          // Swal.fire("Cancelled", "Your imaginary file is safe :)", "error");
        }
      });
    }
  }

  downloadPDF_pn02(taxForm02Id): any {
    // window.open("../../../../../assets/pdf/form_oil_tax1.pdf", "_blank");
    window.open(this.report_pn01 + taxForm02Id, "_blank");
  }

  // จังหวัด
  public getDrpListProvince() {
    this.addressService.getDrpListProvince().subscribe((res) => {
      // console.log("res getDrpListProvince");
      // console.log(res);

      if (res.result) {
        this.drpListProvince = res.result;
      } else {
        this.drpListProvince = [];
      }
    });
  }

  // อำเภอ
  public getDrpListDistrict(provinceId) {
    this.ngxService.start();
    this.addressService.getDrpListDistrict(provinceId).subscribe((res) => {
      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
      // console.log("res getDrpListDistrict");
      // console.log(res);

      if (res.result) {
        this.drpListDistrict = res.result;
      } else {
        this.drpListDistrict = [];
      }
    });
  }

  // ตำบล
  public getDrpListTambon(districtId) {
    this.ngxService.start();
    this.addressService.getDrpListTambon(districtId).subscribe((res) => {
      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
      // console.log("res getDrpListTambon");
      // console.log(res);

      if (res.result) {
        this.drpListTambon = res.result;
      } else {
        this.drpListTambon = [];
      }
    });
  }

  // วัตถุประสงค์
  public getDrpBusinessStatusList() {
    this.ngxService.start();
    this.businessStatusService.getDrpBusinessStatusList().subscribe((res) => {
      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
      console.log("res getDrpBusinessStatusList ---");
      console.log(res);

      if (res.result) {
        this.drpBusinessStatus = res.result;
      } else {
        this.drpBusinessStatus = [];
      }
    });
  }

  public open_statusDesign(item) {
    this.getListStatusLog(item);

    // this.statusDesignModal = item;
  }

  public getListStatusLog(item) {
    const payload = {
      taxForm02Id: item.taxForm02Id,
    };

    this.ngxService.start();

    this._stationName = item.stationName;

    this.approvalTaxForm02Service.getListStatusLog(payload).subscribe((res) => {
      console.log("res getListStatusLog");
      console.log(res);

      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

      if (res.result) {
        res.result.forEach((element) => {
          element.select = true;
        });

        this.statusDesignModal = res.result;

        this.statusDesignModal.stationName = item.stationName;

        console.log("this.statusDesignModal");
        console.log(this.statusDesignModal);
      } else {
        this.statusDesignModal = res.result;
      }

      this.statusDesign_modalReference = this.modalService.open(
        this.statusDesign_modal,
        {
          size: "lg",
          windowClass: "modal-xl",
        }
      );
    });
  }

  public close_statusDesign() {
    this.statusDesign_modalReference.close();
  }
}
