import { Routes, RouterModule } from "@angular/router";
import { HistoryPaymentTaxOilComponent } from "./history-payment-tax-oil.component";
import { HistoryPaymentTaxOilListComponent } from "./history-payment-tax-oil-list/history-payment-tax-oil-list.component";

const TEMPLATE_ROUTES: Routes = [
  {
    path: "",
    component: HistoryPaymentTaxOilComponent,
    children: [
      {
        path: "",
        component: HistoryPaymentTaxOilListComponent,
      },
    ],
  },

  // 404 Page Not Found
  { path: "**", redirectTo: "/" },
];

export const HistoryPaymentTaxOilRoutes = RouterModule.forChild(TEMPLATE_ROUTES);
