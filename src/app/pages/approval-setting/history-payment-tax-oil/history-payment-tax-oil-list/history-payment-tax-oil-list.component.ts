import { ViewChild, TemplateRef } from "@angular/core";
import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { Subject } from "rxjs";
import { finalize, takeUntil } from "rxjs/operators";
import { ConfigService } from "../../../../shared/services/config/config.service";
import { DbMenuService } from "../../../../shared/localstorage/db-menu-service";
import Swal from "sweetalert2";
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";

import { REST_URL } from "../../../../shared/services/config/rest-url";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { DbUserLocalstorage } from "../../../../shared/services/localstorage/db-user-localstorage";
import { HistoryPaymentTaxOilService } from "../history-payment-tax-oil.service";
import Utils from "../../../../shared/util/utils";

@Component({
  selector: 'history-payment-tax-oil-list',
  templateUrl: './history-payment-tax-oil-list.component.html',
  styleUrls: ['./history-payment-tax-oil-list.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
})
export class HistoryPaymentTaxOilListComponent implements OnInit {

  /////////////////// Config ///////////////////
  public allExpandState = false;
  panelOpenState: boolean = false;
  allRowsSelected: boolean;
  selectedCheck = 0;
  isSearchActive: boolean = false;
  ///////////////// End Config /////////////////

  ///////////////// Variable /////////////////
  public model: any = {};
  public loading = false;
  private unsubscribeAll = new Subject();
  /////////////// End Variable ////////////////

  //////////////// Value /////////////////////
  dataList: any[] = [];
  dataListTemp: any[] = [];
  maxitemPerPage: number = this.config.paging.ITEMPERPAGE;
  maxSize: number = this.config.paging.MAXSIZE;
  totalItemdataList: number;
  searchValue = {
    stationName: "",
    yearly: "",
    monthly: "",
    startDate: "",
    endDate: ""
  };
  checkAll: any;
  check_agree: any = false;
  public currentMenuDataBase: any;

  resultPage: any = {
    pageNo: 1,
    totalItem: 0,
    itemPerPage: 0,
    collectionSize: 0,
  };

  Month: any = new Date().getMonth() + 1;
  year: any = new Date().getFullYear() + 543;

  drpyear: any = [];

  currentUser: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public config: ConfigService,
    private dbUserLocalstorage: DbUserLocalstorage,
    private dbMenuService: DbMenuService,
    private modalService: NgbModal,
    public ngxService: NgxUiLoaderService,
    public historyPaymentTaxOilService: HistoryPaymentTaxOilService,
  ) {}

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem("ba_currentUser"));

    this.drpyear.push(this.year - 1);
    this.drpyear.push(this.year);

    this.getList(1);
  }
  ngOnDestroy() { }

  public getList(page) {
    let startDate;
    if (this.searchValue.startDate != null) {
      startDate = Utils.dateTostring(this.searchValue.startDate);
    } else {
      startDate = "";
    }

    let endDate;
    if (this.searchValue.endDate != null) {
      endDate = Utils.dateTostring(this.searchValue.endDate);
    } else {
      endDate = "";
    }

    let data = {
      page: page,
      itemPerPage: this.config.paging.ITEMPERPAGE,
      // orderBy: "createdDate",
      // sort: "desc",

      stationName: this.searchValue.stationName,
      yearly: this.searchValue.yearly,
      monthly: this.searchValue.monthly,
      startDate: startDate,
      endDate: endDate,
      memberName: this.currentUser.memberName,
    };
    // console.log("data");
    // console.log(data);

    this.config.resultPage.pageNo = page;

    this.ngxService.start();

    this.historyPaymentTaxOilService.getListHistory(data).subscribe((res) => {
      console.log("res historyPaymentTaxOilService  getListHistory");
      console.log(res);

      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

      if (res.result) {
        let selected = 0;
        res.result.map((item) => {
          let dataList = this.dataListTemp.find(
            (temp) => item.invoiceId == temp.invoiceId
          );
          if (dataList) {
            selected++;
            item.selected = true;
          } else {
            item.selected = false;
          }
          return item;
        });

        this.dataList = res.result;
        this.totalItemdataList = res.resultPage.totalItem;
        this.config.resultPage.totalItem = res.resultPage.totalItem;

        if (selected == res.result.length && selected > 0) {
          this.allRowsSelected = true;
        } else {
          this.allRowsSelected = false;
        }

        if (res.result.lengt == 0) {
          this.allRowsSelected = false;
        }

        this.resultPage.totalItem = res.resultPage.totalItem;
        if (this.totalItemdataList < selected) {
          this.selectedCheck = res.resultPage.totalItem;
        } else {
          this.selectedCheck = this.dataListTemp.length;
        }
      } else {
        this.dataList = [];
        this.totalItemdataList = 0;
        this.config.resultPage.totalItem = 0;
        this.selectedCheck = 0;
      }
    });

    // this.totalItemdataList = 1;
    // this.config.resultPage.totalItem = 1;

  }

  public searchActive() {
    if (this.isSearchActive == false) {
      this.isSearchActive = true;
    } else {
      this.isSearchActive = false;
    }
    this.config.setIsSearchActive(this.isSearchActive);
  }
  public triggerClose(event) {
    this.isSearchActive = !this.isSearchActive;
  }

  public refreshDataSearch() {
    this.searchValue = {
      stationName: "",
      yearly: "",
      monthly: "",
      startDate: "",
      endDate: ""
    };

    this.config.searchValue = this.searchValue;
    this.getList(1);
  }

  public selectAll(event) {
    this.allRowsSelected = event;
    if (event) {
      this.dataList.map((val) => {
        val.selected = true;

        let hasData = this.dataListTemp.find((dataList) => {
          return dataList.invoiceId == val.invoiceId;
        });

        if (!hasData) {
          return this.dataListTemp.push(val);
        }
      });
    } else {
      this.dataListTemp = this.dataListTemp.reduce((result, val) => {
        let hasData = this.dataList.find((data) => {
          return data.invoiceId == val.invoiceId;
        });

        if (!hasData) {
          result = [...result, val];
        }

        return result;
      }, []);

      this.dataList.map((val) => {
        val.selected = false;
        return val;
      });
    }
    this.selectedCheck = this.dataListTemp.length;
  }

  public countselectedCheck(event, item) {
    this.dataListTemp = this.dataListTemp.filter(
      (temp) => temp.invoiceId != item.invoiceId
    );
    if (event) {
      this.selectedCheck++;
      this.dataListTemp.push(item);
    } else {
      this.selectedCheck--;
    }

    let CountCheckTrue = 0;
    this.dataList.map((item) => {
      let dataList = this.dataListTemp.find(
        (temp) => item.invoiceId == temp.invoiceId
      );

      if (dataList) {
        item.selected = true;
        CountCheckTrue++;
      } else {
        item.selected = false;
      }
      return item;
    });

    if (CountCheckTrue == this.dataList.length) {
      this.allRowsSelected = true;
    } else {
      this.allRowsSelected = false;
    }
  }

}
