import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { NgxsModule } from "@ngxs/store";
import { SharedModule } from "../../../shared/shared.module";
import { HistoryPaymentTaxOilComponent } from "./history-payment-tax-oil.component";
import { HistoryPaymentTaxOilRoutes } from "./history-payment-tax-oil.routes";
import { HistoryPaymentTaxOilService } from "./history-payment-tax-oil.service";
import { HistoryPaymentTaxOilListComponent } from './history-payment-tax-oil-list/history-payment-tax-oil-list.component';
import { HistoryPaymentTaxOilApi } from "../../../api/history-payment-tax-oil-api";


@NgModule({
    declarations: [HistoryPaymentTaxOilComponent, HistoryPaymentTaxOilListComponent],
    providers: [
        HistoryPaymentTaxOilService,
        HistoryPaymentTaxOilApi,
    ],
    imports: [
        CommonModule,
        SharedModule,
        HistoryPaymentTaxOilRoutes,
    ],
})
export class HistoryPaymentTaxOilModule { }
