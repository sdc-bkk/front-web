import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import { HistoryPaymentTaxOilApi } from "../../../api/history-payment-tax-oil-api";

@Injectable({
  providedIn: 'root'
})
export class HistoryPaymentTaxOilService {
  constructor(private historyPaymentTaxOilApi: HistoryPaymentTaxOilApi) { }

  getListHistory(data): Observable<any> {
    return this.historyPaymentTaxOilApi.getListHistory(data);
  }

}
