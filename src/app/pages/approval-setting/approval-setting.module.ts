import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { SharedModule } from "../../shared/shared.module";
import { ApprovalSettingComponent } from "./approval-setting.component";
import { ApprovalSettingRoutes } from "./approval-setting.routes";

@NgModule({
  declarations: [ApprovalSettingComponent],
  providers: [],
  imports: [CommonModule, SharedModule, ApprovalSettingRoutes],
})
export class ApprovalSettingModule {}
