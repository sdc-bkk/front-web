import { Component, ViewEncapsulation, OnInit, Inject } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { NgxUiLoaderService } from "ngx-ui-loader";
import Swal from "sweetalert2";
import { FormControl, Validators } from "@angular/forms";
import { SignInService } from "./sign-in.service";
import { variable } from "@angular/compiler/src/output/output_ast";

@Component({
  selector: ".sign-in",
  templateUrl: "./sign-in.component.html",
  styleUrls: ["./sign-in.component.scss"],
  encapsulation: ViewEncapsulation.Emulated,
})
export class SignInComponent implements OnInit {
  /////////////////// Config ///////////////////

  ///////////////// End Config /////////////////
  ///////////////// Variable /////////////////
  public model: any = {
    username: "",
    password: "",
    confirmPassword: "",
  };

  public loading = false;
  public showLogin = false;
  /////////////// End Variable ////////////////

  public isSuperAdminGroup = false;

  public SubmitValue: any = {
    result: true,
    errMsg: "",
  };

  emailPattern = "[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+.[A-Za-z]{2,3}$";

  emailref: any = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public ngxService: NgxUiLoaderService,
    private signInService: SignInService
  ) {}

  ngOnInit() {
    this.showLogin = true;
    let dr = this.router.url.split("=");

    console.log("dr");
    console.log(dr);
    this.model.username = dr[1];
  }

  ngOnDestroy() {}

  public onSubmit() {
    if (this.model.password == "") {
      Swal.fire({
        icon: "error",
        title: "กรุณาระบุรหัสผ่าน",
        // text: "Modal with a custom image.",
        confirmButtonText: "ตกลง",
      });
    } else if (this.model.confirmPassword == "") {
      Swal.fire({
        icon: "error",
        title: "กรุณาระบุยืนยันรหัสผ่าน",
        // text: "Modal with a custom image.",
        confirmButtonText: "ตกลง",
      });
    } else {
      if (this.model.password != this.model.confirmPassword) {
        Swal.fire({
          icon: "error",
          title: "กรุณาระบุรหัสผ่านให้ตรงกัน",
          // text: "Modal with a custom image.",
          confirmButtonText: "ตกลง",
        });
      } else {
        this.setPassword();
      }
    }
  }

  public cancel() {
    this.router.navigate([""]);
  }

  public setPassword() {
    let d = {
      userName: this.model.username,
      password: this.model.password,
    };

    // console.log("d");
    // console.log(d);

    this.ngxService.start();
    this.signInService.setPassword(d).subscribe((res) => {
      console.log("res");
      console.log(res);
      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
      if (res.result) {
        Swal.fire({
          icon: "success",
          title: "บันทึกสำเร็จ",
          //text: res.message,
          showCancelButton: false,
          confirmButtonText: "เข้าสู่ระบบ",
        }).then((result) => {
          if (result.value) {
            //console.log("res.message");
            //console.log(res.message);
            const anchor = document.createElement("a");
            anchor.href = res.message;
            anchor.click();
            // Swal.fire("ไม่สำเร็จ", "ลบข้อมูลไม่สำเร็จ", "error");
            // For more information about handling dismissals please visit
            // https://sweetalert2.github.io/#handling-dismissals
          } else if (result.dismiss === Swal.DismissReason.cancel) {
            // Swal.fire("Cancelled", "Your imaginary file is safe :)", "error");
          }
        });
      } else {
        Swal.fire("ไม่สำเร็จ", res.message, "error");
      }
    });
  }
}
