import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { MemberApi } from "../../../api/member-api";
import { SharedModule } from "../../../shared/shared.module";
import { SignInComponent } from "./sign-in.component";
import { SignInService } from "./sign-in.service";

const ROUTE = [{ path: "", component: SignInComponent }];

@NgModule({
  providers: [SignInService, MemberApi],
  declarations: [SignInComponent],
  imports: [CommonModule, SharedModule, RouterModule.forChild(ROUTE)],
})
export class SignInModule {}
