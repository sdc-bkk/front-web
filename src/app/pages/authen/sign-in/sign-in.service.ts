import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import { MemberApi } from "../../../api/member-api";

@Injectable()
export class SignInService {
  constructor(private memberApi: MemberApi) {}

  setPassword(data): Observable<any> {
    return this.memberApi.setPassword(data);
  }
}
