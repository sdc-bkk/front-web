import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { MemberApi } from "../../../api/member-api";
import { SharedModule } from "../../../shared/shared.module";
import { SignUpComponent } from "./sign-up.component";
import { SignUpService } from "./sign-up.service";


const ROUTE = [{ path: "", component: SignUpComponent }];

@NgModule({
  providers: [SignUpService,MemberApi],
  declarations: [SignUpComponent],
  imports: [CommonModule, SharedModule, RouterModule.forChild(ROUTE)],
})
export class SignUpModule {}
