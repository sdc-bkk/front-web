import { Component, ViewEncapsulation, OnInit, Inject } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { SignUpService } from "./sign-up.service";
import { NgxUiLoaderService } from "ngx-ui-loader";
import Swal from "sweetalert2";
import { FormControl, Validators } from "@angular/forms";

@Component({
  selector: ".sign-up",
  templateUrl: "./sign-up.component.html",
  styleUrls: ["./sign-up.component.scss"],
  encapsulation: ViewEncapsulation.Emulated,
})
export class SignUpComponent implements OnInit {
  /////////////////// Config ///////////////////

  ///////////////// End Config /////////////////
  ///////////////// Variable /////////////////
  public model: any = {
    username: "",
    password: "",
    confirmPassword: "",
  };

  public loading = false;
  public showLogin = false;
  /////////////// End Variable ////////////////

  public isSuperAdminGroup = false;

  public SubmitValue: any = {
    result: true,
    errMsg: "",
  };

  emailPattern = "[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+.[A-Za-z]{2,3}$";

  emailref: any = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public ngxService: NgxUiLoaderService,
    private signUpService: SignUpService
  ) {}

  ngOnInit() {
    this.showLogin = true;
  }

  ngOnDestroy() {}

  public onSubmit() {
    let d = new FormControl(
      this.model.username,
      Validators.pattern(this.emailPattern)
    );

    // console.log("d");
    // console.log(d);

    // console.log("d");
    // console.log(d.status);

    if (this.model.username == "") {
      Swal.fire({
        icon: "error",
        title: "กรุณาระบุอีเมล",
        // text: "Modal with a custom image.",
        confirmButtonText: "ตกลง",
      });
    } else if (d.status == "INVALID") {
      Swal.fire({
        icon: "error",
        title: "กรุณาระบุรูปแบบอีเมลให้ถูกต้อง",
        // text: "Modal with a custom image.",
        confirmButtonText: "ตกลง",
      });
    } else {
      this.register();
    }
  }

  public cancel() {
    this.router.navigate([""]);
  }

  public register() {
    let d = {
      // userName: "kingwerewolf@gmail.com"
      userName: this.model.username,
    };

    // console.log("d");
    // console.log(d);

    this.ngxService.start();
    this.signUpService.register(d).subscribe((res) => {
      //console.log("res");
      //console.log(res);
      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
      if (res.result) {
        Swal.fire({
          icon: "success",
          title: "สำเร็จ",
          text: res.message,
          showCancelButton: false,
          confirmButtonText: "ตกลง",
        }).then((result) => {
          if (result.value) {
            this.router.navigate([""]);
            // Swal.fire("ไม่สำเร็จ", "ลบข้อมูลไม่สำเร็จ", "error");
            // For more information about handling dismissals please visit
            // https://sweetalert2.github.io/#handling-dismissals
          } else if (result.dismiss === Swal.DismissReason.cancel) {
            // Swal.fire("Cancelled", "Your imaginary file is safe :)", "error");
          }
        });
      } else {
        Swal.fire("ไม่สำเร็จ", res.message, "error");
      }
    });
  }

  public ValidatorsEmail(value) {
    let d = new FormControl(value, Validators.pattern(this.emailPattern));

    // console.log("d");
    // console.log(d);

    console.log("d");
    console.log(d.status);
  }
}
