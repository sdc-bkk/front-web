import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import { MemberApi } from "../../api/member-api";

@Injectable()
export class AuthenService {
  constructor(private memberApi: MemberApi) {}

  forgetPassword(data): Observable<any> {
    return this.memberApi.forgetPassword(data);
  }
  setPassword(data): Observable<any> {
    return this.memberApi.setPassword(data);
  }

  authenMember(data): Observable<any> {
    return this.memberApi.authenMember(data);
  }

  getDataByToken(data): Observable<any> {
    return this.memberApi.getDataByToken(data);
  }
}
