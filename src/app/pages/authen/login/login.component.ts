import { Component, ViewEncapsulation, OnInit, Inject } from "@angular/core";
import { FormControl, Validators } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { NgxUiLoaderService } from "ngx-ui-loader";
import Swal from "sweetalert2";
import { AuthGuardService } from "../../../auth-guard.service";
import { AuthService } from "../../../auth.service";
import { DbUserLocalstorage } from "../../../shared/services/localstorage/db-user-localstorage";
import { AuthenService } from "../authen.service";

@Component({
  selector: ".content_inner_wrapper",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
  encapsulation: ViewEncapsulation.Emulated,
})
export class LoginComponent implements OnInit {
  /////////////////// Config ///////////////////

  ///////////////// End Config /////////////////
  ///////////////// Variable /////////////////
  public model: any = {
    username: "",
    password: "",
  };

  public loading = false;
  public showLogin = false;
  /////////////// End Variable ////////////////

  public isSuperAdminGroup = false;

  public SubmitValue: any = {
    result: true,
    errMsg: "",
  };

  emailPattern = "[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+.[A-Za-z]{2,3}$";

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public dbUserLocalstorage: DbUserLocalstorage,
    public authService: AuthService,
    public ngxService: NgxUiLoaderService,
    public _authenService: AuthenService,
    public _authGuardService: AuthGuardService
  ) {}

  ngOnInit() {
    this.showLogin = true;
    this.dbUserLocalstorage.add_current_auth_success(false);
  }

  ngOnDestroy() {}

  validation_form() {
    let d = new FormControl(
      this.model.username,
      Validators.pattern(this.emailPattern)
    );

    if (this.model.username == "") {
      Swal.fire({
        icon: "error",
        title: "กรุณาระบุอีเมล",
        // text: "Modal with a custom image.",
        confirmButtonText: "ตกลง",
      });
    } else if (this.model.password == "") {
      Swal.fire({
        icon: "error",
        title: "กรุณาระบุรหัส",
        // text: "Modal with a custom image.",
        confirmButtonText: "ตกลง",
      });
    } else if (d.status == "INVALID") {
      Swal.fire({
        icon: "error",
        title: "กรุณาระบุรูปแบบอีเมลให้ถูกต้อง",
        // text: "Modal with a custom image.",
        confirmButtonText: "ตกลง",
      });
    } else {
      this.onSubmit();
    }
  }

  public onSubmit() {
    // ----
    let data = {
      //memberName: this.model.username,
      userName: this.model.username,
      password: this.model.password,
    };

    this.ngxService.start();

    this._authenService.authenMember(data).subscribe((res) => {
      console.log("res authenMember");
      console.log(res);
      if (res.result) {
        let _data = {
          //memberName: this.model.username,
          userName: this.model.username,
          token: res.token,
        };

        this._authenService.getDataByToken(_data).subscribe((_res) => {
          console.log("_res getDataByToken");
          console.log(_res);

          if (_res.result.active) {
            let d = {
              memberName: _res.result.userName,
              active: _res.result.active,
              createdDate: _res.result.createdDate,
              memberId: _res.result.memberId,
              updatedDate: _res.result.updatedDate,
              userName: _res.result.userName,
              token: res.token,
            };

            this.dbUserLocalstorage.add_currentUserDataBase(d);

            Swal.fire({
              icon: "success",
              title: "เข้าสู่ระบบ",
              text: res.message,
              showConfirmButton: false,
              timer: 1500,
            });

            //
            //this.authService.login();
            //console.log("this.authService.isLoggedIn");
            //console.log(this.authService.login());

            setTimeout(() => {
              //if (this.authService.login()) {
              this.dbUserLocalstorage.add_current_auth_success(true);
              this.router.navigate([
                "/fontOffice/approval-setting/approval-tax-form-01",
              ]);
              //}
            }, 1500);

            // this.authService.login().subscribe((__res) => {
            //   console.log("__res login");
            //   console.log(__res);

            //   this.authService.isLoggedIn = __res;

            //   console.log("this.authService.isLoggedIn");
            //   console.log(this.authService.isLoggedIn);

            //   //this._authGuardService._authService.isLoggedIn = __res;

            // });
          } else {
            Swal.fire({
              icon: "warning",
              title: "ไม่สำเร็จ",
              text: res.message,
              // text: "Modal with a custom image.",
              confirmButtonText: "ตกลง",
            });
          }
        });
      } else {
        Swal.fire({
          icon: "warning",
          title: "ไม่สำเร็จ",
          text: res.message,
          // text: "Modal with a custom image.",
          confirmButtonText: "ตกลง",
        });
      }

      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
    });

    // this.authService.login();

    // this.authService.isLoggedIn = true;

    // console.log("this.authService.isLoggedIn");
    // console.log(this.authService.isLoggedIn);

    // console.log("this.authService.login()");
    // console.log(this.authService.login());

    // if (this.authService.isLoggedIn) {
    //   console.log("run");

    //   this.router.navigate([
    //     "/fontOffice/approval-setting/approval-tax-form-01",
    //   ]);
    // }
  }
}
