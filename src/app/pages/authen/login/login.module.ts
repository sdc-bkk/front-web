import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { LoginComponent } from "./login.component";
import { SharedModule } from "../../../shared/shared.module";
import { DbUserLocalstorage } from "../../../shared/services/localstorage/db-user-localstorage";
import { AuthService } from "../../../auth.service";
import { AuthenService } from "../authen.service";
import { MemberApi } from "../../../api/member-api";
import { AuthGuardService } from "../../../auth-guard.service";

const ROUTE = [{ path: "", component: LoginComponent }];

@NgModule({
  providers: [
    // กำนหด providers
    DbUserLocalstorage,
    AuthService, // เนื่งอจากเราจะมีการใช้งาน AuthService
    AuthenService,
    MemberApi,
    AuthGuardService,
  ],
  declarations: [LoginComponent],
  imports: [CommonModule, SharedModule, RouterModule.forChild(ROUTE)],
})
export class LoginModule {}
