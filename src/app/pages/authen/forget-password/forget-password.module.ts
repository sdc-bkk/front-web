import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { MemberApi } from "../../../api/member-api";
import { SharedModule } from "../../../shared/shared.module";
import { AuthenService } from "../authen.service";
import { ForgetPasswordComponent } from "./forget-password.component";

const ROUTE = [{ path: "", component: ForgetPasswordComponent }];

@NgModule({
  providers: [AuthenService,MemberApi],
  declarations: [ForgetPasswordComponent],
  imports: [CommonModule, SharedModule, RouterModule.forChild(ROUTE)],
})
export class ForgetPasswordModule {}
