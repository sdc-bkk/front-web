import { Component, ViewEncapsulation, OnInit, Inject } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { NgxUiLoaderService } from "ngx-ui-loader";
import swal from "sweetalert2";
import { AuthenService } from "../authen.service";
@Component({
  selector: ".activate",
  templateUrl: "./activate.component.html",
  styleUrls: ["./activate.component.scss"],
  encapsulation: ViewEncapsulation.Emulated,
})
export class ActivateComponent implements OnInit {
  /////////////////// Config ///////////////////

  ///////////////// End Config /////////////////
  ///////////////// Variable /////////////////
  public model: any = {
    username: "",
    password: "",
    confirmPassword: "",
  };

  public loading = false;
  public showLogin = false;
  /////////////// End Variable ////////////////

  public isSuperAdminGroup = false;

  public SubmitValue: any = {
    result: true,
    errMsg: "",
  };

  constructor(private router: Router, private route: ActivatedRoute, public ngxService: NgxUiLoaderService,
    private authenService: AuthenService) { }

  ngOnInit() {
    this.showLogin = true;
    console.log("this.route.snapshot.params : ",this.route.snapshot.params["userName"]);
    this.model.userName = this.route.snapshot.params["userName"] || "";
  }

  ngOnDestroy() { }

  public onSubmit() {
    this.setPassword();
  }

  public setPassword() {
    let d = {
      // userName: "kingwerewolf@gmail.com"
      userName: this.model.userName,
      password: this.model.password
    };
    console.log("dddddd", d);
    this.ngxService.start();

    this.authenService.setPassword(d).subscribe((res) => {
      console.log("res");
      console.log(res);

      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId

      if (res.result) {
        swal.fire({
          icon: "success",
          title: "สำเร็จ",
          text: "บันทึกข้อมูลสำเร็จ",
          showConfirmButton: true,
          //timer: 1500,
          confirmButtonText: 'เข้าสู่ระบบ',

        }).then((result) => {

          if (result.isConfirmed) {
            this.router.navigate([""]);
          }
        })

        // setTimeout(() => {
        //   this.router.navigate([""]);
        // }, 1500);
      } else {
        swal.fire("ไม่สำเร็จ", res.message, "error");
      }
    });
  }
}
