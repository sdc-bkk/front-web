import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { MemberApi } from "../../../api/member-api";
import { SharedModule } from "../../../shared/shared.module";
import { AuthenService } from "../authen.service";
import { ActivateComponent } from "./activate.component";
import { ActivateRoutes } from "./activate.routes";

const ROUTE = [{ path: "", component: ActivateComponent }];

@NgModule({
  providers: [AuthenService,MemberApi],
  declarations: [ActivateComponent],
  imports: [CommonModule, SharedModule, ActivateRoutes],
})
export class ActivateModule {}
