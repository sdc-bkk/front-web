import { Routes, RouterModule } from "@angular/router";
import { ActivateComponent } from "./activate.component";


const TEMPLATE_ROUTES: Routes = [
  {
    path: "signup/:userName",
    component: ActivateComponent,
    // children: [
    //   {
    //     path: "",
    //     component: ActivateComponent,
    //   },
    // ],
  },

  // 404 Page Not Found
  { path: "**", redirectTo: "/" },
];

export const ActivateRoutes = RouterModule.forChild(TEMPLATE_ROUTES);
