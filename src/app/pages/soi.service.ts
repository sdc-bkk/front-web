import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import { SoiApi } from "../api/soi-api";

@Injectable()
export class SoiService {
  constructor(private soiApi: SoiApi) {}

  getSoiDrpList(): Observable<any> {
    return this.soiApi.getSoiDrpList();
  }
}
