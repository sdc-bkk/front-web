import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import { FrontretailApi } from "../api/frontretail-api";

@Injectable()
export class FrontretailService {
  constructor(private frontretailApi: FrontretailApi) {}

  getDataByMemberName(data): Observable<any> {
    return this.frontretailApi.getDataByMemberName(data);
  }

  getDataByTaxNo(data): Observable<any> {
    return this.frontretailApi.getDataByTaxNo(data);
  }

  getListStationByTaxNo(data): Observable<any> {
    return this.frontretailApi.getListStationByTaxNo(data);
  }
  getListStationByTaxNo2(data): Observable<any> {
    return this.frontretailApi.getListStationByTaxNo2(data);
  }

  getDataStation(data): Observable<any> {
    return this.frontretailApi.getDataStation(data);
  }
}
