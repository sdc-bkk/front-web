import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";

@Injectable()
export class Unity {
  constructor() {}

  ConvertMonthTH(numberMonth) {
    var month = "";
    if (numberMonth == 0) {
      month = "มกราคม";
    } else if (numberMonth == 1) {
      month = "กุมภาพันธ์";
    } else if (numberMonth == 2) {
      month = "มีนาคม";
    } else if (numberMonth == 3) {
      month = "เมษายน";
    } else if (numberMonth == 4) {
      month = "พฤษภาคม";
    } else if (numberMonth == 5) {
      month = "มิถุนายน";
    } else if (numberMonth == 6) {
      month = "กรกฎาคม";
    } else if (numberMonth == 7) {
      month = "สิงหาคม";
    } else if (numberMonth == 8) {
      month = "กันยายน";
    } else if (numberMonth == 9) {
      month = "ตุลาคม";
    } else if (numberMonth == 10) {
      month = "พฤศจิกายน";
    } else if (numberMonth == 11) {
      month = "ธันวาคม";
    }
    return month;
  }
}
