import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import { BusinessStatusApi } from "../api/business-status-api";

@Injectable()
export class BusinessStatusService {
  constructor(private businessStatusApi: BusinessStatusApi) {}

  getDrpBusinessStatusList(): Observable<any> {
    return this.businessStatusApi.getDrpBusinessStatusList();
  }
}
