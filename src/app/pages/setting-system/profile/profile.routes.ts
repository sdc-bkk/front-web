import { Routes, RouterModule } from "@angular/router";
import { ProfileComponent } from "./profile.component";

const TEMPLATE_ROUTES: Routes = [
  {
    path: "",
    component: ProfileComponent,
    children: [],
  },

  // 404 Page Not Found
  { path: "**", redirectTo: "/" },
];

export const ProfileRoutes = RouterModule.forChild(TEMPLATE_ROUTES);
