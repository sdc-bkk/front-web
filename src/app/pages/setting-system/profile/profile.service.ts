import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import { MemberApi } from "../../../api/member-api";

@Injectable()
export class ProfileService {
  constructor(private memberApi: MemberApi) {}

  getData(data): Observable<any> {
    return this.memberApi.getData(data);
  }
  saveData(data): Observable<any> {
    return this.memberApi.saveData(data);
  }
}
