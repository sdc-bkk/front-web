import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { NgxsModule } from "@ngxs/store";
import { AddressApi } from "../../../api/address-api";
import { BusinessTypeApi } from "../../../api/business-type-api";
import { CustomerTypeApi } from "../../../api/customer-type-api";
import { FrontretailApi } from "../../../api/frontretail-api";
import { Frontaxform01Api } from "../../../api/fronttaxform01-api";
import { MemberApi } from "../../../api/member-api";
import { RoadApi } from "../../../api/road-api";
import { SoiApi } from "../../../api/soi-api";
import { UploadApi } from "../../../api/upload-api";
import { SharedModule } from "../../../shared/shared.module";
import { AddressService } from "../../address.service";
import { ApprovalTaxForm01Service } from "../../approval-setting/approval-tax-form-01/approval-tax-form-01.service";
import { AuthenService } from "../../authen/authen.service";
import { BusinessTypeService } from "../../business-type.service";
import { CustomerTypeService } from "../../customer-type.service";
import { FrontretailService } from "../../frontretail.service";
import { RoadService } from "../../road.service";
import { SoiService } from "../../soi.service";
import { UploadService } from "../../upload.service";
import { ProfileComponent } from "./profile.component";
import { ProfileRoutes } from "./profile.routes";
import { ProfileService } from "./profile.service";

@NgModule({
  declarations: [ProfileComponent],
  providers: [
    Frontaxform01Api,
    ApprovalTaxForm01Service,
    AddressService,
    AddressApi,
    RoadService,
    RoadApi,
    SoiService,
    SoiApi,
    BusinessTypeService,
    BusinessTypeApi,
    CustomerTypeService,
    CustomerTypeApi,
    UploadApi,
    UploadService,
    FrontretailApi,
    FrontretailService,

    AuthenService,
    ProfileService,
    MemberApi,
  ],
  imports: [CommonModule, SharedModule, ProfileRoutes],
})
export class ProfileModule {}
