import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { ConfigService } from "../../../shared/services/config/config.service";
import { AuthenService } from "../../authen/authen.service";
import { FrontretailService } from "../../frontretail.service";
import { ProfileService } from "./profile.service";
import Swal from "sweetalert2";

@Component({
  selector: "profile",
  templateUrl: "./profile.component.html",
  styleUrls: ["./profile.component.scss"],
})
export class ProfileComponent implements OnInit {
  data: any = {
    memberId: "",
    userName: "",
    fullName: "",
    idcard: "",
    mobile: "",
    email: "",
    active: true,
    taxNo: "",
    ownerName: "",
    //"updatedBy": ""
  };

  data_ByMember: any = {
    memberId: "",
    userName: "",
    fullName: "",
    idcard: "",
    mobile: "",
    email: "",
    active: true,
    taxNo: "",
    ownerName: "",
    //"updatedBy": ""
  };

  currentUser: any;

  constructor(
    public config: ConfigService,
    public ngxService: NgxUiLoaderService,
    public profileService: ProfileService,
    private router: Router, // public themes: ThemesService
    private frontretailService: FrontretailService
  ) {}

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem("ba_currentUser"));

    this.config.IconName = "mdi mdi-home md-icon-36";

    console.log("this.currentUser");
    console.log(this.currentUser);

    this.getData();
  }

  public getDataByMemberName() {
    const payload = {
      memberName: this.currentUser.memberName,
    };

    this.ngxService.start();

    this.frontretailService.getDataByMemberName(payload).subscribe((res) => {
      console.log("res getDataByMemberName");
      console.log(res);
      if (res.status == "true") {
        this.data_ByMember = res.result;
      }
      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
    });
  }

  public getData() {
    const payload = {
      memberId: this.currentUser.memberId,
    };

    //this.ngxService.start();

    this.profileService.getData(payload).subscribe((res) => {
      console.log("res getData");
      console.log(res);
      if (res.result) {
        this.setForm(res.result);
      }

      this.getDataByMemberName();

      //this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
    });
  }

  setForm(res: any) {
    // console.log("res setForm");
    // console.log(res);

    this.data = {
      memberId: res.memberId == undefined ? "" : res.memberId,
      userName: res.userName == undefined ? "" : res.userName,
      fullName: res.fullName == undefined ? "" : res.fullName,
      idcard: res.idcard == undefined ? "" : res.idcard,
      mobile: res.mobile == undefined ? "" : res.mobile,
      email: res.email == undefined ? "" : res.email,
      active: res.active == undefined ? "" : res.active,
      taxNo: res.taxNo == undefined ? "" : res.taxNo,
      ownerName: res.ownerName == undefined ? "" : res.ownerName,
    };

    console.log("this.data");
    console.log(this.data);
  }

  profile() {
    this.router.navigate(["/fontOffice/approval-setting/approval-tax-form-01"]);
  }

  cancle() {
    this.router.navigate(["/fontOffice/approval-setting/approval-tax-form-01"]);
  }

  onSubmit() {
    let data = {};
    data = {
      memberId: this.currentUser.memberId,
      userName: this.currentUser.userName,
      //
      fullName: this.data.fullName,
      idcard: this.data.idcard,
      mobile: this.data.mobile,
      //
      email: this.currentUser.userName,
      active: this.currentUser.active,
      updatedBy: this.currentUser.memberId,
    };

    console.log("data");
    console.log(data);

    this.ngxService.start();

    this.profileService.saveData(data).subscribe((res) => {
      //console.log("res");
      //console.log(res);
      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
      if (res.result) {
        Swal.fire({
          icon: "success",
          title: "สำเร็จ",
          text: "บันทึกข้อมูลสำเร็จ",
          showConfirmButton: false,
          timer: 1500,
        });
      } else {
        Swal.fire("ไม่สำเร็จ", res.message, "error");
      }
    });
  }
}
