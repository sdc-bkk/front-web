import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { ConfigService } from "../../../shared/services/config/config.service";
import { ChangePasswordService } from "./change-password.service";
import Swal from "sweetalert2";

@Component({
  selector: "change-password",
  templateUrl: "./change-password.component.html",
  styleUrls: ["./change-password.component.scss"],
})
export class ChangePasswordComponent implements OnInit {
  data: any = {
    userName: "",
    password: "",
    newPassword: "",
    confirmPassword: "",
  };

  currentUser: any;
  constructor(
    public config: ConfigService,
    public ngxService: NgxUiLoaderService,
    public changePasswordService: ChangePasswordService,
    private router: Router // public themes: ThemesService
  ) {}

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem("ba_currentUser"));

    this.config.IconName = "mdi mdi-home md-icon-36";

    //console.log("this.currentUser");
    //console.log(this.currentUser);
    this.ngxService.start();
    setTimeout(() => {
      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
    }, 1000);
  }

  cancle() {
    this.router.navigate(["/fontOffice/approval-setting/approval-tax-form-01"]);
  }

  public onSubmit() {
    if (this.data.password == "") {
      Swal.fire({
        icon: "error",
        title: "กรุณาระบุรหัสผ่านเดิม",
        // text: "Modal with a custom image.",
        confirmButtonText: "ตกลง",
      });
    } else if (this.data.newPassword == "") {
      Swal.fire({
        icon: "error",
        title: "กรุณาระบุรหัสผ่านใหม่",
        // text: "Modal with a custom image.",
        confirmButtonText: "ตกลง",
      });
    } else if (this.data.confirmPassword == "") {
      Swal.fire({
        icon: "error",
        title: "กรุณาระบุยืนยันรหัสผ่านใหม่",
        // text: "Modal with a custom image.",
        confirmButtonText: "ตกลง",
      });
    } else {
      if (this.data.newPassword != this.data.confirmPassword) {
        Swal.fire({
          icon: "error",
          title: "กรุณาระบุรหัสผ่านให้ตรงกัน",
          // text: "Modal with a custom image.",
          confirmButtonText: "ตกลง",
        });
      } else {
        this.changePassword();
      }
    }
  }

  changePassword() {
    let data = {};
    data = {
      userName: this.currentUser.userName,
      password: this.data.password, // รหัสผ่านเก่า
      newPassword: this.data.newPassword, // รหัสผ่านใหม่
    };

    console.log("data");
    console.log(data);

    this.ngxService.start();

    this.changePasswordService.changePassword(data).subscribe((res) => {
      //console.log("res");
      //console.log(res);
      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
      if (res.result) {
        Swal.fire({
          icon: "success",
          title: "สำเร็จ",
          text: "บันทึกข้อมูลสำเร็จ",
          showConfirmButton: false,
          timer: 1500,
        });
      } else {
        Swal.fire("ไม่สำเร็จ", res.message, "error");
      }
    });
  }
}
