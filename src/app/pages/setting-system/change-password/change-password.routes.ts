import { Routes, RouterModule } from "@angular/router";
import { ChangePasswordComponent } from "./change-password.component";

const TEMPLATE_ROUTES: Routes = [
  {
    path: "",
    component: ChangePasswordComponent,
    children: [],
  },

  // 404 Page Not Found
  { path: "**", redirectTo: "/" },
];

export const ChangePasswordRoutes = RouterModule.forChild(TEMPLATE_ROUTES);
