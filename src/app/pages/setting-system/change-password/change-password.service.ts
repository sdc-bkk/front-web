import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import { MemberApi } from "../../../api/member-api";

@Injectable()
export class ChangePasswordService {
  constructor(private memberApi: MemberApi) {}

  changePassword(data): Observable<any> {
    return this.memberApi.changePassword(data);
  }
}
