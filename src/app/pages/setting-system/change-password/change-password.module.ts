import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { NgxsModule } from "@ngxs/store";
import { MemberApi } from "../../../api/member-api";
import { SharedModule } from "../../../shared/shared.module";
import { AuthenService } from "../../authen/authen.service";
import { ProfileService } from "../profile/profile.service";
import { ChangePasswordComponent } from "./change-password.component";
import { ChangePasswordRoutes } from "./change-password.routes";
import { ChangePasswordService } from "./change-password.service";

@NgModule({
  declarations: [ChangePasswordComponent],
  providers: [ChangePasswordService, AuthenService, ProfileService, MemberApi],
  imports: [CommonModule, SharedModule, ChangePasswordRoutes],
})
export class ChangePasswordModule {}
