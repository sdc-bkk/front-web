import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { SharedModule } from "../../shared/shared.module";
import { SettingSystemComponent } from "./setting-system.component";
import { SettingSystemRoutes } from "./setting-system.routes";

@NgModule({
  declarations: [SettingSystemComponent],
  providers: [],
  imports: [CommonModule, SharedModule, SettingSystemRoutes],
})
export class SettingSystemModule {}
