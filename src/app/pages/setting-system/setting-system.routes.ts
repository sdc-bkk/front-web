import { Routes, RouterModule } from "@angular/router";
import { SettingSystemComponent } from "./setting-system.component";

const ROUTES: Routes = [
  {
    path: "",
    component: SettingSystemComponent,
    children: [
      // - ลืมรหัสผ่าน
      {
        path: "change-password",
        loadChildren: () =>
          import("./change-password/change-password.module").then(
            (m) => m.ChangePasswordModule
          ),
      },
      // - โปรไฟล์
      {
        path: "profile",
        loadChildren: () =>
          import("./profile/profile.module").then((m) => m.ProfileModule),
      },

      // - จัดการผู้ดูแล
      {
        path: "member-retail",
        loadChildren: () =>
          import("./member-retail/member-retail.module").then(
            (m) => m.MemberRetailModule
          ),
      },

      {
        path: "",
        redirectTo: "fontOffice/setting-system/change-password",
      },
    ],
  },

  // 404 Page Not Found
  { path: "**", redirectTo: "/" },
];

export const SettingSystemRoutes = RouterModule.forChild(ROUTES);
