import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import { MemberRetailApi } from "../../../api/member-retail-api";

@Injectable()
export class MemberRetailService {
  constructor(private memberRetailApi: MemberRetailApi) {}

  checkEmail(data): Observable<any> {
    return this.memberRetailApi.checkEmail(data);
  }

  getData(data): Observable<any> {
    return this.memberRetailApi.getData(data);
  }

  saveData(data): Observable<any> {
    return this.memberRetailApi.saveData(data);
  }
}
