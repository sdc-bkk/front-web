import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { ConfigService } from "../../../shared/services/config/config.service";
import Swal from "sweetalert2";
import { MemberRetailService } from "./member-retail.service";

@Component({
  selector: "member-retail",
  templateUrl: "./member-retail.component.html",
  styleUrls: ["./member-retail.component.scss"],
})
export class MemberRetailComponent implements OnInit {
  dataList: any[] = [];

  data: any = {
    ownerName: "",
    taxNo: "",
    customerType: "",
    memberList: [
      // {
      //     "email": "1@gmail.com",
      //     "createdBy": "sunanta4@hotmail.com",
      //     "createdDate": "12/02/2564"
      // },
    ],
  };

  _data: any = {
    email: "",
  };

  Day: any = new Date().getDate();
  Month: any = new Date().getMonth() + 1;
  year: any = new Date().getFullYear() + 543;

  currentUser: any;
  constructor(
    public config: ConfigService,
    public ngxService: NgxUiLoaderService,
    public memberRetailService: MemberRetailService,
    private router: Router // public themes: ThemesService
  ) {}

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem("ba_currentUser"));

    this.config.IconName = "mdi mdi-home md-icon-36";

    // console.log("this.Day");
    // console.log(this.Day);

    // console.log("this.Month");
    // console.log(this.Month);

    // console.log("this.year");
    // console.log(this.year);

    console.log("this.currentUser");
    console.log(this.currentUser);
    this.getData();
  }

  getData() {
    let data = {};
    data = {
      email: this.currentUser.userName,
    };

    // console.log("data");
    // console.log(data);

    this.ngxService.start();

    this.memberRetailService.getData(data).subscribe((res) => {
      console.log("res getData");
      console.log(res);
      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
      if (res.result) {
        let memberList = [];
        res.result.memberList.forEach((r) => {
          //if (r.submit) {
          let d = {
            ...r,
            submit: true,
          };
          memberList.push(d);
          //}
        });

        this.data = {
          ownerName:
            res.result.ownerName == undefined ? "" : res.result.ownerName,
          taxNo: res.result.taxNo == undefined ? "" : res.result.taxNo,
          customerType:
            res.result.customerType == undefined ? "" : res.result.customerType,
          memberList: res.result.memberList == undefined ? [] : memberList,
        };

        console.log("this.data");
        console.log(this.data);
      } else {
        this.data = {
          ownerName: "",
          taxNo: "",
          customerType: "",
          memberList: [],
        };
      }
    });
  }

  cancle() {
    this.router.navigate(["/fontOffice/approval-setting/approval-tax-form-01"]);
  }

  addcheckEmail() {
    let d = {
      email: "",
      createdBy: this.currentUser.userName,
      createdDate:
        this.Day.toString() +
        "/" +
        this.Month.toString() +
        "" +
        this.year.toString(),
      submit: false,
    };

    this.data.memberList.push(d);
  }

  checkEmail(i) {
    let data = {};
    data = {
      email: this.data.memberList[i].email,
    };

    // console.log("data");
    // console.log(data);

    this.ngxService.start();

    this.memberRetailService.checkEmail(data).subscribe((res) => {
      console.log("res checkEmail");
      console.log(res);
      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
      if (res.result) {
        // let d = {
        //   email: this._data.email,
        //   createdBy: this.currentUser.userName,
        //   createdDate:
        //     this.Day.toString() +
        //     "/" +
        //     this.Month.toString() +
        //     "" +
        //     this.year.toString(),
        //   submit: false,
        // };

        // this.data.memberList.push(d);

        this.data.memberList[i].submit = true;
      } else {
        Swal.fire("ไม่สำเร็จ", res.message, "error");
        this.data.memberList[i].submit = false;
      }
    });
  }

  remove_memberList(i) {
    Swal.fire({
      title: "คุณต้องการลบใช่หรือไม่",
      // text: 'You will not be able to recover this imaginary file!',
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "ตกลง",
      cancelButtonText: "ยกเลิก",
    }).then((result) => {
      if (result.value) {
        this.data.memberList.splice(i, 1);
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        // Swal.fire("Cancelled", "Your imaginary file is safe :)", "error");
      }
    });
  }

  onSubmit() {
    let memberList = [];

    this.data.memberList.forEach((r) => {
      if (r.submit) {
        let d = {
          email: r.email,
        };
        memberList.push(d);
      }
    });

    let data = {};
    data = {
      taxNo: this.data.taxNo,
      createdBy: this.currentUser.userName,
      memberList: memberList,
    };
    console.log("data");
    console.log(data);
    this.ngxService.start();
    this.memberRetailService.saveData(data).subscribe((res) => {
      //console.log("res");
      //console.log(res);
      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
      if (res.result) {
        Swal.fire({
          icon: "success",
          title: "สำเร็จ",
          text: "บันทึกข้อมูลสำเร็จ",
          showConfirmButton: false,
          timer: 1500,
        });
      } else {
        Swal.fire("ไม่สำเร็จ", res.message, "error");
      }
    });
  }
}
