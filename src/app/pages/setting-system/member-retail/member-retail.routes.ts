import { Routes, RouterModule } from "@angular/router";
import { MemberRetailComponent } from "./member-retail.component";

const TEMPLATE_ROUTES: Routes = [
  {
    path: "",
    component: MemberRetailComponent,
    children: [],
  },

  // 404 Page Not Found
  { path: "**", redirectTo: "/" },
];

export const MemberRetailRoutes = RouterModule.forChild(TEMPLATE_ROUTES);
