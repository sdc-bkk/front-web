import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { NgxsModule } from "@ngxs/store";
import { MemberRetailApi } from "../../../api/member-retail-api";
import { SharedModule } from "../../../shared/shared.module";
import { AuthenService } from "../../authen/authen.service";
import { ProfileService } from "../profile/profile.service";
import { MemberRetailComponent } from "./member-retail.component";
import { MemberRetailRoutes } from "./member-retail.routes";
import { MemberRetailService } from "./member-retail.service";

@NgModule({
  declarations: [MemberRetailComponent],
  providers: [
    MemberRetailService,
    AuthenService,
    ProfileService,
    MemberRetailApi,
  ],
  imports: [CommonModule, SharedModule, MemberRetailRoutes],
})
export class MemberRetailModule {}
