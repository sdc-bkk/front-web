import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import { AddressApi } from "../api/address-api";

@Injectable()
export class AddressService {
  constructor(private addressApi: AddressApi) {}

  getDrpListByPostCodeMain(data): Observable<any> {
    return this.addressApi.getDrpListByPostCodeMain(data);
  }

  getDrpListProvince(): Observable<any> {
    return this.addressApi.getDrpListProvince();
  }

  getDrpListDistrict(data): Observable<any> {
    return this.addressApi.getDrpListDistrict(data);
  }

  getDrpListTambon(data): Observable<any> {
    return this.addressApi.getDrpListTambon(data);
  }
}
