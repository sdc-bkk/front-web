import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import { UploadApi } from "../api/upload-api";

@Injectable()
export class UploadService {
  constructor(private uploadApi: UploadApi) {}

  getFileTemp(data): Observable<any> {
    return this.uploadApi.getFileTemp(data);
  }

  deleteFileTemp(data): Observable<any> {
    return this.uploadApi.deleteFileTemp(data);
  }

  moveFile(data): Observable<any> {
    return this.uploadApi.moveFile(data);
  }

  getFile(data): Observable<any> {
    return this.uploadApi.getFile(data);
  }

  deleteFile(data): Observable<any> {
    return this.uploadApi.deleteFile(data);
  }
}
