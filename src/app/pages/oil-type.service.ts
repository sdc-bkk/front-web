import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import { OilTypeApi } from "../api/oil-type-api";

@Injectable()
export class OilTypeService {
  constructor(private oilTypeApi: OilTypeApi) {}

  getDrpOilTypeList(): Observable<any> {
    return this.oilTypeApi.getDrpOilTypeList();
  }
}
