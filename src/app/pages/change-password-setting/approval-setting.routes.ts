import { Routes, RouterModule } from "@angular/router";
import { ApprovalSettingComponent } from "./approval-setting.component";

const ROUTES: Routes = [
  {
    path: "",
    component: ApprovalSettingComponent,
    children: [
      // - อนุมัติ ภน.01
      {
        path: "approval-tax-form-01",
        loadChildren: () =>
          import("./approval-tax-form-01/approval-tax-form-01.module").then(
            (m) => m.ApprovalTaxForm01Module
          ),
      },
      // - อนุมัติ ภน.02
      {
        path: "approval-tax-form-02",
        loadChildren: () =>
          import("./approval-tax-form-02/approval-tax-form-02.module").then(
            (m) => m.ApprovalTaxForm02Module
          ),
      },

      // - อนุมัติ ภน.03
      {
        path: "approval-tax-form-03",
        loadChildren: () =>
          import("./approval-tax-form-03/approval-tax-form-03.module").then(
            (m) => m.ApprovalTaxForm03Module
          ),
      },

      {
        path: "",
        redirectTo: "fontOffice/approval-setting/approval-tax-form-01",
      },
    ],
  },

  // 404 Page Not Found
  { path: "**", redirectTo: "/" },
];

export const ApprovalSettingRoutes = RouterModule.forChild(ROUTES);
