import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import { CustomerTypeApi } from "../api/customer-type-api";

@Injectable()
export class CustomerTypeService {
  constructor(private customerTypeApi: CustomerTypeApi) {}

  getCustomerTypeDrpList(): Observable<any> {
    return this.customerTypeApi.getCustomerTypeDrpList();
  }
}
