import { BrowserModule, Title } from "@angular/platform-browser";
import { APP_ID, Inject, NgModule, PLATFORM_ID } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MatMenuModule } from "@angular/material/menu";
import { MatButtonModule } from "@angular/material/button";
import { MatIconModule } from "@angular/material/icon";
import { MatCardModule } from "@angular/material/card";
import { MatTabsModule } from "@angular/material/tabs";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { MatDialogModule } from "@angular/material/dialog";
import { MatInputModule } from "@angular/material/input";
import { MatListModule } from "@angular/material/list";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { MatSelectModule } from "@angular/material/select";
import { MatSidenavModule } from "@angular/material/sidenav";
import { MatSortModule } from "@angular/material/sort";
import { MatTableModule } from "@angular/material/table";
import { MatToolbarModule } from "@angular/material/toolbar";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatMomentDateModule } from "@angular/material-moment-adapter";
import { AppState, InternalStateType } from "./app.service";
import { isPlatformBrowser } from "@angular/common";
import { HammerGestureConfig } from "@angular/platform-browser";
import { HttpClient } from "@angular/common/http";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { GlobalState } from "./app.state";
import { StorageServiceModule } from "ngx-webstorage-service";
import { TranslateLoader, TranslateModule } from "@ngx-translate/core";
import { NgxsReduxDevtoolsPluginModule } from "@ngxs/devtools-plugin";
import {
  NgxUiLoaderConfig,
  POSITION,
  SPINNER,
  NgxUiLoaderModule,
} from "ngx-ui-loader";
import { LoadingModule } from "./shared/components/loading/loading.module";
import { ApprovalTaxForm03Service } from "../../src/app/pages/approval-setting/approval-tax-form-03/approval-tax-form-03.service";
import { Frontaxform03Api } from "./api/fronttaxform03-api";

export class HammerConfig extends HammerGestureConfig {
  overrides = <any>{
    swipe: { velocity: 0.4, threshold: 20 },
  };
}

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, "assets/i18n/", ".json");
}

// Application wide providers
const APP_PROVIDERS = [AppState, GlobalState, Title];

export type StoreType = {
  state: InternalStateType;
  restoreInputValues: () => void;
  disposeOldHosts: () => void;
};

const ngxUiLoaderConfig: NgxUiLoaderConfig = {
  // bgsColor: "#00ACC1",
  // bgsOpacity: 0.5,
  // bgsPosition: POSITION.centerCenter,
  // bgsSize: 60,
  // bgsType: SPINNER.rectangleBounce,

  logoPosition: POSITION.centerCenter,
  logoSize: 200,
  // logoSize: 80,
  logoUrl: "./assets/img/logo/bkk_logo.png",

  // fgsColor: "#FFD700", // สี Loading
  blur: 5,
  delay: 0,
  fastFadeOut: true,
  fgsColor: "#00904B",
  fgsPosition: POSITION.centerCenter,
  fgsSize: 60,
  fgsType: "three-bounce",

  // overlayColor: "#032637",

  // text: "กรุณารอสักครู่",
  // textColor: "#ffffff", // สีตัวอักษร
  // textPosition: POSITION.centerCenter,

  //pbColor: "#FFD700",
  //pbDirection: PB_DIRECTION.leftToRight, // progress bar direction
  //pbThickness: 5, // progress bar thickness
};

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    StorageServiceModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient],
      },
    }),
    NgxsReduxDevtoolsPluginModule.forRoot(),
    MatMenuModule,
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatTabsModule,
    MatSidenavModule,
    MatListModule,
    MatToolbarModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    AppRoutingModule,
    MatSelectModule,
    MatDatepickerModule,
    MatMomentDateModule,
    NgxUiLoaderModule.forRoot(ngxUiLoaderConfig),
    LoadingModule,
  ],
  declarations: [
    AppComponent,
    // HomeComponent,
    // AboutComponent,
    // CourseComponent,
    // CoursesCardListComponent,
    // CourseDialogComponent,
  ],
  providers: [APP_PROVIDERS, Frontaxform03Api, ApprovalTaxForm03Service],
  bootstrap: [AppComponent],
  entryComponents: [],
})
export class AppModule {
  constructor(
    public appState: AppState,
    @Inject(PLATFORM_ID) private platformId: Object,
    @Inject(APP_ID) private appId: string
  ) {
    const platform = isPlatformBrowser(platformId)
      ? "in the browser"
      : "on the server";
    console.log(`Running ${platform} with appId=${appId}`);
  }
}
