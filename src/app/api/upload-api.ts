import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { ConfigService } from "../shared/services/config/config.service";
import { REST_URL } from "../shared/services/config/rest-url";
import { environment } from "../../environments/environment";

@Injectable()
export class UploadApi {
  private host: string;
  private path: any;
  private itemPerPage: number;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.host = `${environment.url}${REST_URL.MAIN_PERMISSION.upload}`;
    this.path = REST_URL.MAIN_UPLOADFILE;
    this.itemPerPage = config.paging.ITEMPERPAGE;
  }

  getFileTemp(item) {
    return this.http.get(
      `${this.host}${this.path.getFileTemp + "/" + item.fileName}`
    );
  }

  deleteFileTemp(item) {
    return this.http.get(
      `${this.host}${this.path.deleteFileTemp + "/" + item.fileName}`
    );
  }

  moveFile(item) {
    return this.http.get(
      `${this.host}${
        this.path.moveFile + "/" + item.fileName + "/" + item.location
      }`
    );
  }

  getFile(item) {
    return this.http.get(
      `${this.host}${
        this.path.getFile + "/" + item.fileName + "/" + item.location
      }`
    );
  }

  deleteFile(item) {
    return this.http.get(
      `${this.host}${
        this.path.deleteFile + "/" + item.fileName + "/" + item.location
      }`
    );
  }
}
