import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { ConfigService } from "../shared/services/config/config.service";
import { REST_URL } from "../shared/services/config/rest-url";
import { environment } from "../../environments/environment";

@Injectable()
export class MemberRetailApi {
  private host: string;
  private path = REST_URL.MEMBER_RETAIL_PERMISSION;
  private itemPerPage: number;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.host = `${environment.url}${REST_URL.MAIN_PERMISSION.member}`;
    this.path = REST_URL.MEMBER_RETAIL_PERMISSION;
    this.itemPerPage = config.paging.ITEMPERPAGE;
  }

  checkEmail(payload) {
    return this.http.post(`${this.host}${this.path.checkEmail}`, payload, {
      headers: new HttpHeaders({
        "Content-Type": "application/json;charset=utf8",
      }),
    });
  }

  getData(payload) {
    return this.http.post(`${this.host}${this.path.getData}`, payload, {
      headers: new HttpHeaders({
        "Content-Type": "application/json;charset=utf8",
      }),
    });
  }

  saveData(payload) {
    return this.http.post(`${this.host}${this.path.saveData}`, payload, {
      headers: new HttpHeaders({
        "Content-Type": "application/json;charset=utf8",
      }),
    });
  }
}
