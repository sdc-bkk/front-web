import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { ConfigService } from "../shared/services/config/config.service";
import { REST_URL } from "../shared/services/config/rest-url";
import { environment } from "../../environments/environment";

@Injectable()
export class SoiApi {
  private host: string;
  private path: any;
  private itemPerPage: number;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.host = `${environment.url}${REST_URL.MAIN_PERMISSION.master}`;
    this.path = REST_URL.SOI_PERMISSION;
    this.itemPerPage = config.paging.ITEMPERPAGE;
  }

  getList(payload) {
    return this.http.post(`${this.host}${this.path.getList}`, payload, {
      headers: new HttpHeaders({
        "Content-Type": "application/json;charset=utf8",
      }),
    });
  }

  getData(payload) {
    return this.http.post(`${this.host}${this.path.getData}`, payload, {
      headers: new HttpHeaders({
        "Content-Type": "application/json;charset=utf8",
      }),
    });
  }

  saveData(payload) {
    return this.http.post(`${this.host}${this.path.saveData}`, payload, {
      headers: new HttpHeaders({
        "Content-Type": "application/json;charset=utf8",
      }),
    });
  }

  deleteData(payload) {
    return this.http.post(`${this.host}${this.path.deleteData}`, payload, {
      headers: new HttpHeaders({
        "Content-Type": "application/json;charset=utf8",
      }),
    });
  }

  getSoiDrpList() {
    return this.http.get(`${this.host}${this.path.getDrpList}`);
  }

  getDrpListByRoad(payload) {
    return this.http.post(
      `${this.host}${this.path.getDrpListByRoad}`,
      payload,
      {
        headers: new HttpHeaders({
          "Content-Type": "application/json;charset=utf8",
        }),
      }
    );
  }
}
