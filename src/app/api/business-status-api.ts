import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { ConfigService } from "../shared/services/config/config.service";
import { REST_URL } from "../shared/services/config/rest-url";
import { environment } from "../../environments/environment";

@Injectable()
export class BusinessStatusApi {
  private host: string;
  private path: any;
  private itemPerPage: number;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.host = `${environment.url}${REST_URL.MAIN_PERMISSION.master}`;
    this.path = REST_URL.BUSINESSSTATUS_PERMISSION;
    this.itemPerPage = config.paging.ITEMPERPAGE;
  }

  getDrpBusinessStatusList() {
    return this.http.get(`${this.host}${this.path.getDrpList}`);
  }
}
