import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { ConfigService } from "../shared/services/config/config.service";
import { REST_URL } from "../shared/services/config/rest-url";
import { environment } from "../../environments/environment";

@Injectable()
export class RequestRefundApi {
  private host: string;
  private path: any;
  private itemPerPage: number;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.host = `${environment.url}${REST_URL.MAIN_PERMISSION.frontwab}`;
    this.path = REST_URL.FRONTREFUND_PERMISSION;
    this.itemPerPage = config.paging.ITEMPERPAGE;
  }

  getList(payload) {
    return this.http.post(`${this.host}${this.path.getList}`, payload, {
      headers: new HttpHeaders({
        "Content-Type": "application/json;charset=utf8",
      }),
    });
  }

  getData(payload) {
    return this.http.post(`${this.host}${this.path.getData}`, payload, {
      headers: new HttpHeaders({
        "Content-Type": "application/json;charset=utf8",
      }),
    });
  }

  saveData(data) {
    return this.http.post(`${this.host}${this.path.saveData}`, data, {
      headers: new HttpHeaders({
        "Content-Type": "application/json;charset=utf8",
      }),
    });
  }

  getDataStation(data) {
    return this.http.post(`${this.host}${this.path.getDataStation}`, data, {
      headers: new HttpHeaders({
        "Content-Type": "application/json;charset=utf8",
      }),
    });
  }

    getDrpListStatus() {
    return this.http.get(`${this.host}${this.path.getDrpListStatus}`);
  }

  deleteData(data) {
    return this.http.post(`${this.host}${this.path.deleteData}`, data, {
      headers: new HttpHeaders({
        "Content-Type": "application/json;charset=utf8",
      }),
    });
  }

//   sendApprove(data) {
//     return this.http.post(`${this.host}${this.path.sendApprove}`, data, {
//       headers: new HttpHeaders({
//         "Content-Type": "application/json;charset=utf8",
//       }),
//     });
//   }

//   getDrpListRetail(data) {
//     return this.http.post(`${this.host}${this.path.getDrpListRetail}`, data, {
//       headers: new HttpHeaders({
//         "Content-Type": "application/json;charset=utf8",
//       }),
//     });
//   }

//   getDrpListStation(data) {
//     return this.http.post(`${this.host}${this.path.getDrpListStation}`, data, {
//       headers: new HttpHeaders({
//         "Content-Type": "application/json;charset=utf8",
//       }),
//     });
//   }

//   getListStatusLog(data) {
//     return this.http.post(`${this.host}${this.path.getListStatusLog}`, data, {
//       headers: new HttpHeaders({
//         "Content-Type": "application/json;charset=utf8",
//       }),
//     });
//   }


}
