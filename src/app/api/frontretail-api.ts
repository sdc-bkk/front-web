import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { ConfigService } from "../shared/services/config/config.service";
import { REST_URL } from "../shared/services/config/rest-url";
import { environment } from "../../environments/environment";

@Injectable()
export class FrontretailApi {
  private host: string;
  private path: any;
  private itemPerPage: number;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.host = `${environment.url}${REST_URL.MAIN_PERMISSION.frontwab}`;
    this.path = REST_URL.FRONTRETAIL_PERMISSION;
    this.itemPerPage = config.paging.ITEMPERPAGE;
  }

  getDataByMemberName(payload) {
    return this.http.post(
      `${this.host}${this.path.getDataByMemberName}`,
      payload,
      {
        headers: new HttpHeaders({
          "Content-Type": "application/json;charset=utf8",
        }),
      }
    );
  }

  getDataByTaxNo(payload) {
    return this.http.post(`${this.host}${this.path.getDataByTaxNo}`, payload, {
      headers: new HttpHeaders({
        "Content-Type": "application/json;charset=utf8",
      }),
    });
  }

  getListStationByTaxNo(payload) {
    return this.http.post(
      `${this.host}${this.path.getListStationByTaxNo}`,
      payload,
      {
        headers: new HttpHeaders({
          "Content-Type": "application/json;charset=utf8",
        }),
      }
    );
  }
  getListStationByTaxNo2(payload) {
    return this.http.post(
      `${this.host}${this.path.getListStationByTaxNo2}`,
      payload,
      {
        headers: new HttpHeaders({
          "Content-Type": "application/json;charset=utf8",
        }),
      }
    );
  }

  getDataStation(payload) {
    return this.http.post(`${this.host}${this.path.getDataStation}`, payload, {
      headers: new HttpHeaders({
        "Content-Type": "application/json;charset=utf8",
      }),
    });
  }
}
