import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { ConfigService } from "../shared/services/config/config.service";
import { REST_URL } from "../shared/services/config/rest-url";
import { environment } from "../../environments/environment";

@Injectable()
export class MemberApi {
  private host: string;
  private path = REST_URL.MEMBER_PERMISSION;
  private itemPerPage: number;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.host = `${environment.url}${REST_URL.MAIN_PERMISSION.member}`;
    this.path = REST_URL.MEMBER_PERMISSION;
    this.itemPerPage = config.paging.ITEMPERPAGE;
  }

  getList(payload) {
    return this.http.post(`${this.host}${this.path.getList}`, payload, {
      headers: new HttpHeaders({
        "Content-Type": "application/json;charset=utf8",
      }),
    });
  }

  getData(payload) {
    return this.http.post(`${this.host}${this.path.getData}`, payload, {
      headers: new HttpHeaders({
        "Content-Type": "application/json;charset=utf8",
      }),
    });
  }

  saveData(payload) {
    return this.http.post(`${this.host}${this.path.saveData}`, payload, {
      headers: new HttpHeaders({
        "Content-Type": "application/json;charset=utf8",
      }),
    });
  }

  register(payload) {
    return this.http.post(`${this.host}${this.path.register}`, payload, {
      headers: new HttpHeaders({
        "Content-Type": "application/json;charset=utf8",
      }),
    });
  }

  setPassword(payload) {
    return this.http.post(`${this.host}${this.path.setPassword}`, payload, {
      headers: new HttpHeaders({
        "Content-Type": "application/json;charset=utf8",
      }),
    });
  }

  forgetPassword(payload) {
    return this.http.post(`${this.host}${this.path.forgetPassword}`, payload, {
      headers: new HttpHeaders({
        "Content-Type": "application/json;charset=utf8",
      }),
    });
  }
  changePassword(payload) {
    return this.http.post(`${this.host}${this.path.changePassword}`, payload, {
      headers: new HttpHeaders({
        "Content-Type": "application/json;charset=utf8",
      }),
    });
  }

  authenMember(payload) {
    return this.http.post(`${this.host}${this.path.authenMember}`, payload, {
      headers: new HttpHeaders({
        "Content-Type": "application/json;charset=utf8",
      }),
    });
  }
  getDataByToken(payload) {
    return this.http.post(`${this.host}${this.path.getDataByToken}`, payload, {
      headers: new HttpHeaders({
        "Content-Type": "application/json;charset=utf8",
      }),
    });
  }
}
