import { Routes, RouterModule } from "@angular/router";
import { AuthGuardService } from "../auth-guard.service";
import { LayoutComponent } from "./layout.component";
import { MainMenuComponent } from "./main-menu/main-menu.component";
import { SubMenuComponent } from "./sub-menu/sub-menu.component";

const LAYOUT_ROUTES: Routes = [
  {
    path: "",
    loadChildren: () =>
      import("../pages/authen/login/login.module").then((m) => m.LoginModule),
  },

  {
    path: "sign-up",
    loadChildren: () =>
      import("../pages/authen/sign-up/sign-up.module").then(
        (m) => m.SignUpModule
      ),
  },

  {
    path: "sign-in",
    loadChildren: () =>
      import("../pages/authen/sign-in/sign-in.module").then(
        (m) => m.SignInModule
      ),
  },

  {
    path: "forget-password",
    loadChildren: () =>
      import("../pages/authen/forget-password/forget-password.module").then(
        (m) => m.ForgetPasswordModule
      ),
  },

  {
    path: "activate",
    loadChildren: () =>
      import("../pages/authen/activate/activate.module").then(
        (m) => m.ActivateModule
      ),
  },

  {
    path: "fontOffice",
    component: LayoutComponent,
    // กำหนด guard service ให้กับ canActivate ที่ root ของ module ย่อย
    canActivate: [AuthGuardService],
    children: [
      {
        path: "setting-system",
        // กำหนด guard service ให้กับ canActivate ที่ root ของ module ย่อย
        loadChildren: () =>
          import("../pages/setting-system/setting-system.module").then(
            (m) => m.SettingSystemModule
          ),
      },

      // {
      //   path: "change-password",
      //   loadChildren:
      //     "../pages/profile/profile.module#ProfileModule"
      // },

      {
        path: "",
        // กำหนด guard service ให้กับ canActivate ที่ root ของ module ย่อย
        //canActivate: [AuthGuardService],
        component: SubMenuComponent,
        children: [
          // จัดการการยื่นแบบ
          {
            path: "approval-setting",
            // กำหนด guard service ให้กับ canActivateChild ให้กับ child ของ module ย่อย
            //canActivateChild: [AuthGuardService],
            loadChildren: () =>
              import("../pages/approval-setting/approval-setting.module").then(
                (m) => m.ApprovalSettingModule
              ),
          },
        ],
      },
    ],
  },

  // 404 Page Not Found
  { path: "**", redirectTo: "not-found" },
];

export const LayoutRoutes = RouterModule.forChild(LAYOUT_ROUTES);
