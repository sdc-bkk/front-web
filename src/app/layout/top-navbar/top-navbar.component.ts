import {
  Component,
  OnInit,
  ElementRef,
  ViewEncapsulation,
  Inject,
} from "@angular/core";
import { Router } from "@angular/router";
import Swal from "sweetalert2";
import { GlobalState } from "../../app.state";
import { ConfigService } from "../../shared/services/config/config.service";
import { DbUserLocalstorage } from "../../shared/services/localstorage/db-user-localstorage";

@Component({
  selector: "app-header",
  templateUrl: "./top-navbar.component.html",
  styleUrls: ["./top-navbar.component.scss"],
  encapsulation: ViewEncapsulation.Emulated,
})
export class TopNavbarComponent implements OnInit {
  currentTheme: any;
  currentUser: any;

  public loading = false;

  constructor(
    public config: ConfigService,
    private _elementRef: ElementRef,
    private _state: GlobalState,
    private router: Router, // public themes: ThemesService
    public dbUserLocalstorage: DbUserLocalstorage
  ) {
    this._state.subscribe("app.isCollapsed", (isCollapsed) => {
      this.config.appLayout.isApp_SidebarLeftCollapsed = isCollapsed;
    });
    this._state.subscribe(
      "app.isApp_MobileSidebarLeftOpen",
      (isApp_MobileSidebarLeftOpen) => {
        this.config.appLayout.isApp_MobileSidebarLeftOpen = isApp_MobileSidebarLeftOpen;
      }
    );
    this._state.subscribe(
      "app.isApp_BackdropVisible",
      (isApp_BackdropVisible) => {
        this.config.appLayout.isApp_BackdropVisible = isApp_BackdropVisible;
      }
    );
    this._state.subscribe(
      "app.isApp_SidebarRightOpen",
      (isApp_SidebarRightOpen) => {
        this.config.appLayout.isApp_SidebarRightOpen = isApp_SidebarRightOpen;
      }
    );
  }

  icon: string;
  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem("ba_currentUser"));
    // let d = {
    //   fullName: "Soratin Charaluck",
    // };
    // this.currentUser = d;
    // this.currentUser.fullname = this.currentUser.fullName + "  " + this.currentUser.lastname;
    // console.log("this.currentUser");
    // console.log(this.currentUser);

    // console.log("this.router.url");
    // console.log(this.router.url);

    // console.log("this.config.IconName");
    // console.log(this.config.IconName);

    if (this.router.url == "/fontOffice/setting-system/profile") {
      this.config.IconName = "mdi mdi-home md-icon-36";
    } else {
      this.config.IconName = "mdi mdi-backburger md-icon-36";
    }
  }

  // setTheme() {
  //   this.themes.setTheme(this.currentTheme);
  // }
  toggleProfile() {
    this.config.profile.isProfileVisible = !this.config.profile
      .isProfileVisible;
    this._state.notifyDataChanged(
      "profile.isProfileVisible",
      this.config.profile.isProfileVisible
    );
    return false;
  }

  public toggleMenuSideabar() {
    //console.log("this.router");
    //console.log(this.router.url);

    if (
      this.router.url === "/fontOffice/setting-system/profile" ||
      this.router.url === "/main-menu" ||
      this.router.url === "/fontOffice/setting-system/change-password" ||
      this.router.url === "/fontOffice/setting-system/member-retail"
    ) {
      //this.router.navigate(["/main-menu"]);
      this.config.IconName = "mdi mdi-backburger md-icon-36";
      this.router.navigate([
        "/fontOffice/approval-setting/approval-tax-form-01",
      ]);
    } else {
      this.config.appLayout.isApp_SidebarLeftCollapsed = !this.config.appLayout
        .isApp_SidebarLeftCollapsed;
      this._state.notifyDataChanged(
        "app.isCollapsed",
        this.config.appLayout.isApp_SidebarLeftCollapsed
      );
      return false;
    }
  }
  toggleAppMobileLeftMenuSidebar() {
    this.config.appLayout.isApp_MobileSidebarLeftOpen = !this.config.appLayout
      .isApp_MobileSidebarLeftOpen;
    this.config.appLayout.isApp_BackdropVisible = !this.config.appLayout
      .isApp_BackdropVisible;
    this._state.notifyDataChanged(
      "app.isApp_MobileSidebarLeftOpen",
      this.config.appLayout.isApp_MobileSidebarLeftOpen
    );
    this._state.notifyDataChanged(
      "app.isApp_BackdropVisible",
      this.config.appLayout.isApp_BackdropVisible
    );
    return false;
  }

  toggleAppRightSidebar() {
    this.config.appLayout.isApp_SidebarRightOpen = !this.config.appLayout
      .isApp_SidebarRightOpen;
    this.config.appLayout.isApp_BackdropVisible = !this.config.appLayout
      .isApp_BackdropVisible;
    this._state.notifyDataChanged(
      "app.isApp_SidebarRightOpen",
      this.config.appLayout.isApp_SidebarRightOpen
    );
    this._state.notifyDataChanged(
      "app.isApp_BackdropVisible",
      this.config.appLayout.isApp_BackdropVisible
    );
    return false;
  }

  logout() {
    Swal.fire({
      // title: "คุณต้องการออกจากระบบใช่หรือไม่",
      text: "คุณต้องการออกจากระบบใช่หรือไม่",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "ตกลง",
      cancelButtonText: "ยกเลิก",
    }).then((result) => {
      console.log("result");
      console.log(result);
      if (result.value) {
        this.loading = true;

        setTimeout(() => {
          this.router.navigate([""]);
          this.loading = false;
          this.dbUserLocalstorage.add_current_auth_success(false);
        }, 1000);
        // https://sweetalert2.github.io/#handling-dismissals
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        // Swal.fire("Cancelled", "Your imaginary file is safe :)", "error");
      }
    });
  }

  profile() {
    this.router.navigate(["/fontOffice/setting-system/profile"]);
  }

  changePassword() {
    this.router.navigate(["fontOffice/setting-system/change-password"]);
  }

  member_retail() {
    this.router.navigate(["fontOffice/setting-system/member-retail"]);
  }
}
