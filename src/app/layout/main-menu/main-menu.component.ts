import { Component, ViewEncapsulation, OnInit, Inject } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Subject } from "rxjs";
import { finalize, takeUntil } from "rxjs/operators";
import { AuthService } from "../../auth.service";
import { DbMenuService } from "../../shared/localstorage/db-menu-service";
import { ConfigService } from "../../shared/services/config/config.service";

@Component({
  selector: ".main-menu-inner-wrapper",
  templateUrl: "./main-menu.component.html",
  styleUrls: ["./main-menu.component.scss"],
  encapsulation: ViewEncapsulation.Emulated,
})
export class MainMenuComponent implements OnInit {
  ///////////////// Variable /////////////////
  public model: any = {};
  public loading = false;
  private unsubscribeAll = new Subject();
  /////////////// End Variable ////////////////

  public currentUser: any;

  public Linkpath: any;
  public dataList: any[] = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private dbMenuService: DbMenuService,
    public config: ConfigService,
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem("ba_currentUser"));

    // console.log("this.currentUser");
    // console.log(this.currentUser);

    // this.config.IconName = "mdi mdi-home";
    this.config.IconName = "";
    this.config.isSearchActive = true;
    this.config.IsShowIcon = false;

    this.config.searchValue = {};

    // this.GetMenu(this.currentUser.id);

    // this.getContent();
    // this.onSubmit();

    this.dataList = [
      {
        id: "1",
        icon: "settings",
        name: "จัดการข้อมูลหลัก",
        path: "/master-setting",
        class: "material-icons icons-menu",
      },

      {
        id: "2",
        icon: "supervisor_account",
        name: "จัดการผู้ใช้งานและสิทธิ์",
        path: "/permission-setting",
        class: "fa fa-users fa-5x icons-menu-fa",
      },
      {
        id: "3",
        icon: "assignment",
        name: "จัดการเว็บไซต์",
        path: "/content-setting",
        class: "material-icons icons-menu",
      },
      {
        id: "4",
        icon: "assignment",
        name: "จัดการยื่นแบบ",
        path: "/approval-setting",
        class: "material-icons icons-menu",
      },
    ];

    //this.dataList = this.dbMenuService.getmainmenu();

    // console.log("---  this.dbMenuService.getmainmenu() ----");
    // console.log(this.dbMenuService.getmainmenu());

    console.log("this.authService.isLoggedIn");
    console.log(this.authService.isLoggedIn);
  }

  ngOnDestroy() {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }

  /////////////// End Variable ////////////////

  btnMenuOnClick(item) {
    //this.Linkpath = this.dataList.filter((res) => res.id === id);
    //this.config.IconName = "mdi mdi-backburger";
    //this.router.navigate([this.Linkpath[0].path]);
    //this.dbMenuService.setmenuId(id);
    this.config.IsShowIcon = false;
    this.config.IconName = "mdi mdi-backburger";
    this.router.navigate([item.path]);
    this.dbMenuService.setmenuId(item.id);
  }

  /* select user by id (update) */
  // public GetMenu(id) {
  //   const data = {
  //     userId: id,
  //     isActive: true,
  //     isView: true,
  //   };

  //   this.loading = true;

  //   this.store
  //     .dispatch(new GetMenu(data))
  //     .pipe(
  //       finalize(() => {}),
  //       takeUntil(this.unsubscribeAll)
  //     )
  //     .subscribe(
  //       (value) => {
  //         if (value.menuState.ResultData.resultList) {
  //           this.setForm(value.menuState.ResultData.resultList);
  //         }
  //       },
  //       (error) => {
  //         this.loading = false;
  //       }
  //     );
  // }

  // public setForm(menu: any) {
  //   let dataList = [];

  //   menu.forEach((element) => {
  //     let pathFile =
  //       element.subMenu[0] == undefined
  //         ? []
  //         : element.subMenu[0].subMenu[0].path;

  //     if (pathFile == undefined) {
  //       for (var i = 0; i < element.subMenu.length - 1; i++) {
  //         pathFile = element.subMenu[i].subMenu[0].path;
  //       }
  //     }

  //     const data = {
  //       id: element.id,
  //       icon: element.icon,
  //       name: element.name,
  //       path: pathFile,
  //       class: "material-icons icons-menu",
  //     };

  //     dataList.push(data);
  //   });

  //   this.dataList = dataList;
  //   let datamenu = [];

  //   menu.forEach((element) => {
  //     // let getMainpath = "";
  //     // if (element.name == "ตั้งค่าทั่วไป") {
  //     //   getMainpath = "master-setting";
  //     // } else if (element.name == "โครงการ") {
  //     //   getMainpath = "project-setting";
  //     // } else if (element.name == "โรงงาน") {
  //     //   getMainpath = "production-boq-setting";
  //     // } else if (element.name == "จัดการแผนงาน") {
  //     //   getMainpath = "planning";
  //     // } else if (element.name == "คลังสินค้า") {
  //     //   getMainpath = "planning";
  //     // } else if (element.name == "ส่งออกข้อมูล") {
  //     //   getMainpath = "export";
  //     // } else if (element.name == "จัดการผู้ใช้งานและสิทธิ์") {
  //     //   getMainpath = "permission-setting";
  //     // } else if (element.name == "ระบบรายงาน") {
  //     //   getMainpath = "planning";
  //     // }

  //     const data = {
  //       ...element,
  //       path: element.path,
  //     };

  //     datamenu.push(data);
  //   });

  //   menu = datamenu;

  //   this.dbMenuService.setmenulist(menu);
  //   this.loading = false;
  // }
}
