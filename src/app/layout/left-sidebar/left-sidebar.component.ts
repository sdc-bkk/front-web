import {
  Component,
  OnInit,
  HostListener,
  ViewEncapsulation,
  Input,
} from "@angular/core";
import { PerfectScrollbarConfigInterface } from "ngx-perfect-scrollbar";
import { DbMenuService } from "../../shared/localstorage/db-menu-service";
import { LayoutService } from "../layout.service";
import { ConfigService } from "../../shared/services/config/config.service";
import { Router, ActivatedRoute } from "@angular/router";
import { DbUserLocalstorage } from "../../shared/services/localstorage/db-user-localstorage";

@Component({
  selector: "app-sidebar",
  templateUrl: "./left-sidebar.component.html",
  styleUrls: ["./left-sidebar.component.scss"],
  encapsulation: ViewEncapsulation.Emulated,
})
export class LeftSidebarComponent implements OnInit {
  //dataList: any[] = [];

  id: string;

  dataList: any[] = [
    {
      id: "1",
      dataList: [
        {
          id: "1",
          name: "อนุมัติการยื่นแบบ",
          IsSelect: false,
          subMenu: [
            {
              id: "1-1",
              name: "แบบแจ้งข้อมูลจัดทำทะเบียนสถานการค้าปลีก (ภน.01)",
              path: "approval-setting/approval-tax-form-01",
            },
            {
              id: "1-2",
              name:
                "แบบแจ้งการเลิกกิจการหรือเปลี่ยนแปลงข้อมูลสถานการค้าปลีก (ภน.02)",
              path: "approval-setting/approval-tax-form-02",
            },
            {
              id: "1-3",
              name:
                "แบบยื่นแสดงรายการภาษีบำรุงกรุงเทพมหานครสำหรับน้ำมันฯ (ภน.03)",
              path: "approval-setting/approval-tax-form-03",
            },
            {
              id: "1-4",
              name: "ชำระภาษีบำรุงกรุงเทพมหานครสำหรับน้ำมันฯ",
              path: "approval-setting/payment-tax-oil",
            },
            {
              id: "1-5",
              name: "แบบฟอร์มคำร้องขอรับเงินคืน",
              path: "approval-setting/request-refund",
            },
            {
              id: "1-6",
              name: "ประวัติการชำระภาษีบำรุงกรุงเทพมหานครสำหรับน้ำมันฯ",
              path: "approval-setting/history-payment-tax-oil",
            },
            {
              id: "1-7",
              name: "สถานการค้าปลีกทั้งหมด",
              path: "approval-setting/company",
            },
          ],
        },
      ],
    },
  ];

  public scrollbarOptions = {
    axis: "y",
    theme: "minimal",
    scrollInertia: 0,
    mouseWheel: { preventDefault: true },
  };

  constructor(
    private dbMenuService: DbMenuService,
    private dbUserLocalstorage: DbUserLocalstorage,
    private layoutservice: LayoutService,
    private configService: ConfigService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    let d = this.dataList.find((val) => {
      return val.id == "1";
    });

    console.log("d");
    console.log(d);

    this.dataList = d.dataList;

    // this.router.navigate(["/fontOffice/approval-setting/approval-tax-form-01"]);
  }

  public config: PerfectScrollbarConfigInterface = {};

  getMenudatalist() {
    // this.layoutservice.getMenudatalist().subscribe(res => {
    //   if (res) {
    // 	// console.log("---  res ----");
    // 	// console.log(res);
    // 	this.dbMenuService.setleftmenulist(res);
    // 	if (res.length > 0) {
    // 	// Check path undefined
    // 	//   this.dbMenuService.setleftmenuId(res[0].subMenu[0].id);
    // 	//for (var i = 0; i < res.length - 1; i++) {
    // 	for (var i = 1; i <= res.length; i++) {
    // 		//for (var j = 0; j < res[i].subMenu.length - 1; j++) {
    // 		for (var j = 1; j <= res[i-1].subMenu.length; j++) {
    // 			//this.dbMenuService.setleftmenuId(res[i - 1].subMenu[j - 1].id);
    // 			this.dbMenuService.setleftmenuId(res[res.length - res.length].subMenu[res[i-1].subMenu.length - res[i-1].subMenu.length].id);
    // 			break;
    // 		}
    //    }
    // 	// console.log("---  this.dbMenuService.getleftmenuId ----");
    // 	// console.log(this.dbMenuService.getleftmenuId());
    // 	//   let pathmain = this.dbMenuService.getmainmenuPath();
    // 	  res.map(lv1 => {
    // 		const path = lv1.subMenu[0].path;
    // 		// lv1.path = "/" + pathmain + path;
    // 		lv1.path = path;
    // 		return lv1;
    // 	  });
    // 	//   res.map(lv1 => {
    // 	// 	lv1.subMenu.map(lv2 => {
    // 	// 		lv2.path = "/" + pathmain + lv2.path;
    // 	// 	});
    // 	//   });
    // 	  this.dataList = res;
    // 	//   console.log("---  this.dataList ----");
    // 	//   console.log(this.dataList);
    // 	  this.dbMenuService.getleftmenulist().map(lv1 => {
    // 		lv1.subMenu.map(lv2 => {
    // 		  if (lv2.id == this.dbMenuService.getleftmenuId()) {
    // 			this.dbMenuService.setcurrentmenubypage(lv2);
    // 		  }
    // 		});
    // 	  });
    // 	//   console.log("---  this.dbMenuService.getleftmenulist() ----");
    // 	//   console.log(this.dbMenuService.getleftmenulist());
    // 	}
    //   }
    // });
  }

  btnMenuOnClick(id) {
    this.dbUserLocalstorage.add_currentMounthPN03(null);
    // this.configService.projectId = "default";
    // this.configService.factoryId = "default";
    // this.configService.tabId = "default";
    // this.dbMenuService.setleftmenuId(id);
    // this.dbMenuService.setleftmenuIdnow(id);
    // this.configService.setIsSearchActive(false);
    // this.configService.setPageNo(1);
    // // จัดการผู้ใช้งาน
    // this.configService.searchValue = undefined;
    // this.dbMenuService.getleftmenulist().map((lv1) => {
    //   lv1.subMenu.map((lv2) => {
    //     if (lv2.id == this.dbMenuService.getleftmenuId()) {
    //       this.dbMenuService.setcurrentmenubypage(lv2);
    //       this.dbMenuService.setcurrentmenubypagenow(lv2);
    //     }
    //   });
    // });
  }

  btnMainMenuOnClick() {
    this.configService.projectId = "default";
    this.configService.tabId = "default";
    this.dbMenuService.setcurrentmenubypagenow(null);
    this.dbUserLocalstorage.add_currentMounthPN03(null);
    this.router.navigate(["/main-menu"]);
  }

  scrollbarAutoHide = true;
  scrollbarClass = "mainBar";
  @Input("profile") profile;
  @Input("menus") menuList;

  private getProfile() {
    // const jwt = new JwtHelperService();
    // const token = this.cookieService.get("ba_act");
    // const decodedToken = jwt.decodeToken(token);
  }

  @HostListener("window:resize")
  public onWindowResize(): void {}
}
