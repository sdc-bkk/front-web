import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { LayoutRoutes } from "./layout.routes";
import { FormsModule } from "@angular/forms";

import { LayoutService } from "./layout.service";

import { GlobalState } from "../app.state";
import { LayoutComponent } from "./layout.component";
import { TopNavbarComponent } from "./top-navbar/top-navbar.component";
import { MainMenuComponent } from "./main-menu/main-menu.component";
import { DbMenuService } from "../shared/localstorage/db-menu-service";
import { NavDropDownDirectives } from "../shared/directives/nav-dropdown.directive";
import { LeftSidebarComponent } from "./left-sidebar/left-sidebar.component";
import { SubMenuComponent } from "./sub-menu/sub-menu.component";
import {
  PerfectScrollbarConfigInterface,
  PerfectScrollbarModule,
  PERFECT_SCROLLBAR_CONFIG,
} from "ngx-perfect-scrollbar";
import { SharedModule } from "../shared/shared.module";
import { DbUserLocalstorage } from "../shared/services/localstorage/db-user-localstorage";
import { AuthService } from "../auth.service";
import { AuthGuardService } from "../auth-guard.service";

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true,
};

@NgModule({
  declarations: [
    LayoutComponent,
    LeftSidebarComponent,
    TopNavbarComponent,
    NavDropDownDirectives,
    MainMenuComponent,
    SubMenuComponent,
  ],
  providers: [
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG,
    },
    DbMenuService,
    DbUserLocalstorage,
    GlobalState,
    LayoutService,
    AuthService, //
    AuthGuardService,
  ],
  imports: [
    LayoutRoutes,
    CommonModule,
    SharedModule.forRoot(),
    PerfectScrollbarModule,
    // NgxsModule.forRoot([
    // 	UserState,
    // 	MenuState
    // ]),
  ],
})
export class LayoutModule {}
