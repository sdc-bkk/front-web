import {
  Component,
  ViewEncapsulation,
  ElementRef,
  OnInit,
  HostBinding,
} from "@angular/core";
import { Subject } from "rxjs";
import { finalize, takeUntil } from "rxjs/operators";

@Component({
  selector: "app-layout",
  templateUrl: "./layout.component.html",
  styleUrls: ["./layout.component.scss"],
  encapsulation: ViewEncapsulation.Emulated,
})
export class LayoutComponent implements OnInit {
  constructor() {}

  ngOnInit() {}

  ngOnDestroy() {}
}
