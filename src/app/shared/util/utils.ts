export default class Utils {
  // static stringToDate(dateString: String): Date {
  //   const [date, time] = dateString.split(" ")
  //   const [d, m, y] = date.split("/")
  //   const [hh, mm] = time.split(":");

  //   var dateObject = new Date(
  //      parseInt(y),
  //      parseInt(m),
  //      parseInt(d),
  //      parseInt(hh),
  //      parseInt(mm),
  //      0);

  //      console.log(dateObject);

  //   return dateObject;
  // }

  static stringToDate(dateString: String): any {
    if (dateString != undefined) {
      const [d, m, y] = dateString.split("/");

      let dateObj = {
        date: {
          day: parseInt(d),
          month: parseInt(m),
          year: parseInt(y) - 543,
        },
      };
      return dateObj;
    }
  }

  static dateTostring(dateObj: any): String {
    let d;
    let m;
    if (dateObj.date) {
      if (dateObj.date.day < 10) {
        d = "0" + dateObj.date.day;
      } else {
        d = dateObj.date.day;
      }

      if (dateObj.date.month < 10) {
        m = "0" + dateObj.date.month;
      } else {
        m = dateObj.date.month;
      }

      if (dateObj.date.day < 10) {
        d = "0" + dateObj.date.day;
      } else {
        d = dateObj.date.day;
      }

      const y = dateObj.date.year;
      let dateString = d + "/" + m + "/" + (y + 543);
      return dateString;
    } else {
      return "";
    }
  }

  static checkConditionDate(startDateObj: any, endDateObj: any): boolean {
    const d1 = startDateObj.date.day;
    const m1 = startDateObj.date.month;
    const y1 = startDateObj.date.year;
    let startDate = new Date(y1, m1 - 1, d1).getTime();

    const d2 = endDateObj.date.day;
    const m2 = endDateObj.date.month;
    const y2 = endDateObj.date.year;
    let endDate = new Date(y2, m2 - 1, d2).getTime();

    if (endDate < startDate) {
      return false;
    } else {
      return true;
    }
    //let dateString = d + "/" + m + "/" + (y + 543);
    // return dateString;
  }

  static stringToFloat(item: any): any {
    let a = item.toString().indexOf(",");
    let data;

    if (a != -1) {
      data = parseFloat(item.split(",").join(""));
    } else {
      data = parseFloat(item);
    }

    if (isNaN(data)) {
      data = 0;
    }

    return data;
  }

  static stringToInt(item: any): any {
    let a = item.toString().indexOf(",");
    let data;

    if (a != -1) {
      data = parseInt(item.split(",").join(""));
    } else {
      data = parseInt(item);
    }
    return data;
  }

  static ConvertMonthTH(numberMonth) {
    var month = "";
    if (numberMonth == 1) {
      month = "มกราคม";
    } else if (numberMonth == 2) {
      month = "กุมภาพันธ์";
    } else if (numberMonth == 3) {
      month = "มีนาคม";
    } else if (numberMonth == 4) {
      month = "เมษายน";
    } else if (numberMonth == 5) {
      month = "พฤษภาคม";
    } else if (numberMonth == 6) {
      month = "มิถุนายน";
    } else if (numberMonth == 7) {
      month = "กรกฎาคม";
    } else if (numberMonth == 8) {
      month = "สิงหาคม";
    } else if (numberMonth == 9) {
      month = "กันยายน";
    } else if (numberMonth == 10) {
      month = "ตุลาคม";
    } else if (numberMonth == 11) {
      month = "พฤศจิกายน";
    } else if (numberMonth == 12) {
      month = "ธันวาคม";
    }
    return month;
  }
  static ConvertMonthNumber(numberMonth) {
    var month: any;
    if (numberMonth == "มกราคม") {
      month = 1;
    } else if (numberMonth == "กุมภาพันธ์") {
      month = 2;
    } else if (numberMonth == "มีนาคม") {
      month = 3;
    } else if (numberMonth == "เมษายน") {
      month = 4;
    } else if (numberMonth == "พฤษภาคม") {
      month = 5;
    } else if (numberMonth == "มิถุนายน") {
      month = 6;
    } else if (numberMonth == "กรกฎาคม") {
      month = 7;
    } else if (numberMonth == "สิงหาคม") {
      month = 8;
    } else if (numberMonth == "กันยายน") {
      month = 9;
    } else if (numberMonth == "ตุลาคม") {
      month = 10;
    } else if (numberMonth == "พฤศจิกายน") {
      month = 11;
    } else if (numberMonth == "ธันวาคม") {
      month = 12;
    } else {
      month = "";
    }
    return month;
  }
}
