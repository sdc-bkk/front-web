import { Injectable } from "@angular/core";

@Injectable()
export class DbMenuService {
  currentMenuDataBase = "cur_menuId";
  currentMenuListDataBase = "cur_menulist";

  currentLeftMenuDataBase = "cur_leftmenuId";
  currentLeftMenuListDataBase = "cur_leftmenulist";

  currentLeftMenuNowDataBase = "cur_leftmenunowId";
  currentLeftMenuNowListDataBase = "cur_leftmenunowlist";

  currentMenuByPageDataBase = "cur_menubypage";

  currentPathMainMenuDataBase = "cur_menuPathMain";

  currentMainMenuDataBase = "cur_menumain";

  current_backTaxForm = "cur_backtaxform";

  constructor() {}

  getmainmenu(): any[] {
    return JSON.parse(localStorage.getItem(this.currentMainMenuDataBase));
  }

  setmainmenu(datalist: any[]) {
    // return localStorage.setItem(this.currentMainMenuDataBase, datalist);
    return localStorage.setItem(
      this.currentMainMenuDataBase,
      JSON.stringify(datalist)
    );
  }

  get_backTaxForm() {
    return localStorage.getItem(this.current_backTaxForm);
  }

  set_backTaxForm(path) {
    return localStorage.setItem(this.current_backTaxForm, path);
  }

  getmainmenuPath() {
    return localStorage.getItem(this.currentPathMainMenuDataBase);
  }

  setmainmenuPath(path) {
    return localStorage.setItem(this.currentPathMainMenuDataBase, path);
  }

  // ID

  getmenuId() {
    return localStorage.getItem(this.currentMenuDataBase);
  }

  setmenuId(id) {
    return localStorage.setItem(this.currentMenuDataBase, id);
  }

  getleftmenuId() {
    return localStorage.getItem(this.currentLeftMenuDataBase);
  }

  setleftmenuId(id) {
    return localStorage.setItem(this.currentLeftMenuDataBase, id);
  }

  getleftmenuIdnow() {
    return localStorage.getItem(this.currentLeftMenuNowDataBase);
  }

  setleftmenuIdnow(id) {
    return localStorage.setItem(this.currentLeftMenuNowDataBase, id);
  }

  // List

  getmenulist(): any[] {
    return JSON.parse(localStorage.getItem(this.currentMenuListDataBase));
  }

  setmenulist(datalist: any[]) {
    return localStorage.setItem(
      this.currentMenuListDataBase,
      JSON.stringify(datalist)
    );
  }

  setleftmenulist(datalist: any[]) {
    return localStorage.setItem(
      this.currentLeftMenuListDataBase,
      JSON.stringify(datalist)
    );
  }

  getleftmenulist(): any[] {
    return JSON.parse(localStorage.getItem(this.currentLeftMenuListDataBase));
  }

  // GET SET MENUBYPAGE
  setcurrentmenubypage(datalist) {
    return localStorage.setItem(
      this.currentMenuByPageDataBase,
      JSON.stringify(datalist)
    );
  }

  getcurrentmenubypage() {
    return JSON.parse(localStorage.getItem(this.currentMenuByPageDataBase));
  }

  // GET SET MENUBYPAGENOW
  setcurrentmenubypagenow(datalist) {
    return localStorage.setItem(
      this.currentLeftMenuNowListDataBase,
      JSON.stringify(datalist)
    );
  }

  getcurrentmenubypagenow() {
    return JSON.parse(
      localStorage.getItem(this.currentLeftMenuNowListDataBase)
    );
  }
}
