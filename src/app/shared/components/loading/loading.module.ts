import { LoadingComponent } from "./loading.component";
import { CommonModule } from "@angular/common";
import { NgModule } from '@angular/core';

@NgModule({
	declarations: [LoadingComponent],
	providers: [],
	imports: [
		CommonModule,
	],
	exports: [
		LoadingComponent
	]
})
export class LoadingModule {}