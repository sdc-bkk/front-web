import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { NgModule } from "@angular/core";
import { MatDatePickerTH } from "./mat-date-picker.component";
import { FocusDirective } from "./directives/my-date-picker.focus.directive";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";

@NgModule({
  imports: [CommonModule, FormsModule, MatInputModule, MatIconModule],
  declarations: [MatDatePickerTH, FocusDirective],
  exports: [MatDatePickerTH, FocusDirective],
})
export class MatDatePickerTHModule {}
