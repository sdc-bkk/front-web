import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";

@Injectable()
export class DbUserLocalstorage {
  currentUserDataBase = "ba_currentUser";
  currentMounthPN03 = "ba_currentmounthPn03";

  current_auth_success = "current_auth_success";

  constructor() {}

  add_currentUserDataBase(data: any) {
    localStorage.setItem(this.currentUserDataBase, JSON.stringify(data));
  }

  get_currentUserDataBase(): any {
    return JSON.parse(localStorage.getItem(this.currentUserDataBase));
  }

  add_currentMounthPN03(data: any) {
    localStorage.setItem(this.currentMounthPN03, JSON.stringify(data));
  }

  get_currentMounthPN03(): any {
    return JSON.parse(localStorage.getItem(this.currentMounthPN03));
  }

  // ---- //
  add_current_auth_success(data: any) {
    localStorage.setItem(this.current_auth_success, JSON.stringify(data));
  }

  get_current_auth_success(): any {
    return JSON.parse(localStorage.getItem(this.current_auth_success));
  }
}
