import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root",
})
export class ConfigService {
  public app: any;
  public profile: any;
  public appLayout: any;
  public breakpoint: any;
  public tooltripMsg: any;
  public paging: any;
  public IconName: string;

  //public IsShowIcon: any;

  public projectId: string = "default";
  public factoryId: string = "default";
  public tabId: any = "default";

  public searchValue: any;

  isSearchActive: boolean = false;

  public IsShowIcon: boolean = false;

  public loading: boolean = false;

  public resultPage: any = {
    pageNo: 1,
    totalItem: 0,
    itemPerPage: 0,
    collectionSize: 0,
  };

  public urllinkReport: any;

  constructor() {
    //this.urllinkReport = "http://tauat.excise.go.th/";
    this.urllinkReport = "http://183.89.221.119:8084/";

    this.tooltripMsg = {
      add: "เพิ่ม",
      select: "เลือก",
      edit: "แก้ไข",
      delete: "ลบ",
      deleteAll: "ลบทั้งหมด",
      search: "ค้นหา",
      close: "ปิด",
      info: "รายละเอียด",
      approveData: "ข้อมูลอนุมัติ",
      versionData: "ข้อมูล version",
      detail: "รายละเอียด",
      send: "ส่งรายการ",
      check: "ผ่านการตรวจสอบ",
      checkapproval: "ตรวจสอบข้อมูล",
      reply: "ไม่ผ่านการตรวจสอบ",
      checkAll: "ผ่านการอนุมัติ",
      replyAll: "ไม่ผ่านการอนุมัติ",
      download: "Import",
      upload: "Export",
      copy: "คัดลอก",
      uploadfile: "อัพโหลดไฟล์",
      uploadfileimage: "อัพโหลดไฟล์รูปภาพ",
      syncData: "ซิงค์ข้อมูล ",
      formExp: "แบบฟอร์มตัวอย่าง",
      readmore: "รายละเอียดเพิ่มเติม",
      addPriceSale: "สร้างชุดราคาขาย",
      export: "ส่งออกข้อมูล",
      userEngineer: "ตรวจสอบข้อมูลช่าง",
      refresh: "ล้างข้อมูล",
      preview: "ใบสั่งทำงาน",
      import_address: "นำเข้าระบบ",
      export_address: "ส่งออกระบบ",
      pdf_01: "ภน.01",
      pdf_02: "ภน.02",
      pdf_03: "ภน.03",
      invoice: "ใบแจ้งชำระ",
      or_01: "ใบรับแจ้ง",
      add_pn01: "ยื่นภน.01",
      add_pn02: "ยื่นภน.02",
      add_pn03: "ยื่นภน.03",
      send_plan: "ส่งแบบ",
      add_item: "เพิ่มรายการ",
      send_plants: "ส่งแบบ",
      change: "เปลี่ยน",
    };

    this.IconName = "mdi mdi-backburger";

    this.paging = {
      ITEMPERPAGE: 10,
      MAXSIZE: 10,
    };
    // this.app = {
    // 	name: "MaterialLab"
    // };
    // this.profile = {
    // 	user: "Fredrick Palmer",
    // 	userId:"160d0948-62a7-4439-98aa-0b95f1143b0d",
    // 	userEmail:"fredrick@materiallab.pro",
    // 	userImg:"/assets/img/profiles/18.jpg",
    // 	userTitle:"Frontend Developer",
    // 	isProfileVisible: true,
    // };
    this.appLayout = {
      isApp_Boxed: false,
      isApp_SidebarLeftCollapsed: false,
      isApp_MobileSidebarLeftOpen: false,
      isApp_SidebarRightOpen: false,
      isApp_BackdropVisible: false,
    };
    this.breakpoint = {
      desktopLG: 1280,
      desktop: 992,
      tablet: 768,
      mobile: 576,
    };
  }

  getPageNo() {
    return this.resultPage.pageNo;
  }

  setPageNo(pageNo) {
    this.resultPage.pageNo = pageNo;
  }

  getIsSearchActive() {
    return this.isSearchActive;
  }

  setIsSearchActive(isSearchActive) {
    this.isSearchActive = isSearchActive;
  }
}
