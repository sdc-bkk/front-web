import { Injectable } from "@angular/core";
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest
} from "@angular/common/http";

import { Observable } from "rxjs/Observable";

/** Pass untouched request through to the next request handler. */
@Injectable()
export class HttpClientInterceptor implements HttpInterceptor {
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const token = "gm1GZBMWLlrV1Rn8RnkKq7bQvXz9EfZM";
    request = request.clone({
      setHeaders: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json;charset=utf-8"
      }
    });
    return next.handle(request);
  }
}
