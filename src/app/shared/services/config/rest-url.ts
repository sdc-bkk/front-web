import { environment } from "../../../../environments/environment";

export const UPLOADFILEWEB_URL = "/taxuploadfilewebservice/api";

export const TAXMASTERWEBSERVICE_URL = "/taxmasterwebservice/api";

export const REST_URL = {
  ITEMPERPAGE: 1,
  MAXSIZE: 10,

  //UPLOADFILEWEB_URL: "/taxuploadfilewebservice/api",

  MAIN_PERMISSION: {
    master: "/taxmasterwebservice/api",
    content: "/taxcontentwebservice/api",
    upload: "/taxuploadfilewebservice/api",
    frontwab: "/taxformwebservice/api",
    member: "/taxmemberwebservice/api",
  },

  /* UPLOADFILE UPLOADFILE UPLOADFILE */

  MAIN_UPLOADFILE: {
    uploadFile:
      environment.url +
      // "/uploadfilewebservice/services/uploadfilewebservice/uploadFile",
      UPLOADFILEWEB_URL +
      "/uploadfilewebservice/uploadFile",
    getFileTemp:
      environment.url +
       UPLOADFILEWEB_URL + "/uploadfilewebservice/getFileTemp",
    deleteFileTemp:
      environment.url +
      "/uploadfilewebservice/deleteFileTemp",

    moveFile: 
    environment.url +
     "/uploadfilewebservice/moveFile",
    getFile:
      environment.url + 
      UPLOADFILEWEB_URL + "/uploadfilewebservice/getFile",
    deleteFile: 
    environment.url + 
    "/uploadfilewebservice/deleteFile",

    // uploadFileAndConvertPDF:
    //   environment.url +
    //   UPLOADFILEWEB_URL +
    //   "/uploadfilewebservice/uploadFileAndConvertPDF",
    // deleteFile: "/uploadfilewebservice/deleteFile",
    // uploadFileHandMade: "/uploadfilewebservice/uploadFile",
  },

  // Start Master Data
  /* station */
  STATION_PERMISSION: {
    getList: "/station/getList",
    getData: "/station/getData",
    saveData: "/station/saveData",
    deleteData: "/station/deleteData",
    getDrpList: "/station/getDrpList",
  },
  /* station */

  /* businessType */
  BUSINESSTYPE_PERMISSION: {
    getList: "/businessType/getList",
    getData: "/businessType/getData",
    saveData: "/businessType/saveData",
    deleteData: "/businessType/deleteData",
    getDrpList: "/businessType/getDrpList",
  },
  /* businessType */

  /* businessType */
  BUSINESSSTATUS_PERMISSION: {
    getList: "/businessStatus/getList",
    getData: "/businessStatus/getData",
    saveData: "/businessStatus/saveData",
    deleteData: "/businessStatus/deleteData",
    getDrpList: "/businessStatus/getDrpList",
  },
  /* businessType */

  /* oilType */
  OILTYPE_PERMISSION: {
    getList: "/oilType/getList",
    getData: "/oilType/getData",
    saveData: "/oilType/saveData",
    deleteData: "/oilType/deleteData",
    getDrpList: "/oilType/getDrpList",
  },
  /* oilType */

  /* title */
  TITLE_PERMISSION: {
    getList: "/title/getList",
    getData: "/title/getData",
    saveData: "/title/saveData",
    deleteData: "/title/deleteData",
    getDrpList: "/title/getDrpList",
  },
  /* title */

  /* nationality */
  NATIONALYTY_PERMISSION: {
    getList: "/nationality/getList",
    getData: "/nationality/getData",
    saveData: "/nationality/saveData",
    deleteData: "/nationality/deleteData",
    getDrpList: "/nationality/getDrpList",
  },
  /* nationality */

  /* address */
  ADDRESS_PERMISSION: {
    getList: "/address/getList",
    getDrpListByPostCodeMain: "/address/getDrpListByPostCodeMain",
    getDrpListProvince: "/address/getDrpListProvince",
    getDrpListDistrict: "/address/getDrpListDistrict",
    getDrpListTambon: "/address/getDrpListTambon",
    importData:
      environment.url + TAXMASTERWEBSERVICE_URL + "/address/importData",
  },
  /* address */

  /* road */
  ROAD_PERMISSION: {
    getList: "/road/getList",
    getData: "/road/getData",
    saveData: "/road/saveData",
    deleteData: "/road/deleteData",
    getDrpList: "/road/getDrpList",
    getDrpListByAddress: "/road/getDrpListByAddress",
  },
  /* road */

  /* soi */
  SOI_PERMISSION: {
    getList: "/soi/getList",
    getData: "/soi/getData",
    saveData: "/soi/saveData",
    deleteData: "/soi/deleteData",
    getDrpList: "/soi/getDrpList",
    getDrpListByRoad: "/soi/getDrpListByRoad",
  },
  /* soi */

  /* customerType */
  CUSTOMERTYPE_PERMISSION: {
    getList: "/customerType/getList",
    getData: "/customerType/getData",
    saveData: "/customerType/saveData",
    deleteData: "/customerType/deleteData",
    getDrpList: "/customerType/getDrpList",
  },
  /* customerType */

  /* taxRate */
  TAXRATE_PERMISSION: {
    getList: "/taxRate/getList",
    getData: "/taxRate/getData",
    saveData: "/taxRate/saveData",
    deleteData: "/taxRate/deleteData",
    getDrpList: "/taxRate/getDrpList",
  },
  /* taxRate */

  /* fee */
  FEE_PERMISSION: {
    getList: "/fee/getList",
    getData: "/fee/getData",
    saveData: "/fee/saveData",
    deleteData: "/fee/deleteData",
    getDrpList: "/fee/getDrpList",
  },
  /* fee */

  /* settingGeneral */
  SETTINGGENERAL_PERMISSION: {
    getData: "/settingGeneral/getData",
    saveData: "/settingGeneral/saveData",
    getDataTelephone: "/settingGeneral/getDataTelephone",
    saveDataTelephone: "/settingGeneral/saveDataTelephone",
    getDataEmail: "/settingGeneral/getDataEmail",
    saveDataEmail: "/settingGeneral/saveDataEmail",
  },
  /* settingGeneral */
  // End Master Data

  // Start Content Data
  /* contentType */
  CONTENTTYPE_PERMISSION: {
    getList: "/contentType/getList",
    getData: "/contentType/getData",
    saveData: "/contentType/saveData",
    deleteData: "/contentType/deleteData",
    getDrpList: "/contentType/getDrpList",
  },
  /* contentType */

  /* contentFAQ */
  CONTENTFAQ_PERMISSION: {
    getList: "/contentFAQ/getList",
    getData: "/contentFAQ/getData",
    saveData: "/contentFAQ/saveData",
    deleteData: "/contentFAQ/deleteData",
  },
  /* contentFAQ */

  // End Content Data

  // Start Approval //

  FRONTRETAIL_PERMISSION: {
    getDataByMemberName: "/frontretail/getDataByMemberName",
    getDataByTaxNo: "/frontretail/getDataByTaxNo",
    getDrpListStation: "/frontretail/getDrpListStation",
    getListStationByTaxNo: "/frontretail/getListAllStationByMemberName",
    getListStationByTaxNo2:"/frontretail/getListStationByTaxNo",
    getDataStation: "/frontretail/getDataStation",
  },

  FRONTTAXFORM01_PERMISSION: {
    getList: "/fronttaxform01/getList",
    getData: "/fronttaxform01/getData",
    saveData: "/fronttaxform01/saveData",
    getDataStation: "/fronttaxform01/getDataStation",
    deleteData: "/fronttaxform01/deleteData",
    sendApprove: "/fronttaxform01/sendApprove",
    getDrpListRetail: "/fronttaxform01/getDrpListRetail",
    getDrpListStation: "/fronttaxform01/getDrpListStation",
    getListStatusLog: "/fronttaxform01/getListStatusLog",
    getDrpListStatus: "/taxform01/getDrpListStatus",
  },

  FRONTTAXFORM02_PERMISSION: {
    getList: "/fronttaxform02/getList",
    getData: "/fronttaxform02/getData",
    saveData: "/fronttaxform02/saveData",
    deleteData: "/fronttaxform02/deleteData",
    sendApprove: "/fronttaxform02/sendApprove",
    getListStatusLog: "/fronttaxform02/getListStatusLog",
  },

  FRONTTAXFORM03_PERMISSION: {
    getList: "/fronttaxform03/getList",
    getListByMonthly: "/fronttaxform03/getListByMonthly",
    getData: "/fronttaxform03/getData",
    saveData: "/fronttaxform03/saveData",
    deleteData: "/fronttaxform03/deleteData",
    sendApprove: "/fronttaxform03/sendApprove",
    getStatusAndFinesByMonth: "/fronttaxform03/getStatusAndFinesByMonth",
    getListStatusLog: "/fronttaxform03/getListStatusLog",
  },

  FRONTPAYMENT_PERMISSION: {
    getList: "/frontpayment/getList",
    getListHistory: "/frontpayment/getListHistory",
  },

  FRONTREFUND_PERMISSION: {
    getList: "/fronttaxformrefund/getList",
    getData: "/fronttaxformrefund/getData",
    getDataStation: "/fronttaxformrefund/getDataStation",
    getDrpListStatus: "/fronttaxformrefund/getDrpListStatus",
    saveData: "/fronttaxformrefund/saveData",
    deleteData: "/fronttaxformrefund/deleteData",
   
  },

  // End Approval //
  //start Member//
  MEMBER_PERMISSION: {
    register: "/member/register",
    getList: "/member/getList",
    getData: "/member/getData",
    saveData: "/member/saveData",
    setPassword: "/member/setPassword",
    forgetPassword: "/member/forgetPassword",
    changePassword: "/member/changePassword",
    authenMember: "/member/authenMember",
    getDataByToken: "/member/getDataByToken",
  },

  MEMBER_RETAIL_PERMISSION: {
    checkEmail: "/memberRetail/checkEmail",
    getData: "/memberRetail/getData",
    saveData: "/memberRetail/saveData",
  },
  // End Member //
};
