import {
  Component,
  HostBinding,
  OnInit,
  HostListener,
  ViewContainerRef,
  Inject,
} from "@angular/core";
import { LOCAL_STORAGE, StorageService } from "ngx-webstorage-service";
import { TranslateService } from "@ngx-translate/core";
import { ConfigService } from "./shared/services/config/config.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent {
  constructor(
    @Inject(LOCAL_STORAGE) private localstorage: StorageService,
    public config: ConfigService,
    private translate: TranslateService
  ) {
    this.getTranslate();
  }

  ngOnInit() {
    // this.getTranslate();
    this.config.loading = true;
  }

  private getTranslate() {
    // let lang = this.localstorage.get("lang");
    // if(lang == null || !lang || lang === ""){
    //   this.localstorage.set("lang", "th")
    //     lang = "th";
    // }

    this.translate.addLangs(["th", "en"]);
    this.translate.setDefaultLang("th");
    this.translate.use("th");

    // this.translate.use(lang.match(/en|th|ch/) ? lang : 'en');
  }

  //App Left Sidebar Menu Open/Close Desktop
  @HostBinding("class.app_sidebar-menu-collapsed")
  get isApp_SidebarLeftCollapsed() {
    return this.config.appLayout.isApp_SidebarLeftCollapsed;
  }
  //Left Menu Sidebar Open/Close Tablet & Mobile
  @HostBinding("class.app_sidebar-left-open")
  get isApp_MobileSidebarLeftOpen() {
    return this.config.appLayout.isApp_MobileSidebarLeftOpen;
  }
  //App Right Sidebar Open/Close
  @HostBinding("class.sidebar-overlay-open")
  get isApp_SidebarRightOpen() {
    return this.config.appLayout.isApp_SidebarRightOpen;
  }

  @HostListener("window:resize")
  public onWindowResize(): void {
    if (this._shouldMenuReset()) {
      this.config.appLayout.isApp_SidebarLeftCollapsed = false;
    }
  }

  private _shouldMenuReset(): boolean {
    return window.innerWidth <= this.config.breakpoint.desktopLG;
  }
  @HostListener('document:click', ['$event'])
  clickout(event) {
    console.log(":", event.target.id);
    if(event.target.id=='menu-noi') {
      this.config.appLayout.isApp_MobileSidebarLeftOpen=!this.config.appLayout.isApp_MobileSidebarLeftOpen;
    } else {
      this.config.appLayout.isApp_MobileSidebarLeftOpen=false;
    }
  }

}
