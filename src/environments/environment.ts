// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  //url: "",
  url: "./api" // ไว้อัพขึ้น
  // http://202.28.16.54:7001 proxy.json ตัวจริง
  // http://172.16.1.246:8084 ตัวเทส
  // http://27.254.63.20:7001
  // http://localhost:7001
};
